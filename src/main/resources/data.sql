---------------------------------------
---------------Months---------
---------------------------------------
INSERT INTO public.months (id, fr, gb, it, nl) VALUES (1, 'Janvier', 'January', 'Gennaio', 'Januari');
INSERT INTO public.months (id, fr, gb, it, nl) VALUES (2, 'Février', 'February', 'Febbraio', 'Februari');
INSERT INTO public.months (id, fr, gb, it, nl) VALUES (3, 'Mars', 'March', 'Marzo', 'Maart');
INSERT INTO public.months (id, fr, gb, it, nl) VALUES (4, 'Avril', 'April', 'Aprile', 'April');
INSERT INTO public.months (id, fr, gb, it, nl) VALUES (5, 'Mai', 'May', 'Maggio', 'Mei');
INSERT INTO public.months (id, fr, gb, it, nl) VALUES (6, 'Juin', 'June', 'Giugno', 'Juni');
INSERT INTO public.months (id, fr, gb, it, nl) VALUES (7, 'Juillet', 'July', 'Luglio', 'Juli');
INSERT INTO public.months (id, fr, gb, it, nl) VALUES (8, 'Août', 'August', 'Agosto', 'Augustus');
INSERT INTO public.months (id, fr, gb, it, nl) VALUES (9, 'Septembre', 'September', 'Settembre', 'September');
INSERT INTO public.months (id, fr, gb, it, nl) VALUES (10, 'Octobre', 'October', 'Ottobre', 'Oktober');
INSERT INTO public.months (id, fr, gb, it, nl) VALUES (11, 'Novembre', 'November', 'Novembre', 'November');
INSERT INTO public.months (id, fr, gb, it, nl) VALUES (12, 'Décembre', 'December', 'Dicembre', 'December');



---------------------------------------
---------------Project Page---------
---------------------------------------
INSERT INTO public.project_page_translations (id, fr, gb, it, nl) VALUES ('noProject', 'Vous n''avez pas encore de projets', 'You don''t have any projects yet', 'Non hai ancora nessun progetto', 'U heeft nog geen project');
INSERT INTO public.project_page_translations (id, fr, gb, it, nl) VALUES ('myProjects', 'Mes projets', 'My projects', 'I miei progetti', 'Mijn projecten');
INSERT INTO public.project_page_translations (id, fr, gb, it, nl) VALUES ('subProject', 'Sous-projets', 'Sub-projects', 'Sotto-progetti', 'Deelprojecten');
INSERT INTO public.project_page_translations (id, fr, gb, it, nl) VALUES ('noSubProject', 'Aucun sous-projets', 'No sub-projects', 'Nessun sotto-progetto', 'Geen deelprojecten');
INSERT INTO public.project_page_translations (id, fr, gb, it, nl) VALUES ('inconsistentBudget', 'Budget incohérent', 'Inconsistent budget', 'Budget incoerente', 'Inconsistente budget');
INSERT INTO public.project_page_translations (id, fr, gb, it, nl) VALUES ('name', 'Nom du projet', 'Project name', 'Nome del progetto', 'Naam project');
INSERT INTO public.project_page_translations (id, fr, gb, it, nl) VALUES ('budgetRequired', 'Budget requis', 'Budget required', 'Budget richiesto', 'Budget verijst');
INSERT INTO public.project_page_translations (id, fr, gb, it, nl) VALUES ('deadline', 'Date limite', 'Deadline', 'Scadenza', 'Deadline');
INSERT INTO public.project_page_translations (id, fr, gb, it, nl) VALUES ('chooseImage', 'Choisir une image', 'Choose an image', 'Scegliere un''immagine', 'Kies een afbeelding');
INSERT INTO public.project_page_translations (id, fr, gb, it, nl) VALUES ('budgetObtained', 'Budget épargné', 'Budget saved', 'Budget risparmiato', 'Gespaarde budget');
INSERT INTO public.project_page_translations (id, fr, gb, it, nl) VALUES ('nameError', 'Nom de projet requis', 'Required project name', 'Nome del progetto richiesto', 'Vereiste projectnaam');
INSERT INTO public.project_page_translations (id, fr, gb, it, nl) VALUES ('BudgetRequiredError', 'Budget requis est vide', 'Budget required is empty', 'Il budget richiesto è vuoto', 'Het vereiste budget is leeg');
INSERT INTO public.project_page_translations (id, fr, gb, it, nl) VALUES ('deadlineErrorRequired', 'Date limite requise', 'Deadline required', 'Scadenza richiesta', 'Deadline vereist');
INSERT INTO public.project_page_translations (id, fr, gb, it, nl) VALUES ('budgetObtainedError', 'Budget épargné requis', 'Budget saved required', 'Budget risparmiato richiesto', 'Vereist spaarbudget');
INSERT INTO public.project_page_translations (id, fr, gb, it, nl) VALUES ('imageRequired', 'Image requise', 'Image required', 'Immagine richiesta', 'Beeld vereist');
INSERT INTO public.project_page_translations (id, fr, gb, it, nl) VALUES ('addProject', 'Ajouter un projet', 'Add a project', 'Aggiungere un progetto', 'Een project toevoegen');
INSERT INTO public.project_page_translations (id, fr, gb, it, nl) VALUES ('unexpectedError', 'Une erreur est survenue,veuillez recommencer', 'An error has occurred, please try again', 'Si è verificato un errore, si prega di riprovare', 'Er is een fout opgetreden, probeer het opnieuw');
INSERT INTO public.project_page_translations (id, fr, gb, it, nl) VALUES ('deadlineError', 'La date limite est avant la date de début du projet', 'The deadline is before the start date of the project', 'La scadenza è prima della data di inizio del progetto', 'De termijn valt voor de begindatum van het project');
INSERT INTO public.project_page_translations (id, fr, gb, it, nl) VALUES ('deadlineErrorParent', 'La date limite est après la date limite du projet principal', 'The deadline is after the deadline of the main project', 'La scadenza è successiva a quella del progetto principale', 'De deadline is na de deadline van het hoeft project');
INSERT INTO public.project_page_translations (id, fr, gb, it, nl) VALUES ('delete', 'Supprimer', 'Delete', 'Eliminare', 'Delete');
INSERT INTO public.project_page_translations (id, fr, gb, it, nl) VALUES ('cancel', 'Annuler', 'Cancel', 'Cancellare', 'Annuleren');
INSERT INTO public.project_page_translations (id, fr, gb, it, nl) VALUES ('type', 'Taper', 'Type', 'Scrivere', 'Typen');
INSERT INTO public.project_page_translations (id, fr, gb, it, nl) VALUES ('confirm', 'Confirmer', 'Confirm', 'Confermare', 'Bevestigen');
INSERT INTO public.project_page_translations (id, fr, gb, it, nl) VALUES ('deleteProject', 'Pour supprimer le projet', 'Delete to project', 'Per eliminare il progetto', 'Om het project te verwijderen');
INSERT INTO public.project_page_translations (id, fr, gb, it, nl) VALUES ('add', 'Ajouter', 'Add a project', 'Aggiungere un progetto', 'Toevoegen');
INSERT INTO public.project_page_translations (id, fr, gb, it, nl) VALUES ('modify', 'Modifier', 'Edit', 'Modificare', 'Bewerken');
INSERT INTO public.project_page_translations (id, fr, gb, it, nl) VALUES ('remaining', 'Restant', 'Remaining ', 'Rimanente', 'Overblijvend');
INSERT INTO public.project_page_translations (id, fr, gb, it, nl) VALUES ('errorWord', 'Erreur dans le mot', 'Error in the word', 'Errore nella parola', 'Fout in het woord');
INSERT INTO public.project_page_translations (id, fr, gb, it, nl) VALUES ('success_addProject', 'Le nouveau projet a été créé', 'The new project has been created!', '', '');
INSERT INTO public.project_page_translations (id, fr, gb, it, nl) VALUES ('success_deleteProject', 'Le projet a été supprimé', 'The project has been deleted!', '', '');
INSERT INTO public.project_page_translations (id, fr, gb, it, nl) VALUES ('success_updateProject', 'Le projet a été mis à jour', 'The project has been updated', '', '');



---------------------------------------
--------Recurring Paiement Page--------
---------------------------------------
INSERT INTO public.recurring_paiements_page_translation (id, fr, gb, it, nl) VALUES ('iconFilterPlaceholder', 'Filtrer par nom', 'Filter by name', 'Filtrare per nome', 'Filter op naam');
INSERT INTO public.recurring_paiements_page_translation (id, fr, gb, it, nl) VALUES ('add', 'Ajouter', 'Add', 'Aggiungere', 'Toevoegen');
INSERT INTO public.recurring_paiements_page_translation (id, fr, gb, it, nl) VALUES ('cancel', 'Annuler', 'Cancel', 'Cancellare', 'Annuleren');
INSERT INTO public.recurring_paiements_page_translation (id, fr, gb, it, nl) VALUES ('name', 'Nom ', 'Name', 'Nome', 'Naam');
INSERT INTO public.recurring_paiements_page_translation (id, fr, gb, it, nl) VALUES ('msgAddSuccessful', 'La facture a été ajoutée', 'Invoice   has been successfully added', 'La fattura   è stata aggiunta', 'De factuur   is toegevoegd');
INSERT INTO public.recurring_paiements_page_translation (id, fr, gb, it, nl) VALUES ('title', 'Mes paiements récurrents', 'My recurring paiements', 'I miei pagamenti ricorrenti', 'Mijn terugkerende betalingen');
INSERT INTO public.recurring_paiements_page_translation (id, fr, gb, it, nl) VALUES ('amount', 'Montant', 'Amount', 'Importo', 'Bedrag');
INSERT INTO public.recurring_paiements_page_translation (id, fr, gb, it, nl) VALUES ('startDate', 'Date de commencement', 'Start date', 'Data di inizio', 'Begin datum');
INSERT INTO public.recurring_paiements_page_translation (id, fr, gb, it, nl) VALUES ('endDate', 'Date de fin', 'End date', 'Data di fine', 'Einddatum');
INSERT INTO public.recurring_paiements_page_translation (id, fr, gb, it, nl) VALUES ('icon', 'Icône', 'Icon', 'Icona', 'Icoon');
INSERT INTO public.recurring_paiements_page_translation (id, fr, gb, it, nl) VALUES ('popupTitleAddInvoice', 'Ajouter une facture', 'Add an invoice', 'Aggiungi una fattura', 'Voeg een factuur toe');
INSERT INTO public.recurring_paiements_page_translation (id, fr, gb, it, nl) VALUES ('invoices', 'Factures', 'Invoices', 'Fatture', 'Factuur');
INSERT INTO public.recurring_paiements_page_translation (id, fr, gb, it, nl) VALUES ('transactions', 'Transactions', 'Transactions', 'Transazioni', 'Transacties');
INSERT INTO public.recurring_paiements_page_translation (id, fr, gb, it, nl) VALUES ('until', 'Jusqu''au', 'Until the', 'Fino a', 'Tot');
INSERT INTO public.recurring_paiements_page_translation (id, fr, gb, it, nl) VALUES ('errorMsg1', 'Veuillez remplir tous les champs', 'Please fill in all the fields', 'Si prega di compilare tutti i campi', 'Gelieve alle lege velden in te vullen');
INSERT INTO public.recurring_paiements_page_translation (id, fr, gb, it, nl) VALUES ('errorMsg2', 'Veuillez entrer un montant positif', 'Please enter a positive amount', 'Si prega di inserire un importo positivo', 'Voer een positief bedrag in');
INSERT INTO public.recurring_paiements_page_translation (id, fr, gb, it, nl) VALUES ('popupDeleteTitle', 'Supprimer ?', 'Delete ?', 'Eliminare ?', 'Verwijderen ?');
INSERT INTO public.recurring_paiements_page_translation (id, fr, gb, it, nl) VALUES ('popupDeleteMsg', 'Vous ne serez plus notifier pour cette facture', 'You will no longer be notified for this invoice', 'Non sarai più avvisato per questa fattura', 'U ontvangt geen bericht meer voor deze factuur');
INSERT INTO public.recurring_paiements_page_translation (id, fr, gb, it, nl) VALUES ('delete', 'Supprimer', 'Delete', 'Eliminare', 'Verwijderen');
INSERT INTO public.recurring_paiements_page_translation (id, fr, gb, it, nl) VALUES ('deleteMsgOk', 'Facture supprimée avec succès', 'Invoice successfully deleted', 'Fattura eliminata con successo', 'Factuur succesvol verwijderd');
INSERT INTO public.recurring_paiements_page_translation (id, fr, gb, it, nl) VALUES ('msgUnexpectedError', 'Une erreur est survenue', 'An error has occurred', 'Si è verificato un errore', 'Er is een fout opgetreden');
INSERT INTO public.recurring_paiements_page_translation (id, fr, gb, it, nl) VALUES ('save', 'Enregistrer', 'Save', 'Salvare', 'Opnemen');
INSERT INTO public.recurring_paiements_page_translation (id, fr, gb, it, nl) VALUES ('updateMsgOk', 'Facture modifiée avec succès', 'Invoice successfully modified', 'Fattura modificata con successo', 'Facture modifiée avec succès');
INSERT INTO public.recurring_paiements_page_translation (id, fr, gb, it, nl) VALUES ('notifToday', 'Facture à payer aujourd''hui!', 'Bill to be paid today!', 'Bolletta da pagare oggi!', 'Factuur om vandaag te betalen');
INSERT INTO public.recurring_paiements_page_translation (id, fr, gb, it, nl) VALUES ('notifTomorrow', 'Rappel: facture à payer demain!', 'Reminder: bill to be paid tomorrow!', 'Promemoria: bolletta da pagare domani!', 'Herinnering: factuur om morgen te betalen');
INSERT INTO public.recurring_paiements_page_translation (id, fr, gb, it, nl) VALUES ('notifBody', 'Facture.de.à payer', 'Invoice.of.to be paid', 'Fattura.da.da pagare', 'Factuur.van.te betalen');



---------------------------------------
------------Sign Up Page------------
---------------------------------------
INSERT INTO public.sign_up_page_translations (id, fr, gb, it, nl) VALUES ('signup', 'Inscription', 'Sign up', 'Iscrizione', 'Registratie');
INSERT INTO public.sign_up_page_translations (id, fr, gb, it, nl) VALUES ('firstname', 'Prénom', 'Firstname', 'Nome', 'Voornaam');
INSERT INTO public.sign_up_page_translations (id, fr, gb, it, nl) VALUES ('lastname', 'Nom', 'Lastname', 'Cognome', 'Naam');
INSERT INTO public.sign_up_page_translations (id, fr, gb, it, nl) VALUES ('email', 'Email', 'Email', 'Email', 'Email');
INSERT INTO public.sign_up_page_translations (id, fr, gb, it, nl) VALUES ('password', 'mot de passe', 'Password', 'Password', 'Paswoord');
INSERT INTO public.sign_up_page_translations (id, fr, gb, it, nl) VALUES ('passwordRepeat', 'Retaper le mot de passe', 'Retype the password', 'Reinserire la password', 'Typ het paswoord opnieuw');
INSERT INTO public.sign_up_page_translations (id, fr, gb, it, nl) VALUES ('currency', 'Devise', 'Currency', 'Moneta', 'Munteenheid');
INSERT INTO public.sign_up_page_translations (id, fr, gb, it, nl) VALUES ('status', 'Statut', 'Status', 'Stato', 'Status');
INSERT INTO public.sign_up_page_translations (id, fr, gb, it, nl) VALUES ('terms', 'Oui, j''accepte les termes et conditions', 'Yes, I accept the terms and conditions', 'Sì, accetto i termini e le condizioni', 'Ja, ik accepteer de algemene voorwarden');
INSERT INTO public.sign_up_page_translations (id, fr, gb, it, nl) VALUES ('SignUpNow', 'S''inscrire', 'Sign up now', 'Registrare', 'Registreren');
INSERT INTO public.sign_up_page_translations (id, fr, gb, it, nl) VALUES ('firstnameRequired', 'Prénom requis', 'Firstname required', 'Nome richiesto', 'Voornaam verplicht');
INSERT INTO public.sign_up_page_translations (id, fr, gb, it, nl) VALUES ('lastNameRequired', 'Nom requis', 'Lastname required', 'Cognome richiesto', 'Naam verplicht');
INSERT INTO public.sign_up_page_translations (id, fr, gb, it, nl) VALUES ('emailRequired', 'Email requis', 'Email required', 'Email richiesto', 'Email verplicht');
INSERT INTO public.sign_up_page_translations (id, fr, gb, it, nl) VALUES ('passwordRequired', 'mot de passe requis', 'Password required', 'Password richiesta', 'Paswoord verplicht');
INSERT INTO public.sign_up_page_translations (id, fr, gb, it, nl) VALUES ('passwordTwice', 'Entrez votre mot de passe deux fois', 'Enter your password twice', 'Inserisci la tua password due volte', 'Voer uw  paswoord twee keer in');
INSERT INTO public.sign_up_page_translations (id, fr, gb, it, nl) VALUES ('statusRequired', 'Statut requis', 'Status required', 'Stato richiesto', 'Vereiste status');
INSERT INTO public.sign_up_page_translations (id, fr, gb, it, nl) VALUES ('termsRequired', 'Vous devez accepter les termes et conditions', 'You need to accept the terms and condition', 'Dovete accettare i termini e le condizioni', 'U moet de voorwaarden accepteren');
INSERT INTO public.sign_up_page_translations (id, fr, gb, it, nl) VALUES ('successfulRegistration', 'Inscription réussie !', 'Successful registration!', 'Iscrizione riuscita', 'Succesvolle registratie');
INSERT INTO public.sign_up_page_translations (id, fr, gb, it, nl) VALUES ('emailExist', 'L''adresse électronique existe déjà', 'Email address already exist', 'L''indirizzo e-mail esiste già', 'Email bestaat al');
INSERT INTO public.sign_up_page_translations (id, fr, gb, it, nl) VALUES ('passwordMatch', 'Les mots de passe ne correspondent pas', 'Passwords don''t match', 'Le password non coincidono', 'Paswoord komen niet overeen');
INSERT INTO public.sign_up_page_translations (id, fr, gb, it, nl) VALUES ('passwordPattern', 'Le mot de passe doit contenir au minimum une majuscule, un chiffre, un caractère spécial et au moins 8 caractères.', 'The password must contain at least one capital letter, one number, one special character and at least 8 characters.', 'La password deve contenere almeno una lettera maiuscola, un numero, un carattere speciale e almeno 8 caratteri.', 'Het wachtwoord moet minstens één hoofdletter, één cijfer, één speciaal teken en minstens 8 karakters bevatten.');
INSERT INTO public.sign_up_page_translations (id, fr, gb, it, nl) VALUES ('subStatusLabel', 'Sous-statut', 'Sub-status', 'Sottostato', 'Substatus');
INSERT INTO public.sign_up_page_translations (id, fr, gb, it, nl) VALUES ('accountLabel', 'Compte', 'Account', 'Conto', 'Rekening');
INSERT INTO public.sign_up_page_translations (id, fr, gb, it, nl) VALUES ('currencyRequired', 'Devise requise', 'Currency required','valuta richiesta', 'Valuta vereist');
INSERT INTO public.sign_up_page_translations (id, fr, gb, it, nl) VALUES ('emailPattern', 'Email invalide', 'Invalid email', 'Email non valida', 'ongeldig e-mail');



---------------------------------------
-------Template Category  Page---------
---------------------------------------
INSERT INTO public.template_category_name_translation (id, fr, gb, it, nl) VALUES ('shopping', 'Shopping', 'shopping', NULL, NULL);
INSERT INTO public.template_category_name_translation (id, fr, gb, it, nl) VALUES ('other', 'autre', 'other', NULL, NULL);
INSERT INTO public.template_category_name_translation (id, fr, gb, it, nl) VALUES ('groceries', 'Course', 'Groceries', NULL, NULL);
INSERT INTO public.template_category_name_translation (id, fr, gb, it, nl) VALUES ('Mortgage', 'Crédit hypothécaire', 'Mortgage loan', NULL, NULL);
INSERT INTO public.template_category_name_translation (id, fr, gb, it, nl) VALUES ('royalties', 'Redevance', 'Royalties', NULL, NULL);
INSERT INTO public.template_category_name_translation (id, fr, gb, it, nl) VALUES ('insurance', 'Assurance', 'Insurance', NULL, NULL);
INSERT INTO public.template_category_name_translation (id, fr, gb, it, nl) VALUES ('mutual', 'Mutuelle', 'Mutual Insurance', NULL, NULL);
INSERT INTO public.template_category_name_translation (id, fr, gb, it, nl) VALUES ('salary', 'Salaire', 'Salary', NULL, NULL);
INSERT INTO public.template_category_name_translation (id, fr, gb, it, nl) VALUES ('optician', 'Opticien', 'Optician', NULL, NULL);
INSERT INTO public.template_category_name_translation (id, fr, gb, it, nl) VALUES ('diy', 'Bricolage', 'DIY', NULL, NULL);
INSERT INTO public.template_category_name_translation (id, fr, gb, it, nl) VALUES ('pharmacy', 'Pharmacie', 'Pharmacy', NULL, NULL);
INSERT INTO public.template_category_name_translation (id, fr, gb, it, nl) VALUES ('apple', 'Apple', 'Apple', NULL, NULL);
INSERT INTO public.template_category_name_translation (id, fr, gb, it, nl) VALUES ('music', 'Musique', 'Music', NULL, NULL);
INSERT INTO public.template_category_name_translation (id, fr, gb, it, nl) VALUES ('unknown entries', 'Revenus autres', 'Other income', NULL, NULL);
INSERT INTO public.template_category_name_translation (id, fr, gb, it, nl) VALUES ('unknown income', 'Dépenses autres', 'Other expenses', NULL, NULL);
INSERT INTO public.template_category_name_translation (id, fr, gb, it, nl) VALUES ('electronics', 'Electronique', 'Electronics', NULL, NULL);
INSERT INTO public.template_category_name_translation (id, fr, gb, it, nl) VALUES ('entertainment', 'Divertissement', 'Entertainment', NULL, NULL);



---------------------------------------
------------Sign In Page------------
---------------------------------------
INSERT INTO public.login_page_translation (id, fr, gb, it, nl) VALUES ('email', 'Email', 'Email', 'Email', 'Email');
INSERT INTO public.login_page_translation (id, fr, gb, it, nl) VALUES ('password', 'Mot de passe', 'Password', 'Password', 'Paswoord');
INSERT INTO public.login_page_translation (id, fr, gb, it, nl) VALUES ('connection', 'Connexion', 'Sign In', 'Connessione', 'Verbinding');
INSERT INTO public.login_page_translation (id, fr, gb, it, nl) VALUES ('login', 'Se connecter', 'Login', 'Accedi', 'Inloggen');
INSERT INTO public.login_page_translation (id, fr, gb, it, nl) VALUES ('emailRequired', 'E-mail requis', 'Email required', 'Email richiesto', 'Email verplicht');
INSERT INTO public.login_page_translation (id, fr, gb, it, nl) VALUES ('passwordRequired', 'Mot de passe requis', 'Password required', 'Password richiesta', 'Paswoord verplicht');
INSERT INTO public.login_page_translation (id, fr, gb, it, nl) VALUES ('successfulConnection', 'Connexion réussie', 'Successful connection!', 'Connessione riuscita', 'Succesvolle verbinding');
INSERT INTO public.login_page_translation (id, fr, gb, it, nl) VALUES ('signUp', 'S''inscrire', 'Sign up', 'Registrare', 'Registreren');
INSERT INTO public.login_page_translation (id, fr, gb, it, nl) VALUES ('newAccount', 'Nouveau compte?', 'New account?', 'Nuovo account?', 'Nieuwe account?');
INSERT INTO public.login_page_translation (id, fr, gb, it, nl) VALUES ('connectionError', 'Adresse mail et/ou mot de passe incorrect', 'Incorrect email address and/or password', 'Indirizzo e-mail e/o password errati', 'Onjuist e-mail en/of paswoord');



---------------------------------------
------------Friendship Page------------
---------------------------------------
INSERT INTO public.friendship_translations (id, fr, gb, it, nl) VALUES ('noRequest', 'Aucune demande', 'No request', 'Nessuna richiesta', 'Geen aanvraag');
INSERT INTO public.friendship_translations (id, fr, gb, it, nl) VALUES ('noPending', 'Aucune demande en attente', 'No pending requests', 'Nessuna richiesta pendente', 'Geen aanvragen in behandeling');
INSERT INTO public.friendship_translations (id, fr, gb, it, nl) VALUES ('noFriend', 'Aucun ami trouvé', 'No friend found', 'Nessun amico trovato', 'Geen vrienden gevonden');
INSERT INTO public.friendship_translations (id, fr, gb, it, nl) VALUES ('friendRequest', 'Demande d''ami', 'Friend request', 'Richiesta d''amicizia', 'Verzoek om vrienden');
INSERT INTO public.friendship_translations (id, fr, gb, it, nl) VALUES ('myFriends', 'Mes amis', 'My friends', 'I miei amici', 'Mijn vrienden');
INSERT INTO public.friendship_translations (id, fr, gb, it, nl) VALUES ('pendingRequest', 'Demande en attente', 'Pending request', 'Richieste pendenti', 'Aanvraag in behandeling');
INSERT INTO public.friendship_translations (id, fr, gb, it, nl) VALUES ('socialCodeNF', 'Code social non trouvé', 'Social code not found', 'Codice sociale non trovato', 'Sociale code niet gevonden');
INSERT INTO public.friendship_translations (id, fr, gb, it, nl) VALUES ('relationExist', 'Cette relation existe déjà', 'This relation already exists', 'Questa relazione esiste già', 'Deze relatie bestaat al');
INSERT INTO public.friendship_translations (id, fr, gb, it, nl) VALUES ('error', 'Une erreur est survenue, veuillez reéssayer', 'An error has occurred, please try again', 'Si è verificato un errore, si prega di riprovare', 'Dat is een fout opgetreden, probeer het opnieuw');
INSERT INTO public.friendship_translations (id, fr, gb, it, nl) VALUES ('pseudo', 'Mon pseudo', 'My pseudo', 'Il mio nickname', 'Mijn bijnaam');
INSERT INTO public.friendship_translations (id, fr, gb, it, nl) VALUES ('socialCode', 'Mon code social', 'My social code', 'Il mio codice sociale', 'Mijn sociale code');
INSERT INTO public.friendship_translations (id, fr, gb, it, nl) VALUES ('friendSince', 'Ami.e depuis', 'Friend since', 'Amici da', 'Vrienden sinds');
INSERT INTO public.friendship_translations (id, fr, gb, it, nl) VALUES ('enter', 'Entrer code d''amis', 'Enter friend code', 'Inserisci il codice amico', 'De code van een vriend invoeren');



---------------------------------------
------------Global------------
---------------------------------------
INSERT INTO public.global_translations (id, fr, gb, it, nl) VALUES ('navigation_groceries', 'Course', 'Groceries', 'Spese', 'Boodschappen');
INSERT INTO public.global_translations (id, fr, gb, it, nl) VALUES ('navigation_recurring', 'Récurrent', 'Recurring', 'Ricorrente', 'Terugkerend');
INSERT INTO public.global_translations (id, fr, gb, it, nl) VALUES ('navigation_friend', 'Amis', 'Friends', 'I miei amici', 'Vrienden');
INSERT INTO public.global_translations (id, fr, gb, it, nl) VALUES ('navigation_setting', 'Paramètres', 'Settings', 'Parametri', 'Parameters');
INSERT INTO public.global_translations (id, fr, gb, it, nl) VALUES ('navigation_budget', 'Budget', 'Budget', 'Budget', 'Budget');
INSERT INTO public.global_translations (id, fr, gb, it, nl) VALUES ('navigation_categories', 'Catégories', 'Categories', 'Categorie', 'Categorieën');
INSERT INTO public.global_translations (id, fr, gb, it, nl) VALUES ('navigation_transaction', 'Transactions', 'Transactions', 'Transazioni', 'Transacties');
INSERT INTO public.global_translations (id, fr, gb, it, nl) VALUES ('navigation_projects', 'Projets', 'Projects', 'I miei progetti', 'Projecten');
INSERT INTO public.global_translations (id, fr, gb, it, nl) VALUES ('navigation_home', 'Accueil', 'Home', 'Home', 'Home');
INSERT INTO public.global_translations (id, fr, gb, it, nl) VALUES ('navigation_recipes', 'Recettes', 'Recipes', 'Ricette', 'Recept');
INSERT INTO public.global_translations (id, fr, gb, it, nl) VALUES ('navigation_basket', 'Panier', 'Basket', 'Cestino', 'Mand');
INSERT INTO public.global_translations (id, fr, gb, it, nl) VALUES ('navigation_shoppinglists', 'Liste de courses', 'Shopping Lists', 'Lista della spesa', 'Boodschappenlijst');
INSERT INTO public.global_translations (id, fr, gb, it, nl) VALUES ('transactions_all', 'Toutes les transactions', 'All transactions', 'Tutte le transazioni', 'Alle transacties');
INSERT INTO public.global_translations (id, fr, gb, it, nl) VALUES ('transactions_new', 'Nouvelle transaction', 'New transaction', 'Nuova transazione', 'Nieuwe transactie');
INSERT INTO public.global_translations (id, fr, gb, it, nl) VALUES ('error_required', 'Ce champ est requis', 'This field is required', 'Questo campo è obbligatorio', 'Dit veld is verplicht');
INSERT INTO public.global_translations (id, fr, gb, it, nl) VALUES ('error_pattern', 'Le format de ce champ est incorrect', 'The pattern of this field doesn’t match', 'Il formato della risposta a questo campo non è corretto.', 'Het formaat van dit veld is onjuist');
INSERT INTO public.global_translations (id, fr, gb, it, nl) VALUES ('error_invalid', 'Champ invalide', 'Invalid input', 'Inserimento non valido', 'Ongeldig veld');
INSERT INTO public.global_translations (id, fr, gb, it, nl) VALUES ('error_unexpectedError', 'Une erreur est survenue, veuillez recommencer', 'An error has occurred, please try again', 'Si è verificato un errore, si prega di riprovare', 'Er is een fout opgetreden, probeer het opnieuw');
INSERT INTO public.global_translations (id, fr, gb, it, nl) VALUES ('error_imageRequired', 'Veuillez choisir une image', 'Please choose an image', 'Selezionare un''immagine', 'Kies een afbeelding');
INSERT INTO public.all_transactions_translation (id, fr, gb, it, nl) VALUES ('form_deletionIrreversibleWarning', 'Cette action est irréversible', 'This action is irreversible', 'Quest''azione è irreversibile', 'Deze actie is onomkeerbaar');
INSERT INTO public.global_translations (id, fr, gb, it, nl) VALUES ('form_delete', 'Supprimer', 'Delete', 'Cancellare', 'Verwijder');
INSERT INTO public.global_translations (id, fr, gb, it, nl) VALUES ('form_search', 'Chercher', 'Seach', '', '');
INSERT INTO public.global_translations (id, fr, gb, it, nl) VALUES ('form_add', 'Ajouter', 'Add', '', '');
INSERT INTO public.global_translations (id, fr, gb, it, nl) VALUES ('form_update', 'Modifier', 'Update', '', '');
INSERT INTO public.global_translations (id, fr, gb, it, nl) VALUES ('form_cancel', 'Annuler', 'Cancel', '', '');
INSERT INTO public.global_translations (id, fr, gb, it, nl) VALUES ('form_close', 'Fermer', 'Close', '', '');
INSERT INTO public.global_translations (id, fr, gb, it, nl) VALUES ('form_confirm', 'Confirmer', 'Confirm', '', '');
INSERT INTO public.global_translations (id, fr, gb, it, nl) VALUES ('form_save', 'Sauvegarder', 'Save', '', '');
INSERT INTO public.global_translations (id, fr, gb, it, nl) VALUES ('error_positive', 'Ce champ n''accepte que des valeurs positives', 'This field requires a positive value', '', '');
INSERT INTO public.global_translations (id, fr, gb, it, nl) VALUES ('form_deletionConfirm', 'Êtes-vous sûre de vouloir supprimer ?', 'Are you sure your want to delete?', '', '');
INSERT INTO public.global_translations (id, fr, gb, it, nl) VALUES ('error_addError', 'Ajout échoué', 'Failed to insert', '', '');
INSERT INTO public.global_translations (id, fr, gb, it, nl) VALUES ('error_deletionError', 'Supression échouée', 'Failed to delete', '', '');
INSERT INTO public.global_translations (id, fr, gb, it, nl) VALUES ('error_updateError', 'Modification échouée', 'Failed to update', '', '');



---------------------------------------
------------Home Page------------
---------------------------------------
INSERT INTO public.homepage_translations (id, fr, gb, it, nl) VALUES ('title', 'Bienvenue', 'Welcome', 'Benvenuto', 'Welkom');
INSERT INTO public.homepage_translations (id, fr, gb, it, nl) VALUES ('subTitle1', 'Mon budget', 'My budget', 'Il mio budget', 'Mijn budget');
INSERT INTO public.homepage_translations (id, fr, gb, it, nl) VALUES ('subTitle2', 'Mon solde', 'My balance', 'Il mio saldo', 'Mijn saldo');
INSERT INTO public.homepage_translations (id, fr, gb, it, nl) VALUES ('subTitle3', 'Mon épargne', 'My savings', 'I miei risparmi', 'Mijn spaargeld');
INSERT INTO public.homepage_translations (id, fr, gb, it, nl) VALUES ('estimatedBudget', 'Budget prévisionnel', 'Estimated budget', 'Budget stimato', 'Geraamde budget');
INSERT INTO public.homepage_translations (id, fr, gb, it, nl) VALUES ('spentBudget', 'Budget dépensé', 'Spent budget', 'Budget speso', 'Besteed budget');
INSERT INTO public.homepage_translations (id, fr, gb, it, nl) VALUES ('seeAllTransaction', 'Toutes mes transactions', 'All my transactions', 'Tutte le mie transazioni', 'Al mijn transacties');
INSERT INTO public.homepage_translations (id, fr, gb, it, nl) VALUES ('newTransaction', 'Nouvelle transaction', 'New transaction', 'Nuova transazione', 'Nieuwe transacties');
INSERT INTO public.homepage_translations (id, fr, gb, it, nl) VALUES ('chooseAccount', 'Choisir un compte', 'Choose an account', 'Scegliere un account', 'Kies een account');
INSERT INTO public.homepage_translations (id, fr, gb, it, nl) VALUES ('import', 'Importer un fichier csv', 'Import a csv file', 'Importare un file csv', 'Een CSV bestand importeren');
INSERT INTO public.homepage_translations (id, fr, gb, it, nl) VALUES ('newAccount', 'Nouveau compte', 'New account', 'Nuovo account', 'Nieuw account');
INSERT INTO public.homepage_translations (id, fr, gb, it, nl) VALUES ('wrongCsvFormat', 'Format csv incorrect', 'Wrong csv format', 'Formato csv non corretto', 'Formaat CSV onjuist');
INSERT INTO public.homepage_translations (id, fr, gb, it, nl) VALUES ('accountNotFound', 'Compte non trouvé', 'Account not found', 'Account non trovato', 'Account niet gevonden');
INSERT INTO public.homepage_translations (id, fr, gb, it, nl) VALUES ('transactionRecorded', 'Les transactions ont été enregistrées', 'The transactions were recorded', 'Le transazioni sono state registrate', 'Transacties werden geregistreerd');
INSERT INTO public.homepage_translations (id, fr, gb, it, nl) VALUES ('emptyField', 'Veuillez remplir tous les champs', 'Please fill in all fields', 'Si prega di compilare tutti i campi', 'Gelieve alle velden in te vullen');
INSERT INTO public.homepage_translations (id, fr, gb, it, nl) VALUES ('graphTitle', 'Evolution revenus-dépenses', 'Evolution incomes-expenses', 'Evoluzione del reddito/spesa', 'Evolutie van inkomsten en uitgaven');
INSERT INTO public.homepage_translations (id, fr, gb, it, nl) VALUES ('incomes', 'Revenus', 'Incomes', 'Entrate', 'Inkomsten');
INSERT INTO public.homepage_translations (id, fr, gb, it, nl) VALUES ('expenses', 'Dépenses', 'Expenses', 'Spese', 'Uitgaven');
INSERT INTO public.homepage_translations (id, fr, gb, it, nl) VALUES ('amount', 'Montant', 'Amount', 'Importo', 'Bedraag');
INSERT INTO public.homepage_translations (id, fr, gb, it, nl) VALUES ('today', 'Aujourd''hui', 'Today', 'Oggi', 'Vandaag');
INSERT INTO public.homepage_translations (id, fr, gb, it, nl) VALUES ('week', 'Semaine', 'Week', 'Settimana', 'Week');
INSERT INTO public.homepage_translations (id, fr, gb, it, nl) VALUES ('month', 'Mois', 'Month', 'Mese', 'Maand');



---------------------------------------
---------Add Transaction Page----------
---------------------------------------
INSERT INTO public.add_transaction_translations (id, fr, gb, it, nl) VALUES ('title', 'Nouvelle transaction', 'New transaction', 'Nuova transazione', 'Nieuwe transactie');
INSERT INTO public.add_transaction_translations (id, fr, gb, it, nl) VALUES ('typeInput', 'Revenu', 'Income', 'Entrata', 'Inkomen');
INSERT INTO public.add_transaction_translations (id, fr, gb, it, nl) VALUES ('typeOutput', 'Dépense', 'Expense', 'Spesa', 'Onkosten ');
INSERT INTO public.add_transaction_translations (id, fr, gb, it, nl) VALUES ('amount', 'Montant', 'Amount', 'Importo', 'Bedrag');
INSERT INTO public.add_transaction_translations (id, fr, gb, it, nl) VALUES ('valueDate', 'Date valeur', 'Value date', 'Data valore', 'Valutadatum');
INSERT INTO public.add_transaction_translations (id, fr, gb, it, nl) VALUES ('executionDate', 'Date d''éxecution', 'Execution date', 'Data d''esecuzione', 'Executiedag');
INSERT INTO public.add_transaction_translations (id, fr, gb, it, nl) VALUES ('category', 'Catégorie', 'Category', 'Categoria', 'Category');
INSERT INTO public.add_transaction_translations (id, fr, gb, it, nl) VALUES ('account', 'Compte', 'Account', 'Conto', 'Rekening');
INSERT INTO public.add_transaction_translations (id, fr, gb, it, nl) VALUES ('add', 'Ajouter', 'Add', 'Aggiungere', 'Toevoegen');
INSERT INTO public.add_transaction_translations (id, fr, gb, it, nl) VALUES ('createNew', 'Créer nouveau', 'Create new', 'Crea nuovo', 'Maak nieuw');
INSERT INTO public.add_transaction_translations (id, fr, gb, it, nl) VALUES ('titlePopupAddAccount', 'Ajouter un moyen de paiement', 'Add a payment method', 'Aggiungere un metodo di pagamento', 'Voeg een betalingsmethode toe');
INSERT INTO public.add_transaction_translations (id, fr, gb, it, nl) VALUES ('name', 'Nom', 'Name', 'Nome', 'Naam');
INSERT INTO public.add_transaction_translations (id, fr, gb, it, nl) VALUES ('namePlaceHolder', 'Ex: Liquide', 'Ex: Cash', 'Es: Contante', 'Bv: Contant geld');
INSERT INTO public.add_transaction_translations (id, fr, gb, it, nl) VALUES ('type', 'Type', 'Type', 'Tipo', 'Type');
INSERT INTO public.add_transaction_translations (id, fr, gb, it, nl) VALUES ('confirmMsgAfterAdd', 'Transaction ajoutée avec succès', 'Transaction successfully added', 'Transazione aggiunta con successo', 'Transactie succesvol toegevoegd');
INSERT INTO public.add_transaction_translations (id, fr, gb, it, nl) VALUES ('errorMsg', 'Veuillez remplir tous les champs', 'Please fill in all the fields', 'Si prega di compilare tutti i campi', ' Vul alstublieft alle velden in');
INSERT INTO public.add_transaction_translations (id, fr, gb, it, nl) VALUES ('btnSingleCategory', 'Unique', 'Single', 'Singola', 'Uniek');
INSERT INTO public.add_transaction_translations (id, fr, gb, it, nl) VALUES ('btnMultplipleCategories', 'Multiple', 'Multiple', 'Multipli', 'Meerdere');
INSERT INTO public.add_transaction_translations (id, fr, gb, it, nl) VALUES ('singleCategoryErrorMsg', 'Veuillez sélectionner une catégorie', 'Please select a category', 'Si prega di selezionare una categoria', 'Selecteer een categorie');
INSERT INTO public.add_transaction_translations (id, fr, gb, it, nl) VALUES ('selectACategory', 'Sélectionner une catégorie', 'Select a category', 'Selezionare una categoria', 'Selecteren');
INSERT INTO public.add_transaction_translations (id, fr, gb, it, nl) VALUES ('splitIntoSeverals', 'Diviser en plusieurs catégories', 'Split into several categories', 'Dividere in diverse categorie', 'Opgesplitst in verschillende categorieën');
INSERT INTO public.add_transaction_translations (id, fr, gb, it, nl) VALUES ('amountToSplit', 'Montant à diviser ', 'Amount to split: ', 'Importo da suddividere:', 'Te verdelen bedrag');
INSERT INTO public.add_transaction_translations (id, fr, gb, it, nl) VALUES ('multipleCategoriesErrorMsg1Part1', 'Le total entré doit correspondre ', 'The total must match the ', 'Il totale inserito deve corrispondere a ', 'Het ingevoerde bedrag moet overeenkomen');
INSERT INTO public.add_transaction_translations (id, fr, gb, it, nl) VALUES ('multipleCategoriesErrorMsg1Part2', 'au montant de la transaction', 'amount of the transaction', 'l''importo della transazione', 'Voor het bedrag van de transactie');
INSERT INTO public.add_transaction_translations (id, fr, gb, it, nl) VALUES ('multipleCategoriesErrorMsg2Part1', 'Veuillez entrer un montant positif pour', 'Please enter a positive amount', 'Si prega di inserire un importo positivo per', 'Voer een positief bedrag in voor');
INSERT INTO public.add_transaction_translations (id, fr, gb, it, nl) VALUES ('multipleCategoriesErrorMsg2Part2', 'chaque catégorie sélectionnée', 'for each selected category', 'ogni categoria selezionata', ' elke categorie geselecteerd');
INSERT INTO public.add_transaction_translations (id, fr, gb, it, nl) VALUES ('cancel', 'Annuler', 'Cancel', 'Cancellare', 'Annuleren');
INSERT INTO public.add_transaction_translations (id, fr, gb, it, nl) VALUES ('btnAllTransactions', 'Toutes mes transactions', 'All my transactions', 'Tutte le mie transazioni', 'Al mijn transacties');
INSERT INTO public.add_transaction_translations (id, fr, gb, it, nl) VALUES ('repeteTransaction', 'Répéter cette transaction', 'Repeat this transaction', 'Ripetere questa transazione', 'Herhaal deze transactie');
INSERT INTO public.add_transaction_translations (id, fr, gb, it, nl) VALUES ('msgUnexpectedError', 'Une erreur est survenue', 'An error has occurred', 'Si è verificato un errore', 'Er is een fout opgetreden');
INSERT INTO public.add_transaction_translations (id, fr, gb, it, nl) VALUES ('popupRecurringTitle', 'Répéter chaque mois', 'Repeat each month', 'Ripetere ogni mese', 'Herhaal elke maand');
INSERT INTO public.add_transaction_translations (id, fr, gb, it, nl) VALUES ('popupRecurringPlaceholder', 'Ex: Abonnement téléphone', 'Ex: Phone subscription', 'Es: Abbonamento telefonico', 'Bv: Telefoonabonnement');
INSERT INTO public.add_transaction_translations (id, fr, gb, it, nl) VALUES ('until', 'Jusqu''au', 'Until the', 'Fino a', 'Tot');
INSERT INTO public.add_transaction_translations (id, fr, gb, it, nl) VALUES ('ok', 'OK', 'OK', 'OK', 'OK');
INSERT INTO public.add_transaction_translations (id, fr, gb, it, nl) VALUES ('error_valueDateBeforeExecution', 'La date valeur ne peut pas être postérieure à la date d''éxécution', 'The value date cannot be posterior to the execution date', '', '');
INSERT INTO public.add_transaction_translations (id, fr, gb, it, nl) VALUES ('error_categoriesTotalNotAddingUp', 'La somme des montants des catégories ne correspond pas au montant total de la transaction', 'The categories'' sum doens''t add up to the total amount of the transaction', '', '');
INSERT INTO public.add_transaction_translations (id, fr, gb, it, nl) VALUES ('error_recurringMinimum', 'La date de fin de la récurrence doit être au minimum 1 mois postérieure à la date d''écéxution', 'The end of the recurring date must at least be 1 month after the execution date' , '', '');



---------------------------------------
---------All Transactions Page----------
---------------------------------------
INSERT INTO public.all_transactions_translation (id, fr, gb, it, nl) VALUES ('payment', 'Paiement', 'Payment', 'Pagamento', 'Betaling');
INSERT INTO public.all_transactions_translation (id, fr, gb, it, nl) VALUES ('transfer', 'Virement', 'Transfer', 'Trasferimento', 'Overdacht');
INSERT INTO public.all_transactions_translation (id, fr, gb, it, nl) VALUES ('amount', 'Montant: ', 'Amount:', 'Importo:', 'Bedrag');
INSERT INTO public.all_transactions_translation (id, fr, gb, it, nl) VALUES ('valueDate', 'Date valeur: ', 'Value date:', 'Data valore:', 'Waarde datum');
INSERT INTO public.all_transactions_translation (id, fr, gb, it, nl) VALUES ('executionDate', 'Date d''exécution', 'Excecution date:', 'Data di esecuzione', 'Datum van uitvoering');
INSERT INTO public.all_transactions_translation (id, fr, gb, it, nl) VALUES ('paymentMethod', 'Moyen de paiement:', 'Payment method:', 'Modo di pagamento:', 'Wijze van betaling');
INSERT INTO public.all_transactions_translation (id, fr, gb, it, nl) VALUES ('delete', 'Supprimer', 'Delete', 'Eliminare', 'Verwijderen');
INSERT INTO public.all_transactions_translation (id, fr, gb, it, nl) VALUES ('warningDeleteMsg', 'Cette action est irréversible', 'This action is irreversible', 'Quest''azione è irreversibile', 'Deze actie is onomkeerbaar');
INSERT INTO public.all_transactions_translation (id, fr, gb, it, nl) VALUES ('popupDeleteTitle', 'Supprimer la transaction ?', 'Delete the transaction?', 'Eliminare la transazione?', 'De transactie verwijderen');
INSERT INTO public.all_transactions_translation (id, fr, gb, it, nl) VALUES ('deleteMsgOk', 'Transaction supprimée avec succès', 'Transaction successfully deleted', 'Transazione cancellata con successo', 'Transactie succesvol verwijderd');
INSERT INTO public.all_transactions_translation (id, fr, gb, it, nl) VALUES ('noTransactionForMonth', 'Pas de transaction pour ce mois-ci.', 'No transaction for this month yet.', 'Nessuna transazione per questo mese', 'Geen transacties voor deze maand.');
INSERT INTO public.all_transactions_translation (id, fr, gb, it, nl) VALUES ('balance', 'Solde', 'Balance', '', '');
INSERT INTO public.all_transactions_translation (id, fr, gb, it, nl) VALUES ('currentMonth', 'Mois courant', 'Current month', '', '');



---------------------------------------
----------Categoreies Page-------------
---------------------------------------
INSERT INTO public.budget_translation (id, fr, gb, it, nl) VALUES ('budgetSpend', 'Budget journalier depensé', 'Daily budget spent', 'Budget giornaliero speso', 'Dagelijks uitgegeven budget');
INSERT INTO public.budget_translation (id, fr, gb, it, nl) VALUES ('plannedBudget', 'Budget journalier prévu', 'Planned daily budget', 'Budget giornaliero previsto', 'Geplande dagbudget');
INSERT INTO public.budget_translation (id, fr, gb, it, nl) VALUES ('totalExpenses', 'Dépenses totales', 'Total expenses', 'Totale delle spese', 'Totale uitgaven');
INSERT INTO public.budget_translation (id, fr, gb, it, nl) VALUES ('manageCategories', 'Gérer mes categories', 'Manage my categories', 'Gestire le mie categorie', 'Mijn categorieën beheren');
INSERT INTO public.budget_translation (id, fr, gb, it, nl) VALUES ('myEntries', 'Mes entrées', 'My entries', 'Le mie entrate', 'Mijn inzendingen');
INSERT INTO public.budget_translation (id, fr, gb, it, nl) VALUES ('noEntries', 'Aucune entrées ce-mois-ci', 'No entries this month', 'Nessuna entrata questo mese', 'Geen inzendingen deze maand');
INSERT INTO public.budget_translation (id, fr, gb, it, nl) VALUES ('myExpense', 'Mes dépenses', 'My expenses', 'Le mie spese', 'Mijn onkosten');
INSERT INTO public.budget_translation (id, fr, gb, it, nl) VALUES ('noExpense', 'Aucune dépense ce-mois-ci', 'No expenses this month', 'Nessuna spesa questo mese', 'Geen uitgaven deze maand');
INSERT INTO public.budget_translation (id, fr, gb, it, nl) VALUES ('noData', 'Aucune données', 'No data', 'No dati', 'Geen gegevens');
INSERT INTO public.budget_translation (id, fr, gb, it, nl) VALUES ('transactions', 'Transactions', 'Transactions', 'Transazioni', 'Transacties');
INSERT INTO public.budget_translation (id, fr, gb, it, nl) VALUES ('ok', 'Ok', 'Ok', 'Ok', 'Ok');
INSERT INTO public.budget_translation (id, fr, gb, it, nl) VALUES ('cancel', 'Annuler', 'Cancel', 'Cancellare', 'Annuleren');
INSERT INTO public.budget_translation (id, fr, gb, it, nl) VALUES ('weeklyBudget', 'Budget hebdomadaire', 'Weekly budget', 'Budget settimanale', 'Wekelijks budget');



---------------------------------------
--------------Budget Page--------------
---------------------------------------
INSERT INTO public.categories_management_translations (id, fr, gb, it, nl) VALUES ('title', 'Gérer mon budget', 'Manage my budget', 'Gestire il mio budget', 'Beheerd mijn budget');
INSERT INTO public.categories_management_translations (id, fr, gb, it, nl) VALUES ('income', 'Revenus', 'Income', 'Entrate', 'Inkomen');
INSERT INTO public.categories_management_translations (id, fr, gb, it, nl) VALUES ('expenses', 'Dépenses', 'Expenses', 'Spese', 'Onkosten ');
INSERT INTO public.categories_management_translations (id, fr, gb, it, nl) VALUES ('newCategory', 'Nouvelle catégorie', 'New category', 'Nuova categoria', 'Nieuwe category');
INSERT INTO public.categories_management_translations (id, fr, gb, it, nl) VALUES ('name', 'Nom ', 'Name', 'Nome', 'Naam');
INSERT INTO public.categories_management_translations (id, fr, gb, it, nl) VALUES ('budgetPerMonth', 'Budget par mois', 'Budget per month', 'Budget al mese', 'Budget per maand');
INSERT INTO public.categories_management_translations (id, fr, gb, it, nl) VALUES ('icon', 'Icône', 'Icon', 'Icona', 'Icon');
INSERT INTO public.categories_management_translations (id, fr, gb, it, nl) VALUES ('add', 'Ajouter', 'Add', 'Aggiungere', 'Toevoegen');
INSERT INTO public.categories_management_translations (id, fr, gb, it, nl) VALUES ('cancel', 'Annuler', 'Cancel', 'Cancellare', 'Annuleren');
INSERT INTO public.categories_management_translations (id, fr, gb, it, nl) VALUES ('addConfirmation', 'La catégorie a été ajoutée', 'Category has been successfully added', 'La categoria è stata aggiunta', 'Categorie is toegevoegd');
INSERT INTO public.categories_management_translations (id, fr, gb, it, nl) VALUES ('modificationConfirmation', 'Modification réussie', 'Modified successfully', 'Modificato con successo', 'Succesvolle wijziging');
INSERT INTO public.categories_management_translations (id, fr, gb, it, nl) VALUES ('delete', 'Supprimer', 'Delete', 'Eliminare', 'Verwijderen');
INSERT INTO public.categories_management_translations (id, fr, gb, it, nl) VALUES ('deleteTheCategory', 'Supprimer la catégorie', 'Delete the category', 'Eliminare la categoria', 'Verwijder categorie');
INSERT INTO public.categories_management_translations (id, fr, gb, it, nl) VALUES ('warningDeleteMsg', 'Cette action est irréversible', 'This action is irreversible', 'Quest''azione è irreversibile', 'Deze actie is onomkeerbaar');
INSERT INTO public.categories_management_translations (id, fr, gb, it, nl) VALUES ('deleteConfirmation', 'La catégorie   a été supprimée', 'Category   has been successfully deleted', 'La categoria   è stata cancellata', 'De categorie   is verwijderd');
INSERT INTO public.categories_management_translations (id, fr, gb, it, nl) VALUES ('iconFilterPlaceholder', 'Filtrer par nom', 'Filter by name', 'Filtrare per nome', 'Filter op naam');
INSERT INTO public.categories_management_translations (id, fr, gb, it, nl) VALUES ('budget', 'Budget', 'Budget', 'Budget', 'Budget');
INSERT INTO public.categories_management_translations (id, fr, gb, it, nl) VALUES ('modification', 'Modification', 'Modification', 'Modifica', 'Wijziging');
INSERT INTO public.categories_management_translations (id, fr, gb, it, nl) VALUES ('save', 'Enregistrer', 'Save', 'Salvare', 'Opnemen');
INSERT INTO public.categories_management_translations (id, fr, gb, it, nl) VALUES ('deleteErrorPart1', 'Impossible de supprimer une catégorie contenant', 'Impossible to delete a category containing ', 'Impossibile eliminare una categoria che contiene
', 'Kan een categorie met transacties of ');
INSERT INTO public.categories_management_translations (id, fr, gb, it, nl) VALUES ('deleteErrorPart2', ' des transactions ou sous-catégories', ' transactions or sub-categories', ' transazioni o sottocategorie', 'Subcategorieën niet verwijderen');
INSERT INTO public.categories_management_translations (id, fr, gb, it, nl) VALUES ('addError1', 'Veuillez remplir tous les champs', 'Please fill in all the fields', 'Si prega di compilare tutti i campi', 'Vul alstublieft alle velden in');
INSERT INTO public.categories_management_translations (id, fr, gb, it, nl) VALUES ('addError2', 'Veuillez entrer un budget positif', 'The budget must be positive', 'Si prega si entrare un budget positivo', 'Geef een positief budget op');



---------------------------------------
---------Status Translations----------
---------------------------------------
INSERT INTO public.status_translations (id, fr, gb, it, nl) VALUES ('label', 'Status', 'Status', 'Stato','Toestand');
INSERT INTO public.status_translations (id, fr, gb, it, nl) VALUES ('student', 'Étudiant', 'Student', 'Studente', 'Student');
INSERT INTO public.status_translations (id, fr, gb, it, nl) VALUES ('worker', 'Travailleur', 'Worker', 'Lavoratore', 'Werknemer');
INSERT INTO public.status_translations (id, fr, gb, it, nl) VALUES ('pensioner', 'Pensionnaire', 'Pensioner', 'Residente', 'Gepensioneerde');
INSERT INTO public.status_translations (id, fr, gb, it, nl) VALUES ('unemployed', 'Sans emploi', 'Unemployed', 'Disoccupato', 'Werkloos');



---------------------------------------
---------------Settings-------------
---------------------------------------
INSERT INTO public.settings_page_translations (id, fr, gb, it, nl) VALUES ('signout', 'Se déconnecter', 'Sign out', 'Disconnessione', 'Afmelden');
INSERT INTO public.settings_page_translations (id, fr, gb, it, nl) VALUES ('handleChange', 'Changer mon pseudo', 'Change my handle', 'Cambiare il mio nickname', 'Mijn bijnaam wijzigen');
INSERT INTO public.settings_page_translations (id, fr, gb, it, nl) VALUES ('languageChange', 'Changer de langue', 'Change language', 'Cambiare la lingua', 'Taal wijzigen');
INSERT INTO public.settings_page_translations (id, fr, gb, it, nl) VALUES ('deleteAccount', 'Supprimer mon compte', 'Delete my account', 'Cancellare il mio account', 'Verwijder mijn account');
INSERT INTO public.settings_page_translations (id, fr, gb, it, nl) VALUES ('statusChange', 'Changer de status', 'Change status', '', '');
INSERT INTO public.settings_page_translations (id, fr, gb, it, nl) VALUES ('confirm_deleteAccountTitle', 'Êtes-vous sûre ?', 'Are you sure?', '', '');
INSERT INTO public.settings_page_translations (id, fr, gb, it, nl) VALUES ('confirm_deleteAccountText', 'En supprimant votre compte, toutes vos données seront définitivement effacées et perdues !', 'If you delete your account, all your saved data will be lost and unrecoverable !', '', '');
INSERT INTO public.settings_page_translations (id, fr, gb, it, nl) VALUES ('error_deleteAccount', 'Une erreur est survenue: impossible de supprimer le compte. Veuillez rééssayer plus tard.', 'An error occured: impossible to delete your account. Please retry later.', '', '');



---------------------------------------
---------Shopping Lists Module---------
---------------------------------------
INSERT INTO public.shopping_lists_module_translations (id, fr, gb, it, nl) VALUES ('recipes', 'Les recettes', 'Recipes', '','');
INSERT INTO public.shopping_lists_module_translations (id, fr, gb, it, nl) VALUES ('recipe', 'Recette', 'Recipe', '','');
INSERT INTO public.shopping_lists_module_translations (id, fr, gb, it, nl) VALUES ('ingredients', 'Les ingrédients', 'Ingredients', '','');
INSERT INTO public.shopping_lists_module_translations (id, fr, gb, it, nl) VALUES ('shoppingLists', 'Les listes de courses', 'Shopping lists', '','');
INSERT INTO public.shopping_lists_module_translations (id, fr, gb, it, nl) VALUES ('shoppingList', 'Liste de courses', 'Shopping list', '','');
INSERT INTO public.shopping_lists_module_translations (id, fr, gb, it, nl) VALUES ('proteins', 'Protéines', 'Proteins', '','');
INSERT INTO public.shopping_lists_module_translations (id, fr, gb, it, nl) VALUES ('of', 'de', 'of', '','');
INSERT INTO public.shopping_lists_module_translations (id, fr, gb, it, nl) VALUES ('ofTheMasc', 'du', 'of the', '','');
INSERT INTO public.shopping_lists_module_translations (id, fr, gb, it, nl) VALUES ('servings', 'Portions', 'Servings', '','');
INSERT INTO public.shopping_lists_module_translations (id, fr, gb, it, nl) VALUES ('byLowestPrice', 'Selon le plus petit prix', 'By the lowest price', '','');
INSERT INTO public.shopping_lists_module_translations (id, fr, gb, it, nl) VALUES ('byHighestPrice', 'Selon le plus grand prix', 'By the lowest price', '','');
INSERT INTO public.shopping_lists_module_translations (id, fr, gb, it, nl) VALUES ('byName', 'Selon le nom', 'By name', '','');
INSERT INTO public.shopping_lists_module_translations (id, fr, gb, it, nl) VALUES ('show', 'Afficher', 'Show', '','');
INSERT INTO public.shopping_lists_module_translations (id, fr, gb, it, nl) VALUES ('hide', 'Cacher', 'Hide', '','');
INSERT INTO public.shopping_lists_module_translations (id, fr, gb, it, nl) VALUES ('showOnlySelection', 'Afficher uniquement la selection', 'Show only the selection', '','');
INSERT INTO public.shopping_lists_module_translations (id, fr, gb, it, nl) VALUES ('showAll', 'Tout afficher', 'Show all', '','');
INSERT INTO public.shopping_lists_module_translations (id, fr, gb, it, nl) VALUES ('totalPrice', 'Le prix total est de', 'The total price is', '','');
INSERT INTO public.shopping_lists_module_translations (id, fr, gb, it, nl) VALUES ('yourBasket', 'Votre panier', 'Your basket', '','');
INSERT INTO public.shopping_lists_module_translations (id, fr, gb, it, nl) VALUES ('createdOn', 'Créée le', 'Created on the', '','');
INSERT INTO public.shopping_lists_module_translations (id, fr, gb, it, nl) VALUES ('shoppinglistCreationSuccess', 'La liste de courses a été créée avec succès !', 'The new shopping list has been created! ', '','');
INSERT INTO public.shopping_lists_module_translations (id, fr, gb, it, nl) VALUES ('shoppinglistDeletionSuccess', 'La liste de courses a été supprimée avec succès !', 'The shopping list has been deleted successfully! ', '','');
INSERT INTO public.shopping_lists_module_translations (id, fr, gb, it, nl) VALUES ('shoppinglistCreationError', 'La liste de courses n''a pas été créée.', 'Unable to create the shopping list.', '','');
INSERT INTO public.shopping_lists_module_translations (id, fr, gb, it, nl) VALUES ('shoppinglistDeletionError', 'La liste de courses n''a pas été supprimée.', 'Unable the to delete the shopping list.', '','');
INSERT INTO public.shopping_lists_module_translations (id, fr, gb, it, nl) VALUES ('noSavedShoppingList', 'Il n''y a pour le moment aucune liste de course enregistrée.', 'There isn''t any saved shopping list at the moment.', '','');



---------------------------------------
--------------Icons--------------------
---------------------------------------
INSERT INTO public.icons (iconid, icon_code, type) VALUES (1, 'fas fa-dollar-sign', 'FONTAWESOME');
INSERT INTO public.icons (iconid, icon_code, type) VALUES (2, 'fas fa-euro-sign', 'FONTAWESOME');
INSERT INTO public.icons (iconid, icon_code, type) VALUES (3, 'fas fa-credit-card', 'FONTAWESOME');
INSERT INTO public.icons (iconid, icon_code, type) VALUES (4, 'fab fa-cc-visa', 'FONTAWESOME');
INSERT INTO public.icons (iconid, icon_code, type) VALUES (5, 'fab fa-cc-paypal', 'FONTAWESOME');
INSERT INTO public.icons (iconid, icon_code, type) VALUES (6, 'fab fa-cc-mastercard', 'FONTAWESOME');
INSERT INTO public.icons (iconid, icon_code, type) VALUES (7, 'fab fa-cc-apple-pay', 'FONTAWESOME');
INSERT INTO public.icons (iconid, icon_code, type) VALUES (8, 'fab fa-cc-amex', 'FONTAWESOME');
INSERT INTO public.icons (iconid, icon_code, type) VALUES (9, 'fab fa-cc-amazon-pay', 'FONTAWESOME');
INSERT INTO public.icons (iconid, icon_code, type) VALUES (10, 'fas fa-money-bill-wave', 'FONTAWESOME');
INSERT INTO public.icons (iconid, icon_code, type) VALUES (277, 'far fa-angry', 'FONTAWESOME');
INSERT INTO public.icons (iconid, icon_code, type) VALUES (288, 'fas fa-icons', 'FONTAWESOME');
INSERT INTO public.icons (iconid, icon_code, type) VALUES (305, 'fas fa-concierge-bell', 'FONTAWESOME');
INSERT INTO public.icons (iconid, icon_code, type) VALUES (314, 'far fa-address-book', 'FONTAWESOME');
INSERT INTO public.icons (iconid, icon_code, type) VALUES (324, 'fas fa-apple-alt', 'FONTAWESOME');
INSERT INTO public.icons (iconid, icon_code, type) VALUES (326, 'fas fa-cart-plus', 'FONTAWESOME');
INSERT INTO public.icons (iconid, icon_code, type) VALUES (328, 'fas fa-bus-alt', 'FONTAWESOME');
INSERT INTO public.icons (iconid, icon_code, type) VALUES (331, 'fas fa-train', 'FONTAWESOME');
INSERT INTO public.icons (iconid, icon_code, type) VALUES (334, 'fas fa-tshirt', 'FONTAWESOME');
INSERT INTO public.icons (iconid, icon_code, type) VALUES (347, 'far fa-life-ring', 'FONTAWESOME');
INSERT INTO public.icons (iconid, icon_code, type) VALUES (349, 'fas fa-running', 'FONTAWESOME');
INSERT INTO public.icons (iconid, icon_code, type) VALUES (351, 'far fa-heart', 'FONTAWESOME');
INSERT INTO public.icons (iconid, icon_code, type) VALUES (353, 'fas fa-bread-slice', 'FONTAWESOME');
INSERT INTO public.icons (iconid, icon_code, type) VALUES (360, 'far fa-bell', 'FONTAWESOME');
INSERT INTO public.icons (iconid, icon_code, type) VALUES (371, 'fas fa-shopping-basket', 'FONTAWESOME');
INSERT INTO public.icons (iconid, icon_code, type) VALUES (375, 'fas fa-crow', 'FONTAWESOME');
INSERT INTO public.icons (iconid, icon_code, type) VALUES (377, 'fas fa-carrot', 'FONTAWESOME');
INSERT INTO public.icons (iconid, icon_code, type) VALUES (389, 'fas fa-cat', 'FONTAWESOME');
INSERT INTO public.icons (iconid, icon_code, type) VALUES (404, 'fas fa-female', 'FONTAWESOME');
INSERT INTO public.icons (iconid, icon_code, type) VALUES (406, 'fas fa-highlighter', 'FONTAWESOME');
INSERT INTO public.icons (iconid, icon_code, type) VALUES (412, 'fas fa-gas-pump', 'FONTAWESOME');
INSERT INTO public.icons (iconid, icon_code, type) VALUES (428, 'fas fa-baseball-ball', 'FONTAWESOME');
INSERT INTO public.icons (iconid, icon_code, type) VALUES (431, 'fas fa-swimmer', 'FONTAWESOME');
INSERT INTO public.icons (iconid, icon_code, type) VALUES (450, 'fas fa-tablets', 'FONTAWESOME');
INSERT INTO public.icons (iconid, icon_code, type) VALUES (452, 'fas fa-spider', 'FONTAWESOME');
INSERT INTO public.icons (iconid, icon_code, type) VALUES (475, 'fas fa-dove', 'FONTAWESOME');
INSERT INTO public.icons (iconid, icon_code, type) VALUES (490, 'fas fa-basketball-ball', 'FONTAWESOME');
INSERT INTO public.icons (iconid, icon_code, type) VALUES (495, 'fas fa-football-ball', 'FONTAWESOME');
INSERT INTO public.icons (iconid, icon_code, type) VALUES (497, 'far fa-hospital', 'FONTAWESOME');
INSERT INTO public.icons (iconid, icon_code, type) VALUES (499, 'fas fa-table-tennis', 'FONTAWESOME');
INSERT INTO public.icons (iconid, icon_code, type) VALUES (501, 'fas fa-utensils', 'FONTAWESOME');
INSERT INTO public.icons (iconid, icon_code, type) VALUES (524, 'far fa-grin-hearts', 'FONTAWESOME');
INSERT INTO public.icons (iconid, icon_code, type) VALUES (526, 'fas fa-house-damage', 'FONTAWESOME');
INSERT INTO public.icons (iconid, icon_code, type) VALUES (528, 'far fa-check-square', 'FONTAWESOME');
INSERT INTO public.icons (iconid, icon_code, type) VALUES (535, 'fab fa-reacteurope', 'FONTAWESOME');
INSERT INTO public.icons (iconid, icon_code, type) VALUES (547, 'fas fa-ambulance', 'FONTAWESOME');
INSERT INTO public.icons (iconid, icon_code, type) VALUES (549, 'far fa-grin-tongue-wink', 'FONTAWESOME');
INSERT INTO public.icons (iconid, icon_code, type) VALUES (553, 'fas fa-home', 'FONTAWESOME');
INSERT INTO public.icons (iconid, icon_code, type) VALUES (556, 'far fa-save', 'FONTAWESOME');
INSERT INTO public.icons (iconid, icon_code, type) VALUES (558, 'fas fa-pen-nib', 'FONTAWESOME');
INSERT INTO public.icons (iconid, icon_code, type) VALUES (562, 'fas fa-hospital', 'FONTAWESOME');
INSERT INTO public.icons (iconid, icon_code, type) VALUES (565, 'fas fa-phone', 'FONTAWESOME');
INSERT INTO public.icons (iconid, icon_code, type) VALUES (567, 'fas fa-mobile-alt', 'FONTAWESOME');
INSERT INTO public.icons (iconid, icon_code, type) VALUES (569, 'fas fa-wifi', 'FONTAWESOME');
INSERT INTO public.icons (iconid, icon_code, type) VALUES (575, 'far fa-lightbulb', 'FONTAWESOME');
INSERT INTO public.icons (iconid, icon_code, type) VALUES (616, 'fas fa-shopping-cart', 'FONTAWESOME');
INSERT INTO public.icons (iconid, icon_code, type) VALUES (620, 'fas fa-bacon', 'FONTAWESOME');
INSERT INTO public.icons (iconid, icon_code, type) VALUES (646, 'fas fa-layer-group', 'FONTAWESOME');
INSERT INTO public.icons (iconid, icon_code, type) VALUES (649, 'fas fa-car', 'FONTAWESOME');
INSERT INTO public.icons (iconid, icon_code, type) VALUES (651, 'fas fa-power-off', 'FONTAWESOME');
INSERT INTO public.icons (iconid, icon_code, type) VALUES (666, 'fas fa-bone', 'FONTAWESOME');
INSERT INTO public.icons (iconid, icon_code, type) VALUES (667, 'far fa-window-minimize', 'FONTAWESOME');
INSERT INTO public.icons (iconid, icon_code, type) VALUES (675, 'far fa-building', 'FONTAWESOME');
INSERT INTO public.icons (iconid, icon_code, type) VALUES (679, 'fas fa-teeth-open', 'FONTAWESOME');
INSERT INTO public.icons (iconid, icon_code, type) VALUES (702, 'fas fa-cheese', 'FONTAWESOME');
INSERT INTO public.icons (iconid, icon_code, type) VALUES (715, 'fas fa-arrow-circle-up', 'FONTAWESOME');
INSERT INTO public.icons (iconid, icon_code, type) VALUES (718, 'far fa-arrow-alt-circle-up', 'FONTAWESOME');
INSERT INTO public.icons (iconid, icon_code, type) VALUES (726, 'fas fa-tv', 'FONTAWESOME');
INSERT INTO public.icons (iconid, icon_code, type) VALUES (766, 'far fa-dot-circle', 'FONTAWESOME');
INSERT INTO public.icons (iconid, icon_code, type) VALUES (70, 'fas fa-money-check-alt', 'FONTAWESOME');
INSERT INTO public.icons (iconid, icon_code, type) VALUES (71, 'fas fa-home', 'FONTAWESOME');
INSERT INTO public.icons (iconid, icon_code, type) VALUES (72, 'fas fa-search-dollar', 'FONTAWESOME');
INSERT INTO public.icons (iconid, icon_code, type) VALUES (73, 'fas fa-hand-holding-usd', 'FONTAWESOME');
INSERT INTO public.icons (iconid, icon_code, type) VALUES (767, 'fas fa-glasses', 'FONTAWESOME');
INSERT INTO public.icons (iconid, icon_code, type) VALUES (768, 'fas fa-tools', 'FONTAWESOME');
INSERT INTO public.icons (iconid, icon_code, type) VALUES (769, 'fas fa-prescription-bottle-alt', 'FONTAWESOME');
INSERT INTO public.icons (iconid, icon_code, type) VALUES (770, 'fab fa-apple', 'FONTAWESOME');
INSERT INTO public.icons (iconid, icon_code, type) VALUES (771, 'fas fa-music', 'FONTAWESOME');
INSERT INTO public.icons (iconid, icon_code, type) VALUES (2823, 'far fa-arrow-alt-circle-down', 'FONTAWESOME');
INSERT INTO public.icons (iconid, icon_code, type) VALUES (2933, 'fas fa-pills', 'FONTAWESOME');
INSERT INTO public.icons (iconid, icon_code, type) VALUES (3140, 'fas fa-heartbeat', 'FONTAWESOME');
INSERT INTO public.icons (iconid, icon_code, type) VALUES (3173, 'far fa-address-card', 'FONTAWESOME');
INSERT INTO public.icons (iconid, icon_code, type) VALUES (3676, 'fab fa-internet-explorer', 'FONTAWESOME');
INSERT INTO public.icons (iconid, icon_code, type) VALUES (3737, 'far fa-arrow-alt-circle-right', 'FONTAWESOME');
INSERT INTO public.icons (iconid, icon_code, type) VALUES (3807, 'far fa-calendar-alt', 'FONTAWESOME');
INSERT INTO public.icons (iconid, icon_code, type) VALUES (3808, 'fas fa-yen-sign', 'FONTAWESOME');
INSERT INTO public.icons (iconid, icon_code, type) VALUES (3809, 'fas fa-pound-sign', 'FONTAWESOME');
INSERT INTO public.icons (iconid, icon_code, type) VALUES (5437, 'far fa-arrow-alt-circle-left', 'FONTAWESOME');
INSERT INTO public.icons (iconid, icon_code, type) VALUES (5735, 'far fa-file-alt', 'FONTAWESOME');
INSERT INTO public.icons (iconid, icon_code, type) VALUES (5737, 'far fa-paper-plane', 'FONTAWESOME');
INSERT INTO public.icons (iconid, icon_code, type) VALUES (5740, 'fas fa-business-time', 'FONTAWESOME');
INSERT INTO public.icons (iconid, icon_code, type) VALUES (6117, 'fas fa-hamburger', 'FONTAWESOME');
INSERT INTO public.icons (iconid, icon_code, type) VALUES (6120, 'fas fa-cocktail', 'FONTAWESOME');
INSERT INTO public.icons (iconid, icon_code, type) VALUES (6127, 'fas fa-money-bill-alt', 'FONTAWESOME');
INSERT INTO public.icons (iconid, icon_code, type) VALUES (6133, 'fas fa-piggy-bank', 'FONTAWESOME');
INSERT INTO public.icons (iconid, icon_code, type) VALUES (6153, 'fas fa-coffee', 'FONTAWESOME');
INSERT INTO public.icons (iconid, icon_code, type) VALUES (6166, 'fas fa-balance-scale', 'FONTAWESOME');
INSERT INTO public.icons (iconid, icon_code, type) VALUES (6176, 'fas fa-book-open', 'FONTAWESOME');
INSERT INTO public.icons (iconid, icon_code, type) VALUES (6195, 'fas fa-biking', 'FONTAWESOME');
INSERT INTO public.icons (iconid, icon_code, type) VALUES (6250, 'fas fa-tree', 'FONTAWESOME');
INSERT INTO public.icons (iconid, icon_code, type) VALUES (6278, 'fas fa-film', 'FONTAWESOME');
INSERT INTO public.icons (iconid, icon_code, type) VALUES (6282, 'fas fa-parking', 'FONTAWESOME');



---------------------------------------
------------Ingredients----------------
---------------------------------------
INSERT INTO public.ingredients_translations (id, fr, gb, it, nl) VALUES ('garlic', 'Ail', 'Garlic', '', '');
INSERT INTO public.ingredients (ingredientid, name, unit, quantity, calorific_value, proteic_value, icon)
VALUES (1, 'garlic', 'g', 250, 100, 4.22, 377);
INSERT INTO public.ingredient_month (ingredient, month, price) VALUES (1, 4, 3);


INSERT INTO public.ingredients_translations (id, fr, gb, it, nl) VALUES ('whiteAsparagus', 'Asperges blanches', 'White asparagus', '', '');
INSERT INTO public.ingredients (ingredientid, name, unit, quantity, calorific_value, proteic_value, icon)
VALUES (2, 'whiteAsparagus', 'g', 1000, 18.6, 2.5, 377);
INSERT INTO public.ingredient_month (ingredient, month, price) VALUES (2, 4, 14);
INSERT INTO public.ingredient_month (ingredient, month, price) VALUES (2, 5, 14);
INSERT INTO public.ingredient_month (ingredient, month, price) VALUES (2, 6, 14);

INSERT INTO public.ingredients_translations (id, fr, gb, it, nl) VALUES ('greenAsparagus', 'Asperges vertes', 'Green asparagus', '', '');
INSERT INTO public.ingredients (ingredientid, name, unit, quantity, calorific_value, proteic_value, icon)
VALUES (3, 'greenAsparagus', 'g', 1000, 20, 2.46, 377);
INSERT INTO public.ingredient_month (ingredient, month, price) VALUES (3, 4, 10);
INSERT INTO public.ingredient_month (ingredient, month, price) VALUES (3, 5, 10);
INSERT INTO public.ingredient_month (ingredient, month, price) VALUES (3, 6, 10);

INSERT INTO public.ingredients_translations (id, fr, gb, it, nl) VALUES ('basil', 'Basilic', 'Basil', '', '');
INSERT INTO public.ingredients (ingredientid, name, unit, quantity, calorific_value, proteic_value, icon)
VALUES (4, 'basil', 'tsp', 1, 22, 3.2, 377);
INSERT INTO public.ingredient_month (ingredient, month, price) VALUES (4, 4, 0.3);
INSERT INTO public.ingredient_month (ingredient, month, price) VALUES (4, 5, 0.3);
INSERT INTO public.ingredient_month (ingredient, month, price) VALUES (4, 6, 0.3);
INSERT INTO public.ingredient_month (ingredient, month, price) VALUES (4, 7, 0.3);
INSERT INTO public.ingredient_month (ingredient, month, price) VALUES (4, 8, 0.3);

INSERT INTO public.ingredients_translations (id, fr, gb, it, nl) VALUES ('chickenBreast', 'Blanc de poulet', 'Chicken breast', '', '');
INSERT INTO public.ingredients (ingredientid, name, unit, quantity, calorific_value, proteic_value, icon)
VALUES (5, 'chickenBreast', 'g', 1000, 120, 26.2, 377);
INSERT INTO public.ingredient_month (ingredient, month, price) VALUES (5, 1, 11);
INSERT INTO public.ingredient_month (ingredient, month, price) VALUES (5, 2, 11);
INSERT INTO public.ingredient_month (ingredient, month, price) VALUES (5, 3, 11);
INSERT INTO public.ingredient_month (ingredient, month, price) VALUES (5, 4, 11);
INSERT INTO public.ingredient_month (ingredient, month, price) VALUES (5, 5, 11);
INSERT INTO public.ingredient_month (ingredient, month, price) VALUES (5, 6, 11);
INSERT INTO public.ingredient_month (ingredient, month, price) VALUES (5, 7, 11);
INSERT INTO public.ingredient_month (ingredient, month, price) VALUES (5, 8, 11);
INSERT INTO public.ingredient_month (ingredient, month, price) VALUES (5, 9, 11);
INSERT INTO public.ingredient_month (ingredient, month, price) VALUES (5, 10, 11);
INSERT INTO public.ingredient_month (ingredient, month, price) VALUES (5, 11, 11);
INSERT INTO public.ingredient_month (ingredient, month, price) VALUES (5, 12, 11);

INSERT INTO public.ingredients_translations (id, fr, gb, it, nl) VALUES ('butter', 'Beurre', 'Butter', '', '');
INSERT INTO public.ingredients (ingredientid, name, unit, quantity, calorific_value, proteic_value, icon)
VALUES (6, 'butter', 'g', 250, 717, 0.9, 377);
INSERT INTO public.ingredient_month (ingredient, month, price) VALUES (6, 1, 2);
INSERT INTO public.ingredient_month (ingredient, month, price) VALUES (6, 2, 2);
INSERT INTO public.ingredient_month (ingredient, month, price) VALUES (6, 3, 2);
INSERT INTO public.ingredient_month (ingredient, month, price) VALUES (6, 4, 2);
INSERT INTO public.ingredient_month (ingredient, month, price) VALUES (6, 5, 2);
INSERT INTO public.ingredient_month (ingredient, month, price) VALUES (6, 6, 2);
INSERT INTO public.ingredient_month (ingredient, month, price) VALUES (6, 7, 2);
INSERT INTO public.ingredient_month (ingredient, month, price) VALUES (6, 8, 2);
INSERT INTO public.ingredient_month (ingredient, month, price) VALUES (6, 9, 2);
INSERT INTO public.ingredient_month (ingredient, month, price) VALUES (6, 10, 2);
INSERT INTO public.ingredient_month (ingredient, month, price) VALUES (6, 11, 2);
INSERT INTO public.ingredient_month (ingredient, month, price) VALUES (6, 12, 2);

INSERT INTO public.ingredients_translations (id, fr, gb, it, nl) VALUES ('freshGoat', 'Chèvre frais', 'Fresh goat', '', '');
INSERT INTO public.ingredients (ingredientid, name, unit, quantity, calorific_value, proteic_value, icon)
VALUES (7, 'freshGoat', 'g', 120, 164, 11, 377);
INSERT INTO public.ingredient_month (ingredient, month, price) VALUES (7, 1, 4);
INSERT INTO public.ingredient_month (ingredient, month, price) VALUES (7, 2, 4);
INSERT INTO public.ingredient_month (ingredient, month, price) VALUES (7, 3, 4);
INSERT INTO public.ingredient_month (ingredient, month, price) VALUES (7, 4, 4);
INSERT INTO public.ingredient_month (ingredient, month, price) VALUES (7, 5, 4);
INSERT INTO public.ingredient_month (ingredient, month, price) VALUES (7, 6, 4);
INSERT INTO public.ingredient_month (ingredient, month, price) VALUES (7, 7, 4);
INSERT INTO public.ingredient_month (ingredient, month, price) VALUES (7, 8, 4);
INSERT INTO public.ingredient_month (ingredient, month, price) VALUES (7, 9, 4);
INSERT INTO public.ingredient_month (ingredient, month, price) VALUES (7, 10, 4);
INSERT INTO public.ingredient_month (ingredient, month, price) VALUES (7, 11, 4);
INSERT INTO public.ingredient_month (ingredient, month, price) VALUES (7, 12, 4);

INSERT INTO public.ingredients_translations (id, fr, gb, it, nl) VALUES ('cauliflower', 'Chou-fleur', 'Cauliflower', '', '');
INSERT INTO public.ingredients (ingredientid, name, unit, quantity, calorific_value, proteic_value, icon)
VALUES (8, 'cauliflower', 'g', 600, 25, 1.9, 377);
INSERT INTO public.ingredient_month (ingredient, month, price) VALUES (8, 4, 0.79);
INSERT INTO public.ingredient_month (ingredient, month, price) VALUES (8, 5, 0.79);
INSERT INTO public.ingredient_month (ingredient, month, price) VALUES (8, 6, 0.79);
INSERT INTO public.ingredient_month (ingredient, month, price) VALUES (8, 7, 0.79);

INSERT INTO public.ingredients_translations (id, fr, gb, it, nl) VALUES ('goatCheese', 'Crottin de chèvre', 'Goat Cheese', '', '');
INSERT INTO public.ingredients (ingredientid, name, unit, quantity, calorific_value, proteic_value, icon)
VALUES (9, 'goatCheese', 'piece', 1, 353, 20.5, 377);
INSERT INTO public.ingredient_month (ingredient, month, price) VALUES (9, 1, 2.7);
INSERT INTO public.ingredient_month (ingredient, month, price) VALUES (9, 2, 2.7);
INSERT INTO public.ingredient_month (ingredient, month, price) VALUES (9, 3, 2.7);
INSERT INTO public.ingredient_month (ingredient, month, price) VALUES (9, 4, 2.7);
INSERT INTO public.ingredient_month (ingredient, month, price) VALUES (9, 5, 2.7);
INSERT INTO public.ingredient_month (ingredient, month, price) VALUES (9, 6, 2.7);
INSERT INTO public.ingredient_month (ingredient, month, price) VALUES (9, 7, 2.7);
INSERT INTO public.ingredient_month (ingredient, month, price) VALUES (9, 8, 2.7);
INSERT INTO public.ingredient_month (ingredient, month, price) VALUES (9, 9, 2.7);
INSERT INTO public.ingredient_month (ingredient, month, price) VALUES (9, 10, 2.7);
INSERT INTO public.ingredient_month (ingredient, month, price) VALUES (9, 11, 2.7);
INSERT INTO public.ingredient_month (ingredient, month, price) VALUES (9, 12, 2.7);

INSERT INTO public.ingredients_translations (id, fr, gb, it, nl) VALUES ('spinach', 'Epinard', 'Spinach', '', '');
INSERT INTO public.ingredients (ingredientid, name, unit, quantity, calorific_value, proteic_value, icon)
VALUES (10, 'spinach', 'g', 500, 27, 2.62, 377);
INSERT INTO public.ingredient_month (ingredient, month, price) VALUES (10, 4, 2.7);

INSERT INTO public.ingredients_translations (id, fr, gb, it, nl) VALUES ('farfalle', 'Farfalle', 'Farfalle', '', '');
INSERT INTO public.ingredients (ingredientid, name, unit, quantity, calorific_value, proteic_value, icon)
VALUES (11, 'farfalle', 'g', 500, 365, 5, 377);
INSERT INTO public.ingredient_month (ingredient, month, price) VALUES (11, 1, 2);
INSERT INTO public.ingredient_month (ingredient, month, price) VALUES (11, 2, 2);
INSERT INTO public.ingredient_month (ingredient, month, price) VALUES (11, 3, 2);
INSERT INTO public.ingredient_month (ingredient, month, price) VALUES (11, 4, 2);
INSERT INTO public.ingredient_month (ingredient, month, price) VALUES (11, 5, 2);
INSERT INTO public.ingredient_month (ingredient, month, price) VALUES (11, 6, 2);
INSERT INTO public.ingredient_month (ingredient, month, price) VALUES (11, 7, 2);
INSERT INTO public.ingredient_month (ingredient, month, price) VALUES (11, 8, 2);
INSERT INTO public.ingredient_month (ingredient, month, price) VALUES (11, 9, 2);
INSERT INTO public.ingredient_month (ingredient, month, price) VALUES (11, 10, 2);
INSERT INTO public.ingredient_month (ingredient, month, price) VALUES (11, 11, 2);
INSERT INTO public.ingredient_month (ingredient, month, price) VALUES (11, 12, 2);

INSERT INTO public.ingredients_translations (id, fr, gb, it, nl) VALUES ('flour', 'Farine', 'Flour', '', '');
INSERT INTO public.ingredients (ingredientid, name, unit, quantity, calorific_value, proteic_value, icon)
VALUES (12, 'flour', 'g', 1000, 364, 10, 377);
INSERT INTO public.ingredient_month (ingredient, month, price) VALUES (12, 1, 0.55);
INSERT INTO public.ingredient_month (ingredient, month, price) VALUES (12, 2, 0.55);
INSERT INTO public.ingredient_month (ingredient, month, price) VALUES (12, 3, 0.55);
INSERT INTO public.ingredient_month (ingredient, month, price) VALUES (12, 4, 0.55);
INSERT INTO public.ingredient_month (ingredient, month, price) VALUES (12, 5, 0.55);
INSERT INTO public.ingredient_month (ingredient, month, price) VALUES (12, 6, 0.55);
INSERT INTO public.ingredient_month (ingredient, month, price) VALUES (12, 7, 0.55);
INSERT INTO public.ingredient_month (ingredient, month, price) VALUES (12, 8, 0.55);
INSERT INTO public.ingredient_month (ingredient, month, price) VALUES (12, 9, 0.55);
INSERT INTO public.ingredient_month (ingredient, month, price) VALUES (12, 10, 0.55);
INSERT INTO public.ingredient_month (ingredient, month, price) VALUES (12, 11, 0.55);
INSERT INTO public.ingredient_month (ingredient, month, price) VALUES (12, 12, 0.55);

INSERT INTO public.ingredients_translations (id, fr, gb, it, nl) VALUES ('strawberries', 'Fraises', 'Strawberries', '', '');
INSERT INTO public.ingredients (ingredientid, name, unit, quantity, calorific_value, proteic_value, icon)
VALUES (13, 'strawberries', 'g', 500, 33, 0.7, 377);
INSERT INTO public.ingredient_month (ingredient, month, price) VALUES (13, 4, 6);
INSERT INTO public.ingredient_month (ingredient, month, price) VALUES (13, 5, 6);
INSERT INTO public.ingredient_month (ingredient, month, price) VALUES (13, 6, 6);

INSERT INTO public.ingredients_translations (id, fr, gb, it, nl) VALUES ('gratedCheese', 'Gruyère rapé', 'Grated cheese', '', '');
INSERT INTO public.ingredients (ingredientid, name, unit, quantity, calorific_value, proteic_value, icon)
VALUES (14, 'gratedCheese', 'g', 0, 0, 0, 377);
INSERT INTO public.ingredient_month (ingredient, month, price) VALUES (14, 1, 0);
INSERT INTO public.ingredient_month (ingredient, month, price) VALUES (14, 2, 0);
INSERT INTO public.ingredient_month (ingredient, month, price) VALUES (14, 3, 0);
INSERT INTO public.ingredient_month (ingredient, month, price) VALUES (14, 4, 0);
INSERT INTO public.ingredient_month (ingredient, month, price) VALUES (14, 5, 0);
INSERT INTO public.ingredient_month (ingredient, month, price) VALUES (14, 6, 0);
INSERT INTO public.ingredient_month (ingredient, month, price) VALUES (14, 7, 0);
INSERT INTO public.ingredient_month (ingredient, month, price) VALUES (14, 8, 0);
INSERT INTO public.ingredient_month (ingredient, month, price) VALUES (14, 9, 0);
INSERT INTO public.ingredient_month (ingredient, month, price) VALUES (14, 10, 0);
INSERT INTO public.ingredient_month (ingredient, month, price) VALUES (14, 11, 0);
INSERT INTO public.ingredient_month (ingredient, month, price) VALUES (14, 12, 0);


INSERT INTO public.ingredients_translations (id, fr, gb, it, nl) VALUES ('milk', 'Lait', 'Milk', '', '');
INSERT INTO public.ingredients (ingredientid, name, unit, quantity, calorific_value, proteic_value, icon)
VALUES (15, 'milk', 'ml', 1000, 42, 3.4, 6153);
INSERT INTO public.ingredient_month (ingredient, month, price) VALUES (15, 1, 1.69);
INSERT INTO public.ingredient_month (ingredient, month, price) VALUES (15, 2, 1.69);
INSERT INTO public.ingredient_month (ingredient, month, price) VALUES (15, 3, 1.69);
INSERT INTO public.ingredient_month (ingredient, month, price) VALUES (15, 4, 1.69);
INSERT INTO public.ingredient_month (ingredient, month, price) VALUES (15, 5, 1.69);
INSERT INTO public.ingredient_month (ingredient, month, price) VALUES (15, 6, 1.69);
INSERT INTO public.ingredient_month (ingredient, month, price) VALUES (15, 7, 1.69);
INSERT INTO public.ingredient_month (ingredient, month, price) VALUES (15, 8, 1.69);
INSERT INTO public.ingredient_month (ingredient, month, price) VALUES (15, 9, 1.69);
INSERT INTO public.ingredient_month (ingredient, month, price) VALUES (15, 10, 1.69);
INSERT INTO public.ingredient_month (ingredient, month, price) VALUES (15, 11, 1.69);
INSERT INTO public.ingredient_month (ingredient, month, price) VALUES (15, 12, 1.69);

INSERT INTO public.ingredients_translations (id, fr, gb, it, nl) VALUES ('egg', 'Œuf', 'Egg', '', '');
INSERT INTO public.ingredients (ingredientid, name, unit, quantity, calorific_value, proteic_value, icon)
VALUES (16, 'egg', 'piece', 1, 155, 13, 377);
INSERT INTO public.ingredient_month (ingredient, month, price) VALUES (16, 1, 0.5);
INSERT INTO public.ingredient_month (ingredient, month, price) VALUES (16, 2, 0.5);
INSERT INTO public.ingredient_month (ingredient, month, price) VALUES (16, 3, 0.5);
INSERT INTO public.ingredient_month (ingredient, month, price) VALUES (16, 4, 0.5);
INSERT INTO public.ingredient_month (ingredient, month, price) VALUES (16, 5, 0.5);
INSERT INTO public.ingredient_month (ingredient, month, price) VALUES (16, 6, 0.5);
INSERT INTO public.ingredient_month (ingredient, month, price) VALUES (16, 7, 0.5);
INSERT INTO public.ingredient_month (ingredient, month, price) VALUES (16, 8, 0.5);
INSERT INTO public.ingredient_month (ingredient, month, price) VALUES (16, 9, 0.5);
INSERT INTO public.ingredient_month (ingredient, month, price) VALUES (16, 10, 0.5);
INSERT INTO public.ingredient_month (ingredient, month, price) VALUES (16, 11, 0.5);
INSERT INTO public.ingredient_month (ingredient, month, price) VALUES (16, 12, 0.5);

INSERT INTO public.ingredients_translations (id, fr, gb, it, nl) VALUES ('onion', 'Oignon', 'Onion', '', '');
INSERT INTO public.ingredients (ingredientid, name, unit, quantity, calorific_value, proteic_value, icon)
VALUES (17, 'onion', 'g', 1000, 30, 1.1, 377);
INSERT INTO public.ingredient_month (ingredient, month, price) VALUES (17, 4, 2.7);

INSERT INTO public.ingredients_translations (id, fr, gb, it, nl) VALUES ('grayBread', 'Pain Gris', 'Gray Bread', '', '');
INSERT INTO public.ingredients (ingredientid, name, unit, quantity, calorific_value, proteic_value, icon)
VALUES (18, 'grayBread', 'g', 500, 247, 13, 377);
INSERT INTO public.ingredient_month (ingredient, month, price) VALUES (18, 1, 3.25);
INSERT INTO public.ingredient_month (ingredient, month, price) VALUES (18, 2, 3.25);
INSERT INTO public.ingredient_month (ingredient, month, price) VALUES (18, 3, 3.25);
INSERT INTO public.ingredient_month (ingredient, month, price) VALUES (18, 4, 3.25);
INSERT INTO public.ingredient_month (ingredient, month, price) VALUES (18, 5, 3.25);
INSERT INTO public.ingredient_month (ingredient, month, price) VALUES (18, 6, 3.25);
INSERT INTO public.ingredient_month (ingredient, month, price) VALUES (18, 7, 3.25);
INSERT INTO public.ingredient_month (ingredient, month, price) VALUES (18, 8, 3.25);
INSERT INTO public.ingredient_month (ingredient, month, price) VALUES (18, 9, 3.25);
INSERT INTO public.ingredient_month (ingredient, month, price) VALUES (18, 10, 3.25);
INSERT INTO public.ingredient_month (ingredient, month, price) VALUES (18, 11, 3.25);
INSERT INTO public.ingredient_month (ingredient, month, price) VALUES (18, 12, 3.25);

INSERT INTO public.ingredients_translations (id, fr, gb, it, nl) VALUES ('parsnip', 'Panais', 'Parsnip', '', '');
INSERT INTO public.ingredients (ingredientid, name, unit, quantity, calorific_value, proteic_value, icon)
VALUES (19, 'parsnip', 'g', 1000, 75, 1.2, 377);
INSERT INTO public.ingredient_month (ingredient, month, price) VALUES (19, 4, 4);

INSERT INTO public.ingredients_translations (id, fr, gb, it, nl) VALUES ('parsley', 'Persil', 'Parsley', '', '');
INSERT INTO public.ingredients (ingredientid, name, unit, quantity, calorific_value, proteic_value, icon)
VALUES (20, 'parsley', 'tsp', 1, 36, 3, 377);
INSERT INTO public.ingredient_month (ingredient, month, price) VALUES (20, 4, 0.3);
INSERT INTO public.ingredient_month (ingredient, month, price) VALUES (20, 5, 0.3);
INSERT INTO public.ingredient_month (ingredient, month, price) VALUES (20, 6, 0.3);

INSERT INTO public.ingredients_translations (id, fr, gb, it, nl) VALUES ('oysterMushrooms', 'Pleurotes', 'Oyster mushrooms', '', '');
INSERT INTO public.ingredients (ingredientid, name, unit, quantity, calorific_value, proteic_value, icon)
VALUES (21, 'oysterMushrooms', 'g', 200, 26, 3.1, 377);
INSERT INTO public.ingredient_month (ingredient, month, price) VALUES (21, 1, 2.75);
INSERT INTO public.ingredient_month (ingredient, month, price) VALUES (21, 2, 2.75);
INSERT INTO public.ingredient_month (ingredient, month, price) VALUES (21, 3, 2.75);
INSERT INTO public.ingredient_month (ingredient, month, price) VALUES (21, 4, 2.75);
INSERT INTO public.ingredient_month (ingredient, month, price) VALUES (21, 5, 2.75);
INSERT INTO public.ingredient_month (ingredient, month, price) VALUES (21, 6, 2.75);
INSERT INTO public.ingredient_month (ingredient, month, price) VALUES (21, 7, 2.75);
INSERT INTO public.ingredient_month (ingredient, month, price) VALUES (21, 8, 2.75);
INSERT INTO public.ingredient_month (ingredient, month, price) VALUES (21, 9, 2.75);
INSERT INTO public.ingredient_month (ingredient, month, price) VALUES (21, 10, 2.75);
INSERT INTO public.ingredient_month (ingredient, month, price) VALUES (21, 11, 2.75);
INSERT INTO public.ingredient_month (ingredient, month, price) VALUES (21, 12, 2.75);

INSERT INTO public.ingredients_translations (id, fr, gb, it, nl) VALUES ('leek', 'Poireau', 'Leek', '', '');
INSERT INTO public.ingredients (ingredientid, name, unit, quantity, calorific_value, proteic_value, icon)
VALUES (22, 'leek', 'piece', 1, 27.4, 1.5, 377);
INSERT INTO public.ingredient_month (ingredient, month, price) VALUES (22, 5, 0.33);

INSERT INTO public.ingredients_translations (id, fr, gb, it, nl) VALUES ('radish', 'Radis', 'Radish', '', '');
INSERT INTO public.ingredients (ingredientid, name, unit, quantity, calorific_value, proteic_value, icon)
VALUES (23, 'radish', 'bunch', 1, 16, 0.7, 377);
INSERT INTO public.ingredient_month (ingredient, month, price) VALUES (23, 4, 1.6);

INSERT INTO public.ingredients_translations (id, fr, gb, it, nl) VALUES ('rhubarb', 'Rhubarbe', 'Rhubarb', '', '');
INSERT INTO public.ingredients (ingredientid, name, unit, quantity, calorific_value, proteic_value, icon)
VALUES (24, 'rhubarb', 'g', 1000, 21, 0.78, 377);
INSERT INTO public.ingredient_month (ingredient, month, price) VALUES (24, 4, 0.79);
INSERT INTO public.ingredient_month (ingredient, month, price) VALUES (24, 5, 0.79);
INSERT INTO public.ingredient_month (ingredient, month, price) VALUES (24, 6, 0.79);
INSERT INTO public.ingredient_month (ingredient, month, price) VALUES (24, 7, 0.79);

UPDATE public.ingredients_translations SET 
it = 'Aglio',
nl = 'Knoflook'
WHERE id = 'garlic';

UPDATE public.ingredients_translations SET 
it = 'Asparagi bianchi',
nl = 'Witte asperges'
WHERE id = 'whiteAsparagus';

UPDATE public.ingredients_translations SET 
it = 'Asparagi verdi',
nl = 'Groene asperge'
WHERE id = 'greenAsparagus';

UPDATE public.ingredients_translations SET 
it = 'Basilico',
nl = 'Basilicum'
WHERE id = 'basil';

UPDATE public.ingredients_translations SET 
it = 'Petto di pollo',
nl = 'Kipfilet'
WHERE id = 'chickenBreast';

UPDATE public.ingredients_translations SET 
it = 'Burro',
nl = 'Boter'
WHERE id = 'butter';

UPDATE public.ingredients_translations SET 
it = 'Capra fresca',
nl = 'Verse geit'
WHERE id = 'freshGoat';

UPDATE public.ingredients_translations SET 
it = 'Aglio',
nl = 'Bloemkool'
WHERE id = 'cauliflower';

UPDATE public.ingredients_translations SET 
it = 'Sterco di capra',
nl = 'Geitenkaas'
WHERE id = 'goatCheese';

UPDATE public.ingredients_translations SET 
it = 'Spinaci',
nl = 'Spinazie'
WHERE id = 'spinach';

UPDATE public.ingredients_translations SET 
it = 'Farfalle',
nl = 'Farfalle'
WHERE id = 'farfalle';

UPDATE public.ingredients_translations SET 
it = 'Farina',
nl = ''
WHERE id = 'flour';

UPDATE public.ingredients_translations SET 
it = 'Fragole',
nl = 'Meel'
WHERE id = 'strawberries';

UPDATE public.ingredients_translations SET 
it = 'Groviera grattugiato',
nl = 'Geraspte kaas'
WHERE id = 'gratedCheese';

UPDATE public.ingredients_translations SET 
it = 'Latte',
nl = 'Melk'
WHERE id = 'milk';

UPDATE public.ingredients_translations SET 
it = 'Uovo',
nl = 'Ei'
WHERE id = 'egg';

UPDATE public.ingredients_translations SET 
it = 'Cipolla',
nl = ''
WHERE id = 'onion';

UPDATE public.ingredients_translations SET 
it = 'Pane grigio',
nl = 'Ui'
WHERE id = 'grayBread';

UPDATE public.ingredients_translations SET 
it = 'Pastinaca',
nl = 'Pastinaak'
WHERE id = 'parsnip';

UPDATE public.ingredients_translations SET 
it = 'Prezzemolo',
nl = 'Peterselie'
WHERE id = 'parsley';

UPDATE public.ingredients_translations SET 
it = 'Funghi ostrica',
nl = 'Pleurotes'
WHERE id = 'oysterMushrooms';

UPDATE public.ingredients_translations SET 
it = 'Porro',
nl = 'Prei'
WHERE id = 'leek';

UPDATE public.ingredients_translations SET 
it = 'Ravanello',
nl = 'Radijs'
WHERE id = 'radish';

UPDATE public.ingredients_translations SET 
it = 'Rabarbaro',
nl = 'Rabarber'
WHERE id = 'rhubarb';



---------------------------------------
-------------Recipes------------------
---------------------------------------
INSERT INTO public.recipes_translations (id, fr, gb, it, nl) VALUES ('strawberryParfait', 'Coupe de fraise', 'Strawberry parfait', '', '');
INSERT INTO public.recipes (recipeid, name, servings, icon)
VALUES (1, 'strawberryParfait', 1, 501);
INSERT INTO public.recipe_month (recipe, month) VALUES (1, 1);
INSERT INTO public.recipe_month (recipe, month) VALUES (1, 2);
INSERT INTO public.recipe_month (recipe, month) VALUES (1, 3);
INSERT INTO public.recipe_month (recipe, month) VALUES (1, 4);
INSERT INTO public.recipe_month (recipe, month) VALUES (1, 5);
INSERT INTO public.recipe_month (recipe, month) VALUES (1, 6);
INSERT INTO public.recipe_month (recipe, month) VALUES (1, 7);
INSERT INTO public.recipe_month (recipe, month) VALUES (1, 8);
INSERT INTO public.recipe_month (recipe, month) VALUES (1, 9);
INSERT INTO public.recipe_month (recipe, month) VALUES (1, 10);
INSERT INTO public.recipe_month (recipe, month) VALUES (1, 11);
INSERT INTO public.recipe_month (recipe, month) VALUES (1, 12);
INSERT INTO public.ingredient_recipe (recipe, ingredient, quantity) VALUES (1, 13, 500);

INSERT INTO public.recipes_translations (id, fr, gb, it, nl) VALUES ('creamOfAsparagusSoup', 'Crème d''asperges', 'Cream of asparagus soup', '', '');
INSERT INTO public.recipes (recipeid, name, servings, icon)
VALUES (2, 'creamOfAsparagusSoup', 4, 501);
INSERT INTO public.recipe_month (recipe, month) VALUES (2, 1);
INSERT INTO public.recipe_month (recipe, month) VALUES (2, 2);
INSERT INTO public.recipe_month (recipe, month) VALUES (2, 3);
INSERT INTO public.recipe_month (recipe, month) VALUES (2, 4);
INSERT INTO public.recipe_month (recipe, month) VALUES (2, 5);
INSERT INTO public.recipe_month (recipe, month) VALUES (2, 6);
INSERT INTO public.recipe_month (recipe, month) VALUES (2, 7);
INSERT INTO public.recipe_month (recipe, month) VALUES (2, 8);
INSERT INTO public.recipe_month (recipe, month) VALUES (2, 9);
INSERT INTO public.recipe_month (recipe, month) VALUES (2, 10);
INSERT INTO public.recipe_month (recipe, month) VALUES (2, 11);
INSERT INTO public.recipe_month (recipe, month) VALUES (2, 12);
INSERT INTO public.ingredient_recipe (recipe, ingredient, quantity) VALUES (2, 2, 500);
INSERT INTO public.ingredient_recipe (recipe, ingredient, quantity) VALUES (2, 15, 200);
INSERT INTO public.ingredient_recipe (recipe, ingredient, quantity) VALUES (2, 12, 150);
INSERT INTO public.ingredient_recipe (recipe, ingredient, quantity) VALUES (2, 6, 40);
INSERT INTO public.ingredient_recipe (recipe, ingredient, quantity) VALUES (2, 20, 1);

INSERT INTO public.recipes_translations (id, fr, gb, it, nl)
VALUES ('farfalleOysterMushroomsChicken', 'Farfalles aux pleurotes et poulet', 'Farfalle with oyster mushrooms and chicken', '', '');
INSERT INTO public.recipes (recipeid, name, servings, icon, recipe_url)
VALUES (3, 'farfalleOysterMushroomsChicken', 4, 501, 'https://recettes4saisons.brussels/recettes/farfalles-aux-pleurotes-et-poulet');
INSERT INTO public.recipe_month (recipe, month) VALUES (3, 1);
INSERT INTO public.recipe_month (recipe, month) VALUES (3, 2);
INSERT INTO public.recipe_month (recipe, month) VALUES (3, 3);
INSERT INTO public.recipe_month (recipe, month) VALUES (3, 4);
INSERT INTO public.recipe_month (recipe, month) VALUES (3, 5);
INSERT INTO public.recipe_month (recipe, month) VALUES (3, 6);
INSERT INTO public.recipe_month (recipe, month) VALUES (3, 7);
INSERT INTO public.recipe_month (recipe, month) VALUES (3, 8);
INSERT INTO public.recipe_month (recipe, month) VALUES (3, 9);
INSERT INTO public.recipe_month (recipe, month) VALUES (3, 10);
INSERT INTO public.recipe_month (recipe, month) VALUES (3, 11);
INSERT INTO public.recipe_month (recipe, month) VALUES (3, 12);
INSERT INTO public.ingredient_recipe (recipe, ingredient, quantity) VALUES (3, 11, 500);
INSERT INTO public.ingredient_recipe (recipe, ingredient, quantity) VALUES (3, 5, 200);
INSERT INTO public.ingredient_recipe (recipe, ingredient, quantity) VALUES (3, 21, 250);
INSERT INTO public.ingredient_recipe (recipe, ingredient, quantity) VALUES (3, 20, 1);
INSERT INTO public.ingredient_recipe (recipe, ingredient, quantity) VALUES (3, 4, 1);

INSERT INTO public.recipes_translations (id, fr, gb, it, nl)
VALUES ('springLeek', 'Poireau de printemps', 'Spring leek', '', '');
INSERT INTO public.recipes (recipeid, name, servings, icon)
VALUES (4, 'springLeek', 2, 501);
INSERT INTO public.recipe_month (recipe, month) VALUES (4, 1);
INSERT INTO public.recipe_month (recipe, month) VALUES (4, 2);
INSERT INTO public.recipe_month (recipe, month) VALUES (4, 3);
INSERT INTO public.recipe_month (recipe, month) VALUES (4, 4);
INSERT INTO public.recipe_month (recipe, month) VALUES (4, 5);
INSERT INTO public.recipe_month (recipe, month) VALUES (4, 6);
INSERT INTO public.recipe_month (recipe, month) VALUES (4, 7);
INSERT INTO public.recipe_month (recipe, month) VALUES (4, 8);
INSERT INTO public.recipe_month (recipe, month) VALUES (4, 9);
INSERT INTO public.recipe_month (recipe, month) VALUES (4, 10);
INSERT INTO public.recipe_month (recipe, month) VALUES (4, 11);
INSERT INTO public.recipe_month (recipe, month) VALUES (4, 12);
INSERT INTO public.ingredient_recipe (recipe, ingredient, quantity) VALUES (4, 22, 2);
INSERT INTO public.ingredient_recipe (recipe, ingredient, quantity) VALUES (4, 16, 4);
INSERT INTO public.ingredient_recipe (recipe, ingredient, quantity) VALUES (4, 20, 1);

INSERT INTO public.recipes_translations (id, fr, gb, it, nl)
VALUES ('panFriedFreshSpinach', 'Poêlée d''épinards frais', 'Pan-fried fresh spinach', '', '');
INSERT INTO public.recipes (recipeid, name, servings, icon)
VALUES (5, 'panFriedFreshSpinach', 1, 501);
INSERT INTO public.recipe_month (recipe, month) VALUES (5, 1);
INSERT INTO public.recipe_month (recipe, month) VALUES (5, 2);
INSERT INTO public.recipe_month (recipe, month) VALUES (5, 3);
INSERT INTO public.recipe_month (recipe, month) VALUES (5, 4);
INSERT INTO public.recipe_month (recipe, month) VALUES (5, 5);
INSERT INTO public.recipe_month (recipe, month) VALUES (5, 6);
INSERT INTO public.recipe_month (recipe, month) VALUES (5, 7);
INSERT INTO public.recipe_month (recipe, month) VALUES (5, 8);
INSERT INTO public.recipe_month (recipe, month) VALUES (5, 9);
INSERT INTO public.recipe_month (recipe, month) VALUES (5, 10);
INSERT INTO public.recipe_month (recipe, month) VALUES (5, 11);
INSERT INTO public.recipe_month (recipe, month) VALUES (5, 12);
INSERT INTO public.ingredient_recipe (recipe, ingredient, quantity) VALUES (5, 10, 500);
INSERT INTO public.ingredient_recipe (recipe, ingredient, quantity) VALUES (5, 17, 1);
INSERT INTO public.ingredient_recipe (recipe, ingredient, quantity) VALUES (5, 1, 5);

INSERT INTO public.recipes_translations (id, fr, gb, it, nl)
VALUES ('cauliflowerGratin', 'Gratin de chou-fleur', 'Cauliflower Gratin', 'Cavolfiore gratinato', 'Bloemkoolgratin');
INSERT INTO public.recipes (recipeid, name, servings, icon, preparation)
VALUES (6, 'cauliflowerGratin', 1, 501, '
<br>Cuire les fleurettes du chou-fleur dans une casserole d''eau bouillante jusqu''à ce qu''elles soient tendres.	
<br>Fondre le beurre dans une casserole.
<br>Ajouter la farine et mélanger. 
<br>Verser le lait froid petit à petit, en mélangeant à l’aide d’un fouet. 
<br>Cuire une dizaine de min à feu doux tout en mélangeant et jusqu''à ce que la <br>sauce soit épaisse. 	
<br>Préchauffer le four à 180° C.	
<br>Mettre les fleurettes de chou-fleur dans un plat à gratin. 
<br>Verser la béchamel dessus. 
<br>Parsemer de gruyère et enfourner 30 min.
');
INSERT INTO public.recipe_month (recipe, month) VALUES (6, 4);
INSERT INTO public.recipe_month (recipe, month) VALUES (6, 5);
INSERT INTO public.recipe_month (recipe, month) VALUES (6, 6);
INSERT INTO public.recipe_month (recipe, month) VALUES (6, 7);
INSERT INTO public.ingredient_recipe (recipe, ingredient, quantity) VALUES (6, 8, 600);
INSERT INTO public.ingredient_recipe (recipe, ingredient, quantity) VALUES (6, 6, 25);
INSERT INTO public.ingredient_recipe (recipe, ingredient, quantity) VALUES (6,12, 25); 
INSERT INTO public.ingredient_recipe (recipe, ingredient, quantity) VALUES (6,14,100);
INSERT INTO public.ingredient_recipe (recipe, ingredient, quantity) VALUES (6,15,500);

INSERT INTO public.recipes_translations (id, fr, gb, it, nl)
VALUES ('spreadableGoatCheese', 'Tartinade de fromage de chèvre', 'Spreadable goat cheese', 'Formaggio di capra spalmabile', 'Smeerbare geitenkaas');
INSERT INTO public.recipes (recipeid, name, servings, icon, preparation)
VALUES (7,'spreadableGoatCheese', 1, 501, '
<br>Mixer les crottins, le fromage de chèvre frais, les feuilles de basilic et de cerfeuil.
<br>Tartiner les tranches de pain.	
<br>Couper les radis en rondelles et les déposer sur le fromage.
<br>Couper la ciboulette et la déposer sur le fromage.
');
INSERT INTO public.recipe_month (recipe, month) VALUES (7, 4);
INSERT INTO public.recipe_month (recipe, month) VALUES (7, 5);
INSERT INTO public.recipe_month (recipe, month) VALUES (7, 6);
INSERT INTO public.recipe_month (recipe, month) VALUES (7, 7);
INSERT INTO public.ingredient_recipe (recipe, ingredient, quantity) VALUES (7,18,1);
INSERT INTO public.ingredient_recipe (recipe, ingredient, quantity) VALUES (7,9,2);
INSERT INTO public.ingredient_recipe (recipe, ingredient, quantity) VALUES (7,7,200); 
INSERT INTO public.ingredient_recipe (recipe, ingredient, quantity) VALUES (7,4,1);
INSERT INTO public.ingredient_recipe (recipe, ingredient, quantity) VALUES (7,23,1);

UPDATE public.recipes SET preparation = '
<br>Déposer les fraises dans les coupes
' WHERE recipeid = 1;

UPDATE public.recipes SET preparation = '
<br>Cuire les asperges en les recouvrant d''eau.
<br>Pendant la cuisson, préparer une béchamel en faisant fondre le beurre.
<br>Ajouter la farine en remuant et incorporer le lait progressivement, puis une partie du jus de cuisson des asperges.	
<br>Quand les asperges sont cuites, retirer les pointes et les réserver.	
<br>Mixer le reste des asperges dans le jus de cuisson et incorporer la béchamel.
<br>Servir avec les pointes et parsemer de persil.
' WHERE recipeid = 2;

UPDATE public.recipes SET preparation = '
<br>Hacher finement l''ail et découper le poulet et les pleurotes en petits dés.
<br>Dans un wok, faire blondir l''ail dans l''huile d''olive. 
<br>Y ajouter le poulet et les pleurotes et faire rôtir en mélangeant.
<br>Quand le poulet est doré, ajouter le persil et le basilic.
<br>Laisser cuire à feu doux pendant 15 minutes tout en cuisant les farfalle dans une casserole d''eau bouillante.
<br>Quand les pâtes sont al dente, les égoutter et les incorporer à la sauce dans le wok. 
<br>Prolonger la cuisson de 2 minutes pour une bonne imprégnation.
' WHERE recipeid = 3;

UPDATE public.recipes SET preparation = '
<br>Cuire les œufs.
<br>Cuire à la vapeur ou braiser les poireaux coupés finement.	
<br>Écraser les œufs à la fourchette et les mélanger aux poireaux.
' WHERE recipeid = 4;

UPDATE public.recipes SET preparation = '
<br>Éliminer les queues des feuilles d''épinards. 
<br>Peler et hacher l''oignon et la gousse d''ail.
<br>Chauffer le beurre dans une grande sauteuse.
<br>Lorsqu''il est mousseux, placer-y l''oignon et l''ail hachés.
<br>Laisser-les dorer pendant 5 à 10 min, l''oignon doit être bien blond.	
<br>Ajouter les feuilles d''épinards d''un coup et couvrer-les 2 min. Le volume des épinards va diminuer.	
<br>Retirer le couvercle et mélanger soigneusement, jusqu''à ce que tous les épinards soient tombés. 
<br>Poivrer
' WHERE recipeid = 5;

UPDATE public.ingredients_translations SET 
it = 'Coppa di fragole',
nl = 'Aardbeien kopje'
WHERE id = 'strawberryParfait';

UPDATE public.ingredients_translations SET 
it = 'Crema di asparagi',
nl = 'Crème van asperges'
WHERE id = 'creamOfAsparagusSoup';

UPDATE public.ingredients_translations SET 
it = 'Farfalle con funghi ostrica e pollo',
nl = 'Farfalle met oesterzwammen en kip'
WHERE id = 'farfalleOysterMushroomsChicken';

UPDATE public.ingredients_translations SET 
it = 'Porro primaverile',
nl = 'Lente prei'
WHERE id = 'springLeek';

UPDATE public.ingredients_translations SET 
it = 'Spinaci freschi saltati in padella',
nl = 'Gebakken verse spinazie'
WHERE id = 'panFriedFreshSpinach';

UPDATE public.ingredients_translations SET 
it = 'Cavolfiore gratinato',
nl = 'Bloemkoolgratin'
WHERE id = 'cauliflowerGratin';

UPDATE public.ingredients_translations SET 
it = 'Formaggio di capra spalmabile',
nl = 'Smeerbare geitenkaas'
WHERE id = 'spreadableGoatCheese';



---------------------------------------
--------------Status-----------------
---------------------------------------
INSERT INTO public.status (statusid, name) VALUES (1, 'student');
INSERT INTO public.status (statusid, name) VALUES (2, 'worker');
INSERT INTO public.status (statusid, name) VALUES (3, 'pensioner');
INSERT INTO public.status (statusid, name) VALUES (4, 'unemployed');



---------------------------------------
--------------Sub-Status---------------
---------------------------------------
INSERT INTO public.sub_status (sub_statusid, fr, gb, it, nl) VALUES (1, 'Je possède un appartement', 'I own an apartment', 'Sono proprietario di un appartamento', 'Ik heb een appartement');
INSERT INTO public.sub_status (sub_statusid, fr, gb, it, nl) VALUES (2, 'Je possède une maison', 'I own a house', 'Sono proprietario di una casa', 'Ik heb een huis');
INSERT INTO public.sub_status (sub_statusid, fr, gb, it, nl) VALUES (3, 'Je possède une voiture', 'I own a car', 'Possiedo un''auto', 'Ik heb een auto');
INSERT INTO public.sub_status (sub_statusid, fr, gb, it, nl) VALUES (4, 'Je possède une moto', 'I own a motorcycle', 'Possiedo una moto', 'Ik heb een motor');
INSERT INTO public.sub_status (sub_statusid, fr, gb, it, nl) VALUES (5, 'Je loue un appartement', 'I rent an apartment', 'Affitto un appartamento', 'Ik huur een appartement');
INSERT INTO public.sub_status (sub_statusid, fr, gb, it, nl) VALUES (6, 'Je loue une maison', 'I rent a house', 'Affitto una casa', 'Ik huur een huis');
INSERT INTO public.sub_status (sub_statusid, fr, gb, it, nl) VALUES (7, 'Je loue une voiture', 'I rent a car', 'Noleggio un''auto', 'Ik huur een auto');
INSERT INTO public.sub_status (sub_statusid, fr, gb, it, nl) VALUES (8, 'Je loue une moto', 'I rent a motorcycle', 'Noleggio una moto', 'Ik huur een motor');
INSERT INTO public.sub_status (sub_statusid, fr, gb, it, nl) VALUES (9, 'J''ai au moins un enfant ', 'I have at least one child ', 'Ho almeno un bambino', 'Ik heb minstens één kind');



---------------------------------------
----------Template Categories----------
---------------------------------------
INSERT INTO public.template_categories (template_categoryid, budget, type, icon, name, parent_category) VALUES (10, 300, 'OUTPUT', 371, 'shopping', NULL);
INSERT INTO public.template_categories (template_categoryid, budget, type, icon, name, parent_category) VALUES (11, 300, 'OUTPUT', 288, 'other', NULL);
INSERT INTO public.template_categories (template_categoryid, budget, type, icon, name, parent_category) VALUES (12, 300, 'OUTPUT', 450, 'electronics', NULL);
INSERT INTO public.template_categories (template_categoryid, budget, type, icon, name, parent_category) VALUES (1, 300, 'OUTPUT', 726, 'entertainment', NULL);
INSERT INTO public.template_categories (template_categoryid, budget, type, icon, name, parent_category) VALUES (13, 1000, 'OUTPUT', 288, 'unknown entries', NULL);
INSERT INTO public.template_categories (template_categoryid, budget, type, icon, name, parent_category) VALUES (14, 1000, 'INPUT', 288, 'unknown income', NULL);
INSERT INTO public.template_categories (template_categoryid, budget, type, icon, name, parent_category) VALUES (15, 300, 'OUTPUT', 371, 'groceries', NULL);
INSERT INTO public.template_categories (template_categoryid, budget, type, icon, name, parent_category) VALUES (16, 2000, 'OUTPUT', 71, 'Mortgage', NULL);
INSERT INTO public.template_categories (template_categoryid, budget, type, icon, name, parent_category) VALUES (17, 300, 'OUTPUT', 72, 'royalties', NULL);
INSERT INTO public.template_categories (template_categoryid, budget, type, icon, name, parent_category) VALUES (18, 330, 'OUTPUT', 73, 'insurance', NULL);
INSERT INTO public.template_categories (template_categoryid, budget, type, icon, name, parent_category) VALUES (19, 300, 'OUTPUT', 70, 'mutual', NULL);
INSERT INTO public.template_categories (template_categoryid, budget, type, icon, name, parent_category) VALUES (20, 3000, 'INPUT', 70, 'salary', NULL);
INSERT INTO public.template_categories (template_categoryid, budget, type, icon, name, parent_category) VALUES (21, 400, 'OUTPUT', 767, 'optician', NULL);
INSERT INTO public.template_categories (template_categoryid, budget, type, icon, name, parent_category) VALUES (22, 60, 'OUTPUT', 768, 'diy', NULL);
INSERT INTO public.template_categories (template_categoryid, budget, type, icon, name, parent_category) VALUES (23, 50, 'OUTPUT', 769, 'pharmacy', NULL);
INSERT INTO public.template_categories (template_categoryid, budget, type, icon, name, parent_category) VALUES (24, 20, 'OUTPUT', 770, 'apple', NULL);
INSERT INTO public.template_categories (template_categoryid, budget, type, icon, name, parent_category) VALUES (25, 15, 'OUTPUT', 771, 'music', NULL);



---------------------------------------
-----------Account Types---------------
---------------------------------------
INSERT INTO public.account_types (account_type_id, name, icon) VALUES (1, 'credit', 3);
INSERT INTO public.account_types (account_type_id, name, icon) VALUES (2, 'visa', 4);
INSERT INTO public.account_types (account_type_id, name, icon) VALUES (3, 'paypal', 5);
INSERT INTO public.account_types (account_type_id, name, icon) VALUES (4, 'mastercard', 6);
INSERT INTO public.account_types (account_type_id, name, icon) VALUES (5, 'applePay', 7);
INSERT INTO public.account_types (account_type_id, name, icon) VALUES (6, 'amex', 8);
INSERT INTO public.account_types (account_type_id, name, icon) VALUES (7, 'amazonPay', 9);
INSERT INTO public.account_types (account_type_id, name, icon) VALUES (8, 'cash', 10);



---------------------------------------
----------Categories Status------------
---------------------------------------
INSERT INTO public.category_status (category_status_id, status, template_category) VALUES (9, 1, 12);
INSERT INTO public.category_status (category_status_id, status, template_category) VALUES (10, 2, 12);
INSERT INTO public.category_status (category_status_id, status, template_category) VALUES (11, 3, 12);
INSERT INTO public.category_status (category_status_id, status, template_category) VALUES (12, 4, 12);
INSERT INTO public.category_status (category_status_id, status, template_category) VALUES (1, 1, 10);
INSERT INTO public.category_status (category_status_id, status, template_category) VALUES (2, 2, 10);
INSERT INTO public.category_status (category_status_id, status, template_category) VALUES (3, 3, 10);
INSERT INTO public.category_status (category_status_id, status, template_category) VALUES (4, 4, 10);
INSERT INTO public.category_status (category_status_id, status, template_category) VALUES (20, 1, 1);
INSERT INTO public.category_status (category_status_id, status, template_category) VALUES (21, 2, 1);
INSERT INTO public.category_status (category_status_id, status, template_category) VALUES (22, 3, 1);
INSERT INTO public.category_status (category_status_id, status, template_category) VALUES (23, 4, 1);
INSERT INTO public.category_status (category_status_id, status, template_category) VALUES (24, 1, 13);
INSERT INTO public.category_status (category_status_id, status, template_category) VALUES (25, 2, 13);
INSERT INTO public.category_status (category_status_id, status, template_category) VALUES (26, 3, 13);
INSERT INTO public.category_status (category_status_id, status, template_category) VALUES (27, 4, 13);
INSERT INTO public.category_status (category_status_id, status, template_category) VALUES (28, 1, 14);
INSERT INTO public.category_status (category_status_id, status, template_category) VALUES (29, 2, 14);
INSERT INTO public.category_status (category_status_id, status, template_category) VALUES (30, 3, 14);
INSERT INTO public.category_status (category_status_id, status, template_category) VALUES (31, 4, 14);
INSERT INTO public.category_status (category_status_id, status, template_category) VALUES (32, 1, 15);
INSERT INTO public.category_status (category_status_id, status, template_category) VALUES (33, 2, 15);
INSERT INTO public.category_status (category_status_id, status, template_category) VALUES (34, 3, 15);
INSERT INTO public.category_status (category_status_id, status, template_category) VALUES (35, 4, 15);
INSERT INTO public.category_status (category_status_id, status, template_category) VALUES (36, 1, 16);
INSERT INTO public.category_status (category_status_id, status, template_category) VALUES (37, 2, 16);
INSERT INTO public.category_status (category_status_id, status, template_category) VALUES (38, 3, 16);
INSERT INTO public.category_status (category_status_id, status, template_category) VALUES (39, 4, 16);
INSERT INTO public.category_status (category_status_id, status, template_category) VALUES (40, 1, 17);
INSERT INTO public.category_status (category_status_id, status, template_category) VALUES (41, 2, 17);
INSERT INTO public.category_status (category_status_id, status, template_category) VALUES (42, 3, 17);
INSERT INTO public.category_status (category_status_id, status, template_category) VALUES (43, 4, 17);
INSERT INTO public.category_status (category_status_id, status, template_category) VALUES (44, 1, 18);
INSERT INTO public.category_status (category_status_id, status, template_category) VALUES (45, 2, 18);
INSERT INTO public.category_status (category_status_id, status, template_category) VALUES (46, 3, 18);
INSERT INTO public.category_status (category_status_id, status, template_category) VALUES (47, 4, 18);
INSERT INTO public.category_status (category_status_id, status, template_category) VALUES (48, 1, 19);
INSERT INTO public.category_status (category_status_id, status, template_category) VALUES (49, 2, 19);
INSERT INTO public.category_status (category_status_id, status, template_category) VALUES (50, 3, 19);
INSERT INTO public.category_status (category_status_id, status, template_category) VALUES (51, 4, 19);
INSERT INTO public.category_status (category_status_id, status, template_category) VALUES (52, 1, 20);
INSERT INTO public.category_status (category_status_id, status, template_category) VALUES (53, 2, 20);
INSERT INTO public.category_status (category_status_id, status, template_category) VALUES (54, 3, 20);
INSERT INTO public.category_status (category_status_id, status, template_category) VALUES (55, 4, 20);
INSERT INTO public.category_status (category_status_id, status, template_category) VALUES (56, 1, 21);
INSERT INTO public.category_status (category_status_id, status, template_category) VALUES (57, 2, 21);
INSERT INTO public.category_status (category_status_id, status, template_category) VALUES (58, 3, 21);
INSERT INTO public.category_status (category_status_id, status, template_category) VALUES (59, 4, 21);
INSERT INTO public.category_status (category_status_id, status, template_category) VALUES (60, 1, 22);
INSERT INTO public.category_status (category_status_id, status, template_category) VALUES (61, 2, 22);
INSERT INTO public.category_status (category_status_id, status, template_category) VALUES (62, 3, 22);
INSERT INTO public.category_status (category_status_id, status, template_category) VALUES (63, 4, 22);
INSERT INTO public.category_status (category_status_id, status, template_category) VALUES (64, 1, 23);
INSERT INTO public.category_status (category_status_id, status, template_category) VALUES (65, 2, 23);
INSERT INTO public.category_status (category_status_id, status, template_category) VALUES (66, 3, 23);
INSERT INTO public.category_status (category_status_id, status, template_category) VALUES (67, 4, 23);
INSERT INTO public.category_status (category_status_id, status, template_category) VALUES (68, 1, 24);
INSERT INTO public.category_status (category_status_id, status, template_category) VALUES (69, 2, 24);
INSERT INTO public.category_status (category_status_id, status, template_category) VALUES (70, 3, 24);
INSERT INTO public.category_status (category_status_id, status, template_category) VALUES (71, 4, 24);
INSERT INTO public.category_status (category_status_id, status, template_category) VALUES (72, 1, 25);
INSERT INTO public.category_status (category_status_id, status, template_category) VALUES (73, 2, 25);
INSERT INTO public.category_status (category_status_id, status, template_category) VALUES (74, 3, 25);
INSERT INTO public.category_status (category_status_id, status, template_category) VALUES (75, 4, 25);



---------------------------------------
--------------Currencies---------------
---------------------------------------
INSERT INTO public.currencies (currencyid, type, icon) VALUES (2, 'DOLLARS', 1);
INSERT INTO public.currencies (currencyid, type, icon) VALUES (1, 'EURO', 2);
INSERT INTO public.currencies (currencyid, type, icon) VALUES (3, 'YEN', 3808);
INSERT INTO public.currencies (currencyid, type, icon) VALUES (4, 'POUND SERLING ', 3809);



---------------------------------------
----------Dafault Accounts------------
---------------------------------------
INSERT INTO public.default_accounts (default_account_id, name, account_type_id) VALUES (1, 'BNP Paribas Fortis', 1);
INSERT INTO public.default_accounts (default_account_id, name, account_type_id) VALUES (3, 'Visa', 2);
INSERT INTO public.default_accounts (default_account_id, name, account_type_id) VALUES (2, 'Mastercard', 4);



---------------------------------------
---------------Images---------------
---------------------------------------
INSERT INTO public.images (imageid, name) VALUES (1, 'adoption_204898512.jpg');
INSERT INTO public.images (imageid, name) VALUES (2, 'boat_259030980.jpg');
INSERT INTO public.images (imageid, name) VALUES (3, 'car_123522471.jpg');
INSERT INTO public.images (imageid, name) VALUES (4, 'extrem_218143696.jpg');
INSERT INTO public.images (imageid, name) VALUES (5, 'gastronomy_233304884.jpg');
INSERT INTO public.images (imageid, name) VALUES (6, 'generic_135950321.jpg');
INSERT INTO public.images (imageid, name) VALUES (7, 'invest_234826932.jpg');
INSERT INTO public.images (imageid, name) VALUES (8, 'music_168678030.jpg');
INSERT INTO public.images (imageid, name) VALUES (9, 'spa_180837298.jpg');
INSERT INTO public.images (imageid, name) VALUES (10, 'travel_200801160.jpg');
INSERT INTO public.images (imageid, name) VALUES (11, 'wedding_96768151.jpg');
INSERT INTO public.images (imageid, name) VALUES (12, 'construction_81895775.jpg');



---------------------------------------
-------------Languages-----------------
---------------------------------------
INSERT INTO public.languages (languageid, active, country_code, name) VALUES (1, true, 'FR', 'FRANCAIS');
INSERT INTO public.languages (languageid, active, country_code, name) VALUES (2, true, 'GB', 'ENGLISH');
INSERT INTO public.languages (languageid, active, country_code, name) VALUES (3, true, 'IT', 'ITALIANO');
INSERT INTO public.languages (languageid, active, country_code, name) VALUES (4, true, 'NL', 'NEDERLANDS');



---------------------------------------
----------Meta Categories------------
---------------------------------------
INSERT INTO public.meta_categories (meta_categoryid, details, template_category) VALUES (1, 'MMS ONLINE', 12);
INSERT INTO public.meta_categories (meta_categoryid, details, template_category) VALUES (2, 'SMART2PAY', 1);
INSERT INTO public.meta_categories (meta_categoryid, details, template_category) VALUES (3, 'ACTION', 10);
INSERT INTO public.meta_categories (meta_categoryid, details, template_category) VALUES (4, 'ALIPAYLIMITED', 10);
INSERT INTO public.meta_categories (meta_categoryid, details, template_category) VALUES (5, 'VANDEN BORRE', 12);
INSERT INTO public.meta_categories (meta_categoryid, details, template_category) VALUES (6, 'RG CREATIONS', 15);
INSERT INTO public.meta_categories (meta_categoryid, details, template_category) VALUES (7, 'LI PUMA DAVIDE', 15);
INSERT INTO public.meta_categories (meta_categoryid, details, template_category) VALUES (8, 'COLRUYT', 15);
INSERT INTO public.meta_categories (meta_categoryid, details, template_category) VALUES (9, 'ECKER LOUISE', 15);
INSERT INTO public.meta_categories (meta_categoryid, details, template_category) VALUES (10, 'PROXY', 15);
INSERT INTO public.meta_categories (meta_categoryid, details, template_category) VALUES (11, 'ELANTIS', 16);
INSERT INTO public.meta_categories (meta_categoryid, details, template_category) VALUES (12, 'REDEVANCE', 17);
INSERT INTO public.meta_categories (meta_categoryid, details, template_category) VALUES (13, 'AXA', 18);
INSERT INTO public.meta_categories (meta_categoryid, details, template_category) VALUES (14, 'PARTENAT', 19);
INSERT INTO public.meta_categories (meta_categoryid, details, template_category) VALUES (15, 'SALAIRE', 20);
INSERT INTO public.meta_categories (meta_categoryid, details, template_category) VALUES (16, 'HYPER MONTSTJEAN', 15);
INSERT INTO public.meta_categories (meta_categoryid, details, template_category) VALUES (17, 'SMEETS-VAN HOPPLYNUS', 21);
INSERT INTO public.meta_categories (meta_categoryid, details, template_category) VALUES (18, 'LE COMPTOIR LOCAL', 15);
INSERT INTO public.meta_categories (meta_categoryid, details, template_category) VALUES (19, 'CRF MKT', 15);
INSERT INTO public.meta_categories (meta_categoryid, details, template_category) VALUES (20, 'DELITRAITEUR', 15);
INSERT INTO public.meta_categories (meta_categoryid, details, template_category) VALUES (21, 'DELHAIZE', 15);
INSERT INTO public.meta_categories (meta_categoryid, details, template_category) VALUES (22, 'BRICO', 22);
INSERT INTO public.meta_categories (meta_categoryid, details, template_category) VALUES (23, 'PHARMACIE', 23);
INSERT INTO public.meta_categories (meta_categoryid, details, template_category) VALUES (24, 'APPLE', 24);
INSERT INTO public.meta_categories (meta_categoryid, details, template_category) VALUES (25, 'Spotify', 25);
INSERT INTO public.meta_categories (meta_categoryid, details, template_category) VALUES (26, 'HUBO', 22);