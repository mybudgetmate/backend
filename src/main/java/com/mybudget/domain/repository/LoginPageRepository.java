package com.mybudget.domain.repository;

import com.mybudget.domain.models.translation.LoginPageTranslation;

import org.springframework.data.repository.CrudRepository;


public interface LoginPageRepository extends CrudRepository<LoginPageTranslation, Integer> {

}
