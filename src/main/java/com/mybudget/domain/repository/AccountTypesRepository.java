package com.mybudget.domain.repository;

import com.mybudget.domain.models.AccountType;
import com.mybudget.domain.models.Category;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

/**
 * @author Eva Tumia
 */
public interface AccountTypesRepository extends CrudRepository<AccountType, Integer> {

}
