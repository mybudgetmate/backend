package com.mybudget.domain.repository;

import com.mybudget.domain.models.translation.RecipesTranslations;
import org.springframework.data.repository.CrudRepository;

public interface RecipesTranslationsRepository extends CrudRepository<RecipesTranslations, String> {
}
