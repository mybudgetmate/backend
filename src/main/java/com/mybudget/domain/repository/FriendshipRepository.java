package com.mybudget.domain.repository;

import com.mybudget.domain.models.Friendship;
import com.mybudget.domain.models.User;
import com.mybudget.domain.models.indentities.FriendshipPK;
import org.springframework.data.repository.CrudRepository;

import java.util.List;
import java.util.Optional;

public interface FriendshipRepository extends CrudRepository<Friendship, Integer> {


    List<Friendship> findByFriendshipPK_sender(User user);

    List<Friendship> findByFriendshipPK_receiver(User user);

    Optional<Friendship> findByFriendshipPK(FriendshipPK friendshipPK);

    void deleteByFriendshipPK_sender(User user);

    void deleteByFriendshipPK_receiver(User user);
}
