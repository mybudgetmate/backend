package com.mybudget.domain.repository;

import com.mybudget.domain.models.DefaultAccount;
import org.springframework.data.repository.CrudRepository;

public interface DefaultAccountRepository extends CrudRepository<DefaultAccount, Integer> {
    DefaultAccount findByName(String name);
}
