package com.mybudget.domain.repository;

import com.mybudget.domain.models.RecurringTransaction;
import org.springframework.data.repository.CrudRepository;

public interface RecurringTransactionRepository extends CrudRepository<RecurringTransaction, Integer> {
}
