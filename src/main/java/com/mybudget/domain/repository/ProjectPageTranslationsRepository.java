package com.mybudget.domain.repository;

import com.mybudget.domain.models.translation.ProjectPageTranslation;
import org.springframework.data.repository.CrudRepository;

public interface ProjectPageTranslationsRepository extends CrudRepository<ProjectPageTranslation, Integer> {
}
