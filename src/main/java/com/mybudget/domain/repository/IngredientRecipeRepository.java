package com.mybudget.domain.repository;

import com.mybudget.domain.models.Ingredient;
import com.mybudget.domain.models.IngredientRecipe;
import com.mybudget.domain.models.Recipe;
import org.springframework.data.repository.CrudRepository;

public interface IngredientRecipeRepository extends CrudRepository<IngredientRecipe, Long> {
    Iterable<IngredientRecipe> findAllByRecipe(Recipe recipe);
}
