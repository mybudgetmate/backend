package com.mybudget.domain.repository;

import com.mybudget.domain.models.ShoppingList;
import com.mybudget.domain.models.User;
import org.springframework.data.repository.CrudRepository;

import java.util.List;


public interface ShoppingListRepository extends CrudRepository<ShoppingList, Long>{
        List<ShoppingList> findAllByOwner(User owner);
        }

