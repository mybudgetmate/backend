package com.mybudget.domain.repository;

import com.mybudget.domain.models.translation.ShoppingListsModuleTranslation;
import org.springframework.data.repository.CrudRepository;

public interface ShoppingListsModuleTranslationRepository extends CrudRepository<ShoppingListsModuleTranslation, String> {
}
