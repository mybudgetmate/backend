package com.mybudget.domain.repository;

import com.mybudget.domain.models.translation.IngredientsTranslations;
import org.springframework.data.repository.CrudRepository;

public interface IngredientsTranslationsRepository extends CrudRepository<IngredientsTranslations, String> {

}
