package com.mybudget.domain.repository;

import com.mybudget.domain.models.Image;
import org.springframework.data.repository.CrudRepository;

import java.util.Optional;

/**
 * Soumaya Izmar
 */
public interface ImageRepository extends CrudRepository<Image, Integer> {
    Optional<Image> findByName(String name);
}
