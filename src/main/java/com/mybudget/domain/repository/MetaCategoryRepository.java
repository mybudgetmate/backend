package com.mybudget.domain.repository;

import com.mybudget.domain.models.MetaCategory;
import org.springframework.data.repository.CrudRepository;

public interface MetaCategoryRepository extends CrudRepository<MetaCategory, Integer> {
}
