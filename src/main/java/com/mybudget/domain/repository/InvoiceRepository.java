package com.mybudget.domain.repository;

import com.mybudget.domain.models.Invoice;
import com.mybudget.domain.models.User;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

/**
 * @author Eva Tumia
 */
public interface InvoiceRepository extends CrudRepository<Invoice, Integer> {
    List<Invoice> findByOwner(User currentUser);

    void deleteByOwner(User user);
}
