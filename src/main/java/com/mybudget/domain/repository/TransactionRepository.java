package com.mybudget.domain.repository;

import com.mybudget.domain.models.Transaction;
import com.mybudget.domain.models.User;
import org.springframework.data.repository.CrudRepository;

import java.time.LocalDateTime;
import java.util.List;


/**
 * @author Soumaya Izmar
 */
public interface TransactionRepository extends CrudRepository<Transaction, Integer> {

    Iterable<Transaction> findByAccount_owner(User user);

    Iterable<Transaction> findByAccount_ownerOrderByValueDateDesc(User user);

    List<Transaction> findByValueDateAndAccount_owner(LocalDateTime atStartOfDay, User user);

    List<Transaction> findDistinctByRecurringTransactionNotNull();


    List<Transaction> findDistinctByRecurringTransactionNotNullAndAccount_owner(User currentUser);
}
