package com.mybudget.domain.repository;

import com.mybudget.domain.models.translation.BudgetTranslation;
import org.springframework.data.repository.CrudRepository;

public interface BudgetTranslationsRepository extends CrudRepository<BudgetTranslation, Integer> {
}
