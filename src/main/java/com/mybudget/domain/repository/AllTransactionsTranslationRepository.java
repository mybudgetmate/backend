package com.mybudget.domain.repository;

import com.mybudget.domain.models.translation.AllTransactionsTranslation;
import org.springframework.data.repository.CrudRepository;

/**
 * @author Eva Tumia
 */
public interface AllTransactionsTranslationRepository extends CrudRepository<AllTransactionsTranslation, Integer> {
}
