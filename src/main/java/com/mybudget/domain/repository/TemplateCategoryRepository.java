package com.mybudget.domain.repository;

import com.mybudget.domain.models.TemplateCategory;
import com.mybudget.domain.models.translation.TemplateCategoryNameTranslation;
import org.springframework.data.repository.CrudRepository;

import java.util.Optional;

/**
 * @author Eva Tumia
 */
public interface TemplateCategoryRepository extends CrudRepository<TemplateCategory, Integer> {


    Optional<TemplateCategory> findByName(TemplateCategoryNameTranslation name);
}
