package com.mybudget.domain.repository;

import com.mybudget.domain.models.translation.StatusTranslation;
import org.springframework.data.repository.CrudRepository;

public interface StatusTranslationRepository extends CrudRepository<StatusTranslation, String> {
}
