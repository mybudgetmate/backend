package com.mybudget.domain.repository;

import com.mybudget.domain.models.Account;
import com.mybudget.domain.models.User;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

/**
 * @author Eva Tumia
 */
public interface AccountRepository extends CrudRepository<Account, Integer> {
    List<Account> findAllByOwner(User user);

    void deleteByOwner(User user);
}
