package com.mybudget.domain.repository;

import com.mybudget.domain.models.translation.AddTransactionPageTranslation;
import org.springframework.data.repository.CrudRepository;

/**
 * @author Eva Tumia
 */
public interface AddTransactionPageRepository extends CrudRepository<AddTransactionPageTranslation, Integer> {
}
