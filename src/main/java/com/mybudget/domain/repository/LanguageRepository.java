package com.mybudget.domain.repository;


import com.mybudget.domain.models.Language;
import org.springframework.data.repository.CrudRepository;

import java.util.Optional;

public interface LanguageRepository extends CrudRepository<Language, Integer> {

    Optional<Language> findByCountryCode(String countryCode);
}
