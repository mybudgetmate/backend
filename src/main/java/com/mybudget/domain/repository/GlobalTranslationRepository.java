package com.mybudget.domain.repository;

import com.mybudget.domain.models.translation.GlobalTranslation;
import org.springframework.data.repository.CrudRepository;

public interface GlobalTranslationRepository extends CrudRepository<GlobalTranslation, Integer> {
}
