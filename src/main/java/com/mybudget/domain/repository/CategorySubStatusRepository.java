package com.mybudget.domain.repository;

import com.mybudget.domain.models.CategorySubStatus;
import com.mybudget.domain.models.SubStatus;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

/**
 * @author Soumaya Izmar
 */
public interface CategorySubStatusRepository extends CrudRepository<CategorySubStatus, Integer> {

    List<CategorySubStatus> findTemplateCategoryBySubStatus(SubStatus subStatus);

}
