package com.mybudget.domain.repository;

import com.mybudget.domain.models.Image;
import com.mybudget.domain.models.Project;
import com.mybudget.domain.models.User;
import org.springframework.data.repository.CrudRepository;

import javax.persistence.CascadeType;
import javax.persistence.ManyToOne;

/**
 * @author Soumaya Izmar
 */
public interface ProjectRepository extends CrudRepository<Project, Integer> {


    Iterable<Project> findByParentProjectAndOwner(Project project, User owner);


    Iterable<Project> findByParentProject(Project project);


    Iterable<Project> findByParentProjectIs(Project p);

    boolean existsProjectByImage(Image m);


    void deleteByOwner(User user);
}
