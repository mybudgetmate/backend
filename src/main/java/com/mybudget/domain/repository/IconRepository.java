package com.mybudget.domain.repository;

import com.mybudget.domain.models.Icon;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

/**
 * @author Soumaya Izmar && Eva Tumia
 */
public interface IconRepository extends CrudRepository<Icon, Integer> {
    List<Icon> findAll();

    boolean existsByIconCode(String iconCode);

    <S extends Icon> S save(S icon);

    Icon findByIconCode(String icon);
}
