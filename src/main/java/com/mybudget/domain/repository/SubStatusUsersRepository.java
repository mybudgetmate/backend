package com.mybudget.domain.repository;

import com.mybudget.domain.models.SubStatusUsers;
import com.mybudget.domain.models.User;
import org.springframework.data.repository.CrudRepository;

/**
 * @author Soumaya Izmar
 */
public interface SubStatusUsersRepository  extends CrudRepository<SubStatusUsers, Integer> {
    void deleteByUser(User user);
}
