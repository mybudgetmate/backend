package com.mybudget.domain.repository;

import com.mybudget.domain.models.Category;
import com.mybudget.domain.models.Transaction;
import com.mybudget.domain.models.TransactionCategory;
import com.mybudget.domain.models.User;
import com.mybudget.domain.models.indentities.TransactionCategoryPK;
import org.springframework.data.repository.CrudRepository;

import java.util.List;


/**
 * @author Soumaya Izmar
 */
public interface TransactionCategoryRepository extends CrudRepository<TransactionCategory, Integer> {
    List<TransactionCategory> findByTransactionCategoryPK_TransactionID(Transaction t);

    List<TransactionCategory> findByTransactionCategoryPK_CategoryID(Category c);

    TransactionCategory findByTransactionCategoryPK(TransactionCategoryPK pk);

    void deleteByTransactionCategoryPK_TransactionID(Transaction transac);

    void deleteByTransactionCategoryPK(TransactionCategoryPK transactionCategoryPK);

    void deleteByTransactionCategoryPK_TransactionID_Account_Owner(User user);
}
