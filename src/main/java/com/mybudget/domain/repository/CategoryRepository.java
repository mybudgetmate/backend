package com.mybudget.domain.repository;

import com.mybudget.domain.models.Category;
import com.mybudget.domain.models.User;
import org.springframework.data.repository.CrudRepository;

import java.util.List;
import java.util.Optional;

/**
 * @author Soumaya Izmar && Eva Tumia
 */
public interface CategoryRepository extends CrudRepository<Category, Integer> {

    Optional<Category> findByNameAndOwner(String name, User Owner);
    Optional<Category> findByNameAndOwnerAndType(String name, User Owner, Category.TYPE type);
    List<Category> findAllByOwner(User owner);

    List<Category> findByOwnerAndType(User owner, Category.TYPE type);

    List<Category> findByParentCategoryAndOwner(Category parent, User owner);
    List<Category> findByParentCategoryAndOwnerAndType(Category parent, User owner,Category.TYPE type);

    boolean existsByParentCategory(Category parent);

    void deleteByOwner(User user);
}
