package com.mybudget.domain.repository;

import com.mybudget.domain.models.CategoryStatus;
import com.mybudget.domain.models.Status;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

/**
 * @author Soumaya Izmar
 */
public interface CategoryStatusRepository extends CrudRepository<CategoryStatus, Integer> {


    List<CategoryStatus> findTemplateCategoryByStatus(Status status);
}
