package com.mybudget.domain.repository;

import com.mybudget.domain.models.translation.CategoriesManagementPageTranslation;
import org.springframework.data.repository.CrudRepository;
/**
 * @author Eva Tumia
 */
public interface CategoriesManagementPageRepository extends CrudRepository<CategoriesManagementPageTranslation, Integer> {
}
