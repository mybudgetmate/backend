package com.mybudget.domain.repository;

import com.mybudget.domain.models.SubStatus;
import com.mybudget.domain.models.User;
import org.springframework.data.repository.CrudRepository;
/**
 * @author Soumaya Izmar
 */
public interface SubStatusRepository extends CrudRepository<SubStatus, Integer> {
}
