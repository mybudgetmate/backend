package com.mybudget.domain.repository;

import com.mybudget.domain.dto.RecipeDto;
import com.mybudget.domain.models.Recipe;
import org.springframework.data.repository.CrudRepository;

public interface RecipeRepository extends CrudRepository<Recipe, Long> {
    Recipe findByRecipeID(Long id);

    Recipe findByRecipeID(int id);
}
