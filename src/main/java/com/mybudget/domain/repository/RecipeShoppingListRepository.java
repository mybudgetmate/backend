package com.mybudget.domain.repository;

import com.mybudget.domain.models.RecipeShoppingList;
import com.mybudget.domain.models.ShoppingList;
import org.springframework.data.repository.CrudRepository;

public interface RecipeShoppingListRepository extends CrudRepository<RecipeShoppingList, Long> {

    public Iterable<RecipeShoppingList> findAllByRecipeShoppingListPK_ShoppingList(ShoppingList shoppingList);
}
