package com.mybudget.domain.repository;

import com.mybudget.domain.models.translation.SettingsPageTranslation;
import org.springframework.data.repository.CrudRepository;

public interface SettingsPageTranslationRepository  extends CrudRepository<SettingsPageTranslation, String> {
}
