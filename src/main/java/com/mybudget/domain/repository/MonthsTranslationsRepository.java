package com.mybudget.domain.repository;

import com.mybudget.domain.models.translation.Months;
import org.springframework.data.repository.CrudRepository;

public interface MonthsTranslationsRepository extends CrudRepository<Months, Integer> {
    public Iterable<Months> findAll();
}
