package com.mybudget.domain.repository;

import com.mybudget.domain.models.translation.RecurringPaiementsPageTranslation;
import org.springframework.data.repository.CrudRepository;

/**
 * @author Eva Tumia
 */
public interface RecurringPaiementsTranslationRepository extends CrudRepository<RecurringPaiementsPageTranslation,Integer> {
}
