package com.mybudget.domain.repository;

import com.mybudget.domain.models.translation.HomePageTranslation;
import org.springframework.data.repository.CrudRepository;
/**
 * @author Eva Tumia
 */
public interface HomePageTranslationsRepository extends CrudRepository<HomePageTranslation, Integer> {
}
