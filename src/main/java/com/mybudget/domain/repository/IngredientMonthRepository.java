package com.mybudget.domain.repository;

import com.mybudget.domain.models.Ingredient;
import com.mybudget.domain.models.IngredientMonth;
import org.springframework.data.repository.CrudRepository;

import java.util.Optional;

public interface IngredientMonthRepository extends CrudRepository<IngredientMonth, Integer> {
    Iterable<IngredientMonth> findAllByMonthOrderByPrice(Integer month);
    Iterable<IngredientMonth> findAllByIngredient(Ingredient ingredient);
    Optional <IngredientMonth> findByIngredientAndMonth(Ingredient ingredient, Integer month);
}
