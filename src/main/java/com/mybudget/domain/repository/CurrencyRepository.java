package com.mybudget.domain.repository;

import com.mybudget.domain.models.Currency;
import org.springframework.data.repository.CrudRepository;

import java.util.Optional;

/**
 * @author Soumaya Izmar
 */
public interface CurrencyRepository extends CrudRepository<Currency, Integer> {

    Optional<Currency> findByType(String type);
}
