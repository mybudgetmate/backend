package com.mybudget.domain.repository;

import com.mybudget.domain.models.Status;
import com.mybudget.domain.models.translation.StatusTranslation;
import org.springframework.data.repository.CrudRepository;

import java.util.Optional;


/**
 * @author Soumaya Izmar
 */
public interface StatusRepository extends CrudRepository<Status, Integer> {

    Optional<Status> findByName(StatusTranslation name);

}
