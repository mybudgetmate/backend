package com.mybudget.domain.repository;

import com.mybudget.domain.models.translation.SignUpPageTranslation;
import org.springframework.data.repository.CrudRepository;

public interface SignUpPageRepository extends CrudRepository<SignUpPageTranslation, Integer> {
}
