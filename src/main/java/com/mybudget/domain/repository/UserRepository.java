package com.mybudget.domain.repository;

import com.mybudget.domain.models.User;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;


/**
 * @author Soumaya Izmar
 */
@Repository
public interface UserRepository extends CrudRepository<User, Integer> {

    Optional<User> findByEmail(String email);
    Optional<User> findByPseudo(String pseudo);
    Optional<User> findBySocialCode(String socialCode);

}
