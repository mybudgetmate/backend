package com.mybudget.domain.repository;

import com.mybudget.domain.models.translation.TemplateCategoryNameTranslation;
import org.springframework.data.repository.CrudRepository;

import java.util.Optional;

/**
 * @author Eva Tumia
 */
public interface TemplateCategoryNameTranslationRepository extends CrudRepository<TemplateCategoryNameTranslation,Integer> {
    Optional<TemplateCategoryNameTranslation> findById(String id);
}
