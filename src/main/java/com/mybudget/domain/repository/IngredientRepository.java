package com.mybudget.domain.repository;

import com.mybudget.domain.models.Ingredient;
import com.mybudget.domain.models.translation.IngredientsTranslations;
import org.springframework.data.repository.CrudRepository;

public interface IngredientRepository extends CrudRepository<Ingredient, Integer> {
    Iterable<Ingredient> findAll();
    Ingredient findByIngredientID(Long id);

    IngredientsTranslations  findIngredientByName(String name);



}
