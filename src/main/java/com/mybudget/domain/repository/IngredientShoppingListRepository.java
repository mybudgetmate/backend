package com.mybudget.domain.repository;

import com.mybudget.domain.models.IngredientShoppingList;
import com.mybudget.domain.models.ShoppingList;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface IngredientShoppingListRepository extends CrudRepository<IngredientShoppingList, Long> {

    public List<IngredientShoppingList> findAllByIngredientShoppingListPK_ShoppingList(ShoppingList shoppingList);
}
