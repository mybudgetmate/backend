package com.mybudget.domain.repository;

import com.mybudget.domain.models.RecipeMonth;
import org.springframework.data.repository.CrudRepository;

public interface RecipeMonthRepository extends CrudRepository<RecipeMonth, Long> {
    Iterable<RecipeMonth> findAllByMonth(Integer month);
}
