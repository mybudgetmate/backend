package com.mybudget.domain.repository;

import com.mybudget.domain.models.translation.FriendshipTranslation;
import org.springframework.data.repository.CrudRepository;

public interface FriendshipTranslationRepository extends CrudRepository<FriendshipTranslation, Integer> {
}
