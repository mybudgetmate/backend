package com.mybudget.domain.utils;


import com.google.auth.oauth2.GoogleCredentials;
import com.google.firebase.FirebaseApp;
import com.google.firebase.FirebaseOptions;
import com.google.firebase.messaging.FirebaseMessaging;
import com.mybudget.domain.exceptions.FatalException;
import org.springframework.context.annotation.Bean;
import org.springframework.core.io.ClassPathResource;
import org.springframework.stereotype.Service;

import java.io.IOException;

/**
 * @author Soumaya Izmar
 */
@Service
public class FireBaseAdminInitializeService {

    @Bean
    FirebaseMessaging firebaseMessaging() throws IOException {
        GoogleCredentials googleCredentials = GoogleCredentials
                .fromStream(new ClassPathResource("serviceAccount").getInputStream());
        FirebaseOptions firebaseOptions = FirebaseOptions
                .builder()
                .setCredentials(googleCredentials)
                .build();
        FirebaseApp app;
        if (FirebaseApp.getApps().isEmpty()) {
            app = FirebaseApp.initializeApp(firebaseOptions, "myBudget");
        }else{
            app = FirebaseApp.getApps().stream().filter(f->f.getName().equals("myBudget")).findFirst().orElseThrow(FatalException::new);
        }


        return FirebaseMessaging.getInstance(app);
    }
}
