package com.mybudget.domain.utils;

import org.springframework.stereotype.Component;

import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

/**
 * @author Soumaya Izmar
 */
@Component
public class IntrospectionService {


    public  Map<String, Method> createMethodMap(String className) throws ClassNotFoundException {

        Map<String, Method> languagesMethod = new HashMap<>();
        Class<?> c = Class.forName("com.mybudget.domain.models.translation."+className);
        Method[] allMethods =  c.getDeclaredMethods();

        for (Method m : allMethods) {
            String method =m.getName();
            if(method.startsWith("get")){
                languagesMethod.put(method.substring(3).toLowerCase(Locale.ROOT),m);
            }

        }
        return languagesMethod;

    }

}
