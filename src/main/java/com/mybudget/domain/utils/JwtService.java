package com.mybudget.domain.utils;

import com.auth0.jwt.JWT;
import com.auth0.jwt.JWTVerifier;
import com.auth0.jwt.algorithms.Algorithm;
import com.auth0.jwt.interfaces.DecodedJWT;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Component;

/**
 * @author Soumaya Izmar
 */
@Component
public class JwtService {

    private static final  String SUPERSECRET = "Mybudget2021";

    public String extractEmail(String token) {

        Algorithm algorithm = Algorithm.HMAC512(SUPERSECRET);
        JWTVerifier verifier = JWT.require(algorithm).build();
        DecodedJWT jwt = verifier.verify(token);

        return jwt.getClaim("email").asString();
    }


    public String createToken(String email, String id) {

        return JWT.create().withClaim("id", id)
                .withClaim("email", email)
                .sign(Algorithm.HMAC512(SUPERSECRET));
    }

    public int extractID(String token) {

        Algorithm algorithm = Algorithm.HMAC512(SUPERSECRET);
        JWTVerifier verifier = JWT.require(algorithm).build();
        DecodedJWT jwt = verifier.verify(token);

        return Integer.parseInt(jwt.getClaim("id").asString());
    }


    public Boolean validateToken(String email, UserDetails userDetails) {
        return (email.equals(userDetails.getUsername()));
    }

}
