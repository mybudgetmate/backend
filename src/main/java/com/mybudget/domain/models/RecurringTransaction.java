package com.mybudget.domain.models;

import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Table(name = "recurring_transactions")
public class RecurringTransaction {

/**
 * @author Eva Tumia
 */

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "recurringTransactionID")
    private Integer recurringTransactionID;

    @Column(nullable = false)
    private String name;

    @Column(nullable = false)
    private LocalDateTime endDate;

    public RecurringTransaction(String name, LocalDateTime endDate) {
        this.name = name;
        this.endDate = endDate;
    }

    public RecurringTransaction() {
    }

    public Integer getRecurringTransactionID() {
        return recurringTransactionID;
    }

    public void setRecurringTransactionID(Integer recurringTransactionID) {
        this.recurringTransactionID = recurringTransactionID;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public LocalDateTime getEndDate() {
        return endDate;
    }

    public void setEndDate(LocalDateTime endDate) {
        this.endDate = endDate;
    }

    @Override
    public String toString() {
        return "RecurringTransaction{" +
                "recurringTransactionID=" + recurringTransactionID +
                ", name='" + name + '\'' +
                ", endDate=" + endDate +
                '}';
    }
}
