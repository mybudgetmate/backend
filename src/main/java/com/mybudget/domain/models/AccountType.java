package com.mybudget.domain.models;

import javax.persistence.*;

/**
 * @author Eva Tumia
 */
@Entity
@Table(name = "accountTypes")
public class AccountType {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "accountTypeId")
    private Integer accountTypeID;

    @Column(nullable = false)
    private String name;

    @ManyToOne(fetch = FetchType.LAZY )
    @JoinColumn(name = "icon", nullable = false)
    private Icon icon;

    public Integer getAccountTypeID() {
        return accountTypeID;
    }

    public void setAccountTypeID(Integer accountTypeID) {
        this.accountTypeID = accountTypeID;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Icon getIcon() {
        return icon;
    }

    public void setIcon(Icon icon) {
        this.icon = icon;
    }
}
