package com.mybudget.domain.models;

import javax.persistence.*;

/**
 * @author Soumaya Izmar
 */
@Entity(name = "images")
public class Image {


    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "imageID")
    private Integer imageID;

    @Column(nullable = false)
    private String name;

    public Integer getImageID() {
        return imageID;
    }

    public void setImageID(Integer imageID) {
        this.imageID = imageID;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
