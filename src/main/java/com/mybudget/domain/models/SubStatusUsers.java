package com.mybudget.domain.models;

import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;

import javax.persistence.*;

/**
 * @author Soumaya Izmar
 */
@Entity
@Table(name = "sub_status_users")
public class SubStatusUsers {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int sub_statusUsersID;

    @ManyToOne(fetch = FetchType.LAZY )
    @NotFound(action = NotFoundAction.IGNORE)
    @JoinColumn(name = "sub_status",nullable = true)
    private SubStatus subStatus;

    @ManyToOne(fetch = FetchType.LAZY )
    @NotFound(action = NotFoundAction.IGNORE)
    @JoinColumn(name = "owner",nullable = true)
    private User user;

    public int getSub_statusUsersID() {
        return sub_statusUsersID;
    }

    public void setSub_statusUsersID(int sub_statusUsersID) {
        this.sub_statusUsersID = sub_statusUsersID;
    }

    public SubStatus getSubStatus() {
        return subStatus;
    }

    public void setSubStatus(SubStatus subStatus) {
        this.subStatus = subStatus;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }
}
