package com.mybudget.domain.models.translation;

import javax.persistence.*;

/**
 * @Author: Eva Tumia
 */
@Entity
@Table(name = "templateCategoryNameTranslation")
public class TemplateCategoryNameTranslation {
    @Id
    private String id;
    @Column(nullable = true)
    private String gb;
    @Column(nullable = true)
    private String fr;
    @Column(nullable = true)
    private String nl;
    @Column(nullable = true)
    private String it;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getGb() {
        return gb;
    }

    public void setGb(String gb) {
        this.gb = gb;
    }

    public String getFr() {
        return fr;
    }

    public void setFr(String fr) {
        this.fr = fr;
    }

    public String getNl() {
        return nl;
    }

    public void setNl(String nl) {
        this.nl = nl;
    }

    public String getIt() {
        return it;
    }

    public void setIt(String it) {
        this.it = it;
    }

    public String getTranslation(String countryCode){
        switch (countryCode){
            case "FR":
                return this.fr;
            case "NL":
                return this.nl;
            case "GB":
                return this.gb;
            case "IT":
                return this.it;
            default:
                return this.gb;
        }
    }
}