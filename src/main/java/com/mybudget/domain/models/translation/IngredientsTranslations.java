package com.mybudget.domain.models.translation;

import javax.persistence.*;

@Entity
@Table(name = "ingredients_translations")
public class IngredientsTranslations {

    @Id
    private String id;

    private String gb;

    private String fr;

    private String nl;

    private String it;

    public String getGb() {
        return gb;
    }

    public void setGb(String gb) {
        this.gb = gb;
    }

    public String getFr() {
        return fr;
    }

    public void setFr(String fr) {
        this.fr = fr;
    }

    public String getNl() {
        return nl;
    }

    public void setNl(String nl) {
        this.nl = nl;
    }

    public String getIt() {
        return it;
    }

    public void setIt(String it) {
        this.it = it;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    @Override
    public String toString() {
        return "IngredientsTranslations{" +
                "id='" + id + '\'' +
                ", gb='" + gb + '\'' +
                ", fr='" + fr + '\'' +
                ", nl='" + nl + '\'' +
                ", it='" + it + '\'' +
                '}';
    }
}
