package com.mybudget.domain.models.translation;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * @author Eva Tumia
 */

@Entity
@Table(name = "categoriesManagementTranslations")
public class CategoriesManagementPageTranslation {

    @Id
    private String id;
    @Column(nullable = true)
    private String gb;
    @Column(nullable = true)
    private String fr;
    @Column(nullable = true)
    private String nl;
    @Column(nullable = true)
    private String it;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getGb() {
        return gb;
    }

    public void setGb(String gb) {
        this.gb = gb;
    }

    public String getFr() {
        return fr;
    }

    public void setFr(String fr) {
        this.fr = fr;
    }

    public String getNl() {
        return nl;
    }

    public void setNl(String nl) {
        this.nl = nl;
    }

    public String getIt() {
        return it;
    }

    public void setIt(String it) {
        this.it = it;
    }

}
