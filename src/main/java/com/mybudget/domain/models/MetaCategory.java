package com.mybudget.domain.models;

import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;

import javax.persistence.*;

/**
 * @author Soumaya Izmar
 */
@Entity
@Table(name = "metaCategories")
public class MetaCategory {


    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "metaCategoryID")
    private Integer metaCategoryID;

    @Column(nullable = false)
    private String details;

    @ManyToOne(fetch = FetchType.LAZY )
    @NotFound(action = NotFoundAction.IGNORE)
    @JoinColumn(name = "templateCategory",nullable = true)
    private TemplateCategory templateCategory;

    public Integer getMetaCategoryID() {
        return metaCategoryID;
    }

    public void setMetaCategoryID(Integer metaCategoryID) {
        this.metaCategoryID = metaCategoryID;
    }

    public String getDetails() {
        return details;
    }

    public void setDetails(String details) {
        this.details = details;
    }

    public TemplateCategory getTemplateCategory() {
        return templateCategory;
    }

    public void setTemplateCategory(TemplateCategory templateCategory) {
        this.templateCategory = templateCategory;
    }
}
