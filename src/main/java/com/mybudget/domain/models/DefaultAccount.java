package com.mybudget.domain.models;

import javax.persistence.*;

/**
 * @author Soumaya Izmar
 */
@Entity
@Table(name = "defaultAccounts")
public class DefaultAccount {


    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "defaultAccountId")
    private Integer defaultAccountId;

    @Column(nullable = false)
    private String name;


    @ManyToOne(fetch = FetchType.LAZY )
    @JoinColumn(name = "accountTypeId",nullable = false)
    private AccountType type;



    public Integer getDefaultAccountId() {
        return defaultAccountId;
    }

    public void setDefaultAccountId(Integer defaultAccountId) {
        this.defaultAccountId = defaultAccountId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public AccountType getType() {
        return type;
    }

    public void setType(AccountType type) {
        this.type = type;
    }


}
