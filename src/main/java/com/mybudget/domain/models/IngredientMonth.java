package com.mybudget.domain.models;

import com.mybudget.domain.models.translation.Months;

import javax.persistence.*;

@Entity
@Table(name = "ingredient_month")
public class IngredientMonth {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long ingredientMonthID;

    @ManyToOne(fetch= FetchType.LAZY )
    @JoinColumn(name = "ingredient", nullable = false)
    private Ingredient ingredient;

    @Column(nullable = false)
    private Integer month;

    @Column(nullable = false)
    private Double price;

    public IngredientMonth() {
    }

    public IngredientMonth(Ingredient ingredient, Integer month, Double price) {
        this.ingredient = ingredient;
        this.month = month;
        this.price = price;
    }

    public void setIngredientMonthID(Long ingredientMonthID) {
        this.ingredientMonthID = ingredientMonthID;
    }

    public Long getIngredientMonthID() {
        return ingredientMonthID;
    }

    public Ingredient getIngredient() {
        return ingredient;
    }

    public void setIngredient(Ingredient ingredient) {
        this.ingredient = ingredient;
    }

    public Integer getMonth() {
        return month;
    }

    public void setMonth(Integer month) {
        this.month = month;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }
}
