package com.mybudget.domain.models;

import com.mybudget.domain.models.indentities.RecipeShoppingListPK;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "recipe_shopping_list")
public class RecipeShoppingList {
    @EmbeddedId
    private RecipeShoppingListPK recipeShoppingListPK;

    @Column(nullable = false)
    private Integer servings;

    public RecipeShoppingList() {
    }

    public RecipeShoppingList(RecipeShoppingListPK recipeShoppingListPK, Integer servings) {
        this.recipeShoppingListPK = recipeShoppingListPK;
        this.servings = servings;
    }

    public RecipeShoppingListPK getRecipeShoppingListPK() {
        return recipeShoppingListPK;
    }

    public void setRecipeShoppingListPK(RecipeShoppingListPK recipeShoppingListPK) {
        this.recipeShoppingListPK = recipeShoppingListPK;
    }

    public Integer getServings() {
        return servings;
    }

    public void setServings(Integer servings) {
        this.servings = servings;
    }
}
