package com.mybudget.domain.models;

import com.mybudget.domain.models.translation.TemplateCategoryNameTranslation;

import javax.persistence.*;

/**
 * @author Soumaya Izmar
 */
@Entity
@Table(name = "templateCategories")
public class TemplateCategory {


    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "templateCategoryID")
    private Integer templateCategoryID;

    @OneToOne(fetch= FetchType.LAZY )
    @JoinColumn(name = "name", nullable = false)
    private TemplateCategoryNameTranslation name;

    @ManyToOne(fetch = FetchType.LAZY )
    @JoinColumn(name = "parentCategory", nullable = true, foreignKey = @ForeignKey(name = "FK_CATEGORY_PARENTCATEGORY"))
    private TemplateCategory parentTemplateCategory;

    @Column(nullable = false)
    private Double budget;

    public enum TYPE {
        INPUT, OUTPUT
    }

    @Enumerated(EnumType.STRING)
    @Column(nullable = false)
    private TYPE type;


    @ManyToOne(fetch = FetchType.LAZY ,cascade = CascadeType.REMOVE)
    @JoinColumn(name = "icon", nullable = false)
    private Icon icon;

    public TemplateCategory() {
    }

    public Integer getTemplateCategoryID() {
        return templateCategoryID;
    }

    public void setTemplateCategoryID(Integer templateCategoryID) {
        this.templateCategoryID = templateCategoryID;
    }

    public TemplateCategoryNameTranslation getName() {
        return name;
    }

    public void setName(TemplateCategoryNameTranslation name) {
        this.name = name;
    }

    public TemplateCategory getParentTemplateCategory() {
        return parentTemplateCategory;
    }

    public void setParentTemplateCategory(TemplateCategory parentTemplateCategory) {
        this.parentTemplateCategory = parentTemplateCategory;
    }

    public Double getBudget() {
        return budget;
    }

    public void setBudget(Double budget) {
        this.budget = budget;
    }

    public TYPE getType() {
        return type;
    }

    public void setType(TYPE type) {
        this.type = type;
    }

    public Icon getIcon() {
        return icon;
    }

    public void setIcon(Icon icon) {
        this.icon = icon;
    }
}
