package com.mybudget.domain.models;

import com.mybudget.domain.dto.RecipeDto;
import com.mybudget.domain.models.indentities.IngredientShoppingListPK;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;

import javax.persistence.*;

@Entity
@Table(name = "ingredient_shopping_list")
public class IngredientShoppingList {
    @EmbeddedId
    private IngredientShoppingListPK ingredientShoppingListPK;

    @Column(nullable = false)
    private Float quantity;

    public IngredientShoppingList() {
    }

    public IngredientShoppingList(IngredientShoppingListPK ingredientShoppingListPK, Float quantity) {
        this.ingredientShoppingListPK = ingredientShoppingListPK;
        this.quantity = quantity;
    }


    public IngredientShoppingListPK getIngredientShoppingListPK() {
        return ingredientShoppingListPK;
    }

    public void setIngredientShoppingListPK(IngredientShoppingListPK ingredientShoppingListPK) {
        this.ingredientShoppingListPK = ingredientShoppingListPK;
    }

    public Float getQuantity() {
        return quantity;
    }

    public void setQuantity(Float quantity) {
        this.quantity = quantity;
    }


    @Override
    public String toString() {
        return "IngredientShoppingList{" +
                "ingredientShoppingListPK=" + ingredientShoppingListPK +
                ", quantity=" + quantity +
                '}';
    }

}
