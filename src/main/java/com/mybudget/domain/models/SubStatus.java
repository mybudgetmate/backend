package com.mybudget.domain.models;

import javax.persistence.*;

/**
 * @author Soumaya Izmar
 */
@Entity
@Table(name = "sub_status")
public class SubStatus {


    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int sub_statusID;

    @Column(nullable = true)
    private String gb;
    @Column(nullable = true)
    private String fr;
    @Column(nullable = true)
    private String nl;
    @Column(nullable = true)
    private String it;

    public int getSub_statusID() {
        return sub_statusID;
    }

    public void setSub_statusID(int sub_statusID) {
        this.sub_statusID = sub_statusID;
    }

    public String getGb() {
        return gb;
    }

    public void setGb(String gb) {
        this.gb = gb;
    }

    public String getFr() {
        return fr;
    }

    public void setFr(String fr) {
        this.fr = fr;
    }

    public String getNl() {
        return nl;
    }

    public void setNl(String nl) {
        this.nl = nl;
    }

    public String getIt() {
        return it;
    }

    public void setIt(String it) {
        this.it = it;
    }



    public String getTranslation(String countryCode){
        switch (countryCode){
            case "fr":
                return this.fr;
            case "nl":
                return this.nl;
            case "it":
                return this.it;
            default:
                return this.gb;
        }
    }
}
