package com.mybudget.domain.models;

import javax.persistence.*;

@Entity
@Table(name = "ingredient_recipe")
public class IngredientRecipe {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long ingredientRecipeID;

    @ManyToOne(fetch= FetchType.LAZY )
    @JoinColumn(name = "ingredient", nullable = false)
    private Ingredient ingredient;

    @ManyToOne(fetch= FetchType.LAZY )
    @JoinColumn(name = "recipe", nullable = false)
    private Recipe recipe;

    @Column(nullable = false)
    private Float quantity;

    public IngredientRecipe() {
    }

    public IngredientRecipe(Long ingredientRecipeID, Ingredient ingredient, Recipe recipe, Float quantity) {
        this.ingredientRecipeID = ingredientRecipeID;
        this.ingredient = ingredient;
        this.recipe = recipe;
        this.quantity = quantity;
    }

    public Long getIngredientRecipeID() {
        return ingredientRecipeID;
    }

    public void setIngredientRecipeID(Long ingredientRecipeID) {
        this.ingredientRecipeID = ingredientRecipeID;
    }

    public Ingredient getIngredient() {
        return ingredient;
    }

    public void setIngredient(Ingredient ingredient) {
        this.ingredient = ingredient;
    }

    public Recipe getRecipe() {
        return recipe;
    }

    public void setRecipe(Recipe recipe) {
        this.recipe = recipe;
    }

    public Float getQuantity() {
        return quantity;
    }

    public void setQuantity(Float quantity) {
        this.quantity = quantity;
    }
}
