package com.mybudget.domain.models;

import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;

import javax.persistence.*;
import java.util.Objects;

/**
 * @author Soumaya Izmar
 */
@Entity
@Table(name = "categories")
public class Category {


    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "categoryID")
    private Integer categoryID;

    @Column(nullable = false)
    private String name;

    @ManyToOne(fetch = FetchType.LAZY )
    @NotFound(action = NotFoundAction.IGNORE)
    @JoinColumn(name = "owner")
    private User owner;

    @ManyToOne(fetch = FetchType.LAZY )
    @JoinColumn(name = "parentCategory", nullable = true, foreignKey = @ForeignKey(name = "FK_CATEGORY_PARENTCATEGORY"))
    private Category parentCategory;

    @Column(nullable = false)
    private Double budget;

    public enum TYPE {
        INPUT("input"), OUTPUT("output");
        private String type;

        TYPE(String type) {
            this.type=type;
        }

        public String getType() {
            return type;
        }
    }

    @Enumerated(EnumType.STRING)
    @Column(nullable = false)
    private TYPE type;


    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "icon", nullable = false)
    private Icon icon;


    public Category() {
    }

    public Category(String name, User owner,Double budget, TYPE type, Icon icon) {
        this.name = name;
        this.owner = owner;
        this.budget = budget;
        this.type = type;
        this.icon = icon;
    }

    public Integer getCategoryID() {
        return categoryID;
    }

    public void setCategoryID(Integer categoryID) {
        this.categoryID = categoryID;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public User getOwner() {
        return owner;
    }

    public void setOwner(User owner) {
        this.owner = owner;
    }

    public Category getParentCategory() {
        return parentCategory;
    }

    public void setParentCategory(Category parentCategory) {
        this.parentCategory = parentCategory;
    }

    public Double getBudget() {
        return budget;
    }

    public void setBudget(Double budget) {
        this.budget = budget;
    }

    public TYPE getType() {
        return type;
    }

    public void setType(TYPE type) {
        this.type = type;
    }

    public Icon getIcon() {
        return icon;
    }

    public void setIcon(Icon icon) {
        this.icon = icon;
    }


    @Override
    public String toString() {
        return "Category{" +
                "categoryID=" + categoryID +
                ", name='" + name + '\'' +
                ", owner=" + owner +
                ", parentCategory=" + parentCategory +
                ", budget=" + budget +
                ", type=" + type +
                ", icon=" + icon +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Category category = (Category) o;
        return Objects.equals(categoryID, category.categoryID) && Objects.equals(name, category.name) && Objects.equals(owner, category.owner) && Objects.equals(parentCategory, category.parentCategory) && Objects.equals(budget, category.budget) &&  type == category.type && Objects.equals(icon, category.icon);
    }

    @Override
    public int hashCode() {
        return Objects.hash(categoryID, name, owner, parentCategory, budget, type, icon);
    }
}
