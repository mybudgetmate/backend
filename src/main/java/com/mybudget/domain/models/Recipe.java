package com.mybudget.domain.models;

import com.mybudget.domain.models.translation.RecipesTranslations;

import javax.persistence.*;

@Entity
@Table(name = "recipes")
public class Recipe {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long recipeID;

    @OneToOne(fetch= FetchType.LAZY)
    @JoinColumn(name = "name", nullable = false)
    private RecipesTranslations name;

    @Column(nullable = false)
    private Integer servings;

    @ManyToOne(fetch= FetchType.LAZY)
    @JoinColumn(name = "icon", nullable = false)
    private Icon icon;

    private String recipeUrl;

    @Column(length=1024)
    private String preparation;

    public Recipe() {
    }

    public Recipe(Long recipeID, RecipesTranslations name, Integer servings, Icon icon, String recipeUrl,String preparation) {
        this.recipeID = recipeID;
        this.name = name;
        this.servings = servings;
        this.icon = icon;
        this.recipeUrl = recipeUrl;
        this.preparation = preparation;
    }

    public Long getRecipeID() {
        return recipeID;
    }

    public void setRecipeID(Long recipeID) {
        this.recipeID = recipeID;
    }

    public RecipesTranslations getName() {
        return name;
    }

    public void setName(RecipesTranslations name) {
        this.name = name;
    }

    public Integer getServings() {
        return servings;
    }

    public void setServings(Integer servings) {
        this.servings = servings;
    }

    public Icon getIcon() {
        return icon;
    }

    public void setIcon(Icon icon) {
        this.icon = icon;
    }

    public String getRecipeUrl() {
        return recipeUrl;
    }

    public void setRecipeUrl(String recipeUrl) {
        this.recipeUrl = recipeUrl;
    }

    public String getPreparation() {
        return preparation;
    }

    public void setPreparation(String preparation) {
        this.preparation = preparation;
    }

    @Override
    public String toString() {
        return "Recipe{" +
                "recipeID=" + recipeID +
                ", name=" + name +
                ", servings=" + servings +
                ", icon=" + icon +
                ", recipeUrl='" + recipeUrl + '\'' +
                ", preparation='" + preparation + '\'' +
                '}';
    }
}
