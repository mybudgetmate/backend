package com.mybudget.domain.models;

import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;

import javax.persistence.*;

/**
 * @author Soumaya Izmar
 */
@Entity
@Table(name = "categoryStatus")
public class CategoryStatus {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "categoryStatusId")
    private Integer categoryStatusID;

    @ManyToOne(fetch = FetchType.LAZY )
    @NotFound(action = NotFoundAction.IGNORE)
    @JoinColumn(name = "templateCategory",nullable = true)
    private TemplateCategory templateCategory;

    @ManyToOne(fetch = FetchType.LAZY )
    @NotFound(action = NotFoundAction.IGNORE)
    @JoinColumn(name = "status",nullable = false)
    private Status status;


    public CategoryStatus() {
    }

    public Integer getCategoryStatusID() {
        return categoryStatusID;
    }

    public void setCategoryStatusID(Integer categoryStatusID) {
        this.categoryStatusID = categoryStatusID;
    }

    public TemplateCategory getTemplateCategory() {
        return templateCategory;
    }

    public void setTemplateCategory(TemplateCategory templateCategory) {
        this.templateCategory = templateCategory;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }
}
