package com.mybudget.domain.models;

import com.mybudget.domain.models.indentities.FriendshipPK;

import javax.persistence.*;
import java.time.LocalDate;

/**
 * @author Soumaya Izmar
 */
@Entity
@Table(name = "friendships")
public class Friendship {

    @EmbeddedId
    private FriendshipPK friendshipPK;

    public enum State {
        ACCEPT, PENDING, REJECT
    }

    @Enumerated(EnumType.STRING)
    @Column(nullable = false)
    private State state;

    @Column(nullable = false)
    private LocalDate requestDate;

    @Column(nullable = true)
    private LocalDate acceptDate;

    public Friendship() {
    }

    public FriendshipPK getFriendshipPK() {
        return friendshipPK;
    }

    public void setFriendshipPK(FriendshipPK friendshipPK) {
        this.friendshipPK = friendshipPK;
    }

    public State getState() {
        return state;
    }

    public void setState(State state) {
        this.state = state;
    }

    public LocalDate getRequestDate() {
        return requestDate;
    }

    public void setRequestDate(LocalDate requestDate) {
        this.requestDate = requestDate;
    }

    public LocalDate getAcceptDate() {
        return acceptDate;
    }

    public void setAcceptDate(LocalDate acceptDate) {
        this.acceptDate = acceptDate;
    }
}
