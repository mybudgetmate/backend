package com.mybudget.domain.models;


import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "shoppingList")
public class ShoppingList {


    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "shoppingListID")
    private Long shoppingListID;


    @ManyToOne(fetch = FetchType.LAZY )
    @NotFound(action = NotFoundAction.IGNORE)
    @JoinColumn(name = "owner", nullable = false)
    private User owner;

    @Column(nullable = false)
    private String name;


    @Column(nullable = false)
    private Date shopping_date;



    @Column(nullable = false)
    private Double total;


    public ShoppingList() {
    }

    @Override
    public String toString() {
        return "ShoppingList{" +
                "shoppingListID=" + shoppingListID +
                ", owner=" + owner +
                ", name='" + name + '\'' +
                ", shopping_date=" + shopping_date +
                ", total=" + total +
                '}';
    }

    public ShoppingList(String name, User owner, Date shopping_date, Double total) {
        this.name = name;
        this.owner = owner;
        this.shopping_date=shopping_date;
        this.total = total;
    }

    public Long getShoppingListID() {
        return shoppingListID;
    }

    public void setShoppingListID(Long shoppingListID) {
        this.shoppingListID = shoppingListID;
    }

    public Date getShopping_date() {
        return shopping_date;
    }

    public void setShopping_date(Date shopping_date) {
        this.shopping_date = shopping_date;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public User getOwner() {
        return owner;
    }

    public void setOwner(User owner) {
        this.owner = owner;
    }

    public Double getTotal() {
        return total;
    }

    public void setTotal(Double total) {
        this.total = total;
    }
}
