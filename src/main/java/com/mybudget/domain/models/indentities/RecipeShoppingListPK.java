package com.mybudget.domain.models.indentities;

import com.mybudget.domain.models.Recipe;
import com.mybudget.domain.models.ShoppingList;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;

import javax.persistence.Embeddable;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import java.io.Serializable;
import java.util.Objects;

@Embeddable
public class RecipeShoppingListPK implements Serializable {
    private static final long serialVersionUID = 6600696545002087621L;

    @ManyToOne
    @NotFound(action = NotFoundAction.IGNORE)
    @JoinColumn(name = "shoppingList")
    private ShoppingList shoppingList;

    @ManyToOne
    @NotFound(action = NotFoundAction.IGNORE)
    @JoinColumn(name = "recipe")
    private Recipe recipe;

    public RecipeShoppingListPK() {
    }

    public RecipeShoppingListPK(ShoppingList shoppingList, Recipe recipe) {
        this.shoppingList = shoppingList;
        this.recipe = recipe;
    }

    public ShoppingList getShoppingList() {
        return shoppingList;
    }

    public void setShoppingList(ShoppingList shoppingList) {
        this.shoppingList = shoppingList;
    }

    public Recipe getRecipe() {
        return recipe;
    }

    public void setRecipe(Recipe recipe) {
        this.recipe = recipe;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        RecipeShoppingListPK that = (RecipeShoppingListPK) o;
        return shoppingList.equals(that.shoppingList) && recipe.equals(that.recipe);
    }

    @Override
    public int hashCode() {
        return Objects.hash(shoppingList, recipe);
    }
}
