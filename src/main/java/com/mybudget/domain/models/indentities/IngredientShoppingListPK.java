package com.mybudget.domain.models.indentities;

import com.mybudget.domain.models.Ingredient;
import com.mybudget.domain.models.ShoppingList;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;

import javax.persistence.Embeddable;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import java.io.Serializable;
import java.util.Objects;

@Embeddable
public class IngredientShoppingListPK implements Serializable {

    private static final long serialVersionUID = 6600696545002087621L;

    @ManyToOne
    @NotFound(action = NotFoundAction.IGNORE)
    @JoinColumn(name = "ingredient")
    private Ingredient ingredient;

    @ManyToOne
    @NotFound(action = NotFoundAction.IGNORE)
    @JoinColumn(name = "shoppingList")
    private ShoppingList shoppingList;

    public IngredientShoppingListPK() {
    }

    public IngredientShoppingListPK(Ingredient ingredient, ShoppingList shoppingList) {
        this.ingredient = ingredient;
        this.shoppingList = shoppingList;
    }

    public Ingredient getIngredient() {
        return ingredient;
    }

    public void setIngredient(Ingredient ingredientID) {
        this.ingredient = ingredientID;
    }

    public ShoppingList getShoppingList() {
        return shoppingList;
    }

    public void setShoppingList(ShoppingList shoppingListID) {
        this.shoppingList = shoppingListID;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        IngredientShoppingListPK that = (IngredientShoppingListPK) o;
        return Objects.equals(ingredient, that.ingredient) && Objects.equals(shoppingList, that.shoppingList);
    }

    @Override
    public int hashCode() {
        return Objects.hash(ingredient, shoppingList);
    }

    @Override
    public String toString() {
        return "IngredientShoppingListPK{" +
                "ingredient=" + ingredient +
                ", shoppingList=" + shoppingList +
                '}';
    }
}
