package com.mybudget.domain.models.indentities;

import com.mybudget.domain.models.Category;
import com.mybudget.domain.models.Transaction;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;

import javax.persistence.Embeddable;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import java.io.Serializable;
import java.util.Objects;

/**
 * @author Soumaya Izmar
 */
@Embeddable
public class TransactionCategoryPK implements Serializable {

    private static final long serialVersionUID = 6600696545002087621L;

    @ManyToOne
    @NotFound(action = NotFoundAction.IGNORE)
    @JoinColumn(name = "category")
    private Category categoryID;

    @ManyToOne
    @NotFound(action = NotFoundAction.IGNORE)
    @JoinColumn(name = "transaction")
    private Transaction transactionID;

    public TransactionCategoryPK() {
    }

    public TransactionCategoryPK(Category categoryID, Transaction transactionID) {
        this.categoryID = categoryID;
        this.transactionID = transactionID;
    }

    public Category getCategoryID() {
        return categoryID;
    }

    public void setCategoryID(Category categoryID) {
        this.categoryID = categoryID;
    }


    public Transaction getTransactionID() {
        return transactionID;
    }

    public void setTransactionID(Transaction transactionID) {
        this.transactionID = transactionID;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        TransactionCategoryPK that = (TransactionCategoryPK) o;
        return Objects.equals(categoryID, that.categoryID) && Objects.equals(transactionID, that.transactionID);
    }

    @Override
    public int hashCode() {
        return Objects.hash(categoryID, transactionID);
    }
}
