package com.mybudget.domain.models;

import javax.persistence.*;
import java.time.LocalDate;

/**
 * @author Soumaya Izmar
 */
@Entity
@Table(name = "projects")
public class Project {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "projectID")
    private Integer projectID;

    @Column(nullable = false)
    private String name;

    @Column(nullable = false)
    private double requiredBudget;

    @Column(nullable = false, columnDefinition ="Decimal(10,2) default '0'")
    private double obtainedBudget;

    @Column(nullable = false)
    private LocalDate startDate;

    @Column(nullable = false)
    private LocalDate endDate;

    @ManyToOne(fetch = FetchType.LAZY )
    @JoinColumn(name = "owner", nullable = false)
    private User owner;

    @ManyToOne(fetch = FetchType.LAZY )
    @JoinColumn(name = "parentProject", nullable = true, foreignKey = @ForeignKey(name = "FK_PROJECT_PARENTPROJECT"))
    private Project parentProject;

    @ManyToOne(fetch = FetchType.LAZY )
    @JoinColumn(name = "image", nullable = false)
    private Image image;


    public Integer getProjectID() {
        return projectID;
    }

    public void setProjectID(Integer projectID) {
        this.projectID = projectID;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getRequiredBudget() {
        return requiredBudget;
    }

    public void setRequiredBudget(double requiredBudget) {
        this.requiredBudget = requiredBudget;
    }

    public double getObtainedBudget() {
        return obtainedBudget;
    }

    public void setObtainedBudget(double obtainedBudget) {
        this.obtainedBudget = obtainedBudget;
    }



    public User getOwner() {
        return owner;
    }

    public void setOwner(User owner) {
        this.owner = owner;
    }

    public Project getParentProject() {
        return parentProject;
    }

    public void setParentProject(Project parentProject) {
        this.parentProject = parentProject;
    }

    public Image getImage() {
        return image;
    }

    public void setImage(Image image) {
        this.image = image;
    }

    public LocalDate getStartDate() {
        return startDate;
    }

    public void setStartDate(LocalDate startDate) {
        this.startDate = startDate;
    }

    public LocalDate getEndDate() {
        return endDate;
    }

    public void setEndDate(LocalDate endDate) {
        this.endDate = endDate;
    }
}
