package com.mybudget.domain.models;

import com.mybudget.domain.dto.IngredientDto;
import com.mybudget.domain.models.translation.IngredientsTranslations;

import javax.persistence.*;

@Entity
@Table(name = "ingredients")
public class Ingredient {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long ingredientID;

    @OneToOne(fetch= FetchType.LAZY)
    @JoinColumn(name = "name", nullable = false)
    private IngredientsTranslations name;

    @Column(nullable = false)
    private String unit;

    @Column(nullable = false)
    private Float quantity;

    @Column(nullable = false)
    private Float calorificValue;

    @Column(nullable = false)
    private Float proteicValue;

    @ManyToOne(fetch= FetchType.LAZY)
    @JoinColumn(name = "icon", nullable = false)
    private Icon icon;

    public Ingredient(){}

    public Ingredient(IngredientsTranslations name, String unit, Float quantity, Float calorificValue, Float proteicValue, Icon icon) {
        this.name = name;
        this.unit = unit;
        this.quantity = quantity;
        this.calorificValue = calorificValue;
        this.proteicValue = proteicValue;
        this.icon = icon;
    }

    public Ingredient(IngredientDto ing, IngredientsTranslations name, Icon icon){
        this(ing.getId(),name,ing.getUnit(),ing.getQuantity(),ing.getCalorificValue(),ing.getProteicValue(),icon);
    }

    public Ingredient(Long ingredientID ,IngredientsTranslations name, String unit, Float quantity, Float calorificValue, Float proteicValue, Icon icon) {
        this.name = name;
        this.unit = unit;
        this.quantity = quantity;
        this.calorificValue = calorificValue;
        this.proteicValue = proteicValue;
        this.icon = icon;
        this.ingredientID = ingredientID;
    }


    public void setIngredientID(Long ingredientID) {
        this.ingredientID = ingredientID;
    }
    public Long getIngredientID() {
        return ingredientID;
    }

    public IngredientsTranslations getName() {
        return name;
    }

    public void setName(IngredientsTranslations name) {
        this.name = name;
    }

    public String getUnit() {
        return unit;
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }

    public Float getQuantity() {
        return quantity;
    }

    public void setQuantity(Float quantity) {
        this.quantity = quantity;
    }

    public Float getCalorificValue() {
        return calorificValue;
    }

    public void setCalorificValue(Float calorificValue) {
        this.calorificValue = calorificValue;
    }

    public Float getProteicValue() {
        return proteicValue;
    }

    public void setProteicValue(Float proteicValue) {
        this.proteicValue = proteicValue;
    }

    public Icon getIcon() {
        return icon;
    }

    public void setIcon(Icon icon) {
        this.icon = icon;
    }


    @Override
    public String toString() {
        return "Ingredient{" +
                "ingredientID=" + ingredientID +
                ", name=" + name +
                ", unit='" + unit + '\'' +
                ", quantity=" + quantity +
                ", calorificValue=" + calorificValue +
                ", proteicValue=" + proteicValue +
                ", icon=" + icon +
                '}';
    }
}
