package com.mybudget.domain.models;

import com.mybudget.domain.models.translation.StatusTranslation;

import javax.persistence.*;

/**
 * @author Soumaya Izmar
 */
@Entity
@Table(name = "status")
public class Status {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "statusID")
    private Integer statusID;

    @OneToOne(fetch= FetchType.LAZY )
    @JoinColumn(name = "name", nullable = false)
    private StatusTranslation name;

    public Status() {
    }

    public Integer getStatusID() {
        return statusID;
    }

    public void setStatusID(Integer statusID) {
        this.statusID = statusID;
    }

    public StatusTranslation getName() {
        return name;
    }

    public void setName(StatusTranslation name) {
        this.name = name;
    }
}
