package com.mybudget.domain.models;

import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;

import javax.persistence.*;

/**
 * @author Soumaya Izmar
 */
@Entity
@Table(name = "categories_sub_status")
public class CategorySubStatus {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int categoriesSubStatusUsersID;

    @ManyToOne(fetch = FetchType.LAZY )
    @NotFound(action = NotFoundAction.IGNORE)
    @JoinColumn(name = "sub_status",nullable = true)
    private SubStatus subStatus;

    @ManyToOne(fetch = FetchType.LAZY )
    @NotFound(action = NotFoundAction.IGNORE)
    @JoinColumn(name = "templateCategory",nullable = true)
    private TemplateCategory templateCategory;

    public int getCategoriesSubStatusUsersID() {
        return categoriesSubStatusUsersID;
    }

    public void setCategoriesSubStatusUsersID(int categoriesSubStatusUsersID) {
        this.categoriesSubStatusUsersID = categoriesSubStatusUsersID;
    }

    public SubStatus getSubStatus() {
        return subStatus;
    }

    public void setSubStatus(SubStatus subStatus) {
        this.subStatus = subStatus;
    }

    public TemplateCategory getTemplateCategory() {
        return templateCategory;
    }

    public void setTemplateCategory(TemplateCategory templateCategory) {
        this.templateCategory = templateCategory;
    }
}
