package com.mybudget.domain.models;

import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;

import javax.persistence.*;
import java.time.LocalDateTime;

/**
 * @author Soumaya Izmar && Eva Tumia
 */
@Entity
@Table(name = "transactions")
public class Transaction {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "transactionID")
    private Integer transactionID;

    @ManyToOne(fetch = FetchType.LAZY)
    @NotFound(action = NotFoundAction.IGNORE)
    @JoinColumn(name = "account",nullable = false)
    private Account account;

    @Column(nullable = false)
    private LocalDateTime countDate;

    @Column(nullable = false)
    private LocalDateTime valueDate;

    @Column(nullable = false)
    private Double amount;

    public enum TYPE{
        INPUT("input"),OUTPUT("output");
        private String type;

        TYPE(String type){this.type=type;}

        public String getType(){return type;}
    }

    @Column(nullable = false)
    @Enumerated(EnumType.STRING)
    private TYPE type;

    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "recurringTransaction",nullable = true)
    private RecurringTransaction recurringTransaction;

    @Column(nullable = false)
    private String name;

    public Transaction() {
    }

    public Transaction(Account account, LocalDateTime countDate, LocalDateTime valueDate, Double amount, TYPE type, String name,RecurringTransaction recurringTransaction) {
        this.account = account;
        this.countDate = countDate;
        this.valueDate = valueDate;
        this.amount = amount;
        this.type = type;
        this.name= name;
        this.recurringTransaction = recurringTransaction;
    }

    public Integer getTransactionID() {
        return transactionID;
    }

    public void setTransactionID(Integer transactionID) {
        this.transactionID = transactionID;
    }

    public Account getAccount() {
        return account;
    }

    public void setAccount(Account account) {
        this.account = account;
    }

    public LocalDateTime getCountDate() {
        return countDate;
    }

    public void setCountDate(LocalDateTime countDate) {
        this.countDate = countDate;
    }

    public LocalDateTime getValueDate() {
        return valueDate;
    }

    public void setValueDate(LocalDateTime valueDate) {
        this.valueDate = valueDate;
    }

    public Double getAmount() {
        return amount;
    }

    public void setAmount(Double amount) {
        this.amount = amount;
    }

    public TYPE getType() {
        return type;
    }

    public void setType(TYPE type) {
        this.type = type;
    }

    public RecurringTransaction getRecurringTransaction() {
        return recurringTransaction;
    }

    public void setRecurringTransaction(RecurringTransaction recurringTransaction) {
        this.recurringTransaction = recurringTransaction;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "Transaction{" +
                "transactionID=" + transactionID +
                ", account=" + account +
                ", countDate=" + countDate +
                ", valueDate=" + valueDate +
                ", amount=" + amount +
                ", type=" + type +
                ", recurringTransaction=" + recurringTransaction +
                ", name='" + name + '\'' +
                '}';
    }
}
