package com.mybudget.domain.models;

import javax.persistence.*;

/**
 * @author Soumaya Izmar
 */
@Entity
@Table(name = "languages")
public class Language {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name ="languageID")
    private Integer languageID;


    @Column(nullable = false)
    private String countryCode;

    public enum LanguageName {
        FRANCAIS, ENGLISH, NEDERLANDS, ITALIANO;
    }

    @Enumerated(EnumType.STRING)
    @Column(nullable = false)
    private LanguageName name;

    @Column(columnDefinition = "Boolean", nullable = false)
    private Boolean active;

    public Integer getLanguageID() {
        return languageID;
    }

    public void setLanguageID(Integer languageID) {
        this.languageID = languageID;
    }

    public String getCountryCode() {
        return countryCode;
    }

    public void setCountryCode(String countryCode) {
        this.countryCode = countryCode;
    }

    public LanguageName getName() {
        return name;
    }

    public void setName(LanguageName name) {
        this.name = name;
    }

    public Boolean getActive() {
        return active;
    }

    public void setActive(Boolean active) {
        this.active = active;
    }
}
