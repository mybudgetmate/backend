package com.mybudget.domain.models;

import com.mybudget.domain.models.indentities.TransactionCategoryPK;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * @author Soumaya Izmar
 */
@Entity
@Table(name = "transaction_categories")
public class TransactionCategory {

    @EmbeddedId
    private TransactionCategoryPK transactionCategoryPK;

    @Column(nullable = false)
    private double amount;

    public TransactionCategory(TransactionCategoryPK transactionCategoryPK, double amount) {
        this.transactionCategoryPK = transactionCategoryPK;
        this.amount = amount;
    }

    public TransactionCategory() {
    }

    public TransactionCategoryPK getTransactionCategoryPK() {
        return transactionCategoryPK;
    }

    public void setTransactionCategoryPK(TransactionCategoryPK transactionCategoryPK) {
        this.transactionCategoryPK = transactionCategoryPK;
    }

    public double getAmount() {
        return amount;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }
}
