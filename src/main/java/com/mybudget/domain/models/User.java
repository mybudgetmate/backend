package com.mybudget.domain.models;

import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import javax.persistence.*;
import java.time.LocalDateTime;

/**
 * @author Soumaya Izmar
 */
@Entity
@Table(name = "users")
public class User {

    public User() {
    }


    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "userID")
    private Integer userID;

    @Column(nullable = false)
    private String firstname;

    @Column(nullable = false)
    private String lastname;

    @Column(nullable = false)
    private LocalDateTime registrationDate;

    @Column(nullable = true, unique = true)
    private String pseudo;

    @Column(nullable = false)
    private String password;

    @Column(nullable = false, unique = true)
    private String email;

    @ManyToOne(fetch = FetchType.LAZY )
    @NotFound(action = NotFoundAction.IGNORE)
    @JoinColumn(name = "preferedLanguage", nullable = false, foreignKey = @ForeignKey(name = "FK_LANGUAGE_USER"))
    private Language preferedLanguage;

    @Column(columnDefinition = "Boolean", nullable = false)
    private boolean generalConsent;

    @Column(columnDefinition = "Boolean", nullable = false)
    private boolean gdprConsent;

    @Column(columnDefinition = "Boolean", nullable = false)
    private boolean cookiesConsent;

    @ManyToOne(fetch = FetchType.LAZY)
    @NotFound(action = NotFoundAction.IGNORE)
    @JoinColumn(name = "preferedCurrency", nullable = false)
    private Currency preferedCurrency;

    @Column(nullable = false)
    private String ipAddress;

    @Column(nullable = false)
    private String deviceToken;

    @Column(nullable = true)
    private Integer securityCode;


    @ManyToOne(fetch = FetchType.LAZY )
    @JoinColumn(name = "status", nullable = false)
    private Status status;

    @JoinColumn(name = "socialCode", nullable = false)
    private String socialCode;

    public String getSocialCode() {
        return socialCode;
    }

    public void setSocialCode(String socialCode) {
        this.socialCode = socialCode;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    public User(Integer userID) {
        this.userID = userID;
    }

    public Integer getUserID() {
        return userID;
    }

    public void setUserID(Integer userID) {
        this.userID = userID;
    }

    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public LocalDateTime getRegistrationDate() {
        return registrationDate;
    }

    public void setRegistrationDate(LocalDateTime registrationDate) {
        this.registrationDate = registrationDate;
    }

    public String getPseudo() {
        return pseudo;
    }

    public void setPseudo(String pseudo) {
        this.pseudo = pseudo;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Language getPreferedLanguage() {
        return preferedLanguage;
    }

    public void setPreferedLanguage(Language preferedLanguage) {
        this.preferedLanguage = preferedLanguage;
    }

    public boolean isGeneralConsent() {
        return generalConsent;
    }

    public void setGeneralConsent(boolean generalConsent) {
        this.generalConsent = generalConsent;
    }

    public boolean isGdprConsent() {
        return gdprConsent;
    }

    public void setGdprConsent(boolean gdprConsent) {
        this.gdprConsent = gdprConsent;
    }

    public boolean isCookiesConsent() {
        return cookiesConsent;
    }

    public void setCookiesConsent(boolean cookiesConsent) {
        this.cookiesConsent = cookiesConsent;
    }

    public Currency getPreferedCurrency() {
        return preferedCurrency;
    }

    public void setPreferedCurrency(Currency preferedCurrency) {
        this.preferedCurrency = preferedCurrency;
    }

    public String getIpAddress() {
        return ipAddress;
    }

    public void setIpAddress(String ipAddress) {
        this.ipAddress = ipAddress;
    }

    public String getDeviceToken() {
        return deviceToken;
    }

    public void setDeviceToken(String deviceToken) {
        this.deviceToken = deviceToken;
    }

    public Integer getSecurityCode() {
        return securityCode;
    }

    public void setSecurityCode(Integer securityCode) {
        this.securityCode = securityCode;
    }

    @Override
    public String toString() {
        return "User{" +
                "userID=" + userID +
                '}';
    }
}
