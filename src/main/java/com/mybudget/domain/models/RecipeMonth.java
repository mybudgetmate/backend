package com.mybudget.domain.models;

import javax.persistence.*;

@Entity
@Table(name = "recipe_month")
public class RecipeMonth {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long recipeMonthID;

    @ManyToOne(fetch= FetchType.LAZY )
    @JoinColumn(name = "recipe", nullable = false)
    private Recipe recipe;

    @Column(nullable = false)
    private Integer month;

    public RecipeMonth() {
    }

    public RecipeMonth(Long recipeMonthID, Recipe recipe, Integer month) {
        this.recipeMonthID = recipeMonthID;
        this.recipe = recipe;
        this.month = month;
    }

    public Long getRecipeMonthID() {
        return recipeMonthID;
    }

    public void setRecipeMonthID(Long recipeMonthID) {
        this.recipeMonthID = recipeMonthID;
    }

    public Recipe getRecipe() {
        return recipe;
    }

    public void setRecipe(Recipe recipe) {
        this.recipe = recipe;
    }

    public Integer getMonth() {
        return month;
    }

    public void setMonth(Integer month) {
        this.month = month;
    }
}
