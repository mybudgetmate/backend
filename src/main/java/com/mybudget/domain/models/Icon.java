package com.mybudget.domain.models;

import javax.persistence.*;

/**
 * @author Soumaya Izmar && Eva Tumia
 */
@Entity
@Table(name = "icons")
public class Icon {

    public Icon() {
    }

    public Icon(String iconCode, TYPE type) {
        this.iconCode = iconCode;
        this.type = type;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "iconID")
    private Integer iconID;

    @Column(nullable = false)
    private String iconCode;

    public enum TYPE {
        FONTAWESOME, PERSO
    }

    @Enumerated(EnumType.STRING)
    @Column(nullable = false)
    private TYPE type;


    @Override
    public String toString() {
        return "Icon{" +
                "iconID=" + iconID +
                ", iconCode='" + iconCode + '\'' +
                ", type=" + type +
                '}';
    }


    public Integer getIconID() {
        return iconID;
    }

    public void setIconID(Integer iconID) {
        this.iconID = iconID;
    }

    public String getIconCode() {
        return iconCode;
    }

    public void setIconCode(String iconCode) {
        this.iconCode = iconCode;
    }

    public TYPE getType() {
        return type;
    }

    public void setType(TYPE type) {
        this.type = type;
    }
}
