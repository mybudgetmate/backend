package com.mybudget.domain.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

public class AccountTypesSerializedDto {

    @JsonProperty
    private Integer accountTypeID;

    @JsonProperty
    private String name;

    @JsonProperty
    private String icon;

    public Integer getAccountTypeID() {
        return accountTypeID;
    }

    public void setAccountTypeID(Integer categoryID) {
        this.accountTypeID = categoryID;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getIcon() {
        return icon;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }

    public AccountTypesSerializedDto(Integer accountTypeID, String name, String icon) {
        this.accountTypeID = accountTypeID;
        this.name = name;
        this.icon = icon;
    }

    public AccountTypesSerializedDto() {
    }
}
