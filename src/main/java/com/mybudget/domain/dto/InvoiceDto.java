package com.mybudget.domain.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

public class InvoiceDto {

    @JsonProperty(required = true)
    private int invoiceID;

    @JsonProperty(required = true)
    private String name;

    @JsonProperty(required = true)
    private double amount;

    @JsonProperty(required = true)
    private String startDate;

    @JsonProperty(required = true)
    private String endDate;

    @JsonProperty(required = true)
    private String icon;

    public InvoiceDto() {
    }

    public InvoiceDto(int invoiceID, String name, double amount, String startDate, String endDate, String icon) {
        this.invoiceID = invoiceID;
        this.name = name;
        this.amount = amount;
        this.startDate = startDate;
        this.endDate = endDate;
        this.icon = icon;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getAmount() {
        return amount;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    public String getIcon() {
        return icon;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }

    public int getInvoiceID() {
        return invoiceID;
    }

    public void setInvoiceID(int invoiceID) {
        this.invoiceID = invoiceID;
    }
}
