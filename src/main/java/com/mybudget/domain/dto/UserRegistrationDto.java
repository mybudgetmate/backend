package com.mybudget.domain.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

/**
 * @author Soumaya Izmar
 */
public class UserRegistrationDto {

    @JsonProperty(required = true)
    private String firstName;
    @JsonProperty(required = true)
    private String lastName;
    @JsonProperty(required = true)
    private String password;
    @JsonProperty(required = true)
    private String passwordRepeat;
    @JsonProperty(required = true)
    private String email;
    @JsonProperty(required = true)
    private String preferedCurrency;
    @JsonProperty(required = true)
    private String preferedLanguage;
    @JsonProperty(required = true)
    private String ipAddress;
    @JsonProperty(required = true)
    private String deviceToken;
    @JsonProperty(required = true)
    private boolean generalConsent;
    @JsonProperty(required = true)
    private boolean gdprConsent;
    @JsonProperty(required = true)
    private boolean cookiesConsent;
    @JsonProperty(required = true)
    private String status;
    @JsonProperty(required = true)
    private String[] accounts;
    @JsonProperty(required = true)
    private List<Integer> subStatus;

    public UserRegistrationDto() {
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getPasswordRepeat() {
        return passwordRepeat;
    }

    public void setPasswordRepeat(String passwordRepeat) {
        this.passwordRepeat = passwordRepeat;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPreferedCurrency() {
        return preferedCurrency;
    }

    public void setPreferedCurrency(String preferedCurrency) {
        this.preferedCurrency = preferedCurrency;
    }

    public String getPreferedLanguage() {
        return preferedLanguage;
    }

    public void setPreferedLanguage(String preferedLanguage) {
        this.preferedLanguage = preferedLanguage;
    }

    public String getIpAddress() {
        return ipAddress;
    }

    public void setIpAddress(String ipAddress) {
        this.ipAddress = ipAddress;
    }

    public String getDeviceToken() {
        return deviceToken;
    }

    public void setDeviceToken(String deviceToken) {
        this.deviceToken = deviceToken;
    }

    public boolean isGeneralConsent() {
        return generalConsent;
    }

    public void setGeneralConsent(boolean generalConsent) {
        this.generalConsent = generalConsent;
    }

    public boolean isGdprConsent() {
        return gdprConsent;
    }

    public void setGdprConsent(boolean gdprConsent) {
        this.gdprConsent = gdprConsent;
    }

    public boolean isCookiesConsent() {
        return cookiesConsent;
    }

    public void setCookiesConsent(boolean cookiesConsent) {
        this.cookiesConsent = cookiesConsent;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }


    public String[] getAccounts() {
        return accounts;
    }

    public void setAccounts(String[] accounts) {
        this.accounts = accounts;
    }

    public List<Integer> getSubStatus() {
        return subStatus;
    }

    public void setSubStatus(List<Integer> subStatus) {
        this.subStatus = subStatus;
    }
}
