package com.mybudget.domain.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.mybudget.domain.models.ShoppingList;

import java.util.Date;
import java.util.List;

public class ShoppingListDto {

    @JsonProperty
    private Long id;

    @JsonProperty(required = true)
    private String type;

    @JsonProperty
    private String name;


    @JsonProperty
    private Date shoppingDate;


    @JsonProperty
    private Double total;

    @JsonProperty
    private String icon;

    @JsonProperty(required = true)
    private Boolean inBasket;

    @JsonProperty
    private List<IngredientDto> ingredients;

    @JsonProperty
    private List<RecipeDto> recipes;

    public ShoppingListDto(ShoppingList shoppingList, String icon) {
        this(shoppingList.getShoppingListID(), shoppingList.getName(), shoppingList.getShopping_date(),
                shoppingList.getTotal(), icon);
    }

    public ShoppingListDto(Long id, String name, Date shoppingDate, Double total,
                           String icon) {
        this.id = id;
        this.name = name;
        this.shoppingDate = shoppingDate;
        this.total = total;
        this.icon = icon;
        this.type = "shopping_list";
        this.inBasket = false;
    }

    public ShoppingListDto(Double total , List<IngredientDto> ingredients){
        this.total = total;
        this.ingredients = ingredients;
    }

    public ShoppingListDto(){}

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Double getTotal() {
        return total;
    }

    public void setTotal(Double total) {
        this.total = total;
    }

    public Date getShoppingDate() {
        return shoppingDate;
    }

    public void setShoppingDate(Date shoppingDate) {
        this.shoppingDate = shoppingDate;
    }

    public String getIcon() {
        return icon;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }

    public List<IngredientDto> getIngredients() {
        return ingredients;
    }

    public void setIngredients(List<IngredientDto> ingredients) {
        this.ingredients = ingredients;
    }

    public List<RecipeDto> getRecipes() {
        return recipes;
    }

    public void setRecipes(List<RecipeDto> recipes) {
        this.recipes = recipes;
    }

    @Override
    public String toString() {
        return "ShoppingListDto{" +
                "id=" + id +
                ", type='" + type + '\'' +
                ", name='" + name + '\'' +
                ", shoppingDate=" + shoppingDate +
                ", total=" + total +
                ", icon='" + icon + '\'' +
                ", inBasket=" + inBasket +
                ", ingredients=" + ingredients +
                ", recipes=" + recipes +
                '}';
    }
}
