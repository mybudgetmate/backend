package com.mybudget.domain.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

public class StringDto {
    @JsonProperty
    private String content;

    public String getContent() {
        return content;
    }

    public void setContent(String aString) {
        this.content = aString;
    }
}
