/**
 * @author Eva Tumia
 */

package com.mybudget.domain.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

public class CategoryDto {

    @JsonProperty(required = true)
    private String name;

    @JsonProperty(required = true)
    private double budget;

    @JsonProperty(required = true)
    private String type;

    @JsonProperty(required = true)
    private String icon;

    @JsonProperty(required = true)
    private String tokenUser;


    public CategoryDto() {
    }

    public CategoryDto(String name, double budget, String type, String icon, String tokenUser) {
        this.name = name;
        this.budget = budget;
        this.type = type;
        this.icon = icon;
        this.tokenUser= tokenUser;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getBudget() {
        return budget;
    }

    public void setBudget(double budget) {
        this.budget = budget;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getIcon() {
        return icon;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }

    public String getTokenUser() {
        return tokenUser;
    }

    public void setTokenUser(String tokenUser) {
        this.tokenUser = tokenUser;
    }
}
