package com.mybudget.domain.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.mybudget.domain.models.Icon;
import com.mybudget.domain.models.Ingredient;
import com.mybudget.domain.models.Recipe;
import com.mybudget.domain.models.translation.IngredientsTranslations;

import javax.persistence.*;

public class IngredientDto {
    @JsonProperty(required = true)
    private Long id;

    @JsonProperty(required = true)
    private String type;

    @JsonProperty(required = true)
    private String name;

    @JsonProperty(required = true)
    private String unit;

    @JsonProperty(required = true)
    private Float quantity;

    @JsonProperty(required = true)
    private Float calorificValue;

    @JsonProperty(required = true)
    private Float proteicValue;

    @JsonProperty(required = true)
    private Double price;

    @JsonProperty(required = true)
    private String icon;

    @JsonProperty(required = true)
    private Boolean inBasket;
    public IngredientDto(){}

    public IngredientDto(Long ingredientID, String name, String unit, Float quantity,
                         Float calorificValue, Float proteicValue, Double price, String icon) {
        this.id = ingredientID;
        this.name = name;
        this.unit = unit;
        this.quantity = quantity;
        this.calorificValue = calorificValue;
        this.proteicValue = proteicValue;
        this.price = price;
        this.icon = icon;
        this.type = "ingredient";
        this.inBasket = false;
    }

    public IngredientDto(Ingredient ingredient, String name, Double price, String icon) {
        this(ingredient.getIngredientID(), name, ingredient.getUnit(),
                ingredient.getQuantity(), ingredient.getCalorificValue(), ingredient.getProteicValue(),
                price, icon);
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUnit() {
        return unit;
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }

    public Float getQuantity() {
        return quantity;
    }

    public void setQuantity(Float quantity) {
        this.quantity = quantity;
    }

    public Float getCalorificValue() {
        return calorificValue;
    }

    public void setCalorificValue(Float calorificValue) {
        this.calorificValue = calorificValue;
    }

    public Float getProteicValue() {
        return proteicValue;
    }

    public void setProteicValue(Float proteicValue) {
        this.proteicValue = proteicValue;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public String getIcon() {
        return icon;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }

    @Override
    public String toString() {
        return "IngredientDto{" +
                "id=" + id +
                ", type='" + type + '\'' +
                ", name='" + name + '\'' +
                ", unit='" + unit + '\'' +
                ", quantity=" + quantity +
                ", calorificValue=" + calorificValue +
                ", proteicValue=" + proteicValue +
                ", price=" + price +
                ", icon='" + icon + '\'' +
                ", inBasket=" + inBasket +
                '}';
    }
}
