package com.mybudget.domain.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

public class TransactionCSV {

    @JsonProperty(required = true)
    private List<List<String>> listTransaction;

    @JsonProperty(required = true)
    private String account;


    public List<List<String>> getListTransaction() {
        return listTransaction;
    }

    public void setListTransaction(List<List<String>> listTransaction) {
        this.listTransaction = listTransaction;
    }

    public String getAccount() {
        return account;
    }

    public void setAccount(String account) {
        this.account = account;
    }
}
