package com.mybudget.domain.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.mybudget.domain.models.RecurringTransaction;

import java.time.LocalDateTime;
import java.util.List;

/**
 * @author Eva Tumia
 */
public class TransactionDto {

    @JsonProperty(required = true)
    private Integer transactionID;

    @JsonProperty(required = true)
    private Integer accountID;

    @JsonProperty(required = true)
    private String amount;

    @JsonProperty(required = true)
    private String executionDate;

    @JsonProperty(required = true)
    private String valueDate;

    @JsonProperty(required = true)
    private String type;

    @JsonProperty(required = true)
    private String icon;

    @JsonProperty(required = true)
    private Integer valueDateDay;

    @JsonProperty(required = true)
    private Integer valueDateMonth;

    @JsonProperty(required = true)
    private Integer valueDateYear;

    @JsonProperty(required = true)
    private List<CategorySerializedDto> categories;

    @JsonProperty(required=true)
    private String accountName;

    @JsonProperty
    private String balance;

    @JsonProperty
    private String recurringTransactionName;

    @JsonProperty
    private String recurringTransactionEndDate;

    @JsonProperty
    private String name;

    public TransactionDto() {
    }

    public TransactionDto(Integer transactionID, Integer accountID, String amount, String executionDate, String valueDate, String type, String name) {
        this.transactionID = transactionID;
        this.accountID = accountID;
        this.amount = amount;
        this.executionDate = executionDate;
        this.valueDate = valueDate;
        this.type = type;
        this.name=name;
    }

    public TransactionDto(
            Integer transactionID,
            Integer accountID,
            String amount,
            String executionDate,
            String valueDate,
            String type,
            String icon,
            String accountName,
            Integer valueDateDay,
            Integer valueDateMonth,
            Integer valueDateYear,
            List<CategorySerializedDto> categories,
            String recurringTransactionName,
            String recurringTransactionEndDate,
            String name) {
        this.transactionID = transactionID;
        this.accountID = accountID;
        this.amount = amount;
        this.executionDate = executionDate;
        this.valueDate = valueDate;
        this.type = type;
        this.icon = icon;
        this.accountName = accountName;
        this.valueDateDay = valueDateDay;
        this.valueDateMonth = valueDateMonth;
        this.valueDateYear = valueDateYear;
        this.categories = categories;
        this.recurringTransactionName = recurringTransactionName;
        this.recurringTransactionEndDate = recurringTransactionEndDate;
        this.name=name;
    }

    public Integer getTransactionID() {
        return transactionID;
    }

    public void setTransactionID(Integer transactionID) {
        this.transactionID = transactionID;
    }

    public Integer getAccountID() {
        return accountID;
    }

    public void setAccountID(Integer accountID) {
        this.accountID = accountID;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getExecutionDate() {
        return executionDate;
    }

    public void setExecutionDate(String executionDate) {
        this.executionDate = executionDate;
    }

    public String getValueDate() {
        return valueDate;
    }

    public void setValueDate(String valueDate) {
        this.valueDate = valueDate;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getIcon() {
        return icon;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }

    public Integer getValueDateDay() {
        return valueDateDay;
    }

    public void setValueDateDay(Integer valueDateDay) {
        this.valueDateDay = valueDateDay;
    }

    public Integer getValueDateMonth() {
        return valueDateMonth;
    }

    public void setValueDateMonth(Integer valueDateMonth) {
        this.valueDateMonth = valueDateMonth;
    }

    public List<CategorySerializedDto> getCategories() {
        return categories;
    }

    public void setCategories(List<CategorySerializedDto> categories) {
        this.categories = categories;
    }

    public String getAccountName() {
        return accountName;
    }

    public void setAccountName(String accountName) {
        this.accountName = accountName;
    }

    public Integer getValueDateYear() {
        return valueDateYear;
    }

    public void setValueDateYear(Integer valueDateYear) {
        this.valueDateYear = valueDateYear;
    }

    public String getBalance() {
        return balance;
    }

    public void setBalance(String balance) {
        this.balance = balance;
    }

    public String getRecurringTransactionName() {
        return recurringTransactionName;
    }

    public void setRecurringTransactionName(String recurringTransactionName) {
        this.recurringTransactionName = recurringTransactionName;
    }

    public String getRecurringTransactionEndDate() {
        return recurringTransactionEndDate;
    }

    public void setRecurringTransactionEndDate(String recurringTransactionEndDate) {
        this.recurringTransactionEndDate = recurringTransactionEndDate;
    }

    @Override
    public String toString() {
        return "TransactionDto{" +
                "transactionID=" + transactionID +
                ", accountID=" + accountID +
                ", amount='" + amount + '\'' +
                ", executionDate='" + executionDate + '\'' +
                ", valueDate='" + valueDate + '\'' +
                ", type='" + type + '\'' +
                ", icon='" + icon + '\'' +
                ", valueDateDay=" + valueDateDay +
                ", valueDateMonth=" + valueDateMonth +
                ", valueDateYear=" + valueDateYear +
                ", categories=" + categories +
                ", accountName='" + accountName + '\'' +
                ", balance='" + balance + '\'' +
                ", recurringTransactionName='" + recurringTransactionName + '\'' +
                ", recurringTransactionEndDate=" + recurringTransactionEndDate +
                '}';
    }
}
