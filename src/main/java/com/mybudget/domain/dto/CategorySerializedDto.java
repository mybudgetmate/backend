package com.mybudget.domain.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

public class CategorySerializedDto {

    @JsonProperty
    private Integer categoryID;

    @JsonProperty
    private String name;

    @JsonProperty
    private double budget;

    @JsonProperty
    private String type;

    @JsonProperty
    private String icon;

    @JsonProperty
    private String user;

    @JsonProperty
    private int idParentCategory;

    @JsonProperty
    private double amount;


    public CategorySerializedDto() {

    }

    public CategorySerializedDto(Integer categoryID, String name, double budget, String type, String icon, String user, int idParentCategory) {
        this.categoryID = categoryID;
        this.name = name;
        this.budget = budget;
        this.type = type;
        this.icon = icon;
        this.user = user;
        this.idParentCategory = idParentCategory;
    }


    public Integer getCategoryID() {
        return categoryID;
    }

    public void setCategoryID(Integer categoryID) {
        this.categoryID = categoryID;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getBudget() {
        return budget;
    }

    public void setBudget(double budget) {
        this.budget = budget;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getIcon() {
        return icon;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public int getIdParentCategory() {
        return idParentCategory;
    }

    public void setIdParentCategory(int idParentCategory) {
        this.idParentCategory = idParentCategory;
    }

    public double getAmount() {
        return amount;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }

    @Override
    public String toString() {
        return "CategorySerializedDto{" +
                "categoryID=" + categoryID +
                ", name='" + name + '\'' +
                ", budget=" + budget +
                ", type='" + type + '\'' +
                ", icon='" + icon + '\'' +
                ", user='" + user + '\'' +
                ", idParentCategory=" + idParentCategory +
                ", amount=" + amount +
                '}';
    }
}
