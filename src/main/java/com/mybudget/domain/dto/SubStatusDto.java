package com.mybudget.domain.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

public class SubStatusDto {
    @JsonProperty
    private String name;
    @JsonProperty
    private int id;


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
}
