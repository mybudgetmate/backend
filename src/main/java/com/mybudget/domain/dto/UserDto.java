package com.mybudget.domain.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.mybudget.domain.models.Status;
import com.mybudget.domain.models.User;

import javax.persistence.Column;
import java.time.LocalDateTime;

public class UserDto {

    @JsonProperty(required = true)
    private String pseudo;

    private  String socialCode;
    @JsonProperty(required = true)
    private String firstname;
    @JsonProperty(required = true)
    private String lastname;
    @JsonProperty(required = true)
    private LocalDateTime registrationDate;
    @JsonProperty(required = true)
    private String email;
    @JsonProperty(required = true)
    private String status;

    public UserDto() {
    }

    public UserDto(String pseudo, String socialCode, String firstname, String lastname, LocalDateTime registrationDate, String email, String status) {
        this.pseudo = pseudo;
        this.socialCode = socialCode;
        this.firstname = firstname;
        this.lastname = lastname;
        this.registrationDate = registrationDate;
        this.email = email;
        this.status = status;
    }

    public UserDto(User user, String status) {
        this(user.getPseudo(), user.getSocialCode(), user.getFirstname(), user.getLastname(),
                user.getRegistrationDate(), user.getEmail(), status);
    }

    public String getPseudo() {
        return pseudo;
    }

    public void setPseudo(String pseudo) {
        this.pseudo = pseudo;
    }

    public String getSocialCode() {
        return socialCode;
    }

    public void setSocialCode(String socialCode) {
        this.socialCode = socialCode;
    }

    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public LocalDateTime getRegistrationDate() {
        return registrationDate;
    }

    public void setRegistrationDate(LocalDateTime registrationDate) {
        this.registrationDate = registrationDate;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    @Override
    public String toString() {
        return "UserDto{" +
                "pseudo='" + pseudo + '\'' +
                ", socialCode='" + socialCode + '\'' +
                ", firstname='" + firstname + '\'' +
                ", lastname='" + lastname + '\'' +
                ", registrationDate=" + registrationDate +
                ", email='" + email + '\'' +
                ", status='" + status + '\'' +
                '}';
    }
}
