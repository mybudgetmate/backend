package com.mybudget.domain.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.mybudget.domain.models.Ingredient;

import javax.annotation.Nullable;
import java.util.List;

public class RecipeDto {
    @JsonProperty(required = true)
    private Long id;

    @JsonProperty(required = true)
    private String type;

    @JsonProperty(required = true)
    private String name;

    @JsonProperty(required = true)
    private Integer servings;

    @JsonProperty(required = true)
    private Float calorificValue;

    @JsonProperty(required = true)
    private Float proteicValue;

    @JsonProperty(required = true)
    private Double price;

    @JsonProperty(required = true)
    private String icon;

    @JsonProperty(required = true)
    private String url;

    @JsonProperty(required = true)
    private Boolean inBasket;

    @JsonProperty(required = true)
    private List<IngredientDto> ingredients;

    @JsonProperty
    @Nullable
    private String preparation;

    public RecipeDto() {
    }

    public RecipeDto(Long id, String name, Integer servings, Float calorificValue,
                     Float proteicValue, Double price, String icon, String url, List<IngredientDto> ingredients,String preparation) {
        this.id = id;
        this.name = name;
        this.servings = servings;
        this.calorificValue = calorificValue;
        this.proteicValue = proteicValue;
        this.price = price;
        this.icon = icon;
        this.url = url;
        this.ingredients = ingredients;
        this.type = "recipe";
        this.inBasket = false;
        this.preparation = preparation;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getServings() {
        return servings;
    }

    public void setServings(Integer servings) {
        this.servings = servings;
    }

    public Float getCalorificValue() {
        return calorificValue;
    }

    public void setCalorificValue(Float calorificValue) {
        this.calorificValue = calorificValue;
    }

    public Float getProteicValue() {
        return proteicValue;
    }

    public void setProteicValue(Float proteicValue) {
        this.proteicValue = proteicValue;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public String getIcon() {
        return icon;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public List<IngredientDto> getIngredients() {
        return ingredients;
    }

    public void setIngredients(List<IngredientDto> ingredients) {
        this.ingredients = ingredients;
    }


    public String getPreparation() {
        return preparation;
    }

    public void setPreparation(String preparation) {
        this.preparation = preparation;
    }

    @Override
    public String toString() {
        return "RecipeDto{" +
                "id=" + id +
                ", type='" + type + '\'' +
                ", name='" + name + '\'' +
                ", servings=" + servings +
                ", calorificValue=" + calorificValue +
                ", proteicValue=" + proteicValue +
                ", price=" + price +
                ", icon='" + icon + '\'' +
                ", url='" + url + '\'' +
                ", inBasket=" + inBasket +
                ", ingredients=" + ingredients +
                ", preparation=" + preparation +
                '}';
    }
}
