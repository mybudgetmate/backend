package com.mybudget.domain.dto;

import com.mybudget.domain.models.User;

public class ProjectDto {


    private Integer projectID;

    private String name;

    private double requiredBudget;

    private double obtainedBudget;

    private String startDate;

    private String endDate;

    private User owner;

    private Integer parentProject;

    private String image;

    public Integer getProjectID() {
        return projectID;
    }

    public void setProjectID(Integer projectID) {
        this.projectID = projectID;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getRequiredBudget() {
        return requiredBudget;
    }

    public void setRequiredBudget(double requiredBudget) {
        this.requiredBudget = requiredBudget;
    }

    public double getObtainedBudget() {
        return obtainedBudget;
    }

    public void setObtainedBudget(double obtainedBudget) {
        this.obtainedBudget = obtainedBudget;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    public User getOwner() {
        return owner;
    }

    public void setOwner(User owner) {
        this.owner = owner;
    }


    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public Integer getParentProject() {
        return parentProject;
    }

    public void setParentProject(Integer parentProject) {
        this.parentProject = parentProject;
    }
}
