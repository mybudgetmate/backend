package com.mybudget.domain.dto;

public class FriendShipDto {

    private String friendPseudo;

    private String userPseudo;

    private int receiverID;
    private int senderID;
    private String requestDate;
    private String acceptDate;

    private String state;

    public FriendShipDto() {
    }

    public String getFriendPseudo() {
        return friendPseudo;
    }

    public void setFriendPseudo(String friendPseudo) {
        this.friendPseudo = friendPseudo;
    }

    public String getUserPseudo() {
        return userPseudo;
    }

    public void setUserPseudo(String userPseudo) {
        this.userPseudo = userPseudo;
    }

    public int getReceiverID() {
        return receiverID;
    }

    public void setReceiverID(int receiverID) {
        this.receiverID = receiverID;
    }

    public String getRequestDate() {
        return requestDate;
    }

    public void setRequestDate(String requestDate) {
        this.requestDate = requestDate;
    }

    public String getAcceptDate() {
        return acceptDate;
    }

    public void setAcceptDate(String acceptDate) {
        this.acceptDate = acceptDate;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public int getSenderID() {
        return senderID;
    }

    public void setSenderID(int senderID) {
        this.senderID = senderID;
    }
}
