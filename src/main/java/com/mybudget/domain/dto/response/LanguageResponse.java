package com.mybudget.domain.dto.response;

public class LanguageResponse {

    private String languageCode;
    private String languageName;

    public LanguageResponse() {

    }

    public String getLanguageCode() {
        return languageCode;
    }

    public void setLanguageCode(String languageCode) {
        this.languageCode = languageCode;
    }

    public String getLanguageName() {
        return languageName;
    }

    public void setLanguageName(String languageName) {
        this.languageName = languageName;
    }
}
