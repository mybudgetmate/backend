package com.mybudget.domain.dto.response;

import com.mybudget.domain.dto.ProjectDto;

import java.util.List;

public class SubProjectResponse {


    List<ProjectDto>  projects;
    ProjectDto currentProject;
    Integer p;

    public SubProjectResponse(List<ProjectDto> projects, ProjectDto currentProject, Integer p) {
        this.projects = projects;
        this.currentProject = currentProject;
        this.p = p;
    }

    public List<ProjectDto> getProjects() {
        return projects;
    }

    public void setProjects(List<ProjectDto> projects) {
        this.projects = projects;
    }

    public ProjectDto getCurrentProject() {
        return currentProject;
    }

    public void setCurrentProject(ProjectDto currentProject) {
        this.currentProject = currentProject;
    }

    public Integer getP() {
        return p;
    }

    public void setP(Integer p) {
        this.p = p;
    }
}
