package com.mybudget.domain.dto.response;

public class BudgetResponse {

    private String budgetToSpend;
    private String budgetAlreadySpent;

    public BudgetResponse(String budgetToSpend, String budgetAlreadySpent) {
        this.budgetToSpend = budgetToSpend;
        this.budgetAlreadySpent = budgetAlreadySpent;
    }

    public BudgetResponse() {
    }

    public String getBudgetToSpend() {
        return budgetToSpend;
    }

    public void setBudgetToSpend(String budgetToSpend) {
        this.budgetToSpend = budgetToSpend;
    }

    public String getBudgetAlreadySpent() {
        return budgetAlreadySpent;
    }

    public void setBudgetAlreadySpent(String budgetAlreadySpent) {
        this.budgetAlreadySpent = budgetAlreadySpent;
    }
}
