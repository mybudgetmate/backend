package com.mybudget.domain.dto.response;
/**
 * @author Eva Tumia
 */
public class CurrencyResponse {

    private String name;
    private String icon;

    public CurrencyResponse(String name, String icon) {
        this.name = name;
        this.icon = icon;
    }

    public CurrencyResponse() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getIcon() {
        return icon;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }
}
