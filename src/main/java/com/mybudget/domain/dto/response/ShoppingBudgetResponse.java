package com.mybudget.domain.dto.response;

import java.util.List;

public class ShoppingBudgetResponse {

    private List<ShoppingWeekResponse>  transactionsPerWeek;

    private List<CategoryTransactionResponse> allShoppingTransactions;

    private Double totalBudget;
    private Double totalAmount;

    public List<ShoppingWeekResponse> getTransactionsPerWeek() {
        return transactionsPerWeek;
    }

    public void setTransactionsPerWeek(List<ShoppingWeekResponse> transactionsPerWeek) {
        this.transactionsPerWeek = transactionsPerWeek;
    }

    public List<CategoryTransactionResponse> getAllShoppingTransactions() {
        return allShoppingTransactions;
    }

    public void setAllShoppingTransactions(List<CategoryTransactionResponse> allShoppingTransactions) {
        this.allShoppingTransactions = allShoppingTransactions;
    }

    public Double getTotalBudget() {
        return totalBudget;
    }

    public void setTotalBudget(Double totalBudget) {
        this.totalBudget = totalBudget;
    }

    public Double getTotalAmount() {
        return totalAmount;
    }

    public void setTotalAmount(Double totalAmount) {
        this.totalAmount = totalAmount;
    }
}
