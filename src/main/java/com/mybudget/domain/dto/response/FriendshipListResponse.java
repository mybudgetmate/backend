package com.mybudget.domain.dto.response;

import com.mybudget.domain.dto.FriendShipDto;

import java.util.List;

public class FriendshipListResponse {

    private List<FriendShipDto> demandList;
    private List<FriendShipDto> pendingList;

    public FriendshipListResponse() {
    }

    public List<FriendShipDto> getDemandList() {
        return demandList;
    }

    public void setDemandList(List<FriendShipDto> demandList) {
        this.demandList = demandList;
    }

    public List<FriendShipDto> getPendingList() {
        return pendingList;
    }

    public void setPendingList(List<FriendShipDto> pendingList) {
        this.pendingList = pendingList;
    }
}
