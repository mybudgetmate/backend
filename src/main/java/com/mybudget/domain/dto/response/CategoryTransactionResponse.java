package com.mybudget.domain.dto.response;

import java.util.List;

public class CategoryTransactionResponse {


    private String name;
    private Integer nbTransactions;
    private Double totalAmount;
    private String icon;
    private List<CategoryTransactionResponse> list;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getNbTransactions() {
        return nbTransactions;
    }

    public void setNbTransactions(Integer nbTransactions) {
        this.nbTransactions = nbTransactions;
    }

    public Double getTotalAmount() {
        return totalAmount;
    }

    public void setTotalAmount(Double totalAmount) {
        this.totalAmount = totalAmount;
    }

    public String getIcon() {
        return icon;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }

    public List<CategoryTransactionResponse> getList() {
        return list;
    }

    public void setList(List<CategoryTransactionResponse> list) {
        this.list = list;
    }
}
