package com.mybudget.domain.dto.response;

/**
 * @author Soumaya Izmar
 */
public class LoginTranslationResponse {

    private String email;
    private String password;
    private String connection;
    private String login;
    private String emailRequired;
    private String passwordRequired;
    private String successfulConnection;
    private String signUp;
    private String newAccount;

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getConnection() {
        return connection;
    }

    public void setConnection(String connection) {
        this.connection = connection;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getEmailRequired() {
        return emailRequired;
    }

    public void setEmailRequired(String emailRequired) {
        this.emailRequired = emailRequired;
    }

    public String getPasswordRequired() {
        return passwordRequired;
    }

    public void setPasswordRequired(String passwordRequired) {
        this.passwordRequired = passwordRequired;
    }

    public String getSuccessfulConnection() {
        return successfulConnection;
    }

    public void setSuccessfulConnection(String successfulConnection) {
        this.successfulConnection = successfulConnection;
    }

    public String getSignUp() {
        return signUp;
    }

    public void setSignUp(String signUp) {
        this.signUp = signUp;
    }

    public String getNewAccount() {
        return newAccount;
    }

    public void setNewAccount(String newAccount) {
        this.newAccount = newAccount;
    }
}
