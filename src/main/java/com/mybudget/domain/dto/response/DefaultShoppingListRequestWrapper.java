package com.mybudget.domain.dto.response;

import com.mybudget.domain.dto.ShoppingListDto;

public class DefaultShoppingListRequestWrapper {
    private String languageCode;

    private ShoppingListDto shoppingList;

    public DefaultShoppingListRequestWrapper() {
    }

    public DefaultShoppingListRequestWrapper(String languageCode, ShoppingListDto shoppingList) {
        this.languageCode = languageCode;
        this.shoppingList = shoppingList;
    }

    public String getLanguageCode() {
        return languageCode;
    }

    public void setLanguageCode(String languageCode) {
        this.languageCode = languageCode;
    }

    public ShoppingListDto getShoppingList() {
        return shoppingList;
    }

    public void setShoppingList(ShoppingListDto shoppingList) {
        this.shoppingList = shoppingList;
    }

    @Override
    public String toString() {
        return "DefaultShoppingListRequestWrapper{" +
                "languageCode='" + languageCode + '\'' +
                ", shoppingList=" + shoppingList +
                '}';
    }
}
