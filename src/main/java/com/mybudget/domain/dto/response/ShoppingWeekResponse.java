package com.mybudget.domain.dto.response;

public class ShoppingWeekResponse {

    private Double[] list;
    private String date;

    public ShoppingWeekResponse(Double[] list, String date) {
        this.list = list;
        this.date = date;
    }

    public Double[] getList() {
        return list;
    }

    public void setList(Double[] list) {
        this.list = list;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }
}
