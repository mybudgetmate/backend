package com.mybudget.domain.dto.response;

/**
 * @author: Eva Tumia
 */

public class ObjectResponse
{
    private int id;

    public ObjectResponse(int id){
        this.id=id;
    }

    public int getId(){
        return this.id;
    }

    public void setId(int id){
        this.id=id;
    }
}
