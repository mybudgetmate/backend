package com.mybudget.domain.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

public class AccountDto {

    @JsonProperty
    private Integer accountId;

    @JsonProperty(required = true)
    private String name;

    @JsonProperty(required = true)
    private Integer typeID;

    @JsonProperty
    private String typeName;
    @JsonProperty
    private String typeIcon;

    @JsonProperty
    private Integer owner;

    public Integer getAccountID() {
        return accountId;
    }

    public AccountDto(Integer accountID, String name, Integer typeID, String typeName, String typeIcon, Integer owner) {
        this.accountId = accountID;
        this.name = name;
        this.typeID = typeID;
        this.typeName = typeName;
        this.typeIcon = typeIcon;
        this.owner = owner;
    }

    public AccountDto() {
    }

    public void setAccountID(Integer accountID) {
        this.accountId = accountID;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getTypeID() {
        return typeID;
    }

    public void setTypeID(Integer typeID) {
        this.typeID = typeID;
    }

    public String getTypeName() {
        return typeName;
    }

    public void setTypeName(String typeName) {
        this.typeName = typeName;
    }

    public String getTypeIcon() {
        return typeIcon;
    }

    public void setTypeIcon(String typeIcon) {
        this.typeIcon = typeIcon;
    }

    public Integer getOwner() {
        return owner;
    }

    public void setOwner(Integer owner) {
        this.owner = owner;
    }
}