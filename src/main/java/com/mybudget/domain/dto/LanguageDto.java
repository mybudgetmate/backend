package com.mybudget.domain.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

public class LanguageDto {

    @JsonProperty(required = true)
    private String languageCode;

    public LanguageDto() {
    }

    public LanguageDto(String languageCode) {
        this.languageCode = languageCode;
    }

    public String getLanguageCode() {
        return languageCode;
    }

    public void setLanguageCode(String languageCode) {
        this.languageCode = languageCode;
    }
}
