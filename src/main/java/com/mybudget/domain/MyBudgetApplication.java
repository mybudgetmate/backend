/**
 * @author Eva Tumia
 */
package com.mybudget.domain;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;


@SpringBootApplication
@EnableScheduling
public class MyBudgetApplication {

    public static void main(String[] args) {

        SpringApplication.run(MyBudgetApplication.class, args);
        if (Integer.toString(5000).equals(System.getenv("APP_PORT"))) {
            System.out.println("The server has started on http://localhost:5000/api/");
        }
    }

    @Bean
    public WebMvcConfigurer corsConfigurer() {
        return new WebMvcConfigurer() {
            @Override
            public void addCorsMappings(CorsRegistry registry) {
                registry.addMapping("/**")
                        .allowedOrigins("*")
                        .allowedMethods("*").allowedHeaders("*");
            }
        };
    }

}
