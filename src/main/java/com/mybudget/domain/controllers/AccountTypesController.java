package com.mybudget.domain.controllers;


import com.mybudget.domain.dto.AccountTypesSerializedDto;
import com.mybudget.domain.dto.response.ErrorResponse;
import com.mybudget.domain.services.AccountTypesService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(path="api/accountTypes")
public class AccountTypesController {

    @Autowired
    private AccountTypesService accountTypesService;

    /**
     * Endpoint to use to get all the accountTypes
     * @return A ResponseEntity object containing a list of AccountTypesSerializedDto
     * @see AccountTypesSerializedDto
     */
    @GetMapping("/all")
    public ResponseEntity<?> getAll() {
        try {
            return ResponseEntity.status(HttpStatus.OK).body(accountTypesService.getAll());
        }catch(Exception error){
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(new ErrorResponse(error.getMessage()));
        }
    }

}
