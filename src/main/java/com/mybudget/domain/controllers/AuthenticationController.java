package com.mybudget.domain.controllers;

import com.mybudget.domain.dto.LoginDto;
import com.mybudget.domain.dto.StringDto;
import com.mybudget.domain.dto.UserDto;
import com.mybudget.domain.dto.UserRegistrationDto;
import com.mybudget.domain.dto.response.AuthenticationResponse;
import com.mybudget.domain.dto.response.ErrorResponse;
import com.mybudget.domain.exceptions.FatalException;
import com.mybudget.domain.exceptions.LoginException;
import com.mybudget.domain.exceptions.RegistrationException;
import com.mybudget.domain.services.AuthenticationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import static org.springframework.http.HttpStatus.*;

/**
 * @author Soumaya Izmar
 */
@RestController
@RequestMapping(path = "api/auth")
public class AuthenticationController {

    @Autowired
    private AuthenticationService authenticationService;

    /**
     * Endpoint to use to login a user
     *
     * @param loginRequest: login data to be checked
     * @return A ResponseEntity object containing a AuthenticationResponse
     * @see AuthenticationResponse
     */
    @PostMapping("/login")
    public ResponseEntity<?> loginUser(@RequestBody LoginDto loginRequest) {
        try {
            AuthenticationResponse loginResponse = authenticationService.loginUser(loginRequest);
            return ResponseEntity.status(OK).body(loginResponse);
        } catch (LoginException error) {
            return ResponseEntity.status(400).body(new ErrorResponse(error.getMessage()));
        } catch (ClassNotFoundException error) {
            return ResponseEntity.status(INTERNAL_SERVER_ERROR).body(new ErrorResponse(error.getMessage()));
        }

    }

    /**
     * Endpoint to use to register a user
     *
     * @param registrationRequest: registration data to be checked
     * @return A ResponseEntity object containing a AuthenticationResponse
     * @see AuthenticationResponse
     */
    @PostMapping("/register")
    public ResponseEntity<?> registerUser(@RequestBody UserRegistrationDto registrationRequest) {

        try {
            AuthenticationResponse registrationResponse = authenticationService.registerUser(registrationRequest);
            return ResponseEntity.status(OK).body(registrationResponse);
        } catch (RegistrationException error) {

            return ResponseEntity.status(400).body(new ErrorResponse(error.getMessage()));
        } catch (ClassNotFoundException | FatalException error) {
            return ResponseEntity.status(INTERNAL_SERVER_ERROR).body(new ErrorResponse(error.getMessage()));
        }
    }

    /**
     * Endpoint to use to register a user
     *
     * @param userDto: user data to be change
     * @return A ResponseEntity object containing a UserDto
     * @see UserDto
     */
    @PatchMapping("/pseudo/")
    public ResponseEntity<?> patchPseudo(@RequestBody UserDto userDto) {
        try {
            UserDto userDtoResponse = authenticationService.changePseudo(userDto.getPseudo());
            return ResponseEntity.status(OK).body(userDtoResponse);
        } catch (RegistrationException error) {
            return ResponseEntity.status(400).body(new ErrorResponse(error.getMessage()));
        } catch (Exception error) {
            return ResponseEntity.status(INTERNAL_SERVER_ERROR).body(new ErrorResponse(error.getMessage()));
        }
    }

    /**
     * Endpoint to use to get user pseudo
     *
     * @return A ResponseEntity object containing a UserDto
     * @see UserDto
     */
    @GetMapping("/pseudo/")
    public ResponseEntity<?> getPseudo() {
        try {
            UserDto userDtoResponse = authenticationService.getPseudo();
            return ResponseEntity.status(OK).body(userDtoResponse);
        } catch (RegistrationException error) {
            return ResponseEntity.status(400).body(new ErrorResponse(error.getMessage()));
        } catch (Exception error) {
            return ResponseEntity.status(INTERNAL_SERVER_ERROR).body(new ErrorResponse(error.getMessage()));
        }
    }

    /**
     * Endpoint to use to get user social code
     *
     * @return A ResponseEntity object containing a UserDto
     * @see UserDto
     */
    @GetMapping("/socialCode/")
    public ResponseEntity<?> getSocialCode() {
        try {
            UserDto userDtoResponse = authenticationService.getSocialCode();
            return ResponseEntity.status(OK).body(userDtoResponse);
        } catch (RegistrationException error) {
            return ResponseEntity.status(400).body(new ErrorResponse(error.getMessage()));
        } catch (Exception error) {
            return ResponseEntity.status(INTERNAL_SERVER_ERROR).body(new ErrorResponse(error.getMessage()));
        }
    }

    @DeleteMapping("/delete/")
    public ResponseEntity<?> deleteAccount() {
        try {
            authenticationService.deleteAccount();
            return ResponseEntity.status(NO_CONTENT).build();
        } catch (RegistrationException error) {
            return ResponseEntity.status(400).body(new ErrorResponse(error.getMessage()));
        } catch (Exception error) {
            return ResponseEntity.status(INTERNAL_SERVER_ERROR).body(new ErrorResponse(error.getMessage()));
        }
    }

    @PatchMapping("/password/")
    public ResponseEntity<?> patchUserPassword(@RequestBody StringDto stringDto) {
        try {
            authenticationService.changePassword(stringDto);
            return ResponseEntity.status(NO_CONTENT).build();
        } catch (RegistrationException error) {
            return ResponseEntity.status(400).body(new ErrorResponse(error.getMessage()));
        } catch (Exception error) {
            return ResponseEntity.status(INTERNAL_SERVER_ERROR).body(new ErrorResponse(error.getMessage()));
        }
    }

    /**
     * Endpoint to get infos of the current user
     *
     * @param languageCode: language code of the language used by the user
     * @return A ResponseEntity object containing a UserDto
     * @see UserDto
     */
    @GetMapping("/user/")
    public ResponseEntity<?> gerUserInfos(@RequestParam String languageCode) {
        try {
            UserDto userDtoResponse = authenticationService.getInfos(languageCode);
            return ResponseEntity.status(OK).body(userDtoResponse);
        } catch (RegistrationException error) {
            return ResponseEntity.status(400).body(new ErrorResponse(error.getMessage()));
        } catch (Exception error) {
            return ResponseEntity.status(INTERNAL_SERVER_ERROR).body(new ErrorResponse(error.getMessage()));
        }
    }

}

