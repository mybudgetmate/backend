package com.mybudget.domain.controllers;


import com.mybudget.domain.dto.TransactionCSV;
import com.mybudget.domain.dto.TransactionDto;
import com.mybudget.domain.dto.response.BudgetResponse;
import com.mybudget.domain.dto.response.ErrorResponse;
import com.mybudget.domain.dto.response.ShoppingBudgetResponse;
import com.mybudget.domain.dto.response.ShoppingWeekResponse;
import com.mybudget.domain.exceptions.AccountException;
import com.mybudget.domain.exceptions.CategoriesException;
import com.mybudget.domain.exceptions.TransactionException;
import com.mybudget.domain.models.User;
import com.mybudget.domain.services.AuthenticationService;
import com.mybudget.domain.services.TransactionService;
import com.mybudget.domain.services.TranslationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Locale;
import java.util.Map;


/**
 * @author: Eva Tumia
 */

@RestController
@RequestMapping(path = "api/transactions")
public class TransactionController {

    @Autowired
    private TransactionService transactionService;
    @Autowired
    private TranslationService translationService;
    @Autowired
    private AuthenticationService authenticationService;

    /**
     * Endpoint to use to add a new transaction
     * @param transactionDto: transaction to be added
     * @return A ResponseEntity object containing a TransactionDto
     * @see TransactionDto
     */
    @PostMapping("/add")
    public ResponseEntity<?> addTransaction(@RequestBody TransactionDto transactionDto) {
        try {
            return ResponseEntity.status(HttpStatus.OK).body(transactionService.addTransaction(transactionDto));
        } catch (CategoriesException error) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(new ErrorResponse(error.getMessage()));
        } catch (AccountException error) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(new ErrorResponse(error.getMessage()));
        } catch (Exception error) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(new ErrorResponse(error.getMessage()));
        }
    }

    /**
     * Endpoint to use to get the current user's balance
     * @return A ResponseEntity object containing a TransactionDto
     * @see TransactionDto
     */
    @GetMapping("/userBalance")
    public ResponseEntity<?> getUserBalance() {
        try {
            return ResponseEntity.status(HttpStatus.OK).body(transactionService.getUserBalance());
        } catch (Exception error) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(new ErrorResponse(error.getMessage()));
        }
    }

    /**
     * Endpoint to use to get all the transactions of the current user
     * @return A ResponseEntity object containing a Map<LocalDate,TransactionDto>
     * @see TransactionDto
     */
    @GetMapping("/allByUser")
    public ResponseEntity<?> getAllByUser() {
        try {
            return ResponseEntity.status(HttpStatus.OK).body(transactionService.getAllByUser());
        } catch (Exception error) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(new ErrorResponse(error.getMessage()));
        }
    }

    /**
     * Endpoint to use to update a transaction
     * @param transactionDto: transaction to be updated
     * @return A ResponseEntity object containing a TransactionDto
     * @see TransactionDto
     */
    @PatchMapping("/patch")
    public ResponseEntity<?> patchTransaction(@RequestBody TransactionDto transactionDto) {
        try {
            return ResponseEntity.status(HttpStatus.OK).body(transactionService.patchTransaction(transactionDto));
        } catch (AccountException | TransactionException error) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(new ErrorResponse((error.getMessage())));
        } catch (Exception error) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(new ErrorResponse(error.getMessage()));
        }
    }

    /**
     * Endpoint to use to delete a transaction
     * @param id: Id of the transaction to delete
     * @return A ResponseEntity object containing a ObjectResponse
     */
    @DeleteMapping("/delete/{id}")
    public ResponseEntity<?> deleteTransaction(@PathVariable int id) {
        try {
            return ResponseEntity.status(HttpStatus.OK).body(transactionService.deleteTransaction(id));
        } catch (TransactionException error) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(new ErrorResponse((error.getMessage())));
        } catch (Exception error) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(new ErrorResponse(error.getMessage()));
        }
    }
    /**
     * Endpoint to use to get the groceries budget of the current user
     * @param month : month
     * @param year : year
     * @return A ResponseEntity object containing a ShoppingBudgetResponse
     * @see ShoppingBudgetResponse
     */
    @GetMapping("/{month}/{year}")
    public ResponseEntity<?> getAllCourseTransactionByMonth(@PathVariable("month") Integer month, @PathVariable("year") Integer year) {
        try {
            List<ShoppingWeekResponse> transactionsPerWeek = transactionService.getAmountBudgetTransactionsPerWeek(month, year);
            User user = authenticationService.getCurrentUser();
            Map<String, String> map = translationService.fetchCategoryTemplateTranslation(user.getPreferedLanguage().getCountryCode().toLowerCase(Locale.ROOT));

            ShoppingBudgetResponse shoppingBudgetResponse = transactionService.getAllCourseTransactions(true,month, year, map.get("groceries"), "OUTPUT");
            shoppingBudgetResponse.setTransactionsPerWeek(transactionsPerWeek);
            return ResponseEntity.status(HttpStatus.OK).body(shoppingBudgetResponse);
        } catch (Exception error) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(new ErrorResponse(error.getMessage()));
        }
    }

    /**
     * Endpoint to use to get the daily budget of the current user
     * @return A ResponseEntity object containing a BudgetResponse
     * @see BudgetResponse
     */
    @GetMapping("/dailyBudget")
    public ResponseEntity<?> getDailyBudget() {
        try {
            return ResponseEntity.status(HttpStatus.OK).body(transactionService.getDailyBudget());
        } catch (Exception error) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(new ErrorResponse(error.getMessage()));
        }
    }

    /**
     * Endpoint to use to get the weekly budget of the current user
     * @return A ResponseEntity object containing a BudgetResponse
     * @see BudgetResponse
     */
    @GetMapping("/weeklyBudget")
    public ResponseEntity<?> getWeeklyBudget(){
        try {
            return ResponseEntity.status(HttpStatus.OK).body(transactionService.getWeeklyBudget());
        } catch (Exception error) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(new ErrorResponse(error.getMessage()));
        }
    }

    /**
     * Endpoint to use to get the monthly budget of the current user
     * @return A ResponseEntity object containing a BudgetResponse
     * @see BudgetResponse
     */
    @GetMapping("/monthlyBudget")
    public ResponseEntity<?> getMonthlyBudget(){
        try {
            return ResponseEntity.status(HttpStatus.OK).body(transactionService.getMonthlyBudget());
        } catch (Exception error) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(new ErrorResponse(error.getMessage()));
        }
    }

    /**
     * Endpoint to use to get all the expenses and incomes of the last 6 months of the current user
     * @return A ResponseEntity object containing a Map<LocalDate, Map<String, Double>>
     */
    @GetMapping("/getRecentSavings")
    public ResponseEntity<?> getRecentSavings() {
        try {
            return ResponseEntity.status(HttpStatus.OK).body(transactionService.getRecentSavings());
        } catch (Exception error) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(new ErrorResponse(error.getMessage()));
        }
    }

    /**
     * Endpoint to use to add a set of transactions
     * @param transactionCSV : transactions to be add
     * @return nothing
     */
    @PostMapping("/addCSV")
    public ResponseEntity<?> addTransaction(@RequestBody TransactionCSV transactionCSV) {
        try {
            transactionService.addTransactionWithCSV(transactionCSV);
            return ResponseEntity.status(HttpStatus.NO_CONTENT).build();
        } catch (Exception error) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(new ErrorResponse(error.getMessage()));
        }
    }

    /**
     * Endpoint to use to get all the recurring transactions of current user
     * @return A ResponseEntity object containing a list of TransactionDto
     * @see TransactionDto
     */
    @GetMapping("/recurringTransactions")
    public ResponseEntity<?> getAllRecurringTransactions(){
        try {
            return ResponseEntity.status(HttpStatus.OK).body(transactionService.getAllRecurringTransactions());
        } catch (Exception error) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(new ErrorResponse(error.getMessage()));
        }
    }


}
