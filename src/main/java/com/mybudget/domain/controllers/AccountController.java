package com.mybudget.domain.controllers;

import com.mybudget.domain.dto.AccountDto;
import com.mybudget.domain.dto.response.ErrorResponse;
import com.mybudget.domain.exceptions.AccountTypeException;
import com.mybudget.domain.services.AccountService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

/**
 * @Author Eva Tumia
 */
@RestController
@RequestMapping(path = "api/account")
public class AccountController {

    @Autowired
    private AccountService accountService ;

    /**
     * Endpoint to use to have all the user's accounts
     *
     * @return A ResponseEntity object containing a list of AccountDTO
     * @see AccountDto
     */
    @GetMapping("/all")
    public ResponseEntity<?> getAllByOwner() {
        try {
            return ResponseEntity.status(HttpStatus.OK).body(accountService.getAll());
        } catch (Exception error) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(new ErrorResponse(error.getMessage()));
        }
    }

    /**
     * Endpoint to use to have all the user's default accounts
     *
     * @return A ResponseEntity object containing a list of AccountDTO
     * @see AccountDto
     */
    @GetMapping("/default")
    public ResponseEntity<?> getAllDefaultAccount() {
        try {
            return ResponseEntity.status(HttpStatus.OK).body(accountService.getAllDefaultAccount());
        } catch (Exception error) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(new ErrorResponse(error.getMessage()));
        }
    }

    /**
     * Endpoint to use to add a new account for a specific user
     *
     * @param accountDto: new account to be added
     * @return A ResponseEntity object containing a AccountDTO
     * @see AccountDto
     */
    @PostMapping("/add")
    public ResponseEntity<?> addAccount(@RequestBody AccountDto accountDto) {
        try {
            return ResponseEntity.status(HttpStatus.OK).body(accountService.addAccount(accountDto));
        } catch (AccountTypeException error) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(new ErrorResponse(error.getMessage()));
        } catch (Exception error) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(new ErrorResponse(error.getMessage()));
        }
    }
}
