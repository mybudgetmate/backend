package com.mybudget.domain.controllers;


import com.mybudget.domain.dto.response.CurrencyResponse;
import com.mybudget.domain.dto.response.ErrorResponse;
import com.mybudget.domain.services.CurrencyService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * @author Soumaya Izmar && Eva Tumia
 */
@RestController
@RequestMapping(path = "api/currency")
public class CurrencyController {
    @Autowired
    private CurrencyService currencyService;
    /**
     * Endpoint to use to get all currencies
     * @return A ResponseEntity object containing a list of CurrencyResponse
     * @see CurrencyResponse
     */
    @GetMapping("/")
    public ResponseEntity<?> fetchAllCurrency() {
        try {
            List<CurrencyResponse> currencies = currencyService.fetchAllCurrencies();
            return ResponseEntity.status(HttpStatus.OK).body(currencies);
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(new ErrorResponse(e.getMessage()));
        }
    }

    /**
     * Endpoint to use to get the prefered currency of the current user
     * @return A ResponseEntity object containing a CurrencyResponse
     * @see CurrencyResponse
     */
    @GetMapping("/byUser")
    public ResponseEntity<?> fetchPreferedCurrencyUser(){
        try {
            return ResponseEntity.status(HttpStatus.OK).body(currencyService.fetchPreferedCurrencyUser());
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(new ErrorResponse(e.getMessage()));
        }
    }
}
