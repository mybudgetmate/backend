/**
 * @author Eva Tumia
 */

package com.mybudget.domain.controllers;

import com.mybudget.domain.dto.CategoryDto;
import com.mybudget.domain.dto.CategorySerializedDto;
import com.mybudget.domain.dto.response.CategoryTransactionResponse;
import com.mybudget.domain.dto.response.ErrorResponse;
import com.mybudget.domain.dto.response.ObjectResponse;
import com.mybudget.domain.exceptions.CategoriesException;
import com.mybudget.domain.services.CategoriesService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(path = "api/categories")
public class CategoriesController {

    @Autowired
    private CategoriesService categoriesService;

    /**
     * Enpoint to use to add a new category
     *
     * @param categoryDto: new category to be added
     * @return A ResponseEntity object containing a CategorySerializedDto
     * @see CategorySerializedDto
     */
    @PostMapping
    public ResponseEntity<?> addCategory(@RequestBody CategoryDto categoryDto) {
        try {
            return ResponseEntity.status(HttpStatus.OK).body(categoriesService.addCategory(categoryDto));
        } catch (Exception error) {

            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(new ErrorResponse(error.getMessage()));
        }
    }

    /**
     * Endpoint to use to get all the categories of the current user
     *
     * @return A ResponseEntity object containing a list of CategorySerializedDto
     * @see CategorySerializedDto
     */
    @GetMapping("/all/*")
    public ResponseEntity<?> getAllByOwner() {
        try {
            return ResponseEntity.status(HttpStatus.OK).body(categoriesService.getAllByUser());
        } catch (Exception error) {

            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(new ErrorResponse(error.getMessage()));
        }
    }

    /**
     * Endpoint to use when a category has had its parent updated
     *
     * @param categoryDto: the category that must have its parent updated
     * @return A ResponseEntity object containing a CategorySerializedDto
     * @see CategorySerializedDto
     */
    @PatchMapping("/updateParentCategory")
    public ResponseEntity<?> updateParentCategory(@RequestBody CategorySerializedDto categoryDto) {
        try {
            return ResponseEntity.status(HttpStatus.OK).body(categoriesService.patchParentCategory(categoryDto));
        } catch (CategoriesException error) {

            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(new ErrorResponse((error.getMessage())));
        } catch (Exception error) {

            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(new ErrorResponse(error.getMessage()));
        }
    }

    /**
     * Endpoint to use when a category needs to be updated
     *
     * @param categoryDto: the category to be updated
     * @return A ResponseEntity object containing a CategorySerializedDto
     * @see CategorySerializedDto
     */
    @PatchMapping("/updateCategory")
    public ResponseEntity<?> updateCategory(@RequestBody CategorySerializedDto categoryDto) {
        try {
            return ResponseEntity.status(HttpStatus.OK).body(categoriesService.updateCategory(categoryDto));
        } catch (CategoriesException error) {

            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(new ErrorResponse((error.getMessage())));
        } catch (Exception error) {

            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(new ErrorResponse(error.getMessage()));
        }
    }

    /**
     * Endpoint to use to delete a specific category
     *
     * @param id: Id of the category to delete
     * @return A ResponseEntity object containing a ObjectResponse
     * @see ObjectResponse
     */
    @DeleteMapping("/delete/{id}")
    public ResponseEntity<?> deleteCategory(@PathVariable int id) {
        try {
            return ResponseEntity.status(HttpStatus.OK).body(categoriesService.deleteCategory(id));
        } catch (CategoriesException error) {

            return ResponseEntity.status(HttpStatus.CONFLICT).body(new ErrorResponse(error.getMessage()));
        } catch (Exception error) {

            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(new ErrorResponse(error.getMessage()));
        }
    }

    /**
     * Endpoint to use to get all the categories from a certain type
     *
     * @param type: Type of category to fetch {INPUT,OUTPUT}
     * @return A ResponseEntity object containing a list of CategorySerializedDto
     * @see CategorySerializedDto
     */
    @GetMapping("/categoriesPerType")
    public ResponseEntity<?> getByOwnerAndType(@RequestParam String type) {
        try {
            return ResponseEntity.status(HttpStatus.OK).body(categoriesService.getByOwnerAndType(type));
        } catch (CategoriesException error) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(new ErrorResponse(error.getMessage()));
        } catch (Exception error) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(new ErrorResponse(error.getMessage()));
        }
    }

    /**
     * Endpoint to use to get budget by category
     *
     * @param month: month of the transactions
     * @param year:  year of the transactions
     * @param type:  type of the transactions {INPUT,OUTPUT}
     * @return A ResponseEntity object containing a is of CategoryTransactionResponse
     * @see CategoryTransactionResponse
     */
    @GetMapping("/budget/{month}/{year}/{type}")
    public ResponseEntity<?> getBudgetPerMonth(@PathVariable("month") Integer month, @PathVariable("year") Integer year, @PathVariable("type") String type) {
        try {
            List<CategoryTransactionResponse> list = categoriesService.getCategoryAmountSumPerMonth(month, year, type);
            return ResponseEntity.status(HttpStatus.OK).body(list);
        } catch (Exception error) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(new ErrorResponse(error.getMessage()));
        }
    }

}
