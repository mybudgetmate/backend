package com.mybudget.domain.controllers;

import com.mybudget.domain.dto.response.ErrorResponse;
import com.mybudget.domain.dto.response.LanguageResponse;
import com.mybudget.domain.services.LanguageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

/**
 * @author Soumaya Izmar
 */
@RestController
@RequestMapping(path = "api/language")
public class LanguageController {

    @Autowired
    private LanguageService languageService;

    /**
     * Endpoint to use to get all the languages
     * @return A ResponseEntity object containing a list of LanguageResponse
     * @see LanguageResponse
     */
    @GetMapping("/")
    public ResponseEntity<?> getAllLanguages() {
        try {
            return ResponseEntity.status(HttpStatus.OK).body(languageService.fetchAllLanguages());
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(new ErrorResponse(e.getMessage()));
        }
    }
    /**
     * Endpoint to use to get all the languages
     * @param countryCode : country code of the new language
     * @return A ResponseEntity object containing a String
     */
    @PatchMapping("/")
    public ResponseEntity<?> patchUserLanguage(@RequestBody String countryCode) {
        try {
            return ResponseEntity.status(HttpStatus.OK).body(languageService.changeUserLanguage(countryCode));
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(new ErrorResponse(e.getMessage()));
        }
    }

}
