/**
 * @author Eva Tumia
 */

package com.mybudget.domain.controllers;

import com.mybudget.domain.dto.response.ErrorResponse;
import com.mybudget.domain.models.Icon;
import com.mybudget.domain.services.IconsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;



@RestController
@RequestMapping(path = "api/icons")
public class IconsController {

    @Autowired
    IconsService iconsService;

    /**
     * Endpoint to use to get all the icons
     * @return A ResponseEntity object containing a list of Icon
     * @see Icon
     */
    @GetMapping("/all")
    public ResponseEntity<?> getAll(){
        try{
            return ResponseEntity.status(HttpStatus.OK).body(iconsService.getAll());
        }catch(Exception e){
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(new ErrorResponse(e.getMessage()));
        }
    }
}
