package com.mybudget.domain.controllers;

import com.mybudget.domain.dto.response.ErrorResponse;
import com.mybudget.domain.services.StatusService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

/**
 * @author Soumaya Izmar
 */
@RestController
@RequestMapping(path = "api/status")
public class StatusController {

    @Autowired
    private StatusService statusService;

    /**
     * Endpoint to use to get all the status
     *
     * @return A ResponseEntity object containing a list of String
     */
    @GetMapping("/")
    public ResponseEntity<?> fetchAllStatus(@RequestParam String languageCode) {
        try {
            return ResponseEntity.status(HttpStatus.OK).body(statusService.fetchAllStatus(languageCode));
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(new ErrorResponse(e.getMessage()));
        }
    }


    @GetMapping("/subStatus/{languageCode}")
    public ResponseEntity<?> fetchAllSubStatus(@PathVariable("languageCode") String languageCode) {
        try {
            return ResponseEntity.status(HttpStatus.OK).body(statusService.fetchAllSubStatus(languageCode));
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(new ErrorResponse(e.getMessage()));
        }
    }
}
