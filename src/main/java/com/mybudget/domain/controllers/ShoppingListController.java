package com.mybudget.domain.controllers;


import com.mybudget.domain.dto.IngredientDto;
import com.mybudget.domain.dto.ShoppingListDto;
import com.mybudget.domain.dto.response.DefaultShoppingListRequestWrapper;
import com.mybudget.domain.dto.response.ErrorResponse;
import com.mybudget.domain.models.Ingredient;
import com.mybudget.domain.services.ShoppingListService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;

import static org.springframework.http.HttpStatus.INTERNAL_SERVER_ERROR;
import static org.springframework.http.HttpStatus.OK;


@RestController
@RequestMapping(path = "api/shoppinglists")
public class ShoppingListController {
    @Autowired
    private ShoppingListService shoppingListService;


    @GetMapping("/")
    public ResponseEntity<?> getAllShoppingList(@RequestParam String languageCode) {
        try {
            return ResponseEntity.status(OK).body(shoppingListService.getAllShoppingList(languageCode));
        } catch (Exception error) {
            return ResponseEntity.status(INTERNAL_SERVER_ERROR).body(new ErrorResponse(error.getMessage()));
        }
    }

    @PostMapping("/")
    public  ResponseEntity<?> addShoppingList
            (@RequestBody DefaultShoppingListRequestWrapper requestWrapper){
        try {
            return ResponseEntity.status(OK).body(shoppingListService
                    .addShoppingList(requestWrapper));
        } catch (Exception error) {
            return ResponseEntity.status(INTERNAL_SERVER_ERROR).body(new ErrorResponse(error.getMessage()));
        }
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<?> deleteShoppingList(@PathVariable("id") Long id) {
        try {
            return ResponseEntity.status(OK).body(shoppingListService.deleteShoppingList(id));
        } catch (Exception error) {
            return ResponseEntity.status(INTERNAL_SERVER_ERROR).body(new ErrorResponse(error.getMessage()));
        }
    }
}

