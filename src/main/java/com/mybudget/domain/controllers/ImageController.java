package com.mybudget.domain.controllers;

import com.mybudget.domain.dto.response.ErrorResponse;
import com.mybudget.domain.services.ImageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author Soumaya Izmar
 */

@RestController
@RequestMapping(path = "api/images")
public class ImageController {
    @Autowired
    private ImageService imageService;

    /**
     * Endpoint to use to get all the images urls
     * @return A ResponseEntity object containing a list of String
     */
    @GetMapping("/")
    public ResponseEntity<?> getAllImages() {
        try {
            return ResponseEntity.status(HttpStatus.OK).body(imageService.fetchAllImages());
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(new ErrorResponse(e.getMessage()));
        }
    }


}
