package com.mybudget.domain.controllers;

import com.mybudget.domain.dto.LanguageDto;
import com.mybudget.domain.dto.response.ErrorResponse;
import com.mybudget.domain.exceptions.FatalException;
import com.mybudget.domain.services.TranslationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Map;

import static org.springframework.http.HttpStatus.INTERNAL_SERVER_ERROR;
import static org.springframework.http.HttpStatus.OK;

@RestController
@RequestMapping(path = "api/translation")
public class TranslationController {

    @Autowired
    private TranslationService translationService;

    /**
     * @param languageRequest: translation language to fetch
     * @return A ResponseEntity object containing a Map<String, String>
     */
    @PostMapping("/loginTranslation")
    public ResponseEntity<?> getLoginTranslation(@RequestBody LanguageDto languageRequest) {
        try {
            Map<String, String> map = translationService.fetchLoginTranslation(languageRequest.getLanguageCode());
            return ResponseEntity.status(OK).body(map);
        } catch (ClassNotFoundException | FatalException error) {
            return ResponseEntity.status(INTERNAL_SERVER_ERROR).body(new ErrorResponse(error.getMessage()));
        }

    }

    /**
     * @param languageRequest: translation language to fetch
     * @return A ResponseEntity object containing a Map<String, String>
     */
    @PostMapping("/signUpTranslation")
    public ResponseEntity<?> getSignUpTranslation(@RequestBody LanguageDto languageRequest) {
        try {
            Map<String, String> map = translationService.fetchSignUpTranslation(languageRequest.getLanguageCode());
            return ResponseEntity.status(OK).body(map);
        } catch (ClassNotFoundException | FatalException error) {
            return ResponseEntity.status(INTERNAL_SERVER_ERROR).body(new ErrorResponse(error.getMessage()));
        }

    }

    /**
     * @param languageCode: translation language to fetch
     * @return A ResponseEntity object containing a Map<String, String>
     */
    @GetMapping("/categoriesManagementTranslation")
    public ResponseEntity<?> getCategoriesManagementTranslation(@RequestParam String languageCode) {
        try {
            Map<String, String> map = translationService.fetchCategoriesManagementTranslation(languageCode);
            return ResponseEntity.status(OK).body(map);
        } catch (ClassNotFoundException | FatalException error) {
            return ResponseEntity.status(INTERNAL_SERVER_ERROR).body(new ErrorResponse(error.getMessage()));
        }
    }

    /**
     * @param languageCode: translation language to fetch
     * @return A ResponseEntity object containing a Map<String, String>
     */
    @GetMapping("/projectTranslation")
    public ResponseEntity<?> getProjectTranslation(@RequestParam String languageCode) {
        try {
            Map<String, String> map = translationService.fetchProjectTranslation(languageCode);
            return ResponseEntity.status(OK).body(map);
        } catch (ClassNotFoundException | FatalException error) {
            return ResponseEntity.status(INTERNAL_SERVER_ERROR).body(new ErrorResponse(error.getMessage()));
        }

    }

    /**
     * @param languageCode: translation language to fetch
     * @return A ResponseEntity object containing a Map<String, String>
     */
    @GetMapping("/budgetTranslation")
    public ResponseEntity<?> getBudgetTranslation(@RequestParam String languageCode) {
        try {
            Map<String, String> map = translationService.fetchBudgetTranslation(languageCode);
            return ResponseEntity.status(OK).body(map);
        } catch (ClassNotFoundException | FatalException error) {
            return ResponseEntity.status(INTERNAL_SERVER_ERROR).body(new ErrorResponse(error.getMessage()));
        }

    }

    /**
     * @param languageCode: translation language to fetch
     * @return A ResponseEntity object containing a Map<String, String>
     */
    @GetMapping("/addTransactionTranslation")
    public ResponseEntity<?> getAddTransactionTranslation(@RequestParam String languageCode) {
        try {
            Map<String, String> map = translationService.fetchAddTransactionTranslation(languageCode);
            return ResponseEntity.status(OK).body(map);
        } catch (ClassNotFoundException | FatalException error) {
            return ResponseEntity.status(INTERNAL_SERVER_ERROR).body(new ErrorResponse(error.getMessage()));
        }
    }

    /**
     * @param languageCode: translation language to fetch
     * @return A ResponseEntity object containing a Map<String, String>
     */
    @GetMapping("/monthsTranslation")
    public ResponseEntity<?> getMonthsTranslation(@RequestParam String languageCode) {
        try {
            Map<String, String> map = translationService.fetchMonthsTranslation(languageCode);
            return ResponseEntity.status(OK).body(map);
        } catch (ClassNotFoundException | FatalException error) {

            return ResponseEntity.status(INTERNAL_SERVER_ERROR).body(new ErrorResponse(error.getMessage()));
        }
    }

    /**
     * @param languageCode: translation language to fetch
     * @return A ResponseEntity object containing a Map<String, String>
     */
    @GetMapping("/homepageTranslation")
    public ResponseEntity<?> getHomepageTranslation(@RequestParam String languageCode) {
        try {
            Map<String, String> map = translationService.fetchHomepageTranslation(languageCode);
            return ResponseEntity.status(OK).body(map);
        } catch (ClassNotFoundException | FatalException error) {
            return ResponseEntity.status(INTERNAL_SERVER_ERROR).body(new ErrorResponse(error.getMessage()));
        }
    }

    /**
     * @param languageCode: translation language to fetch
     * @return A ResponseEntity object containing a Map<String, String>
     */
    @GetMapping("/allTransactionsTranslation")
    public ResponseEntity<?> getAllTransactionsTranslation(@RequestParam String languageCode) {
        try {
            Map<String, String> map = translationService.fetchAllTransactionsTranslation(languageCode);
            return ResponseEntity.status(OK).body(map);
        } catch (ClassNotFoundException | FatalException error) {
            return ResponseEntity.status(INTERNAL_SERVER_ERROR).body(new ErrorResponse(error.getMessage()));
        }
    }

    @GetMapping("/friendshipTranslation/")
    public ResponseEntity<?> getAllFriendshipTranslation(@RequestParam String languageCode) {
        try {
            Map<String, String> map = translationService.fetchFriendshipTranslation(languageCode);
            return ResponseEntity.status(OK).body(map);
        } catch (ClassNotFoundException | FatalException error) {

            return ResponseEntity.status(INTERNAL_SERVER_ERROR).body(new ErrorResponse(error.getMessage()));
        }
    }

    /**
     * Get Groceries translation
     *
     * @return A ResponseEntity object containing a Map<String, String>
     */
    @GetMapping("/shopping")
    public ResponseEntity<?> getGroceriesTranslation() {
        try {
            return ResponseEntity.status(HttpStatus.OK).body(translationService.getShoppingTranslation());
        } catch (FatalException error) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(new ErrorResponse(error.getMessage()));
        }
    }

    /**
     * @param languageCode: translation language to fetch
     * @return A ResponseEntity object containing a Map<String, String>
     */
    @GetMapping("/recurringPaiementsTranslation")
    public ResponseEntity<?> getRecurringPaiementsTranslation(@RequestParam String languageCode) {
        try {
            return ResponseEntity.status(HttpStatus.OK).body(translationService.getRecurringPaiementsTranslation(languageCode));
        } catch (ClassNotFoundException | FatalException error) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(new ErrorResponse(error.getMessage()));
        }
    }

    /**
     * @param languageCode: translation language to fetch
     * @return A ResponseEntity object containing a Map<String, String>
     */
    @GetMapping("/globalTranslation")
    public ResponseEntity<?> getGlobalTranslation(@RequestParam String languageCode) {
        try {
            return ResponseEntity.status(HttpStatus.OK).body(translationService.fetchGlobalTranslation(languageCode));
        } catch (ClassNotFoundException | FatalException error) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(new ErrorResponse(error.getMessage()));
        }
    }

    /**
     * @param languageCode: translation language to fetch
     * @return A ResponseEntity object containing a Map<String, String>
     */
    @GetMapping("/settingsPageTranslation")
    public ResponseEntity<?> getSettingsPageTranslation(@RequestParam String languageCode) {
        try {
            Map<String, String> map = translationService.fetchSettingsPageTranslations(languageCode);
            return ResponseEntity.status(OK).body(map);
        } catch (ClassNotFoundException | FatalException error) {
            return ResponseEntity.status(INTERNAL_SERVER_ERROR).body(new ErrorResponse(error.getMessage()));
        }
    }

    /**
     * @param languageCode: translation language to fetch
     * @return A ResponseEntity object containing a Map<String, String>
     */
    @GetMapping("/statusTranslation")
    public ResponseEntity<?> getStatusTranslation(@RequestParam String languageCode) {
        try {
            Map<String, String> map = translationService.fetchStatusTranslations(languageCode);
            return ResponseEntity.status(OK).body(map);
        } catch (ClassNotFoundException | FatalException error) {
            return ResponseEntity.status(INTERNAL_SERVER_ERROR).body(new ErrorResponse(error.getMessage()));
        }
    }

    /**
     * @param languageCode: translation language to fetch
     * @return A ResponseEntity object containing a Map<String, String>
     */
    @GetMapping("/shoppingListsModuleTranslation")
    public ResponseEntity<?> getShoppingListsModuleTranslation(@RequestParam String languageCode) {
        try {
            Map<String, String> map = translationService.fetchShoppingListsModuleTranslations(languageCode);
            return ResponseEntity.status(OK).body(map);
        } catch (ClassNotFoundException | FatalException error) {
            return ResponseEntity.status(INTERNAL_SERVER_ERROR).body(new ErrorResponse(error.getMessage()));
        }
    }

}