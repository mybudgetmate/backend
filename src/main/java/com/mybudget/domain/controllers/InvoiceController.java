package com.mybudget.domain.controllers;

import com.mybudget.domain.dto.InvoiceDto;
import com.mybudget.domain.dto.response.ErrorResponse;
import com.mybudget.domain.dto.response.ObjectResponse;
import com.mybudget.domain.services.InvoiceService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

/**
 * @author: Eva Tumia
 */

@RestController
@RequestMapping(path = "api/invoice")
public class InvoiceController {

    @Autowired
    private InvoiceService invoiceService;

    /**
     * Endpoint to use to add a new invoice
     * @param invoiceDto: invoice to be added
     * @return A ResponseEntity object containing an InvoiceDto
     * @see InvoiceDto
     */
    @PostMapping("/add")
    public ResponseEntity<?> addInvoice(@RequestBody InvoiceDto invoiceDto) {
        try {
            return ResponseEntity.status(HttpStatus.OK).body(invoiceService.addNewInvoice(invoiceDto));
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(new ErrorResponse(e.getMessage()));
        }
    }

    /**
     * Endpoint to use to get all the invoices of the current user
     * @return A ResponseEntity object containing a list of InvoiceDto
     * @see InvoiceDto
     */
    @GetMapping("/all")
    public ResponseEntity<?> getAllByOwner() {
        try {
            return ResponseEntity.status(HttpStatus.OK).body(invoiceService.getAllByOwner());
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(new ErrorResponse(e.getMessage()));
        }
    }

    /**
     * Endpoint to use to delete an invoice
     *
     * @param id: Id of the category to delete
     * @return A ResponseEntity object containing a ObjectResponse
     * @see ObjectResponse
     */
    @DeleteMapping("/delete/{id}")
    public ResponseEntity<?> deleteById(@PathVariable int id){
        try {
            return ResponseEntity.status(HttpStatus.OK).body(invoiceService.deleteById(id));
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(new ErrorResponse(e.getMessage()));
        }
    }

    /**
     * Endpoint to use to update an invoice
     *
     * @param invoiceDto: the invoice to update
     * @return A ResponseEntity object containing an InvoiceDto
     * @see InvoiceDto
     */
    @PatchMapping("/updateInvoice")
    public ResponseEntity<?> updateInvoice(@RequestBody InvoiceDto invoiceDto) {
        try {
            return ResponseEntity.status(HttpStatus.OK).body(invoiceService.updateInvoice(invoiceDto));
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(new ErrorResponse(e.getMessage()));
        }
    }
}
