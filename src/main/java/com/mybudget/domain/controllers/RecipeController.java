package com.mybudget.domain.controllers;

import com.mybudget.domain.dto.LanguageDto;
import com.mybudget.domain.dto.response.ErrorResponse;
import com.mybudget.domain.services.IngredientService;
import com.mybudget.domain.services.RecipeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(path = "api/recipes")
public class RecipeController {
    @Autowired
    private RecipeService recipeService;

    @GetMapping("/")
    public ResponseEntity<?> getAll(@RequestParam String languageCode) {
        try {
            return ResponseEntity.status(HttpStatus.OK).body(recipeService.getAll(languageCode));
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(new ErrorResponse(e.getMessage()));
        }
    }
}
