package com.mybudget.domain.controllers;

import com.mybudget.domain.dto.ProjectDto;
import com.mybudget.domain.dto.response.ErrorResponse;
import com.mybudget.domain.dto.response.SubProjectResponse;
import com.mybudget.domain.exceptions.ProjectException;
import com.mybudget.domain.services.ProjectService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * @author Soumaya Izmar
 */
@RestController
@RequestMapping(path = "api/project")
public class ProjectController {

    @Autowired
    private ProjectService projectService;

    /**
     * Endpoint to use to get all the projects
     * @return A ResponseEntity object containing a list of ProjectDto
     * @see ProjectDto
     */
    @GetMapping("/")
    public ResponseEntity<?> getAllProjects() {
        try {
            return ResponseEntity.status(HttpStatus.OK).body(projectService.fetchAllProject());
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(new ErrorResponse(e.getMessage()));
        }
    }

    /**
     * Endpoint to use to get all the sub projects
     * @param id: project id
     * @return A ResponseEntity object containing a SubProjectResponse
     * @see SubProjectResponse
     */
    @GetMapping("/{id}")
    public ResponseEntity<?> getSubProjects(@PathVariable("id") Integer id) {
        try {
            List<ProjectDto> projects = projectService.fetchSubProject(id);
            ProjectDto projectDto = projectService.fetchOneProject(id);
            Integer project = projectService.getParents(id);
            return ResponseEntity.status(HttpStatus.OK).body(new SubProjectResponse(projects, projectDto,project));
        } catch (ProjectException e) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(new ErrorResponse(e.getMessage()));
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(new ErrorResponse(e.getMessage()));
        }

    }
    /**
     * Endpoint to use to add project
     * @param projectDto : project to be add
     * @return A ResponseEntity object containing a ProjectDto
     * @see ProjectDto
     */
    @PostMapping("/")
    public ResponseEntity<?> addProject(@RequestBody ProjectDto projectDto) {
        try {
            return ResponseEntity.status(HttpStatus.OK).body(projectService.addNewProject(projectDto));
        } catch (ProjectException e) {
            return ResponseEntity.status(400).body(new ErrorResponse(e.getMessage()));
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(new ErrorResponse(e.getMessage()));
        }
    }
    /**
     * Endpoint to use to update project
     * @param projectDto : project to be update
     * @return A ResponseEntity object containing a ProjectDto
     * @see ProjectDto
     */
    @PatchMapping("/")
    public ResponseEntity<?> patchProject(@RequestBody ProjectDto projectDto) {
        try {
            return ResponseEntity.status(HttpStatus.OK).body(projectService.updateProject(projectDto));
        } catch (ProjectException e) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(new ErrorResponse(e.getMessage()));
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(new ErrorResponse(e.getMessage()));
        }
    }
    /**
     * Endpoint to use to update project
     * @param id : project id to be delete
     * @return nothing
     */
    @DeleteMapping("/{id}")
    public ResponseEntity<?> deleteProject(@PathVariable("id") Integer id) {
        try {
            projectService.deleteProject(id);
            return ResponseEntity.status(HttpStatus.NO_CONTENT).build();
        } catch (ProjectException e) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(new ErrorResponse(e.getMessage()));
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(new ErrorResponse(e.getMessage()));
        }
    }


}
