package com.mybudget.domain.controllers;

import com.mybudget.domain.dto.FriendShipDto;
import com.mybudget.domain.dto.response.ErrorResponse;
import com.mybudget.domain.dto.response.FriendshipListResponse;
import com.mybudget.domain.exceptions.FriendshipException;
import com.mybudget.domain.services.FriendshipService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import static org.springframework.http.HttpStatus.INTERNAL_SERVER_ERROR;
import static org.springframework.http.HttpStatus.OK;

/**
 * @Author Soumaya Izmar
 */
@RestController
@RequestMapping(path = "api/friendship")
public class FriendshipController {

    @Autowired
    private FriendshipService friendshipService;

    /**
     * Endpoint to use to get all friendship
     * @return A ResponseEntity object containing a FriendshipListResponse
     * @see FriendshipListResponse
     */
    @GetMapping("/")
    public ResponseEntity<?> getAllFriendship() {
        try {
            return ResponseEntity.status(OK).body(friendshipService.getFriendshipByOwner());
        } catch (Exception error) {
            return ResponseEntity.status(INTERNAL_SERVER_ERROR).body(new ErrorResponse(error.getMessage()));
        }
    }
    /**
     * Endpoint to use to get all friendship
     * @param socialCode: social code
     * @return A ResponseEntity object containing a FriendshipListResponse
     * @see FriendshipListResponse
     */
    @GetMapping("/{socialCode}")
    public ResponseEntity<?> addRequestFriendship(@PathVariable String socialCode) {
        try {
            return ResponseEntity.status(OK).body(friendshipService.addFriendship(socialCode));
        } catch (FriendshipException error) {
            return ResponseEntity.status(404).body(new ErrorResponse(error.getMessage()));
        } catch (Exception error) {
            return ResponseEntity.status(INTERNAL_SERVER_ERROR).body(new ErrorResponse(error.getMessage()));
        }
    }
    /**
     * Endpoint to use to accept a friend request
     * @param id: id of receiver user
     * @return A ResponseEntity object containing FriendShipDto
     * @see FriendShipDto
     */
    @GetMapping("/accept/{id}")
    public ResponseEntity<?> acceptFriendship(@PathVariable("id") Integer id) {
        try {
            return ResponseEntity.status(OK).body(friendshipService.acceptFriendship(id));
        } catch (FriendshipException error) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(new ErrorResponse(error.getMessage()));
        } catch (Exception error) {
            return ResponseEntity.status(INTERNAL_SERVER_ERROR).body(new ErrorResponse(error.getMessage()));
        }
    }

    /**
     * Endpoint to use to refuse a friend request
     * @param id: id of receiver user
     * @return A ResponseEntity object containing FriendShipDto
     * @see FriendShipDto
     */
    @GetMapping("/refuse/{id}")
    public ResponseEntity<?> refuseFriendship(@PathVariable("id") Integer id) {
        try {
            return ResponseEntity.status(OK).body(friendshipService.refuseFriendship(id));
        } catch (FriendshipException error) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(new ErrorResponse(error.getMessage()));
        } catch (Exception error) {
            return ResponseEntity.status(INTERNAL_SERVER_ERROR).body(new ErrorResponse(error.getMessage()));
        }
    }
    /**
     * Endpoint to use to delete a friendship
     * @param receiverID: id of receiver user
     * @param senderID: id of sender user
     * @return A ResponseEntity object containing FriendShipDto
     * @see FriendShipDto
     */
    @DeleteMapping("/{receiverID}/{senderID}")
    public ResponseEntity<?> deleteFriendship(@PathVariable("receiverID") Integer receiverID,@PathVariable("senderID") Integer senderID) {
        try {
            return ResponseEntity.status(OK).body(friendshipService.deleteFriendship(receiverID,senderID));
        } catch (FriendshipException error) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(new ErrorResponse(error.getMessage()));
        } catch (Exception error) {
            return ResponseEntity.status(INTERNAL_SERVER_ERROR).body(new ErrorResponse(error.getMessage()));
        }
    }
}
