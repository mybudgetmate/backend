package com.mybudget.domain.exceptions;

public class ShoppingListException extends  RuntimeException{

    public ShoppingListException() {
    }

    public ShoppingListException(String message) {
        super(message);
    }
}
