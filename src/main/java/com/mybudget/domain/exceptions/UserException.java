package com.mybudget.domain.exceptions;

public class UserException extends RuntimeException{
    public UserException(String message) {
        super(message);
    }
}
