package com.mybudget.domain.exceptions;

public class IngredientException extends RuntimeException {

    public IngredientException() {

    }

    public IngredientException(String message) {
        super(message);
    }
}
