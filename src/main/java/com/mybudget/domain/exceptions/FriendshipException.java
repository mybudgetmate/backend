package com.mybudget.domain.exceptions;

public class FriendshipException extends RuntimeException{
    public FriendshipException(String message) {
        super(message);
    }
}
