/**
 * @author Eva Tumia
 */
package com.mybudget.domain.exceptions;

public class CategoriesException extends RuntimeException{

    public CategoriesException() {
    }

    public CategoriesException(String message) {
        super(message);
    }
}
