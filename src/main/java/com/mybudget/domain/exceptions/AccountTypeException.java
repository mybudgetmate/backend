package com.mybudget.domain.exceptions;

public class AccountTypeException extends  RuntimeException{
    public AccountTypeException() {
    }

    public AccountTypeException(String message) {

        super(message);
    }
}
