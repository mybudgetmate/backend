package com.mybudget.domain.exceptions;

/**
 * @author Eva Tumia
 */
public class TemplateCategoryNameTranslationException extends RuntimeException{
    public TemplateCategoryNameTranslationException() {
    }

    public TemplateCategoryNameTranslationException(String message) {
        super(message);
    }
}
