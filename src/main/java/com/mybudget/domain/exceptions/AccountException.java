package com.mybudget.domain.exceptions;

public class AccountException extends RuntimeException{

    public AccountException() {
    }

    public AccountException(String message) {
        super(message);
    }
}
