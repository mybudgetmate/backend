package com.mybudget.domain.exceptions;

/**
 * @author Eva Tumia
 */
public class TemplateCategoryException extends RuntimeException{
    public TemplateCategoryException() {
    }

    public TemplateCategoryException(String message) {
        super(message);
    }
}
