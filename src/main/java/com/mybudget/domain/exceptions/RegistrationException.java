package com.mybudget.domain.exceptions;

/**
 * @author Soumaya Izmar
 */
public class RegistrationException extends  RuntimeException{

    public RegistrationException(String message) {
        super(message);
    }
}
