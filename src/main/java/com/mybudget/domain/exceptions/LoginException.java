package com.mybudget.domain.exceptions;

/**
 * @author Soumaya Izmar
 */
public class LoginException extends RuntimeException{
    public LoginException() {
    }

    public LoginException(String message) {
        super(message);
    }
}
