package com.mybudget.domain.exceptions;

public class InvoiceException extends RuntimeException{

    public InvoiceException() {
    }

    public InvoiceException(String message) {
        super(message);
    }
}
