package com.mybudget.domain.services;

import com.mybudget.domain.dto.IngredientDto;
import com.mybudget.domain.exceptions.FatalException;
import com.mybudget.domain.exceptions.IngredientException;
import com.mybudget.domain.models.Ingredient;
import com.mybudget.domain.models.IngredientMonth;
import com.mybudget.domain.repository.IngredientMonthRepository;
import com.mybudget.domain.repository.IngredientRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.time.LocalDate;
import java.time.temporal.TemporalAdjusters;
import java.util.*;

@Service
public class IngredientService {
    @Autowired
    private IngredientRepository ingredientRepository;
    @Autowired
    private IngredientMonthRepository ingredientMonthRepository;
    @Autowired
    private TranslationService translationService;

    @Transactional
    public Map<Integer, Map<Long, IngredientDto>> getAll(String language) {
        SortedMap<Integer, Map<Long, IngredientDto>> ingredientList = new TreeMap<>();
        for (int i = 1; i <= 12; i++) {
            List<IngredientMonth> ingredientsOfMonth = (List<IngredientMonth>) ingredientMonthRepository.findAllByMonthOrderByPrice(i);
            SortedMap<Long, IngredientDto> ingredients = new TreeMap<>();
            for (IngredientMonth ingredientOfMonth : ingredientsOfMonth) {
                Ingredient ingredient = ingredientRepository
                        .findByIngredientID(ingredientOfMonth.getIngredient().getIngredientID());
                IngredientDto ingredientDto = serializeIngredientDto(ingredient, i, language);
                ingredients.put(ingredientDto.getId(), ingredientDto);
            }
            ingredientList.put(i, ingredients);
        }
        return ingredientList;
    }


    public IngredientDto serializeIngredientDto(Ingredient ingredient, Integer month, String language) {
        Double price = 0.;
        String name;

        Optional<IngredientMonth> ingredientMonth = ingredientMonthRepository.findByIngredientAndMonth(ingredient, month);
        if (ingredientMonth.isPresent()) {
            price = ingredientMonth.get().getPrice();
        }

        try {
            name = translationService.fetchIngredientsTranslationsById(language, ingredient.getName().getId());
        } catch (ClassNotFoundException e) {
            throw new FatalException();
        }
        return new IngredientDto(
                ingredient.getIngredientID(),
                name,
                ingredient.getUnit(),
                ingredient.getQuantity(),
                ingredient.getCalorificValue(),
                ingredient.getProteicValue(),
                price,
                ingredient.getIcon().getIconCode()
        );
    }
}
