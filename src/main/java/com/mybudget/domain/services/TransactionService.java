package com.mybudget.domain.services;

import com.mybudget.domain.dto.CategorySerializedDto;
import com.mybudget.domain.dto.TransactionCSV;
import com.mybudget.domain.dto.TransactionDto;
import com.mybudget.domain.dto.response.*;
import com.mybudget.domain.exceptions.AccountException;
import com.mybudget.domain.exceptions.CategoriesException;
import com.mybudget.domain.exceptions.FatalException;
import com.mybudget.domain.exceptions.TransactionException;
import com.mybudget.domain.models.*;
import com.mybudget.domain.models.indentities.TransactionCategoryPK;
import com.mybudget.domain.models.translation.TemplateCategoryNameTranslation;
import com.mybudget.domain.repository.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.YearMonth;
import java.time.format.DateTimeFormatter;
import java.util.*;
import java.util.stream.Collectors;

@Service
public class TransactionService {

    @Autowired
    private TransactionRepository transactionRepository;
    @Autowired
    private AccountRepository accountRepository;
    @Autowired
    private CategoryRepository categoryRepository;
    @Autowired
    private TransactionCategoryRepository transactionCategoryRepository;
    @Autowired
    private AuthenticationService authenticationService;
    @Autowired
    private CategoriesService categoriesService;
    @Autowired
    private TranslationService translationService;
    @Autowired
    private TemplateCategoryRepository templateCategoryRepository;
    @Autowired
    private TemplateCategoryNameTranslationRepository templateCategoryNameTranslationRepository;
    @Autowired
    private MetaCategoryRepository metaCategoryRepository;
    @Autowired
    private RecurringTransactionRepository recurringTransactionRepository;

    private static DecimalFormat df = new DecimalFormat("#,##0.00");
    private static DecimalFormatSymbols dfs = new DecimalFormatSymbols();
    private static String dateFormat = "dd/M/yyyy";
    private static int csvSize = 8;

    public TransactionService() {
        dfs.setDecimalSeparator(',');
        dfs.setGroupingSeparator('.');
        df.setDecimalFormatSymbols(dfs);
    }

    /**
     * Adds a new transaction to a user
     * If the transaction is marked as being a recurring transaction, a recurring transaction is added as well
     * Adds as well a TransactionCategory object per category chosen
     *
     * @param transactionDto
     * @return A TransactionDto object, the transaction newly added
     * @throws AccountException if the account chosen does not exist in DB
     * @see TransactionDto
     */
    public TransactionDto addTransaction(TransactionDto transactionDto) throws ClassNotFoundException {
        Account account = accountRepository.findById(transactionDto.getAccountID()).orElseThrow(() -> new AccountException("This account does not exist"));
        LocalDateTime executionDate = LocalDate.parse(transactionDto.getExecutionDate()).atStartOfDay();
        LocalDateTime valueDate = LocalDate.parse(transactionDto.getValueDate()).atStartOfDay();
        double amount = Double.parseDouble(transactionDto.getAmount());
        Transaction.TYPE type = Transaction.TYPE.valueOf(transactionDto.getType().toUpperCase());
        if (type.equals(Transaction.TYPE.OUTPUT)) {
            amount *= -1;
        }
        String name = transactionDto.getRecurringTransactionName();
        if (name == null) {
            Map<String, String> map = translationService.fetchAllTransactionsTranslation(account.getOwner().getPreferedLanguage().getCountryCode().toLowerCase(Locale.ROOT));
            if (type.equals(Transaction.TYPE.OUTPUT))
                name = map.get("payment");
            else
                name = map.get("transfer");
        }

        Transaction transaction = new Transaction(
                account,
                executionDate,
                valueDate,
                amount,
                type,
                name,
                null
        );

        // If transaction is marked as being recurring, a recurring transaction is added
        if (transactionDto.getRecurringTransactionName() != null && transactionDto.getRecurringTransactionEndDate() != null) {
            RecurringTransaction recurringTransaction = new RecurringTransaction();
            recurringTransaction.setEndDate(LocalDate.parse(transactionDto.getRecurringTransactionEndDate()).atStartOfDay());
            recurringTransaction.setName(transactionDto.getRecurringTransactionName());
            recurringTransactionRepository.save(recurringTransaction);
            transaction.setRecurringTransaction(recurringTransaction);
        }

        transaction = transactionRepository.save(transaction);

        // TransactionCategory added for each category chosen
        List<CategorySerializedDto> categories = transactionDto.getCategories();
        for (CategorySerializedDto cs : categories) {
            Category category = categoryRepository.findById(cs.getCategoryID()).orElseThrow(() -> new CategoriesException("The category does not exist"));
            TransactionCategoryPK transactionCategoryPK = new TransactionCategoryPK(category, transaction);
            TransactionCategory transactionCategory = new TransactionCategory(transactionCategoryPK, transaction.getAmount());
            transactionCategory.setAmount(cs.getAmount());
            transactionCategoryRepository.save(transactionCategory);
        }
        return serializeTransaction(transaction);
    }

    /**
     * Gets user's balance (all incomes - all expenses)
     *
     * @return A TransactionDto object, containing the balance
     * @see TransactionDto
     */
    public TransactionDto getUserBalance() {
        User user = authenticationService.getCurrentUser();
        Iterable<Transaction> transactions = transactionRepository.findByAccount_owner(user);
        double balance = 0;
        for (Transaction t : transactions) {
            balance += t.getAmount();
        }
        TransactionDto transactionDto = new TransactionDto();
        transactionDto.setBalance(df.format(balance));
        return transactionDto;
    }

    /**
     * Gets all the transactions of the user
     * Groups all the transactions by year and month
     *
     * @return A Map<LocalDate, List<TransactionDto>> => Transactions grouped by month
     * @see TransactionDto
     */
    public Map<LocalDate, List<TransactionDto>> getAllByUser() {
        User user = authenticationService.getCurrentUser();
        Iterable<Transaction> transactions = transactionRepository.findByAccount_ownerOrderByValueDateDesc(user);

        SortedMap<LocalDate, List<TransactionDto>> transactionsDto = new TreeMap<LocalDate, List<TransactionDto>>();

        for (Transaction t : transactions) {
            LocalDate date = LocalDate.of(t.getValueDate().getYear(), t.getValueDate().getMonthValue(), 1);
            if (!transactionsDto.containsKey(date))
                transactionsDto.put(date, new ArrayList<TransactionDto>());
            transactionsDto.get(date).add(serializeTransactionComplete(t));
        }

        return transactionsDto;
    }

    /**
     * Updates the countDate, valueDate, categories, amount, account of the specified transaction
     * Deletes all the old TransactionCategory linking the transaction to its previous categories
     * Creates all the TransactionCategory linking the transaction to its new categories and adding them in DB
     * Fetches the updated balance
     *
     * @param transactionDto: Transaction to be updated
     * @return A TransactionDto object, the one newly updated
     * @throws AccountException     if the account specified does not exist in DB
     * @throws TransactionException if the transactionID does not belong to any transaction existing in DB
     * @see TransactionDto
     */
    public TransactionDto patchTransaction(TransactionDto transactionDto) {
        Account account = accountRepository.findById(transactionDto.getAccountID()).orElseThrow(() -> new AccountException("Account does not exist"));

        Transaction transaction = transactionRepository.findById(transactionDto.getTransactionID()).orElseThrow(() -> new TransactionException("Transaction does not exist"));
        int a = LocalDate.parse(transactionDto.getValueDate()).atStartOfDay().compareTo(LocalDate.parse(transactionDto.getExecutionDate()).atStartOfDay());
        if(a>0){
            throw new TransactionException("erreur");
        }else {
            transaction.setCountDate(LocalDate.parse(transactionDto.getExecutionDate()).atStartOfDay());
            transaction.setValueDate(LocalDate.parse(transactionDto.getValueDate()).atStartOfDay());
        }
            transaction.setAccount(account);

        List<TransactionCategory> transactionCategoryList = transactionCategoryRepository.findByTransactionCategoryPK_TransactionID(transaction);
        transactionCategoryRepository.deleteAll(transactionCategoryList);

        Double amount = 0.0;

        for (CategorySerializedDto cat : transactionDto.getCategories()) {
            amount += cat.getAmount();
            Category c = categoryRepository.findById(cat.getCategoryID()).orElseThrow(() -> new CategoriesException());
            TransactionCategoryPK tcpk = new TransactionCategoryPK(c, transaction);
            TransactionCategory tc = new TransactionCategory(tcpk, cat.getAmount());
            transactionCategoryRepository.save(tc);
        }

        Transaction.TYPE type = Transaction.TYPE.valueOf(transactionDto.getType().toUpperCase());
        if (type.equals(Transaction.TYPE.OUTPUT)) {
            amount *= -1;
        }
        transaction.setAmount(amount);
        transaction.setType(type);
        transaction = transactionRepository.save(transaction);

        TransactionDto tdto = serializeTransactionComplete(transaction);
        tdto.setBalance(getUserBalance().getBalance());
        return tdto;
    }

    /**
     * Deletes a transaction based on its id
     * Deletes all the TransactionCategory linked to it
     *
     * @param id: Id of the transaction to be deleted
     * @return An ObjectResponse object
     * @throws TransactionException if the id does not belong to any existing transaction in DB
     * @see ObjectResponse
     */
    @Transactional
    public ObjectResponse deleteTransaction(int id) {
        Transaction transac = transactionRepository.findById(id).orElseThrow(() -> new TransactionException("Transaction does not exist"));
        transactionCategoryRepository.deleteByTransactionCategoryPK_TransactionID(transac);
        transactionRepository.deleteById(id);
        return new ObjectResponse(id);
    }

    /**
     * Gets the daily budget of a user => max amount that user can spend today VS the amount already spent
     * Max daily budget is calculated by doing the sum of its monthly budget divided by the amount of days in current month
     * Amount already spent is calculated by doing the sum of all the transactions of the current day
     *
     * @return A BudgetResponseObject
     * @see BudgetResponse
     */
    public BudgetResponse getDailyBudget() {
        List<CategorySerializedDto> categories = categoriesService.getAllByUser();
        double dailyBudget = 0;
        for (CategorySerializedDto cat : categories) {
            if (cat.getType().equals("OUTPUT") && cat.getIdParentCategory() == -1) {
                dailyBudget += cat.getBudget();
            }
        }
        LocalDate today = LocalDate.now();
        int amountOfDayInCurrentMonth = YearMonth.of(today.getYear(), today.getMonthValue()).lengthOfMonth();
        String dailyBudgetFormatted = df.format(dailyBudget / amountOfDayInCurrentMonth);

        User user = authenticationService.getCurrentUser();
        List<Transaction> transactionsOfToday = transactionRepository.findByValueDateAndAccount_owner(today.atStartOfDay(), user);

        double amountAlreadySpent = 0;

        for (Transaction t : transactionsOfToday) {
            if (t.getType().equals(Transaction.TYPE.OUTPUT)) {
                amountAlreadySpent += t.getAmount();
            }
        }
        if (amountAlreadySpent < 0)
            amountAlreadySpent *= (-1);
        return new BudgetResponse(dailyBudgetFormatted, df.format(amountAlreadySpent));
    }

    /**
     * Gets the weekly budget of a user => max amount that user can spend this week VS the amount already spent
     * Max weekly budget is calculated by doing the sum of its monthly budget divided by the amount of weeks in a month
     * Amount already spent is calculated by doing the sum of all the transactions of the current week
     *
     * @return A BudgetResponseObject
     * @see BudgetResponse
     */
    public BudgetResponse getWeeklyBudget() {
        List<CategorySerializedDto> categories = categoriesService.getAllByUser();
        double weeklyBudget = 0;
        for (CategorySerializedDto cat : categories) {
            if (cat.getType().equals("OUTPUT") && cat.getIdParentCategory() == -1) {
                weeklyBudget += cat.getBudget();
            }
        }
        String weeklyBudgetFormatted = df.format(weeklyBudget / 4);

        int dayOfWeek = LocalDateTime.now().getDayOfWeek().getValue();
        LocalDateTime firstDayOfWeek = LocalDate.now().minusDays((long)dayOfWeek - 1).atTime(LocalTime.MIDNIGHT);
        LocalDateTime lastDayOfWeek = LocalDate.now().plusDays((long)7 - dayOfWeek).atTime(LocalTime.MIDNIGHT);

        User user = authenticationService.getCurrentUser();
        Iterable<Transaction> allTransactions = transactionRepository.findByAccount_owner(user);

        double amountAlreadySpent = 0;

        for (Transaction t : allTransactions) {
            LocalDateTime valueDate = t.getValueDate();
            if (t.getType().equals(Transaction.TYPE.OUTPUT)
                    && (valueDate.isEqual(firstDayOfWeek) || valueDate.isAfter(firstDayOfWeek))
                    && (valueDate.isEqual(lastDayOfWeek) || valueDate.isBefore(lastDayOfWeek))) {
                amountAlreadySpent += t.getAmount();
            }
        }
        if (amountAlreadySpent < 0)
            amountAlreadySpent *= (-1);

        return new BudgetResponse(weeklyBudgetFormatted, df.format(amountAlreadySpent));
    }

    /**
     * Gets the monthly budget of a user => max amount that user can spend this month VS the amount already spent
     * Max monthly budget is calculated by doing the sum of its monthly budget
     * Amount already spent is calculated by doing the sum of all the transactions of the current month
     *
     * @return A BudgetResponseObject
     * @see BudgetResponse
     */
    public BudgetResponse getMonthlyBudget() {
        List<CategorySerializedDto> categories = categoriesService.getAllByUser();
        double monthlyBudget = 0;
        for (CategorySerializedDto cat : categories) {
            if (cat.getType().equals("OUTPUT") && cat.getIdParentCategory() == -1) {
                monthlyBudget += cat.getBudget();
            }
        }
        String weeklyBudgetFormatted = df.format(monthlyBudget);

        User user = authenticationService.getCurrentUser();
        Iterable<Transaction> allTransactions = transactionRepository.findByAccount_owner(user);

        LocalDateTime now = LocalDateTime.now();
        double amountAlreadySpent = 0;
        for (Transaction t : allTransactions) {
            LocalDateTime valueDate = t.getValueDate();
            if (t.getType().equals(Transaction.TYPE.OUTPUT)
                    && t.getValueDate().getMonth().equals(now.getMonth())
                    && t.getValueDate().getYear() == now.getYear()) {
                amountAlreadySpent += t.getAmount();
            }
        }
        if (amountAlreadySpent < 0)
            amountAlreadySpent *= (-1);

        return new BudgetResponse(weeklyBudgetFormatted, df.format(amountAlreadySpent));
    }

    /**
     * Gets all the incomes and expenses of the current user of the past 6 months
     * Fetches all the transactions of the user and sums them up by month and by type
     * Each monthly based grouping contains a map with 2 keys
     * INPUT: containing the sum of all the incomes of the month
     * OUTPUT: containing the sum of all the expenses of the month
     *
     * @return Map<LocalDate, Map < String, Double>> => Total amount earn and spent by month
     * @see Transaction
     */
    public Map<LocalDate, Map<String, Double>> getRecentSavings() {
        User user = authenticationService.getCurrentUser();
        Iterable<Transaction> transactions = transactionRepository.findByAccount_ownerOrderByValueDateDesc(user);

        SortedMap<LocalDate, Map<String, Double>> treeMap = new TreeMap<LocalDate, Map<String, Double>>();

        for (Transaction t : transactions) {
            LocalDate date = LocalDate.of(t.getValueDate().getYear(), t.getValueDate().getMonthValue(), 1);
            if (!treeMap.containsKey(date)) {
                Map map = new HashMap<String, Double>();
                map.put(Transaction.TYPE.OUTPUT.toString(), 0.0);
                map.put(Transaction.TYPE.INPUT.toString(), 0.0);
                treeMap.put(date, map);
            }
            treeMap.get(date).put(
                    t.getType().toString(),
                    treeMap.get(date).get(t.getType().toString()) + t.getAmount()
            );
        }

        NavigableSet<LocalDate> dates = ((TreeMap<LocalDate, Map<String, Double>>) treeMap).descendingKeySet();

        SortedMap<LocalDate, Map<String, Double>> recentSavings = new TreeMap<LocalDate, Map<String, Double>>();

        int i = 0;
        int amountMonthToDisplay = 6;
        for (Iterator<LocalDate> it = dates.iterator(); it.hasNext() && i < amountMonthToDisplay; ) {
            LocalDate date = it.next();
            recentSavings.put(date, treeMap.get(date));
            i++;
        }

        return recentSavings;
    }

    /**
     * Gets all the recurring transactions of the current user
     * Sereliazes each of the into a TransactionDto object
     *
     * @return A list of TransactionDto, all of the recurring transactions of the user
     * @see TransactionDto
     */
    public List<TransactionDto> getAllRecurringTransactions() {
        User currentUser = authenticationService.getCurrentUser();
        List<Transaction> recurringTransaction = transactionRepository.findDistinctByRecurringTransactionNotNullAndAccount_owner(currentUser);

        List<TransactionDto> transactionDto = new ArrayList<>();
        for (Transaction t : recurringTransaction) {
            transactionDto.add(serializeTransactionComplete(t));
        }
        return transactionDto;
    }


    /**
     * Serializes a transaction into a TransactionDto, getting primitive types out of objects
     *
     * @param t: Transaction to serialize
     * @return A TransactionDto object, the transaction serialized
     * @see TransactionDto
     */
    private TransactionDto serializeTransaction(Transaction t) {
        TransactionDto transactionDto = new TransactionDto(
                t.getTransactionID(),
                t.getAccount().getAccountId(),
                df.format(t.getAmount()),
                t.getCountDate().toString(),
                t.getValueDate().toString(),
                t.getType().name(),
                t.getName()
        );
        return transactionDto;
    }

    /**
     * Serializes a transaction into a TransactionDto with more fields, getting primitive types out of objects
     *
     * @param t: Transaction to serialize
     * @return A TransactionDto object, the transaction serialized
     * @see TransactionDto
     */
    public TransactionDto serializeTransactionComplete(Transaction t) {
        List<CategorySerializedDto> cat = categoriesService.getAllByTransaction(t);

        TransactionDto transactionDto = new TransactionDto(
                t.getTransactionID(),
                t.getAccount().getAccountId(),
                df.format(t.getAmount()),
                t.getCountDate().toString(),
                t.getValueDate().toString(),
                t.getType().name(),
                t.getAccount().getType().getIcon().getIconCode(),
                t.getAccount().getName(),
                t.getValueDate().getDayOfMonth(),
                t.getValueDate().getMonthValue(),
                t.getValueDate().getYear(),
                cat,
                t.getRecurringTransaction() != null ? t.getRecurringTransaction().getName() : null,
                t.getRecurringTransaction() != null ? t.getRecurringTransaction().getEndDate().toString() : null,
                t.getName()
        );
        return transactionDto;
    }


    @Transactional
    public ShoppingBudgetResponse getAllCourseTransactions(boolean isBudgetShopping, Integer month, Integer year, String category, String typeName) {

        ShoppingBudgetResponse shoppingBudgetResponse = new ShoppingBudgetResponse();
        shoppingBudgetResponse.setTotalAmount(0.0);

        return getAllCourseTransactions(isBudgetShopping, month, year, category, shoppingBudgetResponse, typeName);
    }

    private ShoppingBudgetResponse getAllCourseTransactions(boolean isBudgetShopping, Integer month, Integer year, String category, ShoppingBudgetResponse shoppingBudgetResponseParam, String typeName) {


        User user = authenticationService.getCurrentUser();
        Category.TYPE catType = Category.TYPE.valueOf(typeName.toUpperCase());
        Category c = categoryRepository.findByNameAndOwnerAndType(category, user, catType).orElse(null);

        List<Category> list = categoryRepository.findByParentCategoryAndOwnerAndType(c, user, catType);
        List<CategoryTransactionResponse> categoryTransactionResponses = new ArrayList<>();

        if (isBudgetShopping) {
            if (list.size() == 0 && c != null) {
                list.add(c);
            }
        }
        for (Category cat : list) {

            List<TransactionCategory> listTransactions = transactionCategoryRepository.findByTransactionCategoryPK_CategoryID(cat);
            CategoryTransactionResponse categoryTransactionResponse = new CategoryTransactionResponse();
            categoryTransactionResponse.setName(cat.getName());
            categoryTransactionResponse.setIcon(cat.getIcon().getIconCode());
            categoryTransactionResponse.setNbTransactions(0);
            categoryTransactionResponse.setTotalAmount(0.0);

            for (TransactionCategory transactionCategory : listTransactions) {
                Transaction t = transactionCategory.getTransactionCategoryPK().getTransactionID();
                if (t.getValueDate().getMonthValue() == month && t.getValueDate().getYear() == year) {

                    categoryTransactionResponse.setNbTransactions(categoryTransactionResponse.getNbTransactions() + 1);
                    categoryTransactionResponse.setTotalAmount(categoryTransactionResponse.getTotalAmount() + t.getAmount());

                }
            }

            List<CategoryTransactionResponse> subTransactionList = new ArrayList<>();

            double totalAmount = 0;
            if (list.size() > 2) {
                ShoppingBudgetResponse subTransactions = getAllCourseTransactions(isBudgetShopping, month, year, cat.getName(), shoppingBudgetResponseParam, typeName);
                totalAmount = subTransactions.getTotalAmount();
                subTransactionList = subTransactions.getAllShoppingTransactions();
            }

            categoryTransactionResponse.setList(subTransactionList);
            categoryTransactionResponse.setTotalAmount(Math.abs(categoryTransactionResponse.getTotalAmount() + subTransactionList.stream().mapToDouble(CategoryTransactionResponse::getTotalAmount).sum()));
            categoryTransactionResponse.setNbTransactions(categoryTransactionResponse.getNbTransactions() + subTransactionList.stream().mapToInt(CategoryTransactionResponse::getNbTransactions).sum());

            if (categoryTransactionResponse.getTotalAmount() != 0) {
                categoryTransactionResponses.add(categoryTransactionResponse);
            }

        }

        shoppingBudgetResponseParam.setAllShoppingTransactions(categoryTransactionResponses.stream().sorted((t1, t2) -> Double.compare(t2.getTotalAmount(), t1.getTotalAmount())).collect(Collectors.toList()));
        shoppingBudgetResponseParam.setTotalAmount(shoppingBudgetResponseParam.getAllShoppingTransactions().stream().mapToDouble(CategoryTransactionResponse::getTotalAmount).sum());

        if (c != null) {
            shoppingBudgetResponseParam.setTotalBudget(c.getBudget());
        }
        return shoppingBudgetResponseParam;
    }


    public List<Transaction> getAllCourseTransactionsWithNoResponse(Integer month, Integer year) throws ClassNotFoundException {
        User user = authenticationService.getCurrentUser();
        Map<String, String> map = translationService.fetchCategoryTemplateTranslation(user.getPreferedLanguage().getCountryCode().toLowerCase(Locale.ROOT));

        Category c = categoryRepository.findByNameAndOwner(map.get("groceries"), user).orElse(null);

        List<Category> list = categoryRepository.findByParentCategoryAndOwner(c, user);

        List<Transaction> listTransactionsTotal = new ArrayList<>();
        for (Category cat : list) {
            List<TransactionCategory> listTransactions = transactionCategoryRepository.findByTransactionCategoryPK_CategoryID(cat);

            for (TransactionCategory transactionCategory : listTransactions) {
                Transaction t = transactionCategory.getTransactionCategoryPK().getTransactionID();
                if (t.getValueDate().getMonthValue() == month && t.getValueDate().getYear() == year) {
                    listTransactionsTotal.add(t);
                }
            }
        }
        if (listTransactionsTotal.size() == 0) {
            List<TransactionCategory> listTransactions = transactionCategoryRepository.findByTransactionCategoryPK_CategoryID(c);

            for (TransactionCategory transactionCategory : listTransactions) {
                Transaction t = transactionCategory.getTransactionCategoryPK().getTransactionID();
                if (t.getValueDate().getMonthValue() == month && t.getValueDate().getYear() == year) {
                    listTransactionsTotal.add(t);
                }
            }
        }
        return listTransactionsTotal;
    }

    @Transactional
    public List<ShoppingWeekResponse> getAmountBudgetTransactionsPerWeek(Integer month, Integer year) throws ClassNotFoundException {

        User user = authenticationService.getCurrentUser();
        Map<String, String> map = translationService.fetchMonthsTranslation(user.getPreferedLanguage().getCountryCode().toLowerCase(Locale.ROOT));

        YearMonth yearMonthObject = YearMonth.of(year, month);
        int daysInMonth = yearMonthObject.lengthOfMonth();
        String monthText = map.get(Integer.toString(month));
        Calendar c = Calendar.getInstance();
        Date date = new GregorianCalendar(yearMonthObject.getYear(), month - 1, 1).getTime();
        c.setTime(date);
        int dayOfWeek = c.get(Calendar.DAY_OF_WEEK) - 1;


        List<Transaction> listTransactions = getAllCourseTransactionsWithNoResponse(month, year);
        Double[] list = new Double[7];
        List<ShoppingWeekResponse> listTotal = new ArrayList<>();
        int currentDay = 0;
        int dayPass = dayOfWeek - 1;
        double tempAmount = 0;
        int tabIndex = dayOfWeek - 1;

        for (int i = 0; i <= dayOfWeek - 1; i++) {
            list[i] = null;
        }


        for (int i = 1; i <= daysInMonth; i++) {
            dayPass++;
            currentDay = i;
            for (Transaction t : listTransactions) {
                if (t.getValueDate().getDayOfMonth() == i) {
                    tempAmount += t.getAmount();
                }
            }
            list[tabIndex] = compute(tempAmount);
            tabIndex++;
            tempAmount = 0;

            if (dayPass == 7) {
                int currentDayText = currentDay + 1 - dayPass;
                if (currentDayText < 0) {
                    currentDayText = 1;
                }

                String currentWeek = currentDayText + "-" + currentDay + " " + monthText;
                ShoppingWeekResponse shoppingWeekResponse = new ShoppingWeekResponse(list, currentWeek);
                listTotal.add(shoppingWeekResponse);
                list = new Double[7];
                tabIndex = 0;
                dayPass = 0;
            }
        }


        String currentWeek = currentDay + 1 - dayPass + "-" + currentDay + " " + monthText;
        ShoppingWeekResponse shoppingWeekResponse = new ShoppingWeekResponse(list, currentWeek);
        listTotal.add(shoppingWeekResponse);

        return listTotal;
    }

    public double compute(double factor) {
        factor = Math.abs(factor);
        factor = Math.round(factor * 100);

        return factor / 100;
    }

    public enum CSVINDEX {
        EXECUTION_DATE(1), VALUE_DATE(2), AMOUNT(3), DEVISE(4), DETAILS(6);
        private int value;

        CSVINDEX(int i) {
            this.value = i;
        }

        public int getValue() {
            return value;
        }

        public void setValue(int value) {
            this.value = value;
        }
    }

    public void setVisaMastercardIndex() {

        CSVINDEX.EXECUTION_DATE.setValue(0);
        CSVINDEX.VALUE_DATE.setValue(1);
        CSVINDEX.AMOUNT.setValue(2);
        CSVINDEX.DETAILS.setValue(4);
        csvSize = 7;
    }

    public void setBNPIndex() {
        CSVINDEX.EXECUTION_DATE.setValue(1);
        CSVINDEX.VALUE_DATE.setValue(2);
        CSVINDEX.AMOUNT.setValue(3);
        CSVINDEX.DETAILS.setValue(6);
        csvSize = 8;
    }

    public void addTransactionWithCSV(TransactionCSV transactionCSV) throws ClassNotFoundException {

        User user = authenticationService.getCurrentUser();

        Map<String, String> map = translationService.fetchHomepageTranslation(user.getPreferedLanguage().getCountryCode().toLowerCase(Locale.ROOT));

        transactionCSV.getListTransaction().remove(0);
        List<Account> listAccounts = accountRepository.findAllByOwner(user);
        Account account = listAccounts.stream().filter(a -> a.getName().equals(transactionCSV.getAccount())).findFirst().orElseThrow(FatalException::new);

        chooseAccount(account.getName(), map.get("accountNotFound"));

        for (List<String> list : transactionCSV.getListTransaction()) {

            //it's the last line (last line is always empty)
            if (list.size() == 1) {
                break;
            }
            // the row in the csv is not the right size
            if (list.size() != csvSize) {
                throw new TransactionException(map.get("wrongCsvFormat"));
            }

            Transaction newTransaction = new Transaction();
            newTransaction.setAccount(account);

            LocalDateTime valueDate = validDate(list.get(CSVINDEX.VALUE_DATE.getValue()), map.get("wrongCsvFormat")).atStartOfDay();
            newTransaction.setValueDate(valueDate);

            LocalDateTime executionDate = validDate(list.get(CSVINDEX.EXECUTION_DATE.getValue()), map.get("wrongCsvFormat")).atStartOfDay();

            newTransaction.setCountDate(executionDate);


            newTransaction = checkAmountAndParsing(list.get(CSVINDEX.AMOUNT.getValue()), newTransaction, map.get("wrongCsvFormat"));

            Iterable<MetaCategory> metaCategories = metaCategoryRepository.findAll();
            TemplateCategoryNameTranslation templateCategoryNameTranslation = null;

            for (MetaCategory metaCategory : metaCategories) {
                String details = list.get(CSVINDEX.DETAILS.value);
                if (details.toLowerCase(Locale.ROOT).contains(metaCategory.getDetails().toLowerCase(Locale.ROOT))) {
                    String name = metaCategory.getTemplateCategory().getName().getId();
                    templateCategoryNameTranslation = templateCategoryNameTranslationRepository.findById(name).orElse(null);

                }
            }

            templateCategoryNameTranslation = getGoodTemplateTranslation(templateCategoryNameTranslation, newTransaction);

            TemplateCategory templateCategory = templateCategoryRepository.findByName(templateCategoryNameTranslation).orElseThrow(TransactionException::new);

            Category category = categoryRepository.findByNameAndOwner(templateCategory.getName().getTranslation(user.getPreferedLanguage().getCountryCode()), user).orElseThrow(TransactionException::new);
            TransactionCategoryPK transactionCategoryPK = new TransactionCategoryPK(category, newTransaction);
            TransactionCategory transactionCategory = new TransactionCategory(transactionCategoryPK, newTransaction.getAmount());
            transactionCategory.setAmount(transactionCategory.getAmount());

            categoryRepository.save(category);
            transactionRepository.save(newTransaction);
            transactionCategoryRepository.save(transactionCategory);
        }

    }

    /**
     * Change CSV INDEX to match the right account
     * @param account
     * @param error
     */
    private void chooseAccount(String account, String error) {

        switch (account.toLowerCase(Locale.ROOT)) {
            case "visa":
            case "mastercard":
                setVisaMastercardIndex();
                break;
            case "bnp paribas fortis":
                setBNPIndex();
                break;
            default:
                throw new TransactionException(error);
        }

    }


    private Transaction checkAmountAndParsing(String amountText, Transaction newTransaction, String error) throws ClassNotFoundException {
        Map<String, String> map = translationService.fetchAllTransactionsTranslation(newTransaction.getAccount().getOwner().getPreferedLanguage().getCountryCode().toLowerCase(Locale.ROOT));
        double multiplication = 1;

        String name;
        //check if the transaction is an output or an input
        if (amountText.startsWith("-")) {
            amountText = amountText.split("-")[1];
            newTransaction.setType(Transaction.TYPE.OUTPUT);
            name = map.get("payment");
            multiplication = -1;
        } else {
            newTransaction.setType(Transaction.TYPE.INPUT);
            name = map.get("transfer");

        }
        newTransaction.setName(name);
        amountText = amountText.replace(',', '.');

        try {
            Double amount = Double.parseDouble(amountText) * multiplication;
            newTransaction.setAmount(amount);
        } catch (Exception e) {
            throw new TransactionException(error);
        }

        return newTransaction;
    }

    private TemplateCategoryNameTranslation getGoodTemplateTranslation(TemplateCategoryNameTranslation templateCategoryNameTranslation, Transaction newTransaction) {


        if (templateCategoryNameTranslation == null) {
            if (newTransaction.getType() == Transaction.TYPE.INPUT) {
                return templateCategoryNameTranslationRepository.findById("unknown entries").orElse(null);
            } else {
                return templateCategoryNameTranslationRepository.findById("unknown income").orElse(null);
            }
        }
        return templateCategoryNameTranslation;
    }


    private LocalDate validDate(String date, String error) {
        // throw exception if fail
        try {
            return LocalDate.parse(date, DateTimeFormatter.ofPattern(dateFormat));
        } catch (Exception e) {
            throw new TransactionException(error);
        }
    }


}
