package com.mybudget.domain.services;

import com.mybudget.domain.dto.IngredientDto;
import com.mybudget.domain.dto.RecipeDto;
import com.mybudget.domain.dto.ShoppingListDto;
import com.mybudget.domain.dto.response.DefaultShoppingListRequestWrapper;
import com.mybudget.domain.dto.response.ObjectResponse;
import com.mybudget.domain.exceptions.FatalException;
import com.mybudget.domain.exceptions.ShoppingListException;
import com.mybudget.domain.models.*;
import com.mybudget.domain.models.indentities.IngredientShoppingListPK;
import com.mybudget.domain.models.translation.IngredientsTranslations;
import com.mybudget.domain.repository.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.time.Instant;
import java.time.LocalDate;
import java.util.*;

@Service
public class ShoppingListService {
    @Autowired
    private AuthenticationService authenticationService;

    @Autowired
    private ShoppingListRepository shoppingListRepository;

    @Autowired
    private IngredientShoppingListRepository ingredientShoppingListRepository;

    @Autowired
    private RecipeShoppingListRepository recipeShoppingListRepository;

    @Autowired
    private IconRepository iconRepository;

    @Autowired
    private RecipeRepository recipeRepository;

    @Autowired
    private IngredientService ingredientService;

    @Autowired
    private RecipeService recipeService;

    @Autowired
    private IngredientRepository ingredientRepository;

    @Autowired
    private TranslationService translationService;


    public Map<Long, ShoppingListDto> getAllShoppingList(String language){

        User user = authenticationService.getCurrentUser();

        SortedMap<Long, ShoppingListDto> shoppingLists = new TreeMap<>();
        Integer month = LocalDate.now().getMonthValue();

        List<ShoppingList> shoppingListsQuery = shoppingListRepository.findAllByOwner(user);
        for (ShoppingList shoppingList : shoppingListsQuery) {

            Icon icon = iconRepository.findByIconCode("fas fa-shopping-cart");
            List<IngredientDto> ingredients = new ArrayList<>();
            List<RecipeDto> recipes = new ArrayList<>();

            List<IngredientShoppingList> ingredientsQuery =
                    ingredientShoppingListRepository.findAllByIngredientShoppingListPK_ShoppingList(shoppingList);

           for (IngredientShoppingList ingredientQuery : ingredientsQuery) {
               Ingredient ingredient = ingredientQuery.getIngredientShoppingListPK().getIngredient();

               IngredientDto ingredientDto = ingredientService.serializeIngredientDto(
                       ingredient, month, language);

               float quantite = (ingredientQuery.getQuantity()/ingredientQuery.getIngredientShoppingListPK().getIngredient().getQuantity());


               if (ingredientDto.getPrice() > 0) {
                   ingredientDto.setPrice(
                           (new BigDecimal(ingredientDto.getPrice() * quantite)
                                   .setScale(2, RoundingMode.HALF_UP)).doubleValue()
                   );
               } else {
                   ingredientDto.setPrice(0.);
               }

               if (ingredientDto.getCalorificValue() > 0) {
                   ingredientDto.setCalorificValue(
                           (new BigDecimal(ingredientDto.getCalorificValue() * quantite)
                                   .setScale(2, RoundingMode.HALF_UP)).floatValue()
                   );
               } else {
                   ingredientDto.setCalorificValue(0f);
               }

               if (ingredientDto.getProteicValue() > 0) {
                   ingredientDto.setProteicValue(
                           (new BigDecimal(ingredientDto.getProteicValue() * quantite)
                                   .setScale(2, RoundingMode.HALF_UP)).floatValue()
                   );
               } else {
                   ingredientDto.setProteicValue(0f);
               }

               ingredientDto.setQuantity(ingredientQuery.getQuantity());
               ingredients.add(ingredientDto);
            }

           List<RecipeShoppingList> recipesQuery =
                   (List<RecipeShoppingList>) recipeShoppingListRepository
                           .findAllByRecipeShoppingListPK_ShoppingList(shoppingList);
            for (RecipeShoppingList recipeQuery: recipesQuery) {
               Recipe recipe = recipeQuery.getRecipeShoppingListPK().getRecipe();
               RecipeDto recipeDto = recipeService.getRecipeDtoFromRecipe(
                       recipe, month, language, recipeQuery.getServings());
               recipes.add(recipeDto);
           }

            ShoppingListDto shoppingListDto = serializeShoppingListDto(shoppingList, icon.getIconCode());
            shoppingListDto.setIngredients(ingredients);
            shoppingListDto.setRecipes(recipes);
            shoppingLists.put(shoppingListDto.getId(), shoppingListDto);
        }
        return shoppingLists;
    }


    public ShoppingListDto deleteShoppingList(Long id) {
        ShoppingListDto shoppingListDto = new ShoppingListDto();
        try {
            ShoppingList shoppingList = shoppingListRepository.findById(id).orElseThrow(()
                    -> new ShoppingListException("shopping list does not exist"));
            shoppingListDto = serializeShoppingListDto(shoppingList, null);

            // Delete from ingredient_shopping_list
            List<IngredientShoppingList> ingredientShoppingLists =
                    ingredientShoppingListRepository
                            .findAllByIngredientShoppingListPK_ShoppingList(shoppingList);
            ingredientShoppingListRepository.deleteAll(ingredientShoppingLists);

            // Delete from recipe_shopping_list
            Iterable<RecipeShoppingList> recipeShoppingLists =
                    recipeShoppingListRepository
                            .findAllByRecipeShoppingListPK_ShoppingList(shoppingList);
            recipeShoppingListRepository.deleteAll(recipeShoppingLists);

            // Delete shopping list
            shoppingListRepository.deleteById(id);
        } catch (Exception ex) {
            throw new ShoppingListException("shopping list error");
        }
        return shoppingListDto;
    }


    public ShoppingListDto serializeShoppingListDto(ShoppingList shoppingList, String icon) {
        return new ShoppingListDto(
                shoppingList.getShoppingListID(),
                shoppingList.getName(),
                shoppingList.getShopping_date(),
                shoppingList.getTotal(),
                icon
        );
    }

    public ShoppingListDto addShoppingList(DefaultShoppingListRequestWrapper requestWrapper) {

        User user = authenticationService.getCurrentUser();

        ShoppingListDto shoppingList = requestWrapper.getShoppingList();
        String language = requestWrapper.getLanguageCode();

        Date date =  Date.from(Instant.now());
        String name = "Shopping List";

        try {
            name = translationService
                    .fetchShoppingListsModuleTranslationsById(language, "shoppingList")
                    + " "
                    + translationService.fetchShoppingListsModuleTranslationsById(language, "ofTheMasc")
                    + " "
                    + LocalDate.now().toString();
        } catch (ClassNotFoundException e) {
            throw new FatalException();
        }
        ShoppingList sl = new ShoppingList(
                name,
                user,
                date,
                shoppingList.getTotal()
                );
        sl = shoppingListRepository.save(sl);

        Icon iconShop = iconRepository.findByIconCode("fas fa-shopping-cart");
        ShoppingListDto shoppingListDto = new ShoppingListDto(sl, iconShop.getIconCode());
        shoppingListDto.setIngredients(shoppingList.getIngredients());

        for(int i = 0;i < shoppingList.getIngredients().size();i++){

            Ingredient it = ingredientRepository.findByIngredientID(shoppingList.getIngredients().get(i).getId());
            IngredientsTranslations ii= it.getName();

            Icon icon = iconRepository.findByIconCode(shoppingList.getIngredients().get(i).getIcon());

            Ingredient ing = new Ingredient(shoppingList.getIngredients().get(i),ii,icon);

            IngredientShoppingListPK ingredientShoppingListPK = new IngredientShoppingListPK(ing,sl);

            IngredientShoppingList isl =
                    new IngredientShoppingList(ingredientShoppingListPK,shoppingList.getIngredients().get(i).getQuantity());

            ingredientShoppingListRepository.save(isl);
        }

        return shoppingListDto;

    }
}
