package com.mybudget.domain.services;

import com.aventrix.jnanoid.jnanoid.NanoIdUtils;
import com.mybudget.domain.dto.*;
import com.mybudget.domain.dto.response.AuthenticationResponse;
import com.mybudget.domain.exceptions.FatalException;
import com.mybudget.domain.exceptions.LoginException;
import com.mybudget.domain.exceptions.RegistrationException;
import com.mybudget.domain.models.Currency;
import com.mybudget.domain.models.*;
import com.mybudget.domain.models.translation.StatusTranslation;
import com.mybudget.domain.repository.*;
import com.mybudget.domain.utils.JwtService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.security.SecureRandom;
import java.time.LocalDateTime;
import java.util.*;
import java.util.stream.Collectors;

/**
 * @author Soumaya Izmar
 */
@Component
public class AuthenticationService {

    @Autowired
    private UserRepository userRepository;
    @Autowired
    private LanguageRepository languageRepository;
    @Autowired
    private CurrencyRepository currencyRepository;
    @Autowired
    private StatusRepository statusRepository;
    @Autowired
    private CategoryStatusRepository categoryStatusRepository;
    @Autowired
    private CategoryRepository categoryRepository;
    @Autowired
    private DefaultAccountRepository defaultAccountRepository;
    @Autowired
    private AccountRepository accountRepository;
    @Autowired
    private InvoiceRepository invoiceRepository;
    @Autowired
    private ProjectRepository projectRepository;
    @Autowired
    private RecurringTransactionRepository recurringTransactionRepository;
    @Autowired
    private TransactionRepository transactionRepository;
    @Autowired
    private TransactionCategoryRepository transactionCategoryRepository;
    @Autowired
    private FriendshipRepository friendshipRepository;

    @Autowired
    private JwtService jwtService;
    @Autowired
    private TranslationService translationService;
    @Autowired
    private PasswordEncoder passwordEncoder;
    @Autowired
    private SubStatusUsersRepository subStatusUsersRepository;
    @Autowired
    private CategorySubStatusRepository categorySubStatusRepository;
    @Autowired
    private SubStatusRepository subStatusRepository;


    @Autowired
    private StatusTranslationRepository statusTranslationRepository;

    private final SecureRandom random = new SecureRandom();


    /**
     * login a user
     *
     * @return AuthenticationResponse
     * @see AuthenticationResponse
     */
    @Transactional
    public AuthenticationResponse loginUser(LoginDto loginRequest) throws LoginException, ClassNotFoundException {

        Map<String, String> translations = translationService.fetchLoginTranslation(loginRequest.getPreferredLanguage());
        User currentUser = userRepository.findByEmail(loginRequest.getEmail()).orElseThrow(() -> new LoginException(translations.get("connectionError")));

        if (passwordEncoder.matches(loginRequest.getPassword(), currentUser.getPassword())) {
            String token = jwtService.createToken(currentUser.getEmail(), currentUser.getUserID().toString());

            currentUser.setDeviceToken(loginRequest.getToken());
            userRepository.save(currentUser);
            return new AuthenticationResponse(token);
        } else {
            throw new LoginException(translations.get("connectionError"));
        }
    }

    /**
     * register a user
     *
     * @return AuthenticationResponse
     * @see AuthenticationResponse
     */
    @Transactional
    public AuthenticationResponse registerUser(UserRegistrationDto registrationRequest) throws RegistrationException, ClassNotFoundException {

        if (!registrationRequest.getPassword().equals(registrationRequest.getPasswordRepeat())) {
            Map<String, String> translations = translationService.fetchSignUpTranslation(registrationRequest.getPreferedLanguage());
            throw new RegistrationException(translations.get("passwordMatch"));
        }

        User userWithEmail = userRepository.findByEmail(registrationRequest.getEmail()).orElse(null);

        if (userWithEmail != null) {
            Map<String, String> translations = translationService.fetchSignUpTranslation(registrationRequest.getPreferedLanguage());
            throw new RegistrationException(translations.get("emailExist"));
        }

        Language preferredLanguage = languageRepository.findByCountryCode(registrationRequest.getPreferedLanguage().toUpperCase(Locale.ROOT)).orElseThrow(FatalException::new);
        Currency preferredCurrency = currencyRepository.findByType(registrationRequest.getPreferedCurrency()).orElseThrow(FatalException::new);

        StatusTranslation trans = statusTranslationRepository.findById(registrationRequest.getStatus()).orElseThrow(FatalException::new);
        Status status = statusRepository.findByName(trans).orElseThrow(FatalException::new);

        char[] alphabet = {'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'y', 'z', '0', '1', '2', '3', '4', '5', '6', '7', '8', '9'};
        String id = NanoIdUtils.randomNanoId(random, alphabet, 8);

        User newUser = new User();
        newUser.setSocialCode("#" + id);
        newUser.setFirstname(registrationRequest.getFirstName());
        newUser.setLastname(registrationRequest.getLastName());
        newUser.setRegistrationDate(LocalDateTime.now());
        newUser.setPassword(passwordEncoder.encode(registrationRequest.getPassword()));
        newUser.setEmail(registrationRequest.getEmail());
        newUser.setPreferedLanguage(preferredLanguage);
        newUser.setGeneralConsent(registrationRequest.isGeneralConsent());
        newUser.setGdprConsent(registrationRequest.isGdprConsent());
        newUser.setCookiesConsent(registrationRequest.isCookiesConsent());
        newUser.setPreferedCurrency(preferredCurrency);
        newUser.setIpAddress(registrationRequest.getIpAddress());

        newUser.setDeviceToken(registrationRequest.getDeviceToken());
        newUser.setStatus(status);

        newUser = userRepository.save(newUser);

        createSubStatus(newUser, (registrationRequest.getSubStatus()));
        createUserDefaultAccounts(newUser, registrationRequest.getAccounts());
        createUserDefaultCategories(newUser);

        String token = jwtService.createToken(newUser.getEmail(), newUser.getUserID().toString());

        return new AuthenticationResponse(token);

    }


    private void createSubStatus(User user, List<Integer> subStatusIDs) {

        for (Integer id : subStatusIDs) {

            SubStatus subStatus = subStatusRepository.findById(id).orElseThrow(FatalException::new);
            SubStatusUsers subStatusUsers = new SubStatusUsers();
            subStatusUsers.setSubStatus(subStatus);
            subStatusUsers.setUser(user);
            subStatusUsersRepository.save(subStatusUsers);
            createUserDefaultCategoriesWithSubStatus(user, subStatus);
        }

    }


    /**
     * create default account for user
     *
     * @param user : the user to whom we want to add the accounts
     * @return AuthenticationResponse
     * @see AuthenticationResponse
     */
    private void createUserDefaultAccounts(User user, String[] accounts) {

        for (String s : accounts) {
            DefaultAccount defaultAccount = defaultAccountRepository.findByName(s);
            Account acc = new Account(defaultAccount.getName(), defaultAccount.getType(), user);
            accountRepository.save(acc);
        }

    }

    /**
     * create default categories for user
     *
     * @param user : the user to whom we want to add the default categories
     */
    public void createUserDefaultCategoriesWithSubStatus(User user, SubStatus subStatus) {

        if (user == null) throw new FatalException();

        List<CategorySubStatus> listCategoryStatus = categorySubStatusRepository.findTemplateCategoryBySubStatus(subStatus);

        Set<TemplateCategory> subCategories = new HashSet<>();

        for (CategorySubStatus catStatus : listCategoryStatus) {

            TemplateCategory tempCategory = catStatus.getTemplateCategory();

            if (tempCategory.getParentTemplateCategory() != null) {
                subCategories.add(tempCategory);
                continue;
            }

            Category newCategory = createOneCategory(tempCategory, user);
            newCategory.setParentCategory(null);
            categoryRepository.save(newCategory);

        }

        this.createSubCategories(subCategories, user);

    }

    /**
     * create default categories for user
     *
     * @param user : the user to whom we want to add the default categories
     */
    public void createUserDefaultCategories(User user) {

        if (user == null) throw new FatalException();

        List<CategoryStatus> listCategoryStatus = categoryStatusRepository.findTemplateCategoryByStatus(user.getStatus());

        Set<TemplateCategory> subCategories = new HashSet<>();

        for (CategoryStatus catStatus : listCategoryStatus) {

            TemplateCategory tempCategory = catStatus.getTemplateCategory();

            if (tempCategory.getParentTemplateCategory() != null) {
                subCategories.add(tempCategory);
                continue;
            }

            Category newCategory = createOneCategory(tempCategory, user);
            newCategory.setParentCategory(null);
            categoryRepository.save(newCategory);

        }

        this.createSubCategories(subCategories, user);

    }


    /**
     * create one default categories for user
     *
     * @param user         : the user to whom we want to add the default categories
     * @param tempCategory : category to be add
     * @return the create category
     * @see Category
     */
    private Category createOneCategory(TemplateCategory tempCategory, User user) {

        Category newCategory = new Category();

        newCategory.setBudget(tempCategory.getBudget());

        newCategory.setName(tempCategory.getName().getTranslation(user.getPreferedLanguage().getCountryCode()));
        newCategory.setIcon(tempCategory.getIcon());

        newCategory.setType(Category.TYPE.valueOf(tempCategory.getType().name()));
        newCategory.setOwner(user);

        return newCategory;
    }

    /**
     * create sub default categories for a category
     *
     * @param user          : the user to whom we want to add the default categories
     * @param subCategories : sub category to be add
     */
    private void createSubCategories(Set<TemplateCategory> subCategories, User user) {

        for (TemplateCategory tempCategory : subCategories) {
            Category newCategory = createOneCategory(tempCategory, user);
            Category category = categoryRepository.findByNameAndOwner(
                            tempCategory
                                    .getParentTemplateCategory()
                                    .getName()
                                    .getTranslation(user.getPreferedLanguage().getCountryCode()), user)
                    .orElse(null);
            newCategory.setParentCategory(category);
            categoryRepository.save(newCategory);
        }
    }

    /**
     * get current user
     *
     * @return the user
     * @see User
     */
    public User getCurrentUser() {
        org.springframework.security.core.userdetails.User currentUser = (org.springframework.security.core.userdetails.User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        if (currentUser == null)
            return null;
        return userRepository.findByEmail(currentUser.getUsername()).orElse(null);
    }

    /**
     * get user's infos
     *
     * @return userDto
     * @see UserDto
     */
    public UserDto getInfos(String languageCode) {
        User user = getCurrentUser();
        String status = user.getStatus().getName().getTranslation(languageCode);
        UserDto userDto = new UserDto(user, status);
        return userDto;
    }

    /**
     * Change user's pseudo
     *
     * @param pseudo : the new pseudo
     * @return userDto
     * @see UserDto
     */
    public UserDto changePseudo(String pseudo) {

        if (pseudo.equals("")) {
            throw new RegistrationException("empty pseudo!");
        }

        User user = getCurrentUser();

        if(pseudo.equals(user.getPseudo())){
            UserDto userDto = new UserDto();
            userDto.setPseudo(pseudo);
            return userDto;
        }


        User userExist = userRepository.findByPseudo(pseudo).orElse(null);
        if (userExist != null) {
            throw new RegistrationException("pseudo already exist!!");
        }

        user.setPseudo(pseudo);
        userRepository.save(user);

        UserDto userDto = new UserDto();
        userDto.setPseudo(pseudo);

        return userDto;

    }

    /**
     * get user's pseudo
     *
     * @return userDto
     * @see UserDto
     */
    public UserDto getPseudo() {
        User user = getCurrentUser();
        UserDto userDto = new UserDto();
        userDto.setPseudo(user.getPseudo());
        return userDto;
    }

    /**
     * get user's social code
     *
     * @return userDto
     * @see UserDto
     */
    public UserDto getSocialCode() {

        User user = getCurrentUser();
        UserDto userDto = new UserDto();
        String socialCode = user.getSocialCode().substring(1);

        userDto.setSocialCode(socialCode);
        userDto.setPseudo(user.getPseudo());

        return userDto;
    }

    @Transactional
    public void deleteAccount() {
        User user = getCurrentUser();
        //Deletion of his invoices
        invoiceRepository.deleteByOwner(user);
        //Deletion of his projects
        projectRepository.deleteByOwner(user);
        //Deletion of the records linking the user to a sub status
        subStatusUsersRepository.deleteByUser(user);
        //Deletion of friendships
        friendshipRepository.deleteByFriendshipPK_sender(user);
        friendshipRepository.deleteByFriendshipPK_receiver(user);
        //Deletion of all his records in the table linking categories and transactions
        transactionCategoryRepository.deleteByTransactionCategoryPK_TransactionID_Account_Owner(user);
        //Deletion of all his recurring transactions
        List<Transaction> transactions = (List<Transaction>) transactionRepository.findByAccount_owner(user);
        List<RecurringTransaction> recurringTransactions = transactions.stream()
                .filter(t -> t.getRecurringTransaction() != null)
                .map(Transaction::getRecurringTransaction)
                .distinct()
                .collect(Collectors.toList());
        recurringTransactionRepository.deleteAll(recurringTransactions);
        //Deletion of all his transactions
        transactionRepository.deleteAll(transactions);
        //Deletion of all his categories
        categoryRepository.deleteByOwner(user);
        //Deletion of all this accounts
        accountRepository.deleteByOwner(user);
        //Deletion of the user
        userRepository.delete(user);
    }

    /**
     * Change User's Password
     *
     * @param stringDto: the new password
     */
    public void changePassword(StringDto stringDto) {
        User user = getCurrentUser();
        user.setPassword(passwordEncoder.encode(stringDto.getContent()));
    }
}
