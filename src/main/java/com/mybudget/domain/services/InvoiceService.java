package com.mybudget.domain.services;

import com.mybudget.domain.dto.InvoiceDto;
import com.mybudget.domain.dto.response.ObjectResponse;
import com.mybudget.domain.exceptions.InvoiceException;
import com.mybudget.domain.models.Icon;
import com.mybudget.domain.models.Invoice;
import com.mybudget.domain.models.User;
import com.mybudget.domain.repository.IconRepository;
import com.mybudget.domain.repository.InvoiceRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * @author Eva Tumia
 */

@Service
@EnableScheduling
@Component
public class InvoiceService {

    @Autowired
    private InvoiceRepository invoiceRepository;
    @Autowired
    private AuthenticationService authenticationService;
    @Autowired
    private IconRepository iconRepository;
    @Autowired
    private FirebaseService firebaseService;
    @Autowired
    private TranslationService translationService;

    /**
     * Adds a new invoice to a user
     * If the icon chosen for the invoice does not already exist in DB, it's added too
     * @param invoiceDto: Invoice to add
     * @return A InvoiceDto object, the invoice newly added
     * @see InvoiceDto
     */
    public InvoiceDto addNewInvoice(InvoiceDto invoiceDto) {
        if (!iconRepository.existsByIconCode(invoiceDto.getIcon())) {
            Icon icon = new Icon(invoiceDto.getIcon(), Icon.TYPE.FONTAWESOME);
            iconRepository.save(icon);
        }
        Icon icon = iconRepository.findByIconCode(invoiceDto.getIcon());

        User currentUser = authenticationService.getCurrentUser();
        LocalDateTime startDate = LocalDate.parse(invoiceDto.getStartDate()).atStartOfDay();
        LocalDateTime endDate = LocalDate.parse(invoiceDto.getEndDate()).atStartOfDay();

        Invoice invoice = new Invoice(
                invoiceDto.getName(),
                startDate,
                endDate,
                invoiceDto.getAmount(),
                icon,
                currentUser
        );
        invoice = invoiceRepository.save(invoice);
        return serializeInvoice(invoice);
    }

    /**
     * Get all the invoices belonging to the current user
     * Each of the is being serialized into a InvoiceDto object
     * @return A list of InvoiceDto, the invoices belonging to the current user
     * @see InvoiceDto
     */
    public List<InvoiceDto> getAllByOwner() {
        User currentUser = authenticationService.getCurrentUser();
        List<Invoice> invoices = invoiceRepository.findByOwner(currentUser);
        List<InvoiceDto> invoiceDto = new ArrayList<>();
        for(Invoice i: invoices){
            invoiceDto.add(serializeInvoice(i));
        }
        return invoiceDto;
    }

    /**
     * Serialized a Invoice into a InvoiceDto object, getting primitive types out of objects
     * @param invoice: The invoice to serialize
     * @return An InvoiceDto object, containing the Invoice serialized
     * @see Invoice
     */
    private InvoiceDto serializeInvoice(Invoice invoice){
        return new InvoiceDto(
                invoice.getInvoiceID(),
                invoice.getName(),
                invoice.getAmount(),
                invoice.getStartDate().toString(),
                invoice.getEndDate().toString(),
                invoice.getIcon().getIconCode()
        );
    }

    /**
     * Scheduled method firing each day at noon
     * Verifies among all the invoices if there's some that will have to be paid on the next day or today
     * Users having invoices to be paid today or tomorrow will reveive a notification to remind them
     * See also {@link FirebaseService#sendNotifications(List, String, String)}
     */
    @Transactional
    @Scheduled(cron = "0 00 12 * * ?",zone = "Europe/Paris")
    public void sendPaymentReminderNotification() throws ClassNotFoundException {
        LocalDate todayLocale = LocalDate.now();
        int today = todayLocale.getDayOfMonth();
        Iterable<Invoice> invoices = invoiceRepository.findAll();

        for(Invoice i: invoices){
            if(i.getEndDate().isBefore(LocalDateTime.now()))
                continue;
            LocalDate invoiceStartDate = i.getStartDate().toLocalDate();
            int dayOfPayment = invoiceStartDate.getDayOfMonth();
            int tomorrow = todayLocale.plusDays(1).getDayOfMonth();

            boolean isPaiementDueToday = dayOfPayment == today
                    || (todayLocale.withDayOfMonth(todayLocale.lengthOfMonth()).equals(todayLocale)
                            && invoiceStartDate.withDayOfMonth(invoiceStartDate.lengthOfMonth()).equals(invoiceStartDate));

            boolean isPaiementDueTomorrow =  dayOfPayment == tomorrow
                    || (todayLocale.withDayOfMonth(todayLocale.lengthOfMonth()-1).equals(todayLocale)
                             && invoiceStartDate.withDayOfMonth(invoiceStartDate.lengthOfMonth()).equals(invoiceStartDate));

            if( isPaiementDueTomorrow || isPaiementDueToday) {
                String codePreferedLanguage = i.getOwner().getPreferedLanguage().getCountryCode();
                Map<String, String> translations = translationService.getRecurringPaiementsTranslation(codePreferedLanguage);
                String title="";
                if(isPaiementDueTomorrow){
                    title = translations.get("notifTomorrow");
                }else {
                    title = translations.get("notifToday");
                }
                String[] notifBody = translations.get("notifBody").split("\\.");
                List<String> token = new ArrayList<>();
                token.add(i.getOwner().getDeviceToken());
                firebaseService.sendNotifications(
                        token,
                        title,
                        notifBody[0]+" "+"\""+i.getName()+"\""+" "+notifBody[1]+" "+ i.getAmount()+"€"+notifBody[2]
                );
            }
        }
    }

    /**
     * Deletes the category having "id" as categoryID
     *
     * @param id: Id of the category to deleted
     * @return A ObjectResponse object
     * @see ObjectResponse
     */
    public ObjectResponse deleteById(int id) {
        invoiceRepository.deleteById(id);
        return new ObjectResponse(id);
    }

    /**
     * Updates the invoice name, endDate and amount
     *
     * @throws InvoiceException if invoiceID does not belong to any invoice existing in DB
     * @param invoiceDto: invoice to be updated
     * @return An InvoiceDto object, the invoice newly updated
     * @see InvoiceDto
     */
    public InvoiceDto updateInvoice(InvoiceDto invoiceDto) {
        Invoice invoice = invoiceRepository.findById(invoiceDto.getInvoiceID()).orElseThrow(()-> new InvoiceException());
        invoice.setName(invoiceDto.getName());
        invoice.setAmount(invoiceDto.getAmount());
        invoice.setEndDate(LocalDate.parse(invoiceDto.getEndDate()).atStartOfDay());
        invoice = invoiceRepository.save(invoice);
        return serializeInvoice(invoice);
    }
}
