package com.mybudget.domain.services;


import com.mybudget.domain.dto.response.LanguageResponse;
import com.mybudget.domain.exceptions.UserException;
import com.mybudget.domain.models.Language;
import com.mybudget.domain.models.User;
import com.mybudget.domain.repository.LanguageRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

/**
 * @author Soumaya Izmar
 */
@Service
public class LanguageService {

    @Autowired
    private LanguageRepository languageRepository;
    @Autowired
    private AuthenticationService authenticationService;

    @Transactional
    public List<LanguageResponse> fetchAllLanguages() {

        Iterable<Language> languageIterable = languageRepository.findAll();
        List<LanguageResponse> languages = new ArrayList<>();

        languageIterable.iterator().forEachRemaining(language -> {

            boolean isActive = language.getActive();
            if (isActive) {
                LanguageResponse languageResponse = new LanguageResponse();
                languageResponse.setLanguageCode(language.getCountryCode().toLowerCase(Locale.ROOT));
                languageResponse.setLanguageName(language.getName().name());
                languages.add(languageResponse);
            }
        });

        return languages;
    }


    @Transactional
    public String changeUserLanguage(String countryCode) {

        if (countryCode == null) {
            throw new UserException("Empty language");
        }

        User user = authenticationService.getCurrentUser();
        languageRepository.findByCountryCode(countryCode).ifPresent(user::setPreferedLanguage);

        return user.getPreferedLanguage().getCountryCode();
    }


}
