package com.mybudget.domain.services;

import com.mybudget.domain.dto.FriendShipDto;
import com.mybudget.domain.dto.response.FriendshipListResponse;
import com.mybudget.domain.exceptions.FatalException;
import com.mybudget.domain.exceptions.FriendshipException;
import com.mybudget.domain.models.Friendship;
import com.mybudget.domain.models.User;
import com.mybudget.domain.models.indentities.FriendshipPK;
import com.mybudget.domain.repository.FriendshipRepository;
import com.mybudget.domain.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Service
public class FriendshipService {

    @Autowired
    private AuthenticationService authenticationService;
    @Autowired
    private FriendshipRepository friendshipRepository;
    @Autowired
    private UserRepository userRepository;
    @Autowired
    private TranslationService translationService;
    @Autowired
    private FirebaseService firebaseService;

    /**
     * Get all friendShip of current user
     *
     * @return FriendshipListResponse
     * @see FriendshipListResponse
     */
    public FriendshipListResponse getFriendshipByOwner() {

        User user = authenticationService.getCurrentUser();
        List<FriendShipDto> friendshipsDto = new ArrayList<>();

        List<Friendship> listReceiver = friendshipRepository.findByFriendshipPK_receiver(user);

        for (Friendship f : listReceiver) {
            FriendShipDto friendShipDto = buildFriendshipDto(f, f.getFriendshipPK().getSender(), user);
            friendShipDto.setReceiverID(f.getFriendshipPK().getSender().getUserID());
            friendshipsDto.add(friendShipDto);
        }
        FriendshipListResponse friendshipListResponse = new FriendshipListResponse();
        friendshipListResponse.setDemandList(friendshipsDto);
        friendshipsDto = new ArrayList<>();

        List<Friendship> listSender = friendshipRepository.findByFriendshipPK_sender(user);
        for (Friendship f : listSender) {
            FriendShipDto friendShipDto = buildFriendshipDto(f, f.getFriendshipPK().getReceiver(), user);
            friendShipDto.setReceiverID(f.getFriendshipPK().getReceiver().getUserID());
            friendshipsDto.add(friendShipDto);
        }
        friendshipListResponse.setPendingList(friendshipsDto);
        return friendshipListResponse;
    }

    /**
     * build FriendshipDto
     *
     * @param f           :
     * @param friend      :
     * @param currentUser :
     * @return FriendShipDto
     * @see FriendShipDto
     */
    private FriendShipDto buildFriendshipDto(Friendship f, User friend, User currentUser) {

        FriendShipDto friendShipDto = new FriendShipDto();

        friendShipDto.setReceiverID(f.getFriendshipPK().getReceiver().getUserID());
        friendShipDto.setSenderID(f.getFriendshipPK().getSender().getUserID());
        String startDate = usDateToEuDate(f.getAcceptDate());
        friendShipDto.setAcceptDate(startDate);
        friendShipDto.setState(f.getState().name());
        String requestDate = usDateToEuDate(f.getRequestDate());
        friendShipDto.setRequestDate(requestDate);
        friendShipDto.setFriendPseudo(friend.getPseudo());
        friendShipDto.setUserPseudo(currentUser.getPseudo());

        return friendShipDto;
    }

    /**
     * Add a friend request
     * @param socialCode : the social code of the user of the friend request
     * @return FriendShipDto
     * @throws ClassNotFoundException
     * @see FriendShipDto
     */
    public FriendShipDto addFriendship(String socialCode) throws ClassNotFoundException {

        if (socialCode == null) throw new FatalException();

        socialCode = "#" + socialCode;
        User sender = authenticationService.getCurrentUser();
        Map<String, String> translations = translationService.fetchFriendshipTranslation(sender.getPreferedLanguage().getCountryCode());
        User receiver = userRepository.findBySocialCode(socialCode).orElseThrow(() -> new FriendshipException(translations.get("socialCodeNF")));

        if (receiver.equals(sender)) throw new FriendshipException(translations.get("error"));

        FriendshipPK friendshipPK = new FriendshipPK(sender, receiver);
        Friendship friendshipTest = friendshipRepository.findByFriendshipPK(friendshipPK).orElse(null);
        if (friendshipTest != null && friendshipTest.getState() != Friendship.State.REJECT) {
            throw new FriendshipException(translations.get("relationExist"));
        }



        FriendshipPK friendshipPK2 = new FriendshipPK(receiver, sender);
        Friendship friendshipTest2 = friendshipRepository.findByFriendshipPK(friendshipPK2).orElse(null);
        if (friendshipTest2 != null && friendshipTest2.getState() != Friendship.State.REJECT) {
            throw new FriendshipException(translations.get("relationExist"));
        }




        Friendship friendship = new Friendship();
        friendship.setFriendshipPK(friendshipPK);

        friendship.setRequestDate(LocalDate.now());
        friendship.setState(Friendship.State.PENDING);
        List<String> deviceTokens = new ArrayList<>();
        deviceTokens.add(receiver.getDeviceToken());
        firebaseService.sendNotifications(deviceTokens, "Nouvelle demande d'amis", sender.getPseudo() + ", veut etre votre ami(e)!!");

        return buildFriendshipDto(friendshipRepository.save(friendship), receiver, sender);
    }

    /**
     * Accept a friend request
     * @param id : receiver user id
     * @return
     * @throws ClassNotFoundException
     */
    public FriendShipDto acceptFriendship(int id) throws ClassNotFoundException {

        User receiver = authenticationService.getCurrentUser();
        Map<String, String> translations = translationService.fetchFriendshipTranslation(receiver.getPreferedLanguage().getCountryCode());
        User sender = userRepository.findById(id).orElseThrow(() -> new FriendshipException(translations.get("error")));

        FriendshipPK friendshipPK = new FriendshipPK(sender, receiver);
        Friendship friendship = friendshipRepository.findByFriendshipPK(friendshipPK).orElseThrow(() -> new FriendshipException(translations.get("error")));
        friendship.setState(Friendship.State.ACCEPT);
        friendship.setAcceptDate(LocalDate.now());
        friendshipRepository.save(friendship);

        return buildFriendshipDto(friendshipRepository.save(friendship), sender, receiver);
    }

    /**
     * refuse a friend request
     * @param id : receiver user id
     * @return
     * @throws ClassNotFoundException
     */
    public FriendShipDto refuseFriendship(int id) throws ClassNotFoundException {

        User receiver = authenticationService.getCurrentUser();
        Map<String, String> translations = translationService.fetchFriendshipTranslation(receiver.getPreferedLanguage().getCountryCode());
        User sender = userRepository.findById(id).orElseThrow(() -> new FriendshipException(translations.get("error")));
        FriendshipPK friendshipPK = new FriendshipPK(sender, receiver);

        Friendship friendship = friendshipRepository.findByFriendshipPK(friendshipPK).orElseThrow(() -> new FriendshipException(translations.get("error")));

        friendship.setState(Friendship.State.REJECT);
        friendship.setAcceptDate(LocalDate.now());

        return buildFriendshipDto(friendshipRepository.save(friendship), sender, receiver);
    }

    /**
     * Delete a friendship
     * @param receiverID
     * @param senderID
     * @return FriendShipDto
     * @throws ClassNotFoundException
     * @see FriendShipDto
     */
    public FriendShipDto deleteFriendship(int receiverID, int senderID) throws ClassNotFoundException {

        User user = authenticationService.getCurrentUser();
        Map<String, String> translations = translationService.fetchFriendshipTranslation(user.getPreferedLanguage().getCountryCode());

        User receiver = userRepository.findById(receiverID).orElseThrow(() -> new FriendshipException(translations.get("error")));
        User sender = userRepository.findById(senderID).orElseThrow(() -> new FriendshipException(translations.get("error")));

        FriendshipPK friendshipPK = new FriendshipPK(sender, receiver);
        Friendship friendship = friendshipRepository.findByFriendshipPK(friendshipPK).orElseThrow(() -> new FriendshipException(translations.get("error")));

        friendshipRepository.delete(friendship);

        return buildFriendshipDto(friendship, sender, receiver);

    }

    /**
     * Convert us date format to eu format
     * @param dateTime : date to change
     * @return the text of the date converted
     */
    private String usDateToEuDate(LocalDate dateTime) {
        if (dateTime == null) {
            return null;
        }
        return dateTime.format(DateTimeFormatter.ofPattern("dd-MM-yyyy"));
    }
}
