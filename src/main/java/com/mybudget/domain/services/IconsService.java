/**
 * @author Eva Tumia
 */

package com.mybudget.domain.services;

import com.mybudget.domain.models.Icon;
import com.mybudget.domain.repository.IconRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class IconsService {

    @Autowired
    private IconRepository iconRepository;

    /**
     * Gets all the icons in db
     * @return A list of Icons
     * @see Icon
     */
    public List<Icon> getAll() {
        return iconRepository.findAll();
    }
}
