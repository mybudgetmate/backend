package com.mybudget.domain.services;

import com.google.firebase.messaging.FirebaseMessaging;
import com.google.firebase.messaging.FirebaseMessagingException;
import com.google.firebase.messaging.MulticastMessage;
import com.google.firebase.messaging.Notification;
import com.mybudget.domain.exceptions.FatalException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author Soumaya Izmar
 */
@Service
public class FirebaseService {


    @Autowired
    private FirebaseMessaging firebaseMessaging;

    /**
     * Create a notification and send it to the targeted phone
     * @param tokens : device token
     * @param title : notification title
     * @param body notification body
     */
    public void sendNotifications(List<String> tokens, String title, String body) {

        MulticastMessage message = MulticastMessage.builder()
                .setNotification(Notification.builder()
                        .setTitle(title)
                        .setBody(body)
                        .build())
                .addAllTokens(tokens)
                .build();

        try {
            firebaseMessaging.sendMulticast(message).getResponses();
        } catch (FirebaseMessagingException e) {
            throw new FatalException();
        }

    }


}
