package com.mybudget.domain.services;

import com.mybudget.domain.dto.response.CurrencyResponse;
import com.mybudget.domain.models.Currency;
import com.mybudget.domain.models.User;
import com.mybudget.domain.repository.CurrencyRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Soumaya Izmar && Eva Tumia
 */
@Service
public class CurrencyService {

    @Autowired
    private CurrencyRepository currencyRepository;
    @Autowired
    private AuthenticationService authenticationService;

    /**
     * Gets all the currency
     * @return A list CurrencyResponse object containing the currencies
     * @see CurrencyResponse
     */
    @Transactional
    public List<CurrencyResponse> fetchAllCurrencies() {

        Iterable<Currency> currencyIterable = currencyRepository.findAll();
        List<CurrencyResponse> currencies = new ArrayList<>();
        currencyIterable.iterator().forEachRemaining(currency -> currencies.add(new CurrencyResponse(currency.getType(), currency.getIcon().getIconCode())));

        return currencies;
    }

    /**
     * Gets the current user's prefered currency
     * @return A CurrencyResponse object containing the prefered currency
     * @see CurrencyResponse
     */
    public CurrencyResponse fetchPreferedCurrencyUser() {
        User currentUser = authenticationService.getCurrentUser();
        Currency c = currentUser.getPreferedCurrency();
        return new CurrencyResponse(c.getType(), c.getIcon().getIconCode());
    }
}
