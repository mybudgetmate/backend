package com.mybudget.domain.services;

import com.mybudget.domain.dto.SubStatusDto;
import com.mybudget.domain.exceptions.FatalException;
import com.mybudget.domain.models.Status;
import com.mybudget.domain.models.SubStatus;
import com.mybudget.domain.repository.StatusRepository;
import com.mybudget.domain.repository.SubStatusRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;

/**
 * @author Soumaya Izmar
 */
@Service
public class StatusService {

    @Autowired
    private TranslationService translationService;
    @Autowired
    private StatusRepository statusRepository;
    @Autowired
    private SubStatusRepository subStatusRepository;


    @Transactional
    public Map<String, String> fetchAllStatus(String language) {
        Map<String, String> status = new TreeMap();
        try {
            status = translationService.fetchStatusTranslations(language);
        } catch (ClassNotFoundException e) {
            throw new FatalException();
        }

        return status;
    }

    @Transactional
    public List<SubStatusDto> fetchAllSubStatus(String languageCode) {

        Iterable<SubStatus> statusIterable = subStatusRepository.findAll();
        List<SubStatusDto> subStatus = new ArrayList<>();
        for (SubStatus s : statusIterable) {
            SubStatusDto subStatusDto = new SubStatusDto();
            subStatusDto.setName(s.getTranslation(languageCode));
            subStatusDto.setId(s.getSub_statusID());
            subStatus.add(subStatusDto);
        }
        return subStatus;
    }

}
