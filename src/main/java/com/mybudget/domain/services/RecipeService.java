package com.mybudget.domain.services;

import com.mybudget.domain.dto.IngredientDto;
import com.mybudget.domain.dto.RecipeDto;
import com.mybudget.domain.exceptions.FatalException;
import com.mybudget.domain.models.*;
import com.mybudget.domain.repository.IngredientRecipeRepository;
import com.mybudget.domain.repository.RecipeMonthRepository;
import com.mybudget.domain.repository.RecipeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.*;

@Service
public class RecipeService {

    @Autowired
    private TranslationService translationService;

    @Autowired
    private RecipeMonthRepository recipeMonthRepository;

    @Autowired
    private RecipeRepository recipeRepository;

    @Autowired
    private IngredientRecipeRepository ingredientRecipeRepository;

    @Autowired
    private IngredientService ingredientService;

    @Transactional
    public Map<Integer, Map<Long, RecipeDto>> getAll(String language) {
        SortedMap<Integer, Map<Long, RecipeDto>> recipeList = new TreeMap<>();

        for (int i = 1; i <= 12; i++) {
            List<RecipeMonth> recipesOfMonth = (List<RecipeMonth>) recipeMonthRepository.findAllByMonth(i);

            SortedMap<Long, RecipeDto> recipes = new TreeMap<>();
            for (RecipeMonth recipeOfMonth : recipesOfMonth) {
                Recipe recipe = recipeRepository
                        .findByRecipeID(recipeOfMonth.getRecipe().getRecipeID());

                RecipeDto recipeDto = getRecipeDtoFromRecipe(recipe, i, language);

                recipes.put(recipeDto.getId(), recipeDto);
            }

            recipeList.put(i, recipes);
        }
        return recipeList;
    }

    public RecipeDto getRecipeDtoFromRecipe(Recipe recipe, Integer month, String language) {
        return getRecipeDtoFromRecipe(recipe, month, language, null);
    }

    public RecipeDto getRecipeDtoFromRecipe(Recipe recipe, Integer month, String language, Integer servings) {
        Double recipePrice = .0;
        Float recipeCalorificValue = 0f;
        Float recipeProteicValue = 0f;
        String name = null;
        Float servingCoef = 1f;

        if (servings != null && !recipe.getServings().equals(servings)) {
            servingCoef = new BigDecimal(servings / recipe.getServings()).floatValue();
        }

        try {
            name = translationService.fetchRecipesTranslationsById(language, recipe.getName().getId());
        } catch (ClassNotFoundException e) {
            throw new FatalException();
        }

        List<IngredientRecipe> ingredientRecipes = (List<IngredientRecipe>) ingredientRecipeRepository.findAllByRecipe(recipe);
        List<IngredientDto> ingredients = new ArrayList<>();
        for (int idx = 0; idx < ingredientRecipes.size(); idx++) {
            Ingredient ingredient = ingredientRecipes.get(idx).getIngredient();

            IngredientDto ingredientDto = ingredientService.serializeIngredientDto(ingredient, month, language);
            Float quantity = ingredientRecipes.get(idx).getQuantity() * servingCoef;
            ingredientDto.setQuantity(quantity);

            if (ingredientDto.getCalorificValue() > 0) {
                ingredientDto.setCalorificValue(
                        (new BigDecimal((ingredientDto.getCalorificValue() / ingredient.getQuantity()) * quantity)
                                .setScale(2, RoundingMode.HALF_UP)).floatValue()
                );
            } else {
                ingredientDto.setCalorificValue(0f);
            }

            if (ingredientDto.getProteicValue() > 0) {
                ingredientDto.setProteicValue(
                        (new BigDecimal((ingredientDto.getProteicValue() / ingredient.getQuantity()) * quantity)
                                .setScale(2, RoundingMode.HALF_UP)).floatValue()
                );
            } else {
                ingredientDto.setProteicValue(0f);
            }

            if (ingredientDto.getPrice() > 0) {
                ingredientDto.setPrice(
                        (new BigDecimal((ingredientDto.getPrice() / ingredient.getQuantity()) * quantity)
                                .setScale(2, RoundingMode.HALF_UP)).doubleValue()
                );
            } else {
                ingredientDto.setPrice(0.);
            }

            recipePrice += ingredientDto.getPrice();
            recipeCalorificValue += ingredientDto.getCalorificValue();
            recipeProteicValue += ingredientDto.getProteicValue();

            ingredients.add(ingredientDto);
        }

        recipePrice = new BigDecimal(recipePrice).setScale(2, RoundingMode.HALF_UP).doubleValue();
        recipeCalorificValue = new BigDecimal(recipeCalorificValue).setScale(2, RoundingMode.HALF_UP).floatValue();
        recipeProteicValue = new BigDecimal(recipeProteicValue).setScale(2, RoundingMode.HALF_UP).floatValue();

        return new RecipeDto(
                recipe.getRecipeID(),
                name,
                recipe.getServings(),
                recipeCalorificValue,
                recipeProteicValue,
                recipePrice,
                recipe.getIcon().getIconCode(),
                recipe.getRecipeUrl(),
                ingredients,
                recipe.getPreparation()

        );
    }

}
