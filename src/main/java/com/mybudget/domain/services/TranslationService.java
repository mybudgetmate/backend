package com.mybudget.domain.services;

import com.mybudget.domain.exceptions.FatalException;
import com.mybudget.domain.models.User;
import com.mybudget.domain.models.translation.*;
import com.mybudget.domain.repository.*;
import com.mybudget.domain.utils.IntrospectionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

/**
 * @author Soumaya Izmar & Eva Tumia
 */
@Service
public class TranslationService {

    @Autowired
    private LoginPageRepository loginPageRepository;
    @Autowired
    private SignUpPageRepository signUpPageRepository;
    @Autowired
    private CategoriesManagementPageRepository categoriesManagementPageRepository;
    @Autowired
    private ProjectPageTranslationsRepository projectPageTranslationsRepository;
    @Autowired
    private AddTransactionPageRepository addTransactionPageRepository;
    @Autowired
    private MonthsTranslationsRepository monthsTranslationsRepository;
    @Autowired
    private BudgetTranslationsRepository budgetTranslationsRepository;
    @Autowired
    private HomePageTranslationsRepository homePageTranslationsRepository;
    @Autowired
    private AllTransactionsTranslationRepository allTransactionsTranslationRepository;
    @Autowired
    private TemplateCategoryNameTranslationRepository templateCategoryNameTranslationRepository;
    @Autowired
    private FriendshipTranslationRepository friendshipTranslationRepository;
    @Autowired
    private IntrospectionService introspectionService;
    @Autowired
    private AuthenticationService authenticationService;
    @Autowired
    private RecurringPaiementsTranslationRepository recurringPaiementsTranslationRepository;
    @Autowired
    private GlobalTranslationRepository globalTranslationRepository;
    @Autowired
    private IngredientsTranslationsRepository ingredientsTranslationsRepository;
    @Autowired
    private RecipesTranslationsRepository recipesTranslationsRepository;
    @Autowired
    private StatusTranslationRepository statusTranslationRepository;
    @Autowired
    private SettingsPageTranslationRepository settingsPageTranslationRepository;
    @Autowired
    private ShoppingListsModuleTranslationRepository shoppingListsModuleTranslationRepository;


    @Transactional
    public Map<String, String> fetchLoginTranslation(String language) throws ClassNotFoundException, FatalException {
        Iterable<LoginPageTranslation> translationIterable = loginPageRepository.findAll();
        Map<String, String> translations = new HashMap<>();
        Map<String, Method> languagesMethods = introspectionService.createMethodMap("LoginPageTranslation");
        Method m = languagesMethods.get(language);
        if (m == null) {
            throw new FatalException();
        }
        translationIterable.iterator().forEachRemaining(translation -> {
            try {
                translations.put(translation.getId(), m.invoke(translation).toString());
            } catch (IllegalAccessException | InvocationTargetException e) {
                throw new FatalException();
            }
        });

        return translations;
    }

    @Transactional
    public Map<String, String> fetchSignUpTranslation(String language) throws ClassNotFoundException, FatalException {
        Iterable<SignUpPageTranslation> translationIterable = signUpPageRepository.findAll();
        Map<String, String> translations = new HashMap<>();
        Map<String, Method> languagesMethods = introspectionService.createMethodMap("SignUpPageTranslation");
        Method m = languagesMethods.get(language);
        if (m == null) {
            throw new FatalException();
        }
        translationIterable.iterator().forEachRemaining(translation -> {
            try {
                translations.put(translation.getId(), m.invoke(translation).toString());
            } catch (IllegalAccessException | InvocationTargetException e) {
                throw new FatalException();
            }
        });

        return translations;
    }

    public Map<String, String> fetchCategoriesManagementTranslation(String language) throws ClassNotFoundException {
        Iterable<CategoriesManagementPageTranslation> translationIterable = categoriesManagementPageRepository.findAll();
        Map<String, String> translations = new HashMap<>();
        Map<String, Method> languagesMethods = introspectionService.createMethodMap("CategoriesManagementPageTranslation");
        Method m = languagesMethods.get(language);
        if (m == null) {
            throw new FatalException();
        }
        translationIterable.iterator().forEachRemaining(translation -> {
            try {
                translations.put(translation.getId(), m.invoke(translation).toString());
            } catch (IllegalAccessException | InvocationTargetException e) {
                throw new FatalException();
            }
        });
        return translations;
    }


    @Transactional
    public Map<String, String> fetchProjectTranslation(String language) throws ClassNotFoundException, FatalException {

        Iterable<ProjectPageTranslation> translationIterable = projectPageTranslationsRepository.findAll();
        Map<String, String> translations = new HashMap<>();

        Map<String, Method> languagesMethods = introspectionService.createMethodMap("ProjectPageTranslation");
        Method m = languagesMethods.get(language.toLowerCase(Locale.ROOT));

        if (m == null) {
            throw new FatalException();
        }

        translationIterable.iterator().forEachRemaining(translation -> {
            try {
                translations.put(translation.getId(), m.invoke(translation).toString());
            } catch (IllegalAccessException | InvocationTargetException e) {
                throw new FatalException();
            }
        });

        return translations;
    }


    @Transactional
    public Map<String, String> fetchBudgetTranslation(String language) throws ClassNotFoundException, FatalException {
        Iterable<BudgetTranslation> translationIterable = budgetTranslationsRepository.findAll();
        Map<String, String> translations = new HashMap<>();

        Map<String, Method> languagesMethods = introspectionService.createMethodMap("BudgetTranslation");
        Method m = languagesMethods.get(language.toLowerCase(Locale.ROOT));

        if (m == null) {
            throw new FatalException();
        }

        translationIterable.iterator().forEachRemaining(translation -> {
            try {
                translations.put(translation.getId(), m.invoke(translation).toString());
            } catch (IllegalAccessException | InvocationTargetException e) {
                throw new FatalException();
            }
        });

        return translations;
    }

    public Map<String, String> fetchAddTransactionTranslation(String language) throws ClassNotFoundException {
        Iterable<AddTransactionPageTranslation> translationIterable = addTransactionPageRepository.findAll();
        Map<String, String> translations = new HashMap<>();
        Map<String, Method> languagesMethods = introspectionService.createMethodMap("AddTransactionPageTranslation");
        Method m = languagesMethods.get(language);
        if (m == null) {
            throw new FatalException();
        }

        translationIterable.iterator().forEachRemaining(translation -> {
            try {
                translations.put(translation.getId(), m.invoke(translation).toString());
            } catch (IllegalAccessException | InvocationTargetException e) {
                throw new FatalException();
            }
        });
        return translations;
    }


    public Map<String, String> fetchMonthsTranslation(String language) throws ClassNotFoundException {
        Iterable<Months> translationIterable = monthsTranslationsRepository.findAll();
        Map<String, String> translations = new HashMap<>();
        Map<String, Method> languagesMethods = introspectionService.createMethodMap("Months");
        Method m = languagesMethods.get(language);
        if (m == null) {
            throw new FatalException();
        }

        translationIterable.iterator().forEachRemaining(translation -> {
            try {
                translations.put(translation.getId().toString(), m.invoke(translation).toString());
            } catch (IllegalAccessException | InvocationTargetException e) {
                throw new FatalException();
            }
        });
        return translations;
    }

    public Map<String, String> fetchHomepageTranslation(String language) throws ClassNotFoundException {
        Iterable<HomePageTranslation> translationIterable = homePageTranslationsRepository.findAll();
        Map<String, String> translations = new HashMap<>();
        Map<String, Method> languagesMethods = introspectionService.createMethodMap("HomePageTranslation");
        Method m = languagesMethods.get(language);
        if (m == null) {
            throw new FatalException();
        }

        translationIterable.iterator().forEachRemaining(translation -> {
            try {
                translations.put(translation.getId().toString(), m.invoke(translation).toString());
            } catch (IllegalAccessException | InvocationTargetException e) {
                throw new FatalException();
            }
        });
        return translations;
    }

    public Map<String, String> fetchGlobalTranslation(String language) throws ClassNotFoundException {
        Iterable<GlobalTranslation> translationIterable = globalTranslationRepository.findAll();
        Map<String, String> translations = new HashMap<>();
        Map<String, Method> languagesMethods = introspectionService.createMethodMap("GlobalTranslation");
        Method m = languagesMethods.get(language);
        if (m == null) {
            throw new FatalException();
        }

        translationIterable.iterator().forEachRemaining(translation -> {
            try {
                translations.put(translation.getId().toString(), m.invoke(translation).toString());
            } catch (IllegalAccessException | InvocationTargetException e) {
                throw new FatalException();
            }
        });
        return translations;
    }


    public Map<String, String> fetchAllTransactionsTranslation(String language) throws ClassNotFoundException {
        Iterable<AllTransactionsTranslation> translationIterable = allTransactionsTranslationRepository.findAll();
        Map<String, String> translations = new HashMap<>();
        Map<String, Method> languagesMethods = introspectionService.createMethodMap("AllTransactionsTranslation");
        Method m = languagesMethods.get(language);
        if (m == null) {
            throw new FatalException();
        }
        translationIterable.iterator().forEachRemaining(translation -> {
            try {
                translations.put(translation.getId(), m.invoke(translation).toString());
            } catch (IllegalAccessException | InvocationTargetException e) {
                throw new FatalException();
            }
        });
        return translations;
    }


    @Transactional
    public Map<String, String> fetchCategoryTemplateTranslation(String language) throws ClassNotFoundException, FatalException {
        Iterable<TemplateCategoryNameTranslation> translationIterable = templateCategoryNameTranslationRepository.findAll();
        Map<String, String> translations = new HashMap<>();

        Map<String, Method> languagesMethods = introspectionService.createMethodMap("TemplateCategoryNameTranslation");
        Method m = languagesMethods.get(language.toLowerCase(Locale.ROOT));

        if (m == null) {
            throw new FatalException();
        }

        translationIterable.iterator().forEachRemaining(translation -> {
            try {
                translations.put(translation.getId(), m.invoke(translation).toString());
            } catch (IllegalAccessException | InvocationTargetException e) {
                throw new FatalException();
            }
        });

        return translations;
    }


    @Transactional
    public Map<String, String> fetchFriendshipTranslation(String language) throws ClassNotFoundException, FatalException {
        Iterable<FriendshipTranslation> translationIterable = friendshipTranslationRepository.findAll();
        Map<String, String> translations = new HashMap<>();

        Map<String, Method> languagesMethods = introspectionService.createMethodMap("FriendshipTranslation");
        Method m = languagesMethods.get(language.toLowerCase(Locale.ROOT));

        if (m == null) {
            throw new FatalException();
        }

        translationIterable.iterator().forEachRemaining(translation -> {
            try {
                translations.put(translation.getId(), m.invoke(translation).toString());
            } catch (IllegalAccessException | InvocationTargetException e) {
                throw new FatalException();
            }
        });

        return translations;
    }
    public String getShoppingTranslation() {
        User user = authenticationService.getCurrentUser();
        TemplateCategoryNameTranslation templateCategoryNameTranslation = templateCategoryNameTranslationRepository.findById("groceries").orElseThrow(FatalException::new);
        return templateCategoryNameTranslation.getTranslation(user.getPreferedLanguage().getCountryCode());

    }


    @Transactional
    public Map<String, String> getRecurringPaiementsTranslation(String language) throws ClassNotFoundException {
        Iterable<RecurringPaiementsPageTranslation> translationIterable = recurringPaiementsTranslationRepository.findAll();
        Map<String, String> translations = new HashMap<>();

        Map<String, Method> languagesMethods = introspectionService.createMethodMap("RecurringPaiementsPageTranslation");
        Method m = languagesMethods.get(language.toLowerCase(Locale.ROOT));

        if (m == null) {
            throw new FatalException();
        }

        translationIterable.iterator().forEachRemaining(translation -> {
            try {
                translations.put(translation.getId(), m.invoke(translation).toString());
            } catch (IllegalAccessException | InvocationTargetException e) {
                throw new FatalException();
            }
        });

        return translations;
    }

    @Transactional
    public Map<String, String> fetchIngredientsTranslations(String language) throws ClassNotFoundException {
        Iterable<IngredientsTranslations> translationIterable = ingredientsTranslationsRepository.findAll();
        Map<String, String> translations = new HashMap<>();
        Map<String, Method> languagesMethods = introspectionService.createMethodMap("IngredientsTranslations");
        Method m = languagesMethods.get(language);
        if (m == null) {
            throw new FatalException();
        }
        translationIterable.iterator().forEachRemaining(translation -> {
            try {
                translations.put(translation.getId(), m.invoke(translation).toString());
            } catch (IllegalAccessException | InvocationTargetException e) {
                throw new FatalException();
            }
        });
        return translations;
    }

    @Transactional
    public String fetchIngredientsTranslationsById(String language, String id) throws ClassNotFoundException {
        IngredientsTranslations translationQuery = ingredientsTranslationsRepository.findById(id).get();
        String translation = null;
        Map<String, Method> languagesMethods = introspectionService.createMethodMap("IngredientsTranslations");
        Method m = languagesMethods.get(language);
        if (m == null) {
            throw new FatalException();
        }
        try {
            translation = m.invoke(translationQuery).toString();
        } catch (IllegalAccessException | InvocationTargetException e) {
            throw new FatalException();
        }
        return translation;
    }

    @Transactional
    public Map<String, String> fetchRecipesTranslations(String language) throws ClassNotFoundException {
        Iterable<RecipesTranslations> translationIterable = recipesTranslationsRepository.findAll();
        Map<String, String> translations = new HashMap<>();
        Map<String, Method> languagesMethods = introspectionService.createMethodMap("RecipesTranslations");
        Method m = languagesMethods.get(language);
        if (m == null) {
            throw new FatalException();
        }
        translationIterable.iterator().forEachRemaining(translation -> {
            try {
                translations.put(translation.getId(), m.invoke(translation).toString());
            } catch (IllegalAccessException | InvocationTargetException e) {
                throw new FatalException();
            }
        });
        return translations;
    }

    @Transactional
    public String fetchRecipesTranslationsById(String language, String id) throws ClassNotFoundException {
        RecipesTranslations translationQuery = recipesTranslationsRepository.findById(id).get();
        String translation = null;
        Map<String, Method> languagesMethods = introspectionService.createMethodMap("RecipesTranslations");
        Method m = languagesMethods.get(language);
        if (m == null) {
            throw new FatalException();
        }
        try {
            translation = m.invoke(translationQuery).toString();
        } catch (IllegalAccessException | InvocationTargetException e) {
            throw new FatalException();
        }
        return translation;
    }

    public Map<String, String> fetchStatusTranslations(String language) throws ClassNotFoundException {
        Iterable<StatusTranslation> translationIterable = statusTranslationRepository.findAll();
        Map<String, String> translations = new HashMap<>();
        Map<String, Method> languagesMethods = introspectionService.createMethodMap("StatusTranslation");
        Method m = languagesMethods.get(language);
        if (m == null) {
            throw new FatalException();
        }

        translationIterable.iterator().forEachRemaining(translation -> {
            try {
                translations.put(translation.getId().toString(), m.invoke(translation).toString());
            } catch (IllegalAccessException | InvocationTargetException e) {
                throw new FatalException();
            }
        });
        return translations;
    }

    public Map<String, String> fetchSettingsPageTranslations(String language) throws ClassNotFoundException {
        Iterable<SettingsPageTranslation> translationIterable = settingsPageTranslationRepository.findAll();
        Map<String, String> translations = new HashMap<>();
        Map<String, Method> languagesMethods = introspectionService.createMethodMap("SettingsPageTranslation");
        Method m = languagesMethods.get(language);
        if (m == null) {
            throw new FatalException();
        }

        translationIterable.iterator().forEachRemaining(translation -> {
            try {
                translations.put(translation.getId().toString(), m.invoke(translation).toString());
            } catch (IllegalAccessException | InvocationTargetException e) {
                throw new FatalException();
            }
        });
        return translations;
    }

    public Map<String, String> fetchShoppingListsModuleTranslations(String language) throws ClassNotFoundException {
        Iterable<ShoppingListsModuleTranslation> translationIterable = shoppingListsModuleTranslationRepository.findAll();
        Map<String, String> translations = new HashMap<>();
        Map<String, Method> languagesMethods = introspectionService.createMethodMap("ShoppingListsModuleTranslation");
        Method m = languagesMethods.get(language);
        if (m == null) {
            throw new FatalException();
        }

        translationIterable.iterator().forEachRemaining(translation -> {
            try {
                translations.put(translation.getId().toString(), m.invoke(translation).toString());
            } catch (IllegalAccessException | InvocationTargetException e) {
                throw new FatalException();
            }
        });
        return translations;
    }

    @Transactional
    public String fetchShoppingListsModuleTranslationsById(String language, String id) throws ClassNotFoundException {
        ShoppingListsModuleTranslation translationQuery = shoppingListsModuleTranslationRepository.findById(id).get();
        String translation = null;
        Map<String, Method> languagesMethods = introspectionService.createMethodMap("ShoppingListsModuleTranslation");
        Method m = languagesMethods.get(language);
        if (m == null) {
            throw new FatalException();
        }
        try {
            translation = m.invoke(translationQuery).toString();
        } catch (IllegalAccessException | InvocationTargetException e) {
            throw new FatalException();
        }
        return translation;
    }
}
