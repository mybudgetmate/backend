package com.mybudget.domain.services;

import com.mybudget.domain.models.Image;
import com.mybudget.domain.repository.ImageRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

@Service
public class ImageService {

    @Autowired
    private ImageRepository imageRepository;

    /**
     * get all images
     * @return a list of string
     */
    @Transactional
    public List<String> fetchAllImages() {
        Iterable<Image> imageIterable = imageRepository.findAll();
        List<String> images = new ArrayList<>();

        imageIterable.iterator().forEachRemaining(img ->
                images.add(img.getName())
        );

        return images;

    }


}
