package com.mybudget.domain.services;

import com.mybudget.domain.dto.AccountTypesSerializedDto;
import com.mybudget.domain.models.AccountType;
import com.mybudget.domain.repository.AccountTypesRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class AccountTypesService {

    @Autowired
    AccountTypesRepository accountTypesRepository;

    /**
     * Gets all the account types
     * @return A list of AccountTypesSerializedDto
     * @see AccountTypesSerializedDto
     */
    public List<AccountTypesSerializedDto> getAll() {
        Iterable<AccountType> listToSerialize = accountTypesRepository.findAll();
        List<AccountTypesSerializedDto> listToReturn = new ArrayList<>();
        for(AccountType a: listToSerialize){
            listToReturn.add(serializeAccountType(a));
        }
        return listToReturn;
    }

    /**
     * Serializes an AccountType object into an AccountTypesSerializedDto to make all the fields usable in the frontend
     * @param c: AccountType to serialize
     * @return An AccountTypesSerializedDto
     */
    private AccountTypesSerializedDto serializeAccountType(AccountType c){
        AccountTypesSerializedDto cat = new AccountTypesSerializedDto(
                c.getAccountTypeID(),
                c.getName(),
                c.getIcon().getIconCode()
        );
        return cat;
    }
}
