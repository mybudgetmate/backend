package com.mybudget.domain.services;

import com.fasterxml.jackson.databind.ser.std.StdKeySerializers;
import com.mybudget.domain.dto.ProjectDto;
import com.mybudget.domain.exceptions.ProjectException;
import com.mybudget.domain.models.Image;
import com.mybudget.domain.models.Project;
import com.mybudget.domain.models.User;
import com.mybudget.domain.repository.ImageRepository;
import com.mybudget.domain.repository.ProjectRepository;
import com.mybudget.domain.repository.UserRepository;
import com.mybudget.domain.utils.JwtService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.CascadeType;
import javax.persistence.OneToOne;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;

/**
 * @author Soumaya Izmar
 */
@Service
public class ProjectService {


    @Autowired
    private ProjectRepository projectRepository;
    @Autowired
    private UserRepository userRepository;
    @Autowired
    private ImageRepository imageRepository;

    @Autowired
    private AuthenticationService authenticationService;
    @Autowired
    private JwtService jwtService;
    @Autowired
    private TranslationService translationService;


    public List<ProjectDto> fetchAllProject() {

        User user = authenticationService.getCurrentUser();
        Iterable<Project> projectsIterable = projectRepository.findByParentProjectAndOwner(null, user);

        List<ProjectDto> projects = new ArrayList<>();
        projectsIterable.forEach(p -> {
            ProjectDto projectDto = buildProjectDto(p);
            projects.add(projectDto);
        });
        return projects;
    }

    public List<ProjectDto> fetchSubProject(Integer id) {

        Project parentProject = projectRepository.findById(id).orElse(null);
        Iterable<Project> projectsIterable = projectRepository.findByParentProject(parentProject);
        List<ProjectDto> projects = new ArrayList<>();

        projectsIterable.forEach(p -> {
            ProjectDto projectDto = buildProjectDto(p);
            projects.add(projectDto);

        });

        return projects;
    }

    @Transactional
    public ProjectDto fetchOneProject(Integer id) throws ProjectException, ClassNotFoundException {
        User user = authenticationService.getCurrentUser();

        Map<String, String> translations = translationService.fetchProjectTranslation(user.getPreferedLanguage().getCountryCode());

        Project project = projectRepository.findById(id).orElseThrow(() -> new ProjectException(translations.get("unexpectedError")));

        return buildProjectDto(project);
    }

    /**
     * brings the budgets up to date
     *
     * @param project   : the project to update
     * @param isAUpdate
     * @param add     :
     * @return
     */
    private Project doRecursiveChanges(Project project, boolean isAUpdate, double add) {

        if (isAUpdate) {
            project.setObtainedBudget(project.getObtainedBudget() + add);
            projectRepository.save(project);
        }
        if (project.getParentProject() == null) {
            projectRepository.save(project);
            return project;
        }
        return doRecursiveChanges(project.getParentProject(), isAUpdate, add);
    }

    /**
     *
     * Delete the project and his children
     *
     * @param id
     */
    @Transactional
    public void deleteProject(Integer id) throws ProjectException, ClassNotFoundException {
        User user = authenticationService.getCurrentUser();
        Map<String, String> translations = translationService.fetchProjectTranslation(user.getPreferedLanguage().getCountryCode());
        Project project = projectRepository.findById(id).orElseThrow(() -> new ProjectException(translations.get("unexpectedError")));
        recalculateObtainedBudget(project);
        delete(project);

    }


    private void delete(Project project) {
        Iterable<Project> projectsIterable = projectRepository.findByParentProject(project);
        for (Project p : projectsIterable) {
            delete(p);
        }
        projectRepository.delete(project);
    }

    private Project recalculateObtainedBudget(Project project) {
        if (project.getParentProject() == null) {
            return project;
        }
        project.getParentProject().setObtainedBudget(project.getParentProject().getObtainedBudget() - project.getObtainedBudget());
        projectRepository.save(project);

        return recalculateObtainedBudget(project.getParentProject());
    }

    /**
     *
     * Update the project and his parents
     *
     * @param projectDto
     */
    @Transactional
    public ProjectDto updateProject(ProjectDto projectDto) throws ProjectException, ClassNotFoundException {
        User user = authenticationService.getCurrentUser();
        Map<String, String> translations = translationService.fetchProjectTranslation(user.getPreferedLanguage().getCountryCode());
        Project project = projectRepository.findById(projectDto.getProjectID()).orElseThrow(() -> new ProjectException(translations.get("unexpectedError")));

        buildGenericProject(projectDto, project);

        double difference = projectDto.getObtainedBudget() - project.getObtainedBudget();

        doRecursiveChanges(project, true, difference);

        return buildProjectDto(projectRepository.save(project));
    }

    /**
     *
     * add a project
     *
     * @param projectDto
     */
    @Transactional
    public ProjectDto addNewProject(ProjectDto projectDto) throws ClassNotFoundException {

        Project project = new Project();

        project.setStartDate(LocalDate.now());
        Project parentProject = projectRepository.findById(projectDto.getParentProject()).orElse(null);
        project.setParentProject(parentProject);

        buildGenericProject(projectDto, project);

        Project p = projectRepository.save(project);

        doRecursiveChanges(p, false, 0);

        ProjectDto tempProjectDto = buildProjectDto(project);

        tempProjectDto.setProjectID(p.getProjectID());

        return tempProjectDto;

    }

    private Project buildGenericProject(ProjectDto projectDto, Project project) throws ClassNotFoundException {

        project.setRequiredBudget(projectDto.getRequiredBudget());
        project.setName(projectDto.getName());

        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
        LocalDate endDate = LocalDate.parse(projectDto.getEndDate(), formatter);

        User user = authenticationService.getCurrentUser();

        Map<String, String> translations = translationService.fetchProjectTranslation(user.getPreferedLanguage().getCountryCode());

        if (endDate.isBefore(project.getStartDate())) {
            throw new ProjectException(translations.get("deadlineError"));
        }
        if (project.getParentProject() == null || project.getParentProject().getEndDate().isAfter(endDate)
                || project.getParentProject().getEndDate().isEqual(endDate)) {
            project.setEndDate(endDate);
        } else {
            throw new ProjectException(translations.get("deadlineErrorParent"));
        }
        if (project.getParentProject() != null) {
            if (project.getRequiredBudget() >= project.getParentProject().getRequiredBudget()) {
                throw new ProjectException(translations.get("BudgetRequired"));
            }
            if (project.getObtainedBudget() > project.getRequiredBudget()) {
                throw new ProjectException(translations.get("BudgetObtained"));
            }
            if (projectDto.getObtainedBudget() > project.getRequiredBudget()) {
                throw new ProjectException(translations.get("BudgetObtained"));
            }

            if (projectDto.getObtainedBudget() > project.getObtainedBudget()) {
                double total = projectDto.getObtainedBudget() - project.getObtainedBudget();
                double p2 = parents(project, project.getParentProject(), total, 'a');
                if (p2 > project.getParentProject().getObtainedBudget() + total || project.getParentProject().getObtainedBudget() + total > project.getParentProject().getRequiredBudget()) {
                    throw new ProjectException(translations.get("BudgetObtainedParent"));
                }
            }
            double p = parents(project, project.getParentProject(), project.getRequiredBudget(), 'b');
            if (p > project.getParentProject().getRequiredBudget()) {
                throw new ProjectException(translations.get("BudgetRequiredParent"));
            }
        }
        if (projectDto.getObtainedBudget() < child(project, 'a') && child(project, 'a') != -1) {
            throw new ProjectException(translations.get("BudgetObtainedInf"));
        }
        if (projectDto.getRequiredBudget() < child(project, 'b')) {
            throw new ProjectException(translations.get("BudgetRequiredInf"));
        }
        project.setOwner(user);

        Image image = imageRepository.findByName(projectDto.getImage()).orElse(null);

        if (image == null) {
            throw new ProjectException(translations.get("unexpectedError"));
        }
        project.setImage(image);

        return project;
    }

    private ProjectDto buildProjectDto(Project p) {
        ProjectDto projectDto = new ProjectDto();
        projectDto.setProjectID(p.getProjectID());
        projectDto.setImage(p.getImage().getName());

        String endDate = usDateToEuDate(p.getEndDate());
        String startDate = usDateToEuDate(p.getStartDate());
        projectDto.setEndDate(endDate);
        projectDto.setStartDate(startDate);
        projectDto.setName(p.getName());
        projectDto.setObtainedBudget(p.getObtainedBudget());
        projectDto.setRequiredBudget(p.getRequiredBudget());

        return projectDto;
    }

    private String usDateToEuDate(LocalDate dateTime) {
        return dateTime.format(DateTimeFormatter.ofPattern("dd-MM-yyyy"));
    }

    /**
     *
     * @param p
     * @param parents
     * @param required
     * @param car
     * @return
     */
    private double parents(Project p, Project parents, double required, char car) {
        Iterable<Project> projectsIterable = projectRepository.findByParentProjectIs(parents);
        boolean estPresent = projectRepository.existsProjectByImage(p.getImage());
        double budget = 0;
        if (car == 'b') {
            for (Project pp : projectsIterable) {
                budget += pp.getRequiredBudget();
            }
            if (!estPresent) {
                budget = budget + required;
            }
            return budget;
        } else {
            for (Project pp : projectsIterable) {
                budget += pp.getObtainedBudget();
            }
            if (car == 'a')
                budget += required;
        }
        return budget;
    }


    /**
     *
     * @param p
     * @param car
     * @return
     */
    private double child(Project p, char car) {
        boolean estPresent = projectRepository.existsProjectByImage(p.getImage());
        if (estPresent) {
            Iterable<Project> projectsIterable = projectRepository.findByParentProjectIs(p);
            int total = 0;
            if (projectsIterable instanceof Collection<?>) {
                total = ((Collection<?>) projectsIterable).size();
            }
            if (total == 0) {
                return -1;
            }
            double budget = 0;
            if (car == 'a') {
                for (Project pp : projectsIterable) {
                    budget += pp.getObtainedBudget();
                }
            } else {
                for (Project pp : projectsIterable) {
                    budget += pp.getRequiredBudget();
                }
            }
            return budget;
        }
        return 0;
    }


    public Integer getParents(Integer id) {
        Project project = projectRepository.findById(id).orElse(null);
        if(project.getParentProject() == null){
            return 0;
        }
        return project.getParentProject().getProjectID() ;
    }
}



