package com.mybudget.domain.services;

import com.mybudget.domain.dto.AccountDto;
import com.mybudget.domain.exceptions.AccountTypeException;
import com.mybudget.domain.models.Account;
import com.mybudget.domain.models.AccountType;
import com.mybudget.domain.models.DefaultAccount;
import com.mybudget.domain.models.User;
import com.mybudget.domain.repository.AccountRepository;
import com.mybudget.domain.repository.AccountTypesRepository;
import com.mybudget.domain.repository.DefaultAccountRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * @Author Eva Tumia
 */

@Service
public class AccountService {

    @Autowired
    AuthenticationService authenticationService;
    @Autowired
    AccountRepository accountRepository;
    @Autowired
    AccountTypesRepository accountTypesRepository;
    @Autowired
    private DefaultAccountRepository defaultAccountRepository;


    /**
     * Returns all the accounts of the current user
     *
     * @return A list of AccountDto
     * @see AccountDto
     */
    public List<AccountDto> getAll() {
        User user = authenticationService.getCurrentUser();
        List<Account> accounts = accountRepository.findAllByOwner(user);
        List<AccountDto> listToReturn = new ArrayList<>();
        for (Account a : accounts) {
            listToReturn.add(serializeAccount(a));
        }
        return listToReturn;
    }

    /**
     * Returns all the default accounts
     * @return A list of AccountDto
     * @see AccountDto
     */
    public List<AccountDto> getAllDefaultAccount() {
        Iterable<DefaultAccount> accounts = defaultAccountRepository.findAll();
        List<AccountDto> listToReturn = new ArrayList<>();
        for (DefaultAccount a : accounts) {
            AccountDto accountDto = new AccountDto();
            accountDto.setName(a.getName());
            accountDto.setAccountID(a.getDefaultAccountId());
            listToReturn.add(accountDto);
        }
        return listToReturn;
    }

    /**
     * Adds a new account to the current user
     *
     * @param accountDto: account to be added
     * @return An AccountDto, the one newly added
     * @see AccountDto
     */
    public AccountDto addAccount(AccountDto accountDto) {
        User user = authenticationService.getCurrentUser();
        AccountType accountType = accountTypesRepository.findById(accountDto.getTypeID()).orElseThrow(() -> new AccountTypeException("Account type does not exist"));
        Account acc = new Account(accountDto.getName(), accountType, user);
        acc = accountRepository.save(acc);
        return serializeAccount(acc);
    }

    /**
     * Serialize an object Account into a AccountDto object in order to make all the fields usable in the frontend
     *
     * @param acc: account to serialize
     * @return An AccountDto
     * @see Account
     */
    private AccountDto serializeAccount(Account acc) {
        AccountDto account = new AccountDto(
                acc.getAccountId(),
                acc.getName(),
                acc.getType().getAccountTypeID(),
                acc.getType().getName(),
                acc.getType().getIcon().getIconCode(),
                acc.getOwner().getUserID()
        );
        return account;
    }
}
