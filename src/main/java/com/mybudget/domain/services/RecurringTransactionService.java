package com.mybudget.domain.services;

import com.mybudget.domain.dto.TransactionDto;
import com.mybudget.domain.models.Transaction;
import com.mybudget.domain.repository.RecurringTransactionRepository;
import com.mybudget.domain.repository.TransactionRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;

/**
 * @author Eva Tumia
 */

@Service
@EnableScheduling
@Component
public class RecurringTransactionService {

    @Autowired
    private RecurringTransactionRepository recurringTransactionRepository;
    @Autowired
    private TransactionRepository transactionRepository;
    @Autowired
    private TransactionService transactionService;


    /**
     * Scheduled method firing each day at noon
     * Checks among all the recurring transactions if some of them have to be debit today
     * As for a direct debit, a transaction will automatically be added if needed
     * <p>
     * Conditions:  - the recurring transaction(RT) end date should not be expired
     * - the number of the recurring transaction(RT) day should be the same as today but not the entire same date
     * Ex: RT = 12/06/21 and Today = 12/06/21 => we don't add a new transaction
     * Ex: RT = 12/05/21 and Today = 12/06/21 => we add a new transaction
     * - the number of the day is not equal but they're both at the end of the month
     * Ex: RT= 31/05/21 and Today = 30/06/21 => we add a new transaction
     * <p>
     * See also {@link TransactionService#addTransaction(TransactionDto)}
     */
    @Transactional
    @Scheduled(cron = "0 00 12 * * ?", zone = "Europe/Paris")
    public void addRecurringTransaction() throws ClassNotFoundException {
        LocalDateTime todayDatetime = LocalDateTime.now();
        LocalDate today = LocalDate.now();
        List<Transaction> recurringTransaction = transactionRepository.findDistinctByRecurringTransactionNotNull();
        for (Transaction t : recurringTransaction) {
            LocalDateTime transactionDate = t.getValueDate();
            if (t.getRecurringTransaction().getEndDate().isAfter(todayDatetime)) {
                //See Conditions paragraph in the javadoc
                boolean areTheSameNumberOfDayButNotSameDate = transactionDate.getDayOfMonth() == today.getDayOfMonth() && !transactionDate.toLocalDate().equals(today);
                boolean areBothTheLastDayOfMonth = today.withDayOfMonth(today.lengthOfMonth()).equals(today)
                        && transactionDate.withDayOfMonth(transactionDate.toLocalDate().lengthOfMonth()).equals(transactionDate);
                if (t.getRecurringTransaction().getName().equals("double") || t.getRecurringTransaction().getName().equals("pasdouble")) {

                }
                if (areTheSameNumberOfDayButNotSameDate || areBothTheLastDayOfMonth) {
                    TransactionDto transactionDto = transactionService.serializeTransactionComplete(t);
                    transactionDto.setValueDate(LocalDateTime.now().toString().split("T")[0]);
                    transactionDto.setExecutionDate(LocalDateTime.now().toString().split("T")[0]);
                    Double abs = Math.abs(t.getAmount());
                    transactionDto.setAmount(abs.toString());
                    transactionDto.setRecurringTransactionEndDate(null);
                    transactionService.addTransaction(transactionDto);
                }
            }
        }

    }

}
