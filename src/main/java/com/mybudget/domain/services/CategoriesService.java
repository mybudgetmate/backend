
package com.mybudget.domain.services;

import com.mybudget.domain.dto.CategoryDto;
import com.mybudget.domain.dto.CategorySerializedDto;
import com.mybudget.domain.dto.response.CategoryTransactionResponse;
import com.mybudget.domain.dto.response.ObjectResponse;
import com.mybudget.domain.exceptions.CategoriesException;
import com.mybudget.domain.exceptions.TemplateCategoryException;
import com.mybudget.domain.exceptions.TemplateCategoryNameTranslationException;
import com.mybudget.domain.models.*;
import com.mybudget.domain.models.indentities.TransactionCategoryPK;
import com.mybudget.domain.models.translation.TemplateCategoryNameTranslation;
import com.mybudget.domain.repository.*;
import com.mybudget.domain.utils.JwtService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

/**
 * @author Eva Tumia
 */

@Service
public class CategoriesService {

    @Autowired
    private CategoryRepository categoryRepository;
    @Autowired
    private IconRepository iconRepository;
    @Autowired
    private JwtService jwtService;
    @Autowired
    private UserRepository userRepository;
    @Autowired
    private AuthenticationService authenticationService;
    @Autowired
    private TransactionCategoryRepository transactionCategoryRepository;
    @Autowired
    private TransactionService transactionService;
    @Autowired
    private TemplateCategoryNameTranslationRepository templateCategoryNameTranslationRepository;
    @Autowired
    private TemplateCategoryRepository templateCategoryRepository;

    /**
     * Adds a new category to the current user
     * @param categoryDto: category to be added
     * @return A CategorySerializedDto, the one newly added
     * @see CategorySerializedDto
     */
    public CategorySerializedDto addCategory(CategoryDto categoryDto) {
        if (!iconRepository.existsByIconCode(categoryDto.getIcon())) {
            Icon icon = new Icon(categoryDto.getIcon(), Icon.TYPE.FONTAWESOME);
            iconRepository.save(icon);
        }
        Icon icon = iconRepository.findByIconCode(categoryDto.getIcon());

        User user = authenticationService.getCurrentUser();

        Category newCategory = new Category(
                categoryDto.getName(),
                user,
                categoryDto.getBudget(),
                Category.TYPE.valueOf(categoryDto.getType().toUpperCase()),
                icon);
        newCategory = categoryRepository.save(newCategory);

        return serializeCategory(newCategory);
    }

    /**
     * Gets all the categories of the current user
     * @return A list of CategorySerializedDto
     * @see CategorySerializedDto
     */
    public List<CategorySerializedDto> getAllByUser() {
        User user = authenticationService.getCurrentUser();

        List<CategorySerializedDto> list = new ArrayList<CategorySerializedDto>();
        List<Category> listToSerialize = categoryRepository.findAllByOwner(user);

        for (Category c : listToSerialize) {
            list.add(serializeCategory(c));
        }
        return list;
    }

    /**
     * Updates the parent category of the category given in argument
     * Checks the value of the field idParentCategory of the argument
     *      - if: it's lower than 0 that means that the category has no parent category
     *      - else: fetch the parent category
     *
     * If the parent category has no child yet and contains transactions, a sub category "other" is added to it
     * The budget is updated recursively in old and new parent by removing/adding the budget of the category that has been moved
     * @throws CategoriesException if idParentCategory doesn't belong to any existing category in DB
     *                             if idCategoryID doesn't belong to any existing category in DB
     * @param cat: Category to be patched
     * @return A CategorySerilazedDto object, the one newly patched
     * @see CategorySerializedDto
     * See also {@link #addSubCategoryOther(Double, Category, List)}
     */
    @Transactional
    public CategorySerializedDto patchParentCategory(CategorySerializedDto cat) {
        // Get the new parent of the category
        Category parent = null;
        if (cat.getIdParentCategory() > 0)
            parent = categoryRepository.findById(cat.getIdParentCategory()).orElseThrow(() -> new CategoriesException("Category parent does not exist"));

        //Sum up the amount spent in the parent category
        List<TransactionCategory> transactionOfParent = transactionCategoryRepository.findByTransactionCategoryPK_CategoryID(parent);
        double amountParent = 0.0;
        for(TransactionCategory tc: transactionOfParent){
            amountParent+=tc.getAmount();
        }

        Category catToPatch = categoryRepository.findById(cat.getCategoryID()).orElseThrow(() -> new CategoriesException("Category does not exist"));
        boolean hasChild = categoryRepository.existsByParentCategory(parent);

        //If parent has no child yet and has transactions, we reassign his transactions to a default category called "other"
        if(!hasChild && amountParent>0){
            this.addSubCategoryOther(amountParent, parent, transactionOfParent);
        }else if(!hasChild && parent!=null){//if the parent is a child of another category, we update the budget of them recursively by removing the budget of the current parent
            Category temp = parent.getParentCategory();
            while(temp!=null){
                temp.setBudget(temp.getBudget()-parent.getBudget());
                categoryRepository.save(temp);
                temp = temp.getParentCategory();
            }
            parent.setBudget(0.0);
        }

        //Update budget of new parent category (sum of all its child)
        Category currentParent = parent;
        while(currentParent!=null){
            currentParent.setBudget(currentParent.getBudget()+catToPatch.getBudget());
            categoryRepository.save(currentParent);
            currentParent = currentParent.getParentCategory();
        }

        //Update budget of old parent category if exists
        currentParent = catToPatch.getParentCategory();
        while(currentParent!=null){
            currentParent.setBudget(currentParent.getBudget()-catToPatch.getBudget());
            categoryRepository.save(currentParent);
            currentParent = currentParent.getParentCategory();
        }

        catToPatch.setParentCategory(parent);
        categoryRepository.save(catToPatch);

        return serializeCategory(catToPatch);
    }

    /**
     * Private methode called in patchParentCategory() method
     * Adds a new category "Other" to a category that will become a parent
     * The category "other" will replace the parent in all the transactions that was linked to it
     * @param amountParent: Sum of all the transactions linked with a parent category
     * @param parent: The parent category that needs a subcategory "other"
     * @param transactionOfParent: List of all the transactions linked to the category "parent"
     * @see TransactionCategory
     * See also {@link #patchParentCategory(CategorySerializedDto)}
     */
    @Transactional
    private void addSubCategoryOther(Double amountParent, Category parent, List<TransactionCategory> transactionOfParent){
        if(parent==null)
            throw new IllegalArgumentException();
        Category newCategory = new Category();
        newCategory.setBudget(parent.getBudget());
        TemplateCategoryNameTranslation templateTr = templateCategoryNameTranslationRepository.findById("other").orElseThrow(()-> new TemplateCategoryNameTranslationException("Template translation does not exist"));
        newCategory.setName(templateTr.getTranslation(parent.getOwner().getPreferedLanguage().getCountryCode()));
        TemplateCategory template = templateCategoryRepository.findByName(templateTr).orElseThrow(()-> new TemplateCategoryException("Template does not exist"));
        newCategory.setIcon(template.getIcon());
        newCategory.setType(parent.getType());
        newCategory.setOwner(parent.getOwner());
        newCategory.setParentCategory(parent);
        newCategory = categoryRepository.save(newCategory);
        categoryRepository.save(newCategory);
        for(TransactionCategory tc: transactionOfParent){
            TransactionCategoryPK pk = new TransactionCategoryPK(newCategory,tc.getTransactionCategoryPK().getTransactionID());
            TransactionCategory newTc = new TransactionCategory(pk,tc.getAmount());
            transactionCategoryRepository.deleteByTransactionCategoryPK(tc.getTransactionCategoryPK());
            transactionCategoryRepository.save(newTc);
        }
    }

    /**
     * Method updating the budget and the name of a category
     * The budget is recursively updated for all the ancestors of the category
     * @throws CategoriesException if categoryID does not belong to any category existing in DB
     * @param categoryDto: category to be updated
     * @return A CategorySerializedDto object, the category newly updated
     * @see CategorySerializedDto
     */
    public CategorySerializedDto updateCategory(CategorySerializedDto categoryDto) {

        Category catToPatch = categoryRepository.findById(categoryDto.getCategoryID()).orElseThrow(() -> new CategoriesException("Category does not exist"));
        double previousAmount = catToPatch.getBudget();
        catToPatch.setBudget(categoryDto.getBudget());
        catToPatch.setName(categoryDto.getName());
        categoryRepository.save(catToPatch);

        //Update budget of parent category
        Category currentParent = catToPatch.getParentCategory();
        while(currentParent!=null){
            currentParent.setBudget(currentParent.getBudget()-previousAmount+catToPatch.getBudget());
            categoryRepository.save(currentParent);
            currentParent = currentParent.getParentCategory();
        }
        return serializeCategory(catToPatch);
    }

    /**
     * Deletes the category having "id" as categoryID
     * The method updates the budget of the ancestors of the category to be deleted by removing its budget from them
     * @param id: Id of the category to deleted
     * @throws CategoriesException if "id" does not belong to any category existing in DB
     * @throws CategoriesException if the category trying to be deleted has child categories or is linked to transactions
     * @return A ObjectResponse object
     * @see ObjectResponse
     */
    public ObjectResponse deleteCategory(int id) {
        try {
            Category catToPatch = categoryRepository.findById(id).orElseThrow(() -> new CategoriesException("Category does not exist"));
            categoryRepository.deleteById(id);

            //Update budget of parent category
            Category currentParent = catToPatch.getParentCategory();
            while(currentParent!=null){
                currentParent.setBudget(currentParent.getBudget()-catToPatch.getBudget());
                categoryRepository.save(currentParent);
                currentParent = currentParent.getParentCategory();
            }
        } catch (Exception ex) {
            throw new CategoriesException("Impossible to delete a category containing transactions and/or sub-categories");
        }
        return new ObjectResponse(id);
    }

    /**
     * Gets all the categories of a certain type belonging to the current user
     * @param type: Type of categories {INPUT,OUTPUT}
     * @throws CategoriesException if the type specified in the parameters does not exist
     * @return A list of CategorySerializeDto : the list of all the categories of the type desired belonging to the current user
     */
    public List<CategorySerializedDto> getByOwnerAndType(String type) {
        if (!type.toUpperCase(Locale.ROOT).equals("INPUT") && !type.toUpperCase(Locale.ROOT).equals("OUTPUT"))
            throw new CategoriesException("Category type does not exist");
        User user = authenticationService.getCurrentUser();
        List<Category> categories = categoryRepository.findByOwnerAndType(user, Category.TYPE.valueOf(type.toUpperCase()));
        List<CategorySerializedDto> listToReturn = new ArrayList<>();
        for (Category c : categories) {
            listToReturn.add(serializeCategory(c));
        }
        return listToReturn;
    }

    /**
     * Gets all the categories linked to a specific transaction
     * Serializes each Category fetched into CategorySerializedDto
     * @param t: Transaction to fetch all the categories from
     * @return: A list of CategorySerializedDto, list of all the categories of the specified transaction
     * @see Transaction
     * @see CategorySerializedDto
     */
    public List<CategorySerializedDto> getAllByTransaction(Transaction t) {
        List<TransactionCategory> transCat = transactionCategoryRepository.findByTransactionCategoryPK_TransactionID(t);
        List<CategorySerializedDto> cat = new ArrayList<>();
        for (TransactionCategory tc : transCat) {
            CategorySerializedDto c = serializeCategory(tc.getTransactionCategoryPK().getCategoryID());
            c.setAmount(tc.getAmount());
            cat.add(c);
        }
        return cat;
    }

    /**
     * Serializes a category into a CategorySerializedDto, transforming objects into primitive types
     * @param c: Category to serialize
     * @return A CategorySerializedDto, the category that has been serialized
     */
    private CategorySerializedDto serializeCategory(Category c) {
        int idParent = c.getParentCategory() == null ? -1 : c.getParentCategory().getCategoryID();
        CategorySerializedDto cat = new CategorySerializedDto(
                c.getCategoryID(),
                c.getName(),
                c.getBudget(),
                c.getType().name(),
                c.getIcon().getIconCode(),
                c.getOwner().getDeviceToken(),
                idParent
        );
        return cat;
    }

    public List<CategoryTransactionResponse> getCategoryAmountSumPerMonth(int month, int year,String type) {
        return transactionService.getAllCourseTransactions(false,month, year, null, type).getAllShoppingTransactions();
    }



}
