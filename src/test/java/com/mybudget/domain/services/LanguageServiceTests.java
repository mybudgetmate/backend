package com.mybudget.domain.services;

import com.mybudget.domain.dto.response.LanguageResponse;
import com.mybudget.domain.exceptions.UserException;
import com.mybudget.domain.models.Language;
import com.mybudget.domain.models.User;
import com.mybudget.domain.repository.LanguageRepository;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.ArrayList;
import java.util.List;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.BDDMockito.given;

@RunWith(SpringRunner.class)
@SpringBootTest
public class LanguageServiceTests {

    @Autowired
    private LanguageService languageService;

    @MockBean
    private AuthenticationService authenticationService;
    @MockBean
    private LanguageRepository languageRepository;


    @Before
    public void setUp() {

        List<Language> listMock = new ArrayList<>();
        Language l = new Language();
        l.setName(Language.LanguageName.ENGLISH);
        l.setActive(true);
        l.setCountryCode("fr");
        listMock.add(l);
        User user = new User();
        user.setPreferedLanguage(l);

        Mockito.when(languageRepository.findAll())
                .thenReturn(listMock);
        Mockito.when(languageRepository.findByCountryCode(any(String.class)))
                .thenReturn(java.util.Optional.of(l));
        given(authenticationService.getCurrentUser()).willReturn(user);
    }

    @Test
    public void noEmptyListLanguage() {
        List<LanguageResponse> list = languageService.fetchAllLanguages();
        assertThat(list).isNotNull();
    }

    @Test(expected = UserException.class)
    public void changeUserLanguageCountryCodeIsNull() {
        languageService.changeUserLanguage(null);
    }

    @Test
    public void changeUserLanguageWithGoodCountryCode() {
        assertThat(languageService.changeUserLanguage("fr")).contains("fr");
    }


}
