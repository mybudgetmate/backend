package com.mybudget.domain.services;

import com.mybudget.domain.dto.CategoryDto;
import com.mybudget.domain.dto.CategorySerializedDto;
import com.mybudget.domain.exceptions.CategoriesException;
import com.mybudget.domain.models.*;
import com.mybudget.domain.models.indentities.TransactionCategoryPK;
import com.mybudget.domain.repository.CategoryRepository;
import com.mybudget.domain.repository.IconRepository;
import com.mybudget.domain.repository.TransactionCategoryRepository;
import com.mybudget.domain.repository.UserRepository;
import com.mybudget.domain.utils.JwtService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.ArrayList;
import java.util.List;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.hasSize;
import static org.mockito.ArgumentMatchers.any;

/**
 * @author Eva Tumia
 */
@RunWith(SpringRunner.class)
@SpringBootTest
public class CategoryServiceTest {

    @Autowired
    private CategoriesService categoriesService;
    @Autowired
    private JwtService jwtService;
    @MockBean
    private CategoryRepository categoryRepository;
    @MockBean
    private UserRepository userRepository;
    @MockBean
    private IconRepository iconRepository;
    @MockBean
    private AuthenticationService authenticationService;
    @MockBean
    private TransactionCategoryRepository transactionCategoryRepository;


    @Before
    public void setUp(){
        User lara = new User();
        lara.setUserID(2);

        Category parent = new Category();
        parent.setName("parentCategory");
        parent.setBudget(100.0);
        parent.setCategoryID(1);
        parent.setType(Category.TYPE.valueOf("OUTPUT"));
        parent.setOwner(lara);
        parent.setIcon(new Icon());
        parent.setParentCategory(null);

        Category child = new Category();
        child.setName("enfantCategory");
        child.setBudget(100.0);
        child.setCategoryID(2);
        child.setType(Category.TYPE.valueOf("OUTPUT"));
        child.setOwner(lara);
        child.setIcon(new Icon());
        child.setParentCategory(parent);

        List<Category> list = new ArrayList<Category>();
        list.add(child);
        list.add(parent);

        Mockito.when(categoryRepository.findAllByOwner(lara)).thenReturn(list);

        Mockito.when(categoryRepository.findById(2))
                .thenReturn(java.util.Optional.of(child));

        Mockito.when(categoryRepository.findById(1))
                .thenReturn(java.util.Optional.of(parent));

        Mockito.when(authenticationService.getCurrentUser()).thenReturn(lara);

        Mockito.when(categoryRepository.save(any(Category.class))).thenReturn(child);

        Mockito.when(categoryRepository.findByOwnerAndType(any(User.class), any(Category.TYPE.class))).thenReturn(list);
    }



    @Test
    public void addCategoryWithGoodData(){
        CategoryDto categoryDto = new CategoryDto();
        categoryDto.setName("newCategory");
        categoryDto.setBudget(100);
        categoryDto.setType("OUTPUT");
        categoryDto.setIcon("fas fa-angry");

        Icon icon = new Icon(categoryDto.getIcon(), Icon.TYPE.FONTAWESOME);
        icon.setIconID(10);
        Mockito.when(iconRepository.save(icon)).thenReturn(icon);
        Mockito.when(iconRepository.findByIconCode(categoryDto.getIcon())).thenReturn(icon);

        assertThat(categoriesService.addCategory(categoryDto)).isNotNull();
    }

    @Test
    public void getAllWithGoodData(){
        assertThat(categoriesService.getAllByUser()).isNotNull();
        assertThat(categoriesService.getAllByUser(), hasSize(2));
    }


    @Test(expected = CategoriesException.class)
    public void patchParentCategoryWithParentCategoryInexistent(){
        CategorySerializedDto categorySerializedDto = new CategorySerializedDto();
        categorySerializedDto.setIdParentCategory(10);
        categorySerializedDto.setCategoryID(2);
        categoriesService.patchParentCategory(categorySerializedDto);
    }

    @Test(expected = CategoriesException.class)
    public void patchParentCategoryWithSubCategoryInexistent(){
        CategorySerializedDto categorySerializedDto = new CategorySerializedDto();
        categorySerializedDto.setIdParentCategory(1);
        categorySerializedDto.setCategoryID(4);
        categoriesService.patchParentCategory(categorySerializedDto);
    }

    @Test
    public void patchParentCategoryWithGoodDataWithParent() {
        CategorySerializedDto categorySerializedDto = new CategorySerializedDto();
        categorySerializedDto.setIdParentCategory(1);
        categorySerializedDto.setCategoryID(2);
        assertThat(categoriesService.patchParentCategory(categorySerializedDto)).isNotNull();
    }

    @Test
    public void patchParentCategoryWithGoodDataWithNoParent() {
        CategorySerializedDto categorySerializedDto = new CategorySerializedDto();
        categorySerializedDto.setIdParentCategory(-1);
        categorySerializedDto.setCategoryID(2);
        assertThat(categoriesService.patchParentCategory(categorySerializedDto)).isNotNull();
    }

    @Test(expected = CategoriesException.class)
    public void updateCategoryWithCategoryInexistent(){
        CategorySerializedDto categorySerializedDto = new CategorySerializedDto();
        categorySerializedDto.setCategoryID(13);
        categoriesService.updateCategory(categorySerializedDto);
    }

    @Test
    public void updateCategoryWithGoodData(){
        CategorySerializedDto categorySerializedDto = new CategorySerializedDto();
        categorySerializedDto.setCategoryID(1);
        assertThat(categoriesService.updateCategory(categorySerializedDto)).isNotNull();
    }

    @Test(expected = CategoriesException.class)
    public void deleteCategoryKeyViolation(){
        Mockito.doThrow(new RuntimeException()).when(categoryRepository).deleteById(1);
        categoriesService.deleteCategory(1);
    }

    @Test
    public void deleteCategoryWithGoodId(){
        assertThat(categoriesService.deleteCategory(2)).isNotNull();
    }

    @Test
    public void getByOwnerAndTypeWithGoodData(){
        assertThat(categoriesService.getByOwnerAndType("input")).isNotNull();
    }

    @Test(expected = CategoriesException.class)
    public void getByOwnerAndTypeWithWrongType(){
        categoriesService.getByOwnerAndType("wrongType");
    }

    @Test
    public void getAllByTransactionCorrectData(){
        Category cat = new Category();
        cat.setCategoryID(1);
        cat.setName("cat");
        cat.setBudget(100.0);
        cat.setType(Category.TYPE.OUTPUT);
        cat.setIcon(new Icon());
        cat.setOwner(new User());
        TransactionCategoryPK transactionCategoryPK = new TransactionCategoryPK();
        transactionCategoryPK.setCategoryID(cat);
        TransactionCategory tc = new TransactionCategory();
        tc.setTransactionCategoryPK(transactionCategoryPK);

        List<TransactionCategory> transactionCategoryList = new ArrayList<TransactionCategory>();
        transactionCategoryList.add(tc);

        Transaction t = new Transaction();
        t.setTransactionID(1);

        Mockito.when(transactionCategoryRepository.findByTransactionCategoryPK_TransactionID(any(Transaction.class))).thenReturn(transactionCategoryList);
        assertThat(categoriesService.getAllByTransaction(t)).isNotNull();
    }
}
