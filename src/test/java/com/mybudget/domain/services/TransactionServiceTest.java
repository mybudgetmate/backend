package com.mybudget.domain.services;


import com.mybudget.domain.dto.CategorySerializedDto;
import com.mybudget.domain.dto.TransactionCSV;
import com.mybudget.domain.dto.TransactionDto;
import com.mybudget.domain.exceptions.AccountException;
import com.mybudget.domain.exceptions.CategoriesException;
import com.mybudget.domain.exceptions.TransactionException;
import com.mybudget.domain.models.*;
import com.mybudget.domain.models.indentities.TransactionCategoryPK;
import com.mybudget.domain.models.translation.TemplateCategoryNameTranslation;
import com.mybudget.domain.repository.*;
import com.mybudget.domain.utils.JwtService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
import static org.mockito.ArgumentMatchers.any;

/**
 * @author Eva Tumia && Soumaya Izmar
 */
@RunWith(SpringRunner.class)
@SpringBootTest
public class TransactionServiceTest {

    @Autowired
    private TransactionService transactionService;
    @Autowired
    private JwtService jwtService;
    @MockBean
    private TransactionRepository transactionRepository;
    @MockBean
    private AccountRepository accountRepository;
    @MockBean
    private CategoryRepository categoryRepository;
    @MockBean
    private TransactionCategoryRepository transactionCategoryRepository;
    @MockBean
    private MetaCategoryRepository metaCategoryRepository;
    @MockBean
    private TemplateCategoryNameTranslationRepository templateCategoryNameTranslationRepository;
    @MockBean
    private TemplateCategoryRepository templateCategoryRepository;
    @MockBean
    private AuthenticationService authenticationService;
    @MockBean
    private TranslationService translationService;
    @MockBean
    private CategoriesService categoriesService;

    private User user = new User();

    @Test
    public void getAllCourseTransactionsWithNoAmountTransaction() {
        User u = new User();
        Mockito.when(authenticationService.getCurrentUser()).thenReturn(u);
        List<TransactionCategory> listTransactions = new ArrayList<>();
        Transaction t = new Transaction();
        t.setAmount(0.0);
        t.setType(Transaction.TYPE.INPUT);

        TransactionCategoryPK transactionCategoryPK = new TransactionCategoryPK();
        transactionCategoryPK.setTransactionID(t);

        TransactionCategory transactionCategory = new TransactionCategory();
        transactionCategory.setTransactionCategoryPK(transactionCategoryPK);
        listTransactions.add(transactionCategory);
        Mockito.when(transactionCategoryRepository.findByTransactionCategoryPK_CategoryID(any(Category.class))).thenReturn(listTransactions);
        System.out.println(transactionService.getAllCourseTransactions(true, 2, 2021, "course", "INPUT").getAllShoppingTransactions());
        assertThat(transactionService.getAllCourseTransactions(true, 2, 2021, "course", "INPUT").getAllShoppingTransactions()).asList().isEmpty();
    }

    @Test
    public void getAllCourseTransactionsWithTransactions() {
        User u = new User();
        Mockito.when(authenticationService.getCurrentUser()).thenReturn(u);
        List<Category> list = new ArrayList<>();
        List<Category> list2 = new ArrayList<>();
        Category c = new Category();
        c.setName("superCategory");
        Icon icon = new Icon();
        icon.setIconCode("fas fa");
        c.setIcon(icon);
        list.add(c);

        Mockito.when(categoryRepository.findByNameAndOwnerAndType(any(String.class), any(User.class), any(Category.TYPE.class))).thenReturn(java.util.Optional.of(c));
        Mockito.when(categoryRepository.findByParentCategoryAndOwnerAndType(any(Category.class), any(User.class), any(Category.TYPE.class))).thenReturn(list).thenReturn(list2);

        List<TransactionCategory> listTransactions = new ArrayList<>();
        Transaction t = new Transaction();
        t.setAmount(100.0);
        t.setCountDate(LocalDateTime.now());
        t.setValueDate(LocalDateTime.now());
        t.setType(Transaction.TYPE.INPUT);
        System.out.println(t.getValueDate().getDayOfMonth());
        TransactionCategoryPK transactionCategoryPK = new TransactionCategoryPK();
        transactionCategoryPK.setTransactionID(t);
        TransactionCategory transactionCategory = new TransactionCategory();
        transactionCategory.setAmount(100);
        transactionCategory.setTransactionCategoryPK(transactionCategoryPK);
        listTransactions.add(transactionCategory);

        List<TransactionCategory> listTransactionsEmpty = new ArrayList<>();
        TransactionCategoryPK transactionCategoryPKEmpty = new TransactionCategoryPK();

        Transaction t2 = new Transaction();
        t2.setAmount(10.0);
        t2.setCountDate(LocalDateTime.now());
        t2.setValueDate(LocalDateTime.now());
        t2.setType(Transaction.TYPE.INPUT);
        transactionCategoryPKEmpty.setTransactionID(t2);

        TransactionCategory transactionCategoryEmpty = new TransactionCategory();
        transactionCategoryEmpty.setAmount(10);
        transactionCategoryEmpty.setTransactionCategoryPK(transactionCategoryPKEmpty);
        listTransactionsEmpty.add(transactionCategoryEmpty);

        Mockito.when(transactionCategoryRepository.findByTransactionCategoryPK_CategoryID(any(Category.class))).thenReturn(listTransactions).thenReturn(listTransactions).thenReturn(listTransactionsEmpty);

        assertThat(transactionService.getAllCourseTransactions(true, LocalDateTime.now().getMonthValue(), LocalDateTime.now().getYear(), "course", "INPUT").getAllShoppingTransactions()).asList().isNotEmpty();

    }

    @Test
    public void getAmountBudgetTransactionsPerWeekEmptyList() throws ClassNotFoundException {
        User u = new User();
        Language l = new Language();
        l.setCountryCode("gb");
        u.setPreferedLanguage(l);
        Mockito.when(authenticationService.getCurrentUser()).thenReturn(u);
        Map<String, String> map = new HashMap<>();
        map.put("3", "march");
        Mockito.when(translationService.fetchMonthsTranslation(any(String.class))).thenReturn(map);
        List<Transaction> list = new ArrayList<>();
        Mockito.when(transactionService.getAllCourseTransactionsWithNoResponse(3, 2021)).thenReturn(list);

        assertThat(transactionService.getAmountBudgetTransactionsPerWeek(3, 2021)).asList().size().isEqualTo(5);
    }

    @Test
    public void getAmountBudgetTransactionsPerWeekNotEmptyList() throws ClassNotFoundException {
        User u = new User();
        Language l = new Language();
        l.setCountryCode("gb");
        u.setPreferedLanguage(l);
        Mockito.when(authenticationService.getCurrentUser()).thenReturn(u);
        Map<String, String> map = new HashMap<>();
        map.put("3", "march");
        List<Category> list = new ArrayList<>();

        Category c = new Category();
        c.setName("superCategory");
        Icon icon = new Icon();
        icon.setIconCode("fas fa");
        c.setIcon(icon);
        list.add(c);
        Mockito.when(translationService.fetchMonthsTranslation(any(String.class))).thenReturn(map);
        List<Transaction> list3 = new ArrayList<>();
        Transaction t = new Transaction();
        t.setAmount(100.0);
        t.setCountDate(LocalDateTime.now());
        t.setValueDate(LocalDateTime.now());
        t.setType(Transaction.TYPE.INPUT);
        list3.add(t);
        List<TransactionCategory> list1 = new ArrayList<>();
        TransactionCategory transactionCategory = new TransactionCategory();
        TransactionCategoryPK transactionCategoryPK = new TransactionCategoryPK();
        transactionCategoryPK.setTransactionID(t);
        transactionCategory.setTransactionCategoryPK(transactionCategoryPK);
        list1.add(transactionCategory);
        Mockito.when(transactionCategoryRepository.findByTransactionCategoryPK_CategoryID(any(Category.class))).thenReturn(list1);
        Mockito.when(categoryRepository.findByNameAndOwner(any(String.class), any(User.class))).thenReturn(java.util.Optional.of(c));
        Mockito.when(categoryRepository.findByParentCategoryAndOwner(any(Category.class), any(User.class))).thenReturn(list).thenReturn(list);

        assertThat(transactionService.getAmountBudgetTransactionsPerWeek(3, 2021)).asList().size().isEqualTo(5);
    }


    @Test(expected = TransactionException.class)
    public void importCsvWrongSize() throws ClassNotFoundException {

        List<List<String>> mainList = new ArrayList<>();
        List<String> subList1 = new ArrayList<>();
        List<String> subList2 = new ArrayList<>();
        subList1.add("2021-0157");
        subList1.add("2021-0157");
        subList1.add("2021-0157");
        subList2.add("2021-0157");
        subList2.add("2021-0157");
        subList2.add("2021-0157");
        mainList.add(subList1);
        mainList.add(subList2);


        TransactionCSV transactionCSV = new TransactionCSV();
        transactionCSV.setListTransaction(mainList);
        transactionCSV.setAccount("MasterCard");

        Account account = new Account();
        account.setName("MasterCard");
        account.setAccountId(1);
        account.setOwner(user);
        List<Account> accounts = new ArrayList<>();
        accounts.add(account);

        Mockito.when(accountRepository.findAllByOwner(any(User.class))).thenReturn(accounts);

        transactionService.addTransactionWithCSV(transactionCSV);

    }

    @Test(expected = TransactionException.class)
    public void importCsvWrongAccount() throws ClassNotFoundException {

        List<List<String>> mainList = new ArrayList<>();
        List<String> subList1 = new ArrayList<>();
        List<String> subList2 = new ArrayList<>();
        subList1.add("2021-0157");
        subList1.add("2021-0157");
        subList1.add("2021-0157");
        subList2.add("2021-0157");
        subList2.add("2021-0157");
        subList2.add("2021-0157");
        mainList.add(subList1);
        mainList.add(subList2);


        TransactionCSV transactionCSV = new TransactionCSV();
        transactionCSV.setListTransaction(mainList);
        transactionCSV.setAccount("mastercord");

        Account account = new Account();
        account.setName("mastercord");
        account.setAccountId(1);
        account.setOwner(user);
        List<Account> accounts = new ArrayList<>();
        accounts.add(account);

        Mockito.when(accountRepository.findAllByOwner(any(User.class))).thenReturn(accounts);

        transactionService.addTransactionWithCSV(transactionCSV);

    }


    @Test(expected = TransactionException.class)
    public void importCsvWrongValueDate() throws ClassNotFoundException {

        List<List<String>> mainList = new ArrayList<>();
        List<String> subList1 = new ArrayList<>();
        List<String> subList2 = new ArrayList<>();

        subList1.add("31/03/2021");
        subList1.add("31/03/2021");
        subList1.add("-1,5");
        subList1.add("EUR");
        subList1.add("NUMERO 6703 04XX XXXX X410 9 PROXY");
        subList1.add("BE12345678901234");
        subList1.add("2021-0157");


        subList2.add("31/03/2021");
        subList2.add("311/03/2021");
        subList2.add("1,5");
        subList2.add("EUR");
        subList2.add("NUMERO 6703 04XX XXXX X410 9 PROXY");
        subList2.add("BE12345678901234");
        subList2.add("2021-0157");

        mainList.add(subList1);
        mainList.add(subList2);

        TransactionCSV transactionCSV = new TransactionCSV();
        transactionCSV.setListTransaction(mainList);
        transactionCSV.setAccount("MasterCard");

        Account account = new Account();
        account.setName("MasterCard");
        account.setAccountId(1);
        List<Account> accounts = new ArrayList<>();
        accounts.add(account);

        Mockito.when(accountRepository.findAllByOwner(any(User.class))).thenReturn(accounts);

        transactionService.addTransactionWithCSV(transactionCSV);

    }

    @Test(expected = TransactionException.class)
    public void importCsvWrongAmount() throws ClassNotFoundException {

        List<List<String>> mainList = new ArrayList<>();
        List<String> subList1 = new ArrayList<>();
        List<String> subList2 = new ArrayList<>();

        subList1.add("12/03/2021");
        subList1.add("11/10/2021");
        subList1.add("aaaa");
        subList1.add("EUR");
        subList1.add("NUMERO 6703 04XX XXXX X410 9 PROXY");
        subList1.add("BE12345678901234");
        subList1.add("2021-0157");

        subList2.add("12/03/2021");
        subList2.add("07/03/2021");
        subList2.add("-1,5");
        subList2.add("EUR");
        subList2.add("NUMERO 6703 04XX XXXX X410 9 PROXY");
        subList2.add("BE12345678901234");
        subList2.add("2021-0157");

        mainList.add(subList2);
        mainList.add(subList1);


        TransactionCSV transactionCSV = new TransactionCSV();
        transactionCSV.setListTransaction(mainList);
        transactionCSV.setAccount("MasterCard");

        Account account = new Account();
        account.setName("MasterCard");
        account.setAccountId(1);
        account.setOwner(user);
        List<Account> accounts = new ArrayList<>();
        accounts.add(account);

        Mockito.when(accountRepository.findAllByOwner(any(User.class))).thenReturn(accounts);

        transactionService.addTransactionWithCSV(transactionCSV);

    }

    @Test
    public void importCsvGoodData() throws ClassNotFoundException {

        List<List<String>> mainList = new ArrayList<>();
        List<String> subList1 = new ArrayList<>();
        List<String> subList2 = new ArrayList<>();

        subList1.add("05/03/2021");
        subList1.add("31/03/2021");
        subList1.add("-1,5");
        subList1.add("EUR");
        subList1.add("NUMERO 6703 04XX XXXX X410 9 PROXY");
        subList1.add("BE12345678901234");
        subList1.add("2021-0157");

        subList2.add("05/03/2021");
        subList2.add("31/03/2021");
        subList2.add("-1,5");
        subList2.add("EUR");
        subList2.add("NUMERO 6703 04XX XXXX X410 9 PROXY");
        subList2.add("BE12345678901234");
        subList2.add("2021-0157");


        mainList.add(subList1);
        mainList.add(subList2);

        TransactionCSV transactionCSV = new TransactionCSV();
        transactionCSV.setListTransaction(mainList);
        transactionCSV.setAccount("MasterCard");

        Account account = new Account();
        account.setName("MasterCard");
        account.setAccountId(1);
        account.setOwner(user);
        List<Account> accounts = new ArrayList<>();
        accounts.add(account);

        Mockito.when(accountRepository.findAllByOwner(any(User.class))).thenReturn(accounts);

        List<MetaCategory> metaCategories = new ArrayList<>();

        MetaCategory metaCategory = new MetaCategory();
        metaCategory.setDetails("PROXY");
        TemplateCategoryNameTranslation templateCategoryNameTranslation = new TemplateCategoryNameTranslation();
        templateCategoryNameTranslation.setFr("PROXY");
        templateCategoryNameTranslation.setGb("PROXY");
        templateCategoryNameTranslation.setId("PROXY");


        TemplateCategory templateCategory = new TemplateCategory();
        templateCategory.setName(templateCategoryNameTranslation);

        metaCategory.setTemplateCategory(templateCategory);
        metaCategories.add(metaCategory);

        Category c = new Category();
        c.setBudget(300.0);
        c.setName("test");
        c.setCategoryID(2);
        Mockito.when(categoryRepository.findByNameAndOwner(any(String.class), any(User.class))).thenReturn(java.util.Optional.of(c));
        Mockito.when(metaCategoryRepository.findAll()).thenReturn(metaCategories);
        Mockito.when(templateCategoryNameTranslationRepository.findById(any(String.class))).thenReturn(java.util.Optional.of(templateCategoryNameTranslation));
        Mockito.when(templateCategoryRepository.findByName(any(TemplateCategoryNameTranslation.class))).thenReturn(java.util.Optional.of(templateCategory));


        transactionService.addTransactionWithCSV(transactionCSV);

        transactionCSV.setAccount("BNP Paribas Fortis");
        account.setName("BNP Paribas Fortis");
        transactionService.addTransactionWithCSV(transactionCSV);

    }


    @Before
    public void setUp() throws ClassNotFoundException {

        user.setUserID(1);
        Language l = new Language();
        l.setCountryCode("gb");
        user.setPreferedLanguage(l);

        Mockito.when(authenticationService.getCurrentUser()).thenReturn(user);

        Category category = new Category();
        category.setCategoryID(1);
        category.setOwner(user);

        Icon icon = new Icon();
        icon.setIconCode("far far-angry");
        icon.setIconID(1);


        AccountType accType = new AccountType();
        accType.setAccountTypeID(1);
        accType.setName("Paypal");
        accType.setIcon(icon);

        Account account = new Account();
        account.setAccountId(1);
        account.setOwner(user);
        account.setType(accType);

        Transaction transaction = new Transaction();
        transaction.setAccount(account);
        transaction.setCountDate(LocalDateTime.now());
        transaction.setValueDate(LocalDateTime.now());
        transaction.setAmount(30.0);
        transaction.setTransactionID(1);
        transaction.setType(Transaction.TYPE.OUTPUT);
        transaction.setAccount(account);

        TransactionCategoryPK transactionCategoryPK = new TransactionCategoryPK(category, transaction);
        TransactionCategory transactionCategory = new TransactionCategory(transactionCategoryPK, transaction.getAmount());

        List<Transaction> it = new ArrayList<Transaction>();
        it.add(transaction);
        Map<String, String> map = new HashMap<>();
        map.put("csv", "csv");
        map.put("wrongCsvFormat", "csv");
        map.put("accountNotFound", "csv");
        map.put("payment", "csv");
        map.put("transfer", "csv");
        Mockito.when(translationService.fetchAllTransactionsTranslation(any(String.class))).thenReturn(map);
        Mockito.when(categoryRepository.findById(1)).thenReturn(java.util.Optional.of(category));
        Mockito.when(accountRepository.findById(1)).thenReturn(java.util.Optional.of(account));
        Mockito.when((transactionRepository.findById(1))).thenReturn(java.util.Optional.of(transaction));
        Mockito.when(transactionRepository.save(any(Transaction.class))).thenReturn(transaction);
        Mockito.when(transactionCategoryRepository.save(any(TransactionCategory.class))).thenReturn(transactionCategory);
        Mockito.when(categoryRepository.save(any(Category.class))).thenReturn(category);
        Mockito.when(authenticationService.getCurrentUser()).thenReturn(user);
        Mockito.when(transactionRepository.findByAccount_ownerOrderByValueDateDesc(any(User.class))).thenReturn(it);
        Mockito.when(transactionRepository.findByAccount_owner(any(User.class))).thenReturn(it);
        Mockito.when(transactionCategoryRepository.findByTransactionCategoryPK(any(TransactionCategoryPK.class))).thenReturn(transactionCategory);
        Mockito.when(transactionRepository.findByValueDateAndAccount_owner(any(LocalDateTime.class), any(User.class))).thenReturn(it);
        Mockito.when(translationService.fetchHomepageTranslation(any(String.class))).thenReturn(map);

    }

    @Test(expected = AccountException.class)
    public void addTransactionWithInexistentAccount() {
        TransactionDto transactionDto = new TransactionDto();
        transactionDto.setAccountID(10);
        try {
            transactionService.addTransaction(transactionDto);
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
    }

    @Test(expected = CategoriesException.class)
    public void addTransactionWithInexistentCategory() {
        TransactionDto transactionDto = new TransactionDto();
        transactionDto.setAccountID(1);
        transactionDto.setExecutionDate("2020-03-20");
        transactionDto.setValueDate("2020-03-20");
        transactionDto.setType("input");
        transactionDto.setAmount("100.0");
        CategorySerializedDto cat = new CategorySerializedDto();
        cat.setCategoryID(1);
        CategorySerializedDto catInexistent = new CategorySerializedDto();
        cat.setCategoryID(2);
        List<CategorySerializedDto> categories = new ArrayList<CategorySerializedDto>();
        categories.add(cat);
        categories.add(catInexistent);
        transactionDto.setCategories(categories);
        try {
            assertThat(transactionService.addTransaction(transactionDto)).isNotNull();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void addTransactionWithGoodData() {
        TransactionDto transactionDto = new TransactionDto();
        transactionDto.setAccountID(1);
        transactionDto.setExecutionDate("2020-03-20");
        transactionDto.setValueDate("2020-03-20");
        transactionDto.setType("input");
        transactionDto.setAmount("100.0");
        CategorySerializedDto cat = new CategorySerializedDto();
        cat.setCategoryID(1);
        List<CategorySerializedDto> categories = new ArrayList<CategorySerializedDto>();
        categories.add(cat);
        transactionDto.setCategories(categories);
        try {
            assertThat(transactionService.addTransaction(transactionDto)).isNotNull();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void getUserBalance() {
        assertThat(transactionService.getUserBalance()).isNotNull();
    }

    @Test
    public void getAllByUser() {
        assertThat(transactionService.getAllByUser()).isNotNull();
    }

    @Test(expected = AccountException.class)
    public void patchTransactionWithInexistentAccount() {
        TransactionDto transactionDto = new TransactionDto();
        transactionDto.setAccountID(2);
        transactionService.patchTransaction(transactionDto);
    }

    @Test(expected = TransactionException.class)
    public void patchTransactionWithInexistentTransaction() {
        TransactionDto transactionDto = new TransactionDto();
        transactionDto.setAccountID(1);
        transactionDto.setTransactionID(10);
        transactionService.patchTransaction(transactionDto);
    }

  /*  @Test
    public void patchTransactionWithGoodData() {
        TransactionDto transactionDto = new TransactionDto();
        transactionDto.setTransactionID(1);
        transactionDto.setAccountID(1);
        transactionDto.setAmount("100.0");
        transactionDto.setValueDate("2020-02-20");
        transactionDto.setExecutionDate("2020-02-20");
        assertThat(transactionService.patchTransaction(transactionDto)).isNotNull();
    }*/

    @Test(expected = TransactionException.class)
    public void deleteInexistentTransaction() {
        transactionService.deleteTransaction(10);
    }

    @Test()
    public void deleteTransaction() {
        assertThat(transactionService.deleteTransaction(1)).isNotNull();
    }

    @Test()
    public void getDailyBudget() {
        CategorySerializedDto cat1 = new CategorySerializedDto();
        cat1.setBudget(100);
        cat1.setType("OUTPUT");
        CategorySerializedDto cat2 = new CategorySerializedDto();
        cat2.setBudget(100);
        cat2.setType("INPUT");

        List<CategorySerializedDto> list = new ArrayList<CategorySerializedDto>();
        list.add(cat1);
        list.add(cat2);

        Mockito.when(categoriesService.getAllByUser()).thenReturn(list);

        assertThat(transactionService.getDailyBudget()).isNotNull();
    }
}

