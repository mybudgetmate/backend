package com.mybudget.domain.services;

import com.mybudget.domain.models.Icon;
import com.mybudget.domain.repository.IconRepository;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.ArrayList;
import java.util.List;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.hasSize;

/**
 * @author: Eva Tumia
 */
@RunWith(SpringRunner.class)
@SpringBootTest
public class IconsServiceTest {

    @MockBean
    private IconRepository iconRepository;

    @MockBean
    private IconsService iconsService;

    @Before
    public void setUp(){
        List<Icon> list = new ArrayList<Icon>();
        list.add(new Icon());
        Mockito.when(iconRepository.findAll()).thenReturn(list);
    }

    @Test
    public void getAllWithGoodData(){
        assertThat(iconsService.getAll()).isNotNull();
    }
}
