package com.mybudget.domain.services;

import com.mybudget.domain.models.Image;
import com.mybudget.domain.repository.ImageRepository;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.ArrayList;
import java.util.List;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;

@RunWith(SpringRunner.class)
@SpringBootTest
public class ImageServiceTests {

    @Autowired
    private ImageService imageService;

    @MockBean
    private ImageRepository imageRepository;

    @Test
    public void fetchAllImagesNotEmpty() {

        List<Image> images = new ArrayList<>();
        Image image = new Image();
        image.setImageID(1);
        image.setName("image1");

        images.add(image);

        Mockito.when(imageRepository.findAll()).thenReturn(images);

        assertThat(imageService.fetchAllImages()).isNotNull();
    }


}
