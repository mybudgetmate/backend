package com.mybudget.domain.services;

import com.mybudget.domain.dto.LoginDto;
import com.mybudget.domain.dto.UserRegistrationDto;
import com.mybudget.domain.dto.response.AuthenticationResponse;
import com.mybudget.domain.exceptions.FatalException;
import com.mybudget.domain.exceptions.LoginException;
import com.mybudget.domain.exceptions.RegistrationException;
import com.mybudget.domain.models.*;
import com.mybudget.domain.models.translation.StatusTranslation;
import com.mybudget.domain.repository.*;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.test.context.junit4.SpringRunner;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import static org.assertj.core.api.AssertionsForInterfaceTypes.assertThat;
import static org.mockito.ArgumentMatchers.any;

@RunWith(SpringRunner.class)
@SpringBootTest
public class AuthenticationServiceTests {

    @Autowired
    private AuthenticationService authenticationService;

    @MockBean
    private UserRepository userRepository;
    @MockBean
    private StatusRepository statusRepository;
    @MockBean
    private CurrencyRepository currencyRepository;
    @MockBean
    private LanguageRepository languageRepository;
    @MockBean
    private CategoryStatusRepository categoryStatusRepository;
    @MockBean
    private CategoryRepository categoryRepository;
    @MockBean
    private DefaultAccountRepository defaultAccountRepository;
    @MockBean
    private AccountRepository accountRepository;
    @MockBean
    private SubStatusRepository subStatusRepository;
    @MockBean
    private CategorySubStatusRepository categorySubStatusRepository;
    @MockBean
    private SubStatusUsersRepository subStatusUsersRepository;

    @MockBean
    private StatusTranslationRepository statusTranslationRepository;

    @Before
    public void setUp() {
        User lara = new User();
        lara.setFirstname("Lara");
        lara.setLastname("Croft");
        lara.setRegistrationDate(LocalDateTime.now());
        lara.setPassword("$2a$10$YqW8nGNXcXGW8XYclNdLVeE19F5TqwAO.GF3x5rVlyhkBtv2Mu1M2");
        lara.setEmail("lara_croft@hotmail.com");
        lara.setPreferedLanguage(new Language());
        lara.setGeneralConsent(true);
        lara.setGdprConsent(true);
        lara.setCookiesConsent(true);
        lara.setPreferedCurrency(new Currency());
        lara.setIpAddress("127.0.0.1");
        lara.setDeviceToken("myToken");
        lara.setStatus(new Status());

        lara.setUserID(2);
        Mockito.when(userRepository.findByEmail(lara.getEmail()))
                .thenReturn(java.util.Optional.of(lara));

        Language language = new Language();
        language.setCountryCode("gb");
        Status status = new Status();
        status.setName(new StatusTranslation());
        Currency currency = new Currency();
        currency.setType("DOLLARS");

        Mockito.when(currencyRepository.findByType("DOLLARS")).thenReturn(java.util.Optional.of(currency));
        Mockito.when(languageRepository.findByCountryCode("gb")).thenReturn(java.util.Optional.of(language));
        Mockito.when(statusRepository.findByName(new StatusTranslation())).thenReturn(java.util.Optional.of(status));


    }

    @Test(expected = LoginException.class)
    public void loginWithWrongPassword() throws ClassNotFoundException {

        LoginDto loginDto = new LoginDto();
        loginDto.setPreferredLanguage("gb");
        loginDto.setPassword("tomb");
        loginDto.setEmail("lara_croft@hotmail.com");
        authenticationService.loginUser(loginDto);
    }

    @Test(expected = LoginException.class)
    public void loginWithWrongEmail() throws ClassNotFoundException {

        LoginDto loginDto = new LoginDto();
        loginDto.setPreferredLanguage("gb");
        loginDto.setPassword("tombRaider");
        loginDto.setEmail("lara_craft@hotmail.com");
        authenticationService.loginUser(loginDto);
    }

    @Test
    public void loginWithGoodPasswordAndEmail() throws ClassNotFoundException {

        LoginDto loginDto = new LoginDto();
        loginDto.setPreferredLanguage("gb");
        loginDto.setPassword("tombRaider");
        loginDto.setEmail("lara_croft@hotmail.com");
        AuthenticationResponse authenticationResponse = authenticationService.loginUser(loginDto);
        assertThat(authenticationResponse.getToken())
                .isNotEmpty();
    }

    @Test(expected = FatalException.class)
    public void loginWithNoLanguage() throws ClassNotFoundException {

        LoginDto loginDto = new LoginDto();
        loginDto.setPreferredLanguage("abc");
        loginDto.setPassword("tombRaider");
        loginDto.setEmail("lara_croft@hotmail.com");
        AuthenticationResponse authenticationResponse = authenticationService.loginUser(loginDto);
        assertThat(authenticationResponse.getToken())
                .isNotEmpty();
    }


    @Test(expected = RegistrationException.class)
    public void signUpWithPasswordsDontMatch() throws ClassNotFoundException {
        UserRegistrationDto userRegistrationDto = new UserRegistrationDto();
        userRegistrationDto.setCookiesConsent(true);
        userRegistrationDto.setGdprConsent(true);
        userRegistrationDto.setGeneralConsent(true);
        userRegistrationDto.setDeviceToken("myToken");
        userRegistrationDto.setPassword("tombNotRaider");
        userRegistrationDto.setPasswordRepeat("tombRaider");
        userRegistrationDto.setFirstName("Lara");
        userRegistrationDto.setLastName("Croft");
        userRegistrationDto.setPreferedCurrency("DOLLARS");
        userRegistrationDto.setIpAddress("127.0.0.1");
        userRegistrationDto.setEmail("lara_croft@hotmail.com");
        userRegistrationDto.setPreferedLanguage("gb");
        userRegistrationDto.setStatus("student");

        authenticationService.registerUser(userRegistrationDto);

    }


    @Test(expected = RegistrationException.class)
    public void signUpWithExistEmail() throws ClassNotFoundException {
        UserRegistrationDto userRegistrationDto = new UserRegistrationDto();
        userRegistrationDto.setCookiesConsent(true);
        userRegistrationDto.setGdprConsent(true);
        userRegistrationDto.setGeneralConsent(true);
        userRegistrationDto.setDeviceToken("myToken");
        userRegistrationDto.setPassword("tombRaider");
        userRegistrationDto.setPasswordRepeat("tombRaider");
        userRegistrationDto.setFirstName("Lara");
        userRegistrationDto.setLastName("Croft");
        userRegistrationDto.setPreferedCurrency("DOLLARS");
        userRegistrationDto.setIpAddress("127.0.0.1");
        userRegistrationDto.setEmail("lara_croft@hotmail.com");
        userRegistrationDto.setPreferedLanguage("gb");
        userRegistrationDto.setStatus("worker");
        authenticationService.registerUser(userRegistrationDto);
    }

    @Test
    public void signUpWithGoodData() throws ClassNotFoundException {
        UserRegistrationDto userRegistrationDto = new UserRegistrationDto();
        userRegistrationDto.setCookiesConsent(true);
        userRegistrationDto.setGdprConsent(true);
        userRegistrationDto.setGeneralConsent(true);
        userRegistrationDto.setDeviceToken("myToken");
        userRegistrationDto.setPassword("tombRaider");
        userRegistrationDto.setPasswordRepeat("tombRaider");
        userRegistrationDto.setFirstName("Lara");
        userRegistrationDto.setLastName("Croft");
        userRegistrationDto.setPreferedCurrency("DOLLARS");
        userRegistrationDto.setIpAddress("127.0.0.1");
        userRegistrationDto.setEmail("lara_croft1@hotmail.com");
        userRegistrationDto.setPreferedLanguage("gb");
        userRegistrationDto.setStatus("worker");
        List<Integer> subStatus = new ArrayList<>();
        subStatus.add(1);
        userRegistrationDto.setSubStatus(subStatus);
        String[] accounts= {"a"};

        userRegistrationDto.setAccounts(accounts);

        Language language = new Language();
        language.setCountryCode("gb");

        Status status = new Status();
        StatusTranslation trans = new StatusTranslation();
        Mockito.when(statusTranslationRepository.findById(any(String.class))).thenReturn(java.util.Optional.of(trans));
        status.setName(trans);

        Currency currency = new Currency();
        currency.setType("DOLLARS");

        Mockito.when(currencyRepository.findByType(any(String.class))).thenReturn(java.util.Optional.of(currency));
        Mockito.when(languageRepository.findByCountryCode(any(String.class))).thenReturn(java.util.Optional.of(language));
        Mockito.when(statusRepository.findByName(new StatusTranslation())).thenReturn(java.util.Optional.of(status));

        User u = new User();
        u.setPreferedLanguage(language);
        u.setStatus(status);
        u.setDeviceToken(null);
        u.setIpAddress("123");
        u.setFirstname("Lara");
        u.setCookiesConsent(true);
        u.setGdprConsent(true);
        u.setGeneralConsent(true);
        u.setDeviceToken("myToken");
        u.setPassword("tombRaider");
        u.setLastname("Croft");
        u.setUserID(2);

        DefaultAccount defaultAccount = new DefaultAccount();
        defaultAccount.setName("a");
        AccountType accountType = new AccountType();
        accountType.setAccountTypeID(2);
        accountType.setName("e");
        defaultAccount.setType(accountType);

        Account account = new Account();
        List<CategoryStatus> categoryStatusList = new ArrayList<>();
        List<CategorySubStatus> categorySubStatusList = new ArrayList<>();
        Category c= new Category();


        SubStatus subStatus1 = new SubStatus();
        SubStatusUsers subStatusUsers = new SubStatusUsers();
        Mockito.when(   subStatusUsersRepository.save(any(SubStatusUsers.class))).thenReturn(subStatusUsers);
        Mockito.when(subStatusRepository.findById(any(Integer.class))).thenReturn(java.util.Optional.of(subStatus1));

        Mockito.when( categorySubStatusRepository.findTemplateCategoryBySubStatus(any(SubStatus.class))).thenReturn(categorySubStatusList);

        Mockito.when(defaultAccountRepository.findByName(any(String.class))).thenReturn(defaultAccount);
        Mockito.when(userRepository.save(any(User.class))).thenReturn(u);
        Mockito.when(accountRepository.save(any(Account.class))).thenReturn(account);

        Mockito.when(categoryStatusRepository.findTemplateCategoryByStatus(any(Status.class))).thenReturn(categoryStatusList);
        Mockito.when(categoryRepository.findByNameAndOwner(any(String.class),any(User.class))).thenReturn(java.util.Optional.of(c));


        /*AuthenticationResponse authenticationResponse = null;
        Mockito.when(authenticationService.registerUser(userRegistrationDto))
                .thenReturn(authenticationResponse);

        assertThat(authenticationResponse.getToken())
                .isNotEmpty();*/
    }

    @Test(expected = FatalException.class)
    public void signUpWithNoLanguage() throws ClassNotFoundException {
        UserRegistrationDto userRegistrationDto = new UserRegistrationDto();
        userRegistrationDto.setCookiesConsent(true);
        userRegistrationDto.setGdprConsent(true);
        userRegistrationDto.setGeneralConsent(true);
        userRegistrationDto.setDeviceToken("myToken");
        userRegistrationDto.setPassword("tombRaider");
        userRegistrationDto.setPasswordRepeat("tombRaider");
        userRegistrationDto.setFirstName("Lara");
        userRegistrationDto.setLastName("Croft");
        userRegistrationDto.setPreferedCurrency("DOLLARS");
        userRegistrationDto.setIpAddress("127.0.0.1");
        userRegistrationDto.setEmail("lara_@hotmail.com");
        userRegistrationDto.setPreferedLanguage("lr");
        userRegistrationDto.setStatus("worker");

        Currency currency = new Currency();
        currency.setType("DOLLARS");

        Status status = new Status();
        status.setName(new StatusTranslation());

        Mockito.when(currencyRepository.findByType(any(String.class))).thenReturn(java.util.Optional.of(currency));
        Mockito.when(languageRepository.findByCountryCode(any(String.class))).thenThrow(new FatalException());
        Mockito.when(statusRepository.findByName(new StatusTranslation())).thenReturn(java.util.Optional.of(status));
        authenticationService.registerUser(userRegistrationDto);

    }

    @Test(expected = FatalException.class)
    public void signUpWithNoCurrency() throws ClassNotFoundException {
        UserRegistrationDto userRegistrationDto = new UserRegistrationDto();
        userRegistrationDto.setCookiesConsent(true);
        userRegistrationDto.setGdprConsent(true);
        userRegistrationDto.setGeneralConsent(true);
        userRegistrationDto.setDeviceToken("myToken");
        userRegistrationDto.setPassword("tombRaider");
        userRegistrationDto.setPasswordRepeat("tombRaider");
        userRegistrationDto.setFirstName("Lara");
        userRegistrationDto.setLastName("Croft");
        userRegistrationDto.setPreferedCurrency("KUNNA");
        userRegistrationDto.setIpAddress("127.0.0.1");
        userRegistrationDto.setEmail("lara_@hotmail.com");
        userRegistrationDto.setPreferedLanguage("fr");
        userRegistrationDto.setStatus("worker");
        Language language = new Language();
        language.setCountryCode("gb");
        Status status = new Status();
        status.setName(new StatusTranslation());
        Mockito.when(currencyRepository.findByType(any(String.class))).thenThrow(new FatalException());
        Mockito.when(languageRepository.findByCountryCode(any(String.class))).thenReturn(java.util.Optional.of(language));
        Mockito.when(statusRepository.findByName(new StatusTranslation())).thenReturn(java.util.Optional.of(status));
        authenticationService.registerUser(userRegistrationDto);
    }

    @Test(expected = FatalException.class)
    public void signUpWithNoStatus() throws ClassNotFoundException {
        UserRegistrationDto userRegistrationDto = new UserRegistrationDto();
        userRegistrationDto.setCookiesConsent(true);
        userRegistrationDto.setGdprConsent(true);
        userRegistrationDto.setGeneralConsent(true);
        userRegistrationDto.setDeviceToken("myToken");
        userRegistrationDto.setPassword("tombRaider");
        userRegistrationDto.setPasswordRepeat("tombRaider");
        userRegistrationDto.setFirstName("Lara");
        userRegistrationDto.setLastName("Croft");
        userRegistrationDto.setPreferedCurrency("DOLLARS");
        userRegistrationDto.setIpAddress("127.0.0.1");
        userRegistrationDto.setEmail("lara_@hotmail.com");
        userRegistrationDto.setPreferedLanguage("gb");
        userRegistrationDto.setStatus("no");
        Language language = new Language();
        language.setCountryCode("gb");
        Currency currency = new Currency();
        currency.setType("DOLLARS");
        Mockito.when(currencyRepository.findByType(any(String.class))).thenReturn(java.util.Optional.of(currency));
        Mockito.when(languageRepository.findByCountryCode(any(String.class))).thenReturn(java.util.Optional.of(language));
        Mockito.when(statusRepository.findByName(new StatusTranslation())).thenThrow(new FatalException());
        authenticationService.registerUser(userRegistrationDto);
    }

    @Test
    public void getCurrentUserWithNoUser() {
        Authentication authentication = Mockito.mock(Authentication.class);
        SecurityContext securityContext = Mockito.mock(SecurityContext.class);
        Mockito.when(securityContext.getAuthentication()).thenReturn(authentication);
        SecurityContextHolder.setContext(securityContext);
        Mockito.when(SecurityContextHolder.getContext().getAuthentication().getPrincipal()).thenReturn(null);
        assertThat(authenticationService.getCurrentUser()).isNull();

    }


    @Test(expected = FatalException.class)
    public void createSubCategoriesEmptyList() throws ClassNotFoundException {
        authenticationService.createUserDefaultCategories(null);
    }

    @Test(expected = RegistrationException.class)
    public void changePseudoEmptyPseudo(){
        authenticationService.changePseudo("");
    }

   /* @Test
    public void changePseudoWithPseudoAlreadyExist(){
        User u = new User();
        Mockito.when(authenticationService.getCurrentUser()).thenReturn(u);
        Mockito.when(userRepository.findByPseudo(any(String.class))).thenReturn(java.util.Optional.of(new User()));
        authenticationService.changePseudo("pseudo");
    }
*/

}
