package com.mybudget.domain.services;

import com.mybudget.domain.exceptions.FatalException;
import com.mybudget.domain.exceptions.FriendshipException;
import com.mybudget.domain.models.Friendship;
import com.mybudget.domain.models.Language;
import com.mybudget.domain.models.User;
import com.mybudget.domain.models.indentities.FriendshipPK;
import com.mybudget.domain.repository.FriendshipRepository;
import com.mybudget.domain.repository.UserRepository;
import com.mybudget.domain.utils.JwtService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
import static org.mockito.ArgumentMatchers.any;

/**
 * @author Soumaya Izmar
 */
@RunWith(SpringRunner.class)
@SpringBootTest
public class FriendshipServiceTests {

    @Autowired
    private FriendshipService friendshipService;

    @MockBean
    private UserRepository userRepository;
    @MockBean
    private AuthenticationService authenticationService;
    @MockBean
    private JwtService jwtService;
    @MockBean
    private TranslationService translationService;
    @MockBean
    private FriendshipRepository friendshipRepository;

    private List<Friendship> listReceiver = new ArrayList<>();
    private User user1 = new User();
    private User user2 = new User();
    private Friendship f = new Friendship();
    ;

    @Before
    public void setUp() throws ClassNotFoundException {

        user1 = new User();
        Language l1 = new Language();
        l1.setCountryCode("fr");
        user1.setPreferedLanguage(l1);
        user1.setUserID(1);
        user1.setPseudo("user1");
        User user2 = new User();
        Language l2 = new Language();
        l2.setCountryCode("fr");
        user2.setPreferedLanguage(l2);
        user2.setUserID(2);
        user2.setPseudo("user2");


        FriendshipPK friendshipPK = new FriendshipPK();
        friendshipPK.setReceiver(user1);
        friendshipPK.setSender(user2);

        f.setFriendshipPK(friendshipPK);
        f.setAcceptDate(LocalDate.now());
        f.setRequestDate(LocalDate.now());
        f.setState(Friendship.State.ACCEPT);

        Map<String, String> translations = new HashMap<>();
        translations.put("exception", "exception");

        Mockito.when(translationService.fetchProjectTranslation(any(String.class))).thenReturn(translations);
        Mockito.when(authenticationService.getCurrentUser()).thenReturn(user1);
        Mockito.when(friendshipRepository.findByFriendshipPK_receiver(any(User.class))).thenReturn(listReceiver);
        Mockito.when(friendshipRepository.findByFriendshipPK_sender(any(User.class))).thenReturn(listReceiver);
        Mockito.when(friendshipRepository.save(any(Friendship.class))).thenReturn(f);

    }

    @Test
    public void getFriendshipByOwnerEmptyList() {

        assertThat(friendshipService.getFriendshipByOwner().getDemandList()).asList().isEmpty();
        assertThat(friendshipService.getFriendshipByOwner().getPendingList()).asList().isEmpty();
    }

    @Test
    public void getFriendshipByOwnerNoEmptyList() {

        listReceiver.add(f);
        assertThat(friendshipService.getFriendshipByOwner().getDemandList()).asList().isNotEmpty();
        assertThat(friendshipService.getFriendshipByOwner().getPendingList()).asList().isNotEmpty();
    }

    @Test(expected = FriendshipException.class)
    public void acceptFriendWrongID() throws ClassNotFoundException {

        Mockito.when(userRepository.findById(-1)).thenThrow(new FriendshipException("exception"));
        friendshipService.acceptFriendship(-1);
    }

    @Test(expected = FriendshipException.class)
    public void acceptFriendWrongRelation() throws ClassNotFoundException {

        Mockito.when(userRepository.findById(-1)).thenReturn(java.util.Optional.ofNullable(user2));
        Mockito.when(friendshipRepository.findByFriendshipPK(any(FriendshipPK.class))).thenThrow(new FriendshipException("exception"));
        friendshipService.acceptFriendship(1);
    }

    @Test
    public void acceptFriendWithGoodData() throws ClassNotFoundException {
        Mockito.when(userRepository.findById(any(Integer.class))).thenReturn(java.util.Optional.ofNullable(user2));
        Mockito.when(friendshipRepository.findByFriendshipPK(any(FriendshipPK.class))).thenReturn(java.util.Optional.ofNullable(this.f));
        assertThat(friendshipService.acceptFriendship(1)).isNotNull();
    }


    @Test(expected = FriendshipException.class)
    public void refuseFriendWithWrongID() throws ClassNotFoundException {
        Mockito.when(userRepository.findById(-1)).thenThrow(new FriendshipException("exception"));
        friendshipService.refuseFriendship(-1);
    }

    @Test(expected = FriendshipException.class)
    public void refuseFriendWrongRelation() throws ClassNotFoundException {
        Mockito.when(userRepository.findById(-1)).thenReturn(java.util.Optional.ofNullable(user2));
        Mockito.when(friendshipRepository.findByFriendshipPK(any(FriendshipPK.class))).thenThrow(new FriendshipException("exception"));
        friendshipService.refuseFriendship(1);
    }
    @Test
    public void refuseFriendWithGoodData() throws ClassNotFoundException {

        Mockito.when(userRepository.findById(any(Integer.class))).thenReturn(java.util.Optional.ofNullable(user2));
        Mockito.when(friendshipRepository.findByFriendshipPK(any(FriendshipPK.class))).thenReturn(java.util.Optional.ofNullable(this.f));
        assertThat(friendshipService.refuseFriendship(1)).isNotNull();
    }

    @Test
    public void deleteFriendGoodData() throws ClassNotFoundException {
        Mockito.when(userRepository.findById(1)).thenReturn(java.util.Optional.ofNullable(user1));
        Mockito.when(userRepository.findById(2)).thenReturn(java.util.Optional.ofNullable(user2));
        Mockito.when(friendshipRepository.findByFriendshipPK(any(FriendshipPK.class))).thenReturn(java.util.Optional.ofNullable(this.f));

        assertThat(friendshipService.deleteFriendship(1, 2)).isNotNull();
    }

    @Test(expected = FriendshipException.class)
    public void deleteFriendWrongReceiverID() throws ClassNotFoundException {
        Mockito.when(userRepository.findById(1)).thenThrow(new FriendshipException("exception"));
        assertThat(friendshipService.deleteFriendship(1, 2)).isNotNull();
    }

    @Test(expected = FriendshipException.class)
    public void deleteFriendWrongSenderID() throws ClassNotFoundException {
        Mockito.when(userRepository.findById(1)).thenReturn(java.util.Optional.ofNullable(user1));
        Mockito.when(userRepository.findById(2)).thenThrow(new FriendshipException("exception"));
        assertThat(friendshipService.deleteFriendship(1, 2)).isNotNull();
    }
    @Test(expected = FriendshipException.class)
    public void deleteFriendNoRelation() throws ClassNotFoundException {
        Mockito.when(userRepository.findById(1)).thenReturn(java.util.Optional.ofNullable(user1));
        Mockito.when(userRepository.findById(2)).thenReturn(java.util.Optional.ofNullable(user2));
        Mockito.when(friendshipRepository.findByFriendshipPK(any(FriendshipPK.class))).thenThrow(new FriendshipException("exception"));
        assertThat(friendshipService.deleteFriendship(1, 2)).isNotNull();
    }

    @Test(expected = FatalException.class)
    public void addFriendNoSocialCode() throws ClassNotFoundException {
        friendshipService.addFriendship(null);
    }
    @Test(expected = FriendshipException.class)
    public void addFriendWrongSocialCode() throws ClassNotFoundException {
        Mockito.when(userRepository.findBySocialCode(any(String.class))).thenThrow(new FriendshipException("exception"));
        friendshipService.addFriendship("pseudo");
    }

    @Test(expected = FriendshipException.class)
    public void addFriendSameUser() throws ClassNotFoundException {
        Mockito.when(userRepository.findBySocialCode(any(String.class))).thenReturn(java.util.Optional.ofNullable(user1));
        Mockito.when(authenticationService.getCurrentUser()).thenReturn(user1);
        friendshipService.addFriendship("pseudo");
    }

    @Test(expected = FriendshipException.class)
    public void addFriendRelationAlreadyExist() throws ClassNotFoundException {

        Mockito.when(userRepository.findBySocialCode(any(String.class))).thenReturn(java.util.Optional.ofNullable(user1));
        Mockito.when(authenticationService.getCurrentUser()).thenReturn(user1);

        FriendshipPK pk = new FriendshipPK();
        pk.setSender(user1);
        pk.setReceiver(user2);

        Mockito.when(friendshipRepository.findByFriendshipPK(pk)).thenReturn(java.util.Optional.ofNullable(f));

        friendshipService.addFriendship("pseudo");
    }

    @Test(expected = FriendshipException.class)
    public void addFriendRelationUnexpectedError() throws ClassNotFoundException {

        Mockito.when(userRepository.findBySocialCode(any(String.class))).thenReturn(java.util.Optional.ofNullable(user1));

        Mockito.when(authenticationService.getCurrentUser()).thenReturn(user1);

        FriendshipPK pk = new FriendshipPK();
        pk.setSender(user1);
        pk.setReceiver(user2);
        f.setState(Friendship.State.REJECT);
        Mockito.when( friendshipRepository.findByFriendshipPK(pk)).thenReturn(java.util.Optional.ofNullable(f));

        friendshipService.addFriendship("pseudo");
    }

    @Test(expected = FriendshipException.class)
    public void addFriendRelationWithGoodData() throws ClassNotFoundException {
        Mockito.when(userRepository.findBySocialCode(any(String.class))).thenReturn(java.util.Optional.ofNullable(user1));

        Mockito.when(authenticationService.getCurrentUser()).thenReturn(user1);

        FriendshipPK pk = new FriendshipPK();
        pk.setSender(user1);
        pk.setReceiver(user2);

        Mockito.when( friendshipRepository.findByFriendshipPK(pk)).thenReturn(null);

        friendshipService.addFriendship("pseudo");
    }

}
