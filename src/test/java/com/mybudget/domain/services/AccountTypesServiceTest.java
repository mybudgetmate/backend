package com.mybudget.domain.services;

import com.mybudget.domain.dto.AccountTypesSerializedDto;
import com.mybudget.domain.models.AccountType;
import com.mybudget.domain.repository.AccountTypesRepository;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.ArrayList;
import java.util.List;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;

/**
 * @author Eva Tumia
 */
@RunWith(SpringRunner.class)
@SpringBootTest
public class AccountTypesServiceTest {

    @MockBean
    private AccountTypesService accountTypesService;

    @MockBean
    private AccountTypesRepository accountTypesRepository;

    @Before
    public void setUp(){
        List<AccountType> list = new ArrayList<AccountType>();
        list.add(new AccountType());

        Mockito.when(accountTypesRepository.findAll()).thenReturn(list);
    }

    @Test
    public void getAll(){
        assertThat(accountTypesService.getAll()).isNotNull();
    }
}
