package com.mybudget.domain.services;

import com.mybudget.domain.dto.ProjectDto;
import com.mybudget.domain.exceptions.ProjectException;
import com.mybudget.domain.models.Image;
import com.mybudget.domain.models.Language;
import com.mybudget.domain.models.Project;
import com.mybudget.domain.models.User;
import com.mybudget.domain.repository.ImageRepository;
import com.mybudget.domain.repository.ProjectRepository;
import com.mybudget.domain.repository.UserRepository;
import com.mybudget.domain.utils.JwtService;
import org.junit.Before;
import org.junit.Test;
import org.junit.jupiter.api.DisplayName;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.*;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
import static org.mockito.ArgumentMatchers.any;

@RunWith(SpringRunner.class)
@SpringBootTest
public class ProjectServiceTests {

    @Autowired
    private ProjectService projectService;
    @MockBean
    private ProjectRepository projectRepository;
    @MockBean
    private UserRepository userRepository;
    @MockBean
    private ImageRepository imageRepository;
    @MockBean
    private AuthenticationService authenticationService;
    @MockBean
    private JwtService jwtService;
    @MockBean
    private TranslationService translationService;


    @Before
    public void setUp() throws ClassNotFoundException {

        Project p = new Project();
        p.setObtainedBudget(23);
        p.setParentProject(null);
        p.setOwner(new User());
        p.setRequiredBudget(5000);
        Image image = new Image();
        image.setName("car_123522471.jpeg");
        p.setImage(image);
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
        LocalDate endDate = LocalDate.parse("2040-06-20", formatter);
        p.setEndDate(endDate);
        p.setStartDate(LocalDate.now());
        p.setName("p1");

        List<Project> list = new ArrayList<>();
        list.add(p);

        Mockito.when(projectRepository.findById(2)).thenReturn(java.util.Optional.of(p));
        User user = new User();
        Language l = new Language();
        l.setCountryCode("fr");
        user.setPreferedLanguage(l);
        Mockito.when(authenticationService.getCurrentUser()).thenReturn(user);

        Map<String, String> map = new HashMap<>();
        map.put("deadlineError", "deadline error");
        Mockito.when(translationService.fetchProjectTranslation("fr"))
                .thenReturn(map);
        Iterable<Project> iterable = list;
        Mockito.when(projectRepository.findByParentProject(any(Project.class))).thenReturn(list);


        Mockito.when(projectRepository.save(any(Project.class))).thenReturn(p);
    }

    @Test(expected = ProjectException.class)
    public void addProjectWithEndDateBeforeStartDate() throws ClassNotFoundException {
        ProjectDto projectDto = new ProjectDto();
        projectDto.setEndDate("2010-06-02");
        projectDto.setParentProject(2);
        projectDto.setName("sdf");
        projectDto.setRequiredBudget(2133);
        projectService.addNewProject(projectDto);
    }

    @Test(expected = ProjectException.class)
    public void addProjectWithParentDateBeforeChildDate() throws ClassNotFoundException {
        ProjectDto projectDto = new ProjectDto();
        projectDto.setEndDate("2050-06-02");
        projectDto.setParentProject(2);
        projectDto.setName("sdf");
        projectDto.setRequiredBudget(2133);
        projectService.addNewProject(projectDto);

    }

    @Test
    public void addProjectGoodData() throws ClassNotFoundException {
        ProjectDto projectDto = new ProjectDto();
        projectDto.setEndDate("2030-06-02");
        projectDto.setParentProject(2);
        projectDto.setName("sdf");
        projectDto.setRequiredBudget(2133);
        projectDto.setImage("adoption_204898512.jpeg");
        Image image = new Image();
        image.setName("adoption_204898512.jpeg");

        Mockito.when(imageRepository.findByName(projectDto.getImage())).thenReturn(java.util.Optional.of(image));
        projectService.addNewProject(projectDto);
        assertThat(projectService.addNewProject(projectDto)).isNotNull();

        ProjectDto projectDto2 = new ProjectDto();
        projectDto2.setEndDate("2030-06-02");
        projectDto2.setParentProject(2);
        projectDto2.setName("sdf");
        projectDto2.setRequiredBudget(50);
        projectDto2.setImage("adoption_204898512.jpeg");
        Image image2 = new Image();
        image2.setName("adoption_204898512.jpeg");

        Mockito.when(imageRepository.findByName(projectDto2.getImage())).thenReturn(java.util.Optional.of(image));
        projectService.addNewProject(projectDto2);
        assertThat(projectService.addNewProject(projectDto2)).isNotNull();
    }

    @Test(expected = ProjectException.class)
    public void addProjectWithNoImage() throws ClassNotFoundException {
        ProjectDto projectDto = new ProjectDto();
        projectDto.setEndDate("2030-06-02");
        projectDto.setParentProject(2);
        projectDto.setName("sdf");
        projectDto.setRequiredBudget(2133);
        projectDto.setImage("car_123522471.jpeg");
        projectService.addNewProject(projectDto);
    }

    @Test(expected = ProjectException.class)
    public void updateProjectWrongProjectID() throws ClassNotFoundException {
        ProjectDto projectDto = new ProjectDto();
        projectDto.setEndDate("2030-06-02");
        projectDto.setParentProject(-1);
        projectDto.setName("sdf");
        projectDto.setRequiredBudget(2133);
        projectDto.setImage("car_123522471.jpeg");
        Image image = new Image();
        image.setName("car_123522471.jpeg");

        Mockito.when(imageRepository.findByName(projectDto.getImage())).thenReturn(java.util.Optional.of(image));
        Mockito.when(projectRepository.findById(-1)).thenThrow(new ProjectException("Project not found"));
        projectService.updateProject(projectDto);
    }

    @Test(expected = ProjectException.class)
    @DisplayName("requiredBudget too high")
    public void updateProjectWrongDataRequired() throws ClassNotFoundException {
        ProjectDto projectDto = new ProjectDto();
        projectDto.setEndDate("2030-06-02");
        projectDto.setParentProject(2);
        projectDto.setName("sdf");
        projectDto.setRequiredBudget(5200);
        projectDto.setImage("car_123522471.jpeg");
        Image image = new Image();
        image.setName("car_123522471.jpeg");
        Mockito.when(imageRepository.findByName(projectDto.getImage())).thenReturn(java.util.Optional.of(image));
        assertThat(projectService.updateProject(projectDto)).isNotNull();

    }


    @Test(expected = ProjectException.class)
    @DisplayName("obtainedBudget too high")
    public void updateProjectWrongDataObtained() throws ClassNotFoundException {
        ProjectDto projectDto = new ProjectDto();
        projectDto.setEndDate("2030-06-02");
        projectDto.setParentProject(2);
        projectDto.setName("sdf");
        projectDto.setObtainedBudget(10000);
        projectDto.setImage("car_123522471.jpeg");
        Image image = new Image();
        image.setName("car_123522471.jpeg");
        Mockito.when(imageRepository.findByName(projectDto.getImage())).thenReturn(java.util.Optional.of(image));
        assertThat(projectService.updateProject(projectDto)).isNotNull();

    }

    @Test(expected = ProjectException.class)
    @DisplayName("obtainedBudget >requiredBudget")
    public void updateProjectWrongDataObtainedBis() throws ClassNotFoundException {
        ProjectDto projectDto = new ProjectDto();
        projectDto.setEndDate("2030-06-02");
        projectDto.setParentProject(2);
        projectDto.setName("sdf");
        projectDto.setRequiredBudget(250);
        projectDto.setObtainedBudget(500);
        projectDto.setImage("car_123522471.jpeg");
        Image image = new Image();
        image.setName("car_123522471.jpeg");
        Mockito.when(imageRepository.findByName(projectDto.getImage())).thenReturn(java.util.Optional.of(image));
        assertThat(projectService.updateProject(projectDto)).isNotNull();

    }



    public void updateProjectGoodData() throws ClassNotFoundException {
        ProjectDto projectDto = new ProjectDto();
        projectDto.setEndDate("2030-06-02");
        projectDto.setParentProject(2);
        projectDto.setName("sdf");
        projectDto.setRequiredBudget(2133);
        projectDto.setImage("car_123522471.jpeg");
        Image image = new Image();
        image.setName("car_123522471.jpeg");
        Mockito.when(imageRepository.findByName(projectDto.getImage())).thenReturn(java.util.Optional.of(image));
        assertThat(projectService.updateProject(projectDto)).isNotNull();

    }

    @Test(expected = ProjectException.class)
    public void deleteProjectWrongProjectID() throws ClassNotFoundException {
        Mockito.when(projectRepository.findById(-1)).thenThrow(new ProjectException("Project not found"));
        projectService.deleteProject(-1);
    }

    @Test
    public void noEmptyListProject() {
        List<ProjectDto> list = projectService.fetchAllProject();
        assertThat(list).isNotNull();
    }

    @Test
    public void noEmptyListSubProject() {
        Image image = new Image();
        image.setName("car_123522471.jpeg");
        Mockito.when(imageRepository.findByName("car_123522471.jpeg")).thenReturn(java.util.Optional.of(image));
        List<ProjectDto> list = projectService.fetchSubProject(2);
        assertThat(list).isNotNull();
    }

    @Test
    public void noEmptyListOneProject() throws ClassNotFoundException {
        Image image = new Image();
        image.setName("car_123522471.jpeg");
        Mockito.when(imageRepository.findByName("car_123522471.jpeg")).thenReturn(java.util.Optional.of(image));
        ProjectDto projectDto = projectService.fetchOneProject(2);
        assertThat(projectDto).isNotNull();
    }


    @Test(expected = ProjectException.class)
    public void noEmptyListOneProjectWithException() throws ClassNotFoundException {
        Mockito.when(projectRepository.findById(-1)).thenThrow(new ProjectException("Project not found"));
        ProjectDto projectDto = projectService.fetchOneProject(-1);
    }



}
