package com.mybudget.domain.services;

import com.mybudget.domain.dto.response.CurrencyResponse;
import com.mybudget.domain.models.Currency;
import com.mybudget.domain.models.Icon;
import com.mybudget.domain.models.User;
import com.mybudget.domain.repository.CurrencyRepository;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.ArrayList;
import java.util.List;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;

@RunWith(SpringRunner.class)
@SpringBootTest
public class CurrencyServiceTests {


    @Autowired
    private CurrencyService currencyService;

    @MockBean
    private CurrencyRepository currencyRepository;

    @MockBean
    private AuthenticationService authenticationService;

    @Before
    public void setUp() {
        List<Currency> listMock = new ArrayList<>();
        Currency newCurrency = new Currency();
        newCurrency.setIcon(new Icon());
        newCurrency.setType("FONTAWESOME");
        listMock.add(newCurrency);
        listMock.add(newCurrency);
        Mockito.when(currencyRepository.findAll())
                .thenReturn(listMock);
    }

    @Test
    public void noEmptyListCurrency() {
        List<CurrencyResponse> list = currencyService.fetchAllCurrencies();
        assertThat(list).isNotNull();

    }

    @Test
    public void fetchPreferedCurrencyUser(){
        Currency c = new Currency();
        c.setIcon(new Icon());
        User user = new User();
        user.setPreferedCurrency(c);

        Mockito.when(authenticationService.getCurrentUser()).thenReturn(user);
        assertThat(currencyService.fetchPreferedCurrencyUser()).isNotNull();
    }


}
