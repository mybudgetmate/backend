package com.mybudget.domain.services;

import com.mybudget.domain.dto.AccountDto;
import com.mybudget.domain.exceptions.AccountTypeException;
import com.mybudget.domain.models.*;
import com.mybudget.domain.repository.AccountRepository;
import com.mybudget.domain.repository.AccountTypesRepository;
import com.mybudget.domain.repository.DefaultAccountRepository;
import com.mybudget.domain.utils.JwtService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.ArrayList;
import java.util.List;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
import static org.mockito.ArgumentMatchers.any;

/**
 * @author Eva Tumia
 */
@RunWith(SpringRunner.class)
@SpringBootTest
public class AccountServiceTest {

    @Autowired
    private AccountService accountService;
    @Autowired
    private JwtService jwtService;
    @MockBean
    AuthenticationService authenticationService;
    @MockBean
    AccountRepository accountRepository;
    @MockBean
    AccountTypesRepository accountTypesRepository;
    @MockBean
    DefaultAccountRepository defaultAccountRepository;

    @Before
    public void setUp(){
        User user = new User();
        user.setUserID(1);


        AccountType accType = new AccountType();
        accType.setIcon(new Icon());
        accType.setAccountTypeID(1);
        Account acc = new Account();
        acc.setName("Acc");
        acc.setOwner(user);
        acc.setType(accType);
        acc.setAccountId(1);


        DefaultAccount defaultAccount = new DefaultAccount();
        defaultAccount.setName("default account");
        defaultAccount.setType(accType);
        defaultAccount.setDefaultAccountId(1);


        List<DefaultAccount> defaultAccounts = new ArrayList<>();
        defaultAccounts.add(defaultAccount);

        List<Account> accounts = new ArrayList<>();
        accounts.add(acc);

        AccountType accountType = new AccountType();
        accountType.setAccountTypeID(1);


        Mockito.when(authenticationService.getCurrentUser()).thenReturn(user);
        Mockito.when(accountRepository.findAllByOwner(user)).thenReturn(accounts);
        Mockito.when(accountTypesRepository.findById(1)).thenReturn(java.util.Optional.of(accountType));
        Mockito.when(accountRepository.save(any(Account.class))).thenReturn(acc);
        Mockito.when(defaultAccountRepository.findAll()).thenReturn(defaultAccounts);
    }

    @Test
    public void getAll(){
        assertThat(accountService.getAll()).isNotNull();
    }

    @Test
    public void getAllDefaultAccount(){
        assertThat(accountService.getAllDefaultAccount()).isNotNull();
    }

    @Test(expected = AccountTypeException.class)
    public void addAccountWithInexistentAccountType(){
        AccountDto accountDto = new AccountDto();
        accountDto.setName("Paypal");
        accountDto.setOwner(1);
        accountDto.setTypeID(5);
        accountService.addAccount(accountDto);
    }

    @Test
    public void addAccountWithGoodData(){
        AccountDto accountDto = new AccountDto();
        accountDto.setName("Paypal");
        accountDto.setOwner(1);
        accountDto.setTypeID(1);
        assertThat(accountService.addAccount(accountDto)).isNotNull();
    }


}
