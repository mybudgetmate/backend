package com.mybudget.domain.services;

import com.mybudget.domain.dto.InvoiceDto;
import com.mybudget.domain.models.Icon;
import com.mybudget.domain.models.Invoice;
import com.mybudget.domain.models.User;
import com.mybudget.domain.repository.IconRepository;
import com.mybudget.domain.repository.InvoiceRepository;
import com.mybudget.domain.utils.JwtService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
import static org.mockito.ArgumentMatchers.any;

/**
 * @author Eva Tumia
 */
@RunWith(SpringRunner.class)
@SpringBootTest
public class InvoiceServiceTests {

    @Autowired
    private InvoiceService invoiceService;
    @Autowired
    private JwtService jwtService;
    @MockBean
    private FirebaseService firebaseService;
    @MockBean
    private IconRepository iconRepository;
    @MockBean
    private AuthenticationService authenticationService;
    @MockBean
    private InvoiceRepository invoiceRepository;

    @Before
    public void setUp() throws ClassNotFoundException {
        Icon icon = new Icon();
        icon.setIconID(1);
        icon.setIconCode("fa fa-angry");
        icon.setType(Icon.TYPE.FONTAWESOME);

        User user = new User();
        user.setUserID(1);
        user.setFirstname("Eva");
        user.setLastname("Pitt");
        user.setDeviceToken("1234567789SF");

        Invoice invoice = new Invoice();
        invoice.setInvoiceID(1);
        invoice.setName("Proximax");
        invoice.setAmount(30.0);
        invoice.setStartDate(LocalDateTime.now().minusDays(4));
        invoice.setEndDate(LocalDateTime.now().plusDays(30));
        invoice.setIcon(icon);

        List<Invoice> invoices = new ArrayList<Invoice>();
        invoices.add(invoice);

        Mockito.when(iconRepository.existsByIconCode(any(String.class))).thenReturn(false);
        Mockito.when(iconRepository.findByIconCode(any(String.class))).thenReturn(icon);
        Mockito.when(authenticationService.getCurrentUser()).thenReturn(user);
        Mockito.when(invoiceRepository.save(any(Invoice.class))).thenReturn(invoice);
        Mockito.when(invoiceRepository.findByOwner(any(User.class))).thenReturn(invoices);
        Mockito.when(invoiceRepository.findAll()).thenReturn(invoices);
    }

    @Test
    public void addNewInvoiceWithGoodData() {
        InvoiceDto invoiceDto = new InvoiceDto(
                1,
                "VOO",
                30.0,
                "2010-11-09",
                "2010-11-10",
                "fa fa-angry"
        );

        assertThat(invoiceService.addNewInvoice(invoiceDto)).isNotNull();
    }

    @Test
    public void getAllByOwnerCorrect() {
        assertThat(invoiceService.getAllByOwner()).isNotNull();
    }


    @Test
    public void sendPaymentReminderNotificationCorrect(){
        try {
            invoiceService.sendPaymentReminderNotification();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
    }
}