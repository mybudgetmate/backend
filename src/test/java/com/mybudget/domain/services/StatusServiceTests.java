package com.mybudget.domain.services;

import com.mybudget.domain.models.Status;
import com.mybudget.domain.models.translation.StatusTranslation;
import com.mybudget.domain.repository.StatusRepository;
import com.mybudget.domain.repository.StatusTranslationRepository;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;

@RunWith(SpringRunner.class)
@SpringBootTest
public class StatusServiceTests {

    @Autowired
    private StatusService statusService;

    @MockBean
    private StatusRepository statusRepository;

    @MockBean
    private StatusTranslationRepository statusTranslationRepository;

    @Before
    public void setUp() {
        List<Status> listMock = new ArrayList<>();
        Status s = new Status();
        StatusTranslation trans = new StatusTranslation();
        s.setName(trans);
        listMock.add(s);
        Mockito.when(statusRepository.findAll())
                .thenReturn(listMock);
    }

    @Test
    public void noEmptyListStatus() {
        Map<String, String> list = statusService.fetchAllStatus("fr");
        assertThat(list).isNotNull();
    }
}
