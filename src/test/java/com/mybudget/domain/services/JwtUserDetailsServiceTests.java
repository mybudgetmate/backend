package com.mybudget.domain.services;

import com.mybudget.domain.models.User;
import com.mybudget.domain.repository.UserRepository;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
import static org.mockito.ArgumentMatchers.any;

@RunWith(SpringRunner.class)
@SpringBootTest
public class JwtUserDetailsServiceTests {

    @Autowired
    private JwtUserDetailsService jwtUserDetailsService;
    @MockBean
    private UserRepository userRepository;

    @Test
    public void testLoadUserByUsernameEmailNotFound(){
        assertThat(jwtUserDetailsService.loadUserByUsername("email")).isNull();
    }

    @Test
    public void testLoadUserByUsernameEmailFound(){
        User user = new User();
        user.setEmail("email");
        Mockito.when(userRepository.findByEmail(any(String.class))).thenReturn(java.util.Optional.of(user));
        assertThat(jwtUserDetailsService.loadUserByUsername("email")).isNotNull();
    }
}
