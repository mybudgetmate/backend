package com.mybudget.domain.controllers;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.mybudget.domain.dto.AccountTypesSerializedDto;
import com.mybudget.domain.exceptions.FatalException;
import com.mybudget.domain.services.AccountTypesService;
import com.mybudget.domain.services.JwtUserDetailsService;
import com.mybudget.domain.utils.JwtService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;

import java.util.ArrayList;

import static org.mockito.BDDMockito.given;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@WebMvcTest(AccountTypesController.class)
@AutoConfigureMockMvc(secure = false)
public class AccountTypesControllerTests {

    @Autowired
    private MockMvc mvc;

    @MockBean
    private AccountTypesService accountTypesService;
    @MockBean
    private JwtService jwtService;
    @MockBean
    private JwtUserDetailsService jwtUserDetailsService;

    @Test
    public void getAll() throws Exception {
        given(accountTypesService.getAll()).willReturn(new ArrayList<AccountTypesSerializedDto>());

        MvcResult test = mvc.perform(get("/api/accountTypes/all"))
                .andExpect(status().isOk())
                .andExpect(mvcResult -> asJsonString(new ArrayList<AccountTypesSerializedDto>()))
                .andReturn();
    }

    @Test
    public void getAllWithUnexpextedError() throws Exception {
        given(accountTypesService.getAll()).willThrow(new FatalException());

        MvcResult test = mvc.perform(get("/api/accountTypes/all"))
                .andExpect(status().is5xxServerError())
                .andReturn();
    }

    public String asJsonString(final Object obj) {
        try {
            return new ObjectMapper().writeValueAsString(obj);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }
}
