package com.mybudget.domain.controllers;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.mybudget.domain.dto.LanguageDto;
import com.mybudget.domain.exceptions.FatalException;
import com.mybudget.domain.services.JwtUserDetailsService;
import com.mybudget.domain.services.TranslationService;
import com.mybudget.domain.utils.JwtService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.HashMap;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.BDDMockito.given;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

/**
 * @author Soumaya Izmar
 */
@RunWith(SpringRunner.class)
@WebMvcTest(TranslationController.class)
@AutoConfigureMockMvc(secure = false)
public class TranslationControllerTests {


    @Autowired
    private MockMvc mvc;
    @MockBean
    private TranslationService translationService;
    @MockBean
    private JwtService jwtService;
    @MockBean
    private JwtUserDetailsService jwtUserDetailsService;

    @Before
    public void setUp() throws ClassNotFoundException {

        given(translationService.fetchCategoriesManagementTranslation(any(String.class))).willReturn(new HashMap<>());
        given(translationService.fetchLoginTranslation(any(String.class))).willReturn(new HashMap<>());
        given(translationService.fetchSignUpTranslation(any(String.class))).willReturn(new HashMap<>());
        given(translationService.fetchAddTransactionTranslation(any(String.class))).willReturn(new HashMap<>());
        given(translationService.fetchBudgetTranslation(any(String.class))).willReturn(new HashMap<>());
        given(translationService.fetchCategoryTemplateTranslation(any(String.class))).willReturn(new HashMap<>());
        given(translationService.fetchAllTransactionsTranslation(any(String.class))).willReturn(new HashMap<>());
        given(translationService.fetchFriendshipTranslation(any(String.class))).willReturn(new HashMap<>());
        given(translationService.fetchGlobalTranslation(any(String.class))).willReturn(new HashMap<>());
        given(translationService.fetchHomepageTranslation(any(String.class))).willReturn(new HashMap<>());
        given(translationService.fetchProjectTranslation(any(String.class))).willReturn(new HashMap<>());
        given(translationService.getRecurringPaiementsTranslation(any(String.class))).willReturn(new HashMap<>());
        given(translationService.fetchStatusTranslations(any(String.class))).willReturn(new HashMap<>());
        given(translationService.fetchSettingsPageTranslations(any(String.class))).willReturn(new HashMap<>());

        given(translationService.getShoppingTranslation()).willReturn("shopping");
    }

    @Test
    public void testFetchTranslation() throws Exception {


        mvc.perform(get("/api/translation/categoriesManagementTranslation?languageCode=fr")).andExpect(status().isOk()).andExpect(mvcResult -> new ArrayList<>());
        mvc.perform(get("/api/translation/projectTranslation/?languageCode=fr")).andExpect(status().isOk()).andExpect(mvcResult -> new ArrayList<>());
        mvc.perform(get("/api/translation/budgetTranslation/?languageCode=fr")).andExpect(status().isOk()).andExpect(mvcResult -> new ArrayList<>());
        mvc.perform(get("/api/translation/addTransactionTranslation/?languageCode=fr")).andExpect(status().isOk()).andExpect(mvcResult -> new ArrayList<>());
        mvc.perform(get("/api/translation/monthsTranslation/?languageCode=fr")).andExpect(status().isOk()).andExpect(mvcResult -> new ArrayList<>());
        mvc.perform(get("/api/translation/homepageTranslation/?languageCode=fr")).andExpect(status().isOk()).andExpect(mvcResult -> new ArrayList<>());
        mvc.perform(get("/api/translation/friendshipTranslation/?languageCode=fr")).andExpect(status().isOk()).andExpect(mvcResult -> new ArrayList<>());
        mvc.perform(get("/api/translation/shopping/?languageCode=fr")).andExpect(status().isOk()).andExpect(mvcResult -> new ArrayList<>());
        mvc.perform(get("/api/translation/recurringPaiementsTranslation/?languageCode=fr")).andExpect(status().isOk()).andExpect(mvcResult -> new ArrayList<>());
        mvc.perform(get("/api/translation/globalTranslation/?languageCode=fr")).andExpect(status().isOk()).andExpect(mvcResult -> new ArrayList<>());
        mvc.perform(get("/api/translation/allTransactionsTranslation/?languageCode=fr")).andExpect(status().isOk()).andExpect(mvcResult -> new ArrayList<>());
        mvc.perform(get("/api/translation/allTransactionsTranslation/?languageCode=fr")).andExpect(status().isOk()).andExpect(mvcResult -> new ArrayList<>());
        mvc.perform(get("/api/translation/settingsPageTranslation/?languageCode=fr")).andExpect(status().isOk()).andExpect(mvcResult -> new ArrayList<>());
        mvc.perform(get("/api/translation/statusTranslation/?languageCode=fr")).andExpect(status().isOk()).andExpect(mvcResult -> new ArrayList<>());
    }

    @Test
    public void testFetchTranslationWithUnexpectedError() throws Exception {

        given(translationService.fetchCategoriesManagementTranslation(any(String.class))).willThrow(new FatalException());
        given(translationService.fetchAddTransactionTranslation(any(String.class))).willThrow(new FatalException());
        given(translationService.fetchBudgetTranslation(any(String.class))).willThrow(new FatalException());
        given(translationService.fetchCategoryTemplateTranslation(any(String.class))).willThrow(new FatalException());
        given(translationService.fetchAllTransactionsTranslation(any(String.class))).willThrow(new FatalException());
        given(translationService.fetchFriendshipTranslation(any(String.class))).willThrow(new FatalException());
        given(translationService.fetchGlobalTranslation(any(String.class))).willThrow(new FatalException());
        given(translationService.fetchHomepageTranslation(any(String.class))).willThrow(new FatalException());
        given(translationService.fetchProjectTranslation(any(String.class))).willThrow(new FatalException());
        given(translationService.getRecurringPaiementsTranslation(any(String.class))).willThrow(new FatalException());
        given(translationService.getRecurringPaiementsTranslation(any(String.class))).willThrow(new FatalException());
        given(translationService.fetchMonthsTranslation(any(String.class))).willThrow(new FatalException());
        given(translationService.getShoppingTranslation()).willThrow(new FatalException());
        given(translationService.fetchSettingsPageTranslations(any(String.class))).willThrow(new FatalException());
        given(translationService.fetchStatusTranslations(any(String.class))).willThrow(new FatalException());

        mvc.perform(get("/api/translation/monthsTranslation/?languageCode=fr")).andExpect(status().is5xxServerError());
        mvc.perform(get("/api/translation/addTransactionTranslation/?languageCode=fr")).andExpect(status().is5xxServerError());
        mvc.perform(get("/api/translation/categoriesManagementTranslation/?languageCode=fr")).andExpect(status().is5xxServerError());
        mvc.perform(get("/api/translation/projectTranslation/?languageCode=fr")).andExpect(status().is5xxServerError());
        mvc.perform(get("/api/translation/budgetTranslation/?languageCode=fr")).andExpect(status().is5xxServerError());
        mvc.perform(get("/api/translation/allTransactionsTranslation/?languageCode=fr")).andExpect(status().is5xxServerError());
        mvc.perform(get("/api/translation/homepageTranslation/?languageCode=fr")).andExpect(status().is5xxServerError());
        mvc.perform(get("/api/translation/friendshipTranslation/?languageCode=fr")).andExpect(status().is5xxServerError());
        mvc.perform(get("/api/translation/shopping/?languageCode=fr")).andExpect(status().is5xxServerError());
        mvc.perform(get("/api/translation/recurringPaiementsTranslation/?languageCode=fr")).andExpect(status().is5xxServerError());
        mvc.perform(get("/api/translation/globalTranslation/?languageCode=fr")).andExpect(status().is5xxServerError());
        mvc.perform(get("/api/translation/settingsPageTranslation/?languageCode=fr")).andExpect(status().is5xxServerError());
        mvc.perform(get("/api/translation/statusTranslation/?languageCode=fr")).andExpect(status().is5xxServerError());

    }

    @Test
    public void testTranslationWithException() throws Exception {
        MediaType MEDIA_TYPE_JSON_UTF8 = new MediaType("application", "json", StandardCharsets.UTF_8);
        given(translationService.fetchLoginTranslation(any(String.class))).willThrow(new FatalException());
        given(translationService.fetchSignUpTranslation(any(String.class))).willThrow(new FatalException());


        LanguageDto languageDto = new LanguageDto();
        languageDto.setLanguageCode("fr");
        mvc.perform(post("/api/translation/loginTranslation").
                accept(MEDIA_TYPE_JSON_UTF8).
                contentType(MEDIA_TYPE_JSON_UTF8).
                content(asJsonString(languageDto))).andExpect(status().is5xxServerError());

        mvc.perform(post("/api/translation/signUpTranslation").
                accept(MEDIA_TYPE_JSON_UTF8).
                contentType(MEDIA_TYPE_JSON_UTF8).
                content(asJsonString(languageDto))).andExpect(status().is5xxServerError());
    }

    @Test
    public void testAddTranslationWithGoodData() throws Exception {
        MediaType MEDIA_TYPE_JSON_UTF8 = new MediaType("application", "json", StandardCharsets.UTF_8);

        LanguageDto languageDto = new LanguageDto();
        languageDto.setLanguageCode("fr");

        mvc.perform(post("/api/translation/loginTranslation").
                accept(MEDIA_TYPE_JSON_UTF8).
                contentType(MEDIA_TYPE_JSON_UTF8).
                content(asJsonString(languageDto))).andExpect(status().isOk()).andExpect(mvcResult -> asJsonString(languageDto));

        mvc.perform(post("/api/translation/signUpTranslation").
                accept(MEDIA_TYPE_JSON_UTF8).
                contentType(MEDIA_TYPE_JSON_UTF8).
                content(asJsonString(languageDto))).andExpect(status().isOk()).andExpect(mvcResult -> asJsonString(languageDto));

    }

    public String asJsonString(final Object obj) {
        try {
            return new ObjectMapper().writeValueAsString(obj);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

}
