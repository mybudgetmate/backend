package com.mybudget.domain.controllers;


import com.fasterxml.jackson.databind.ObjectMapper;
import com.mybudget.domain.exceptions.FatalException;
import com.mybudget.domain.services.JwtUserDetailsService;
import com.mybudget.domain.services.LanguageService;
import com.mybudget.domain.utils.JwtService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import java.nio.charset.StandardCharsets;
import java.util.ArrayList;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.BDDMockito.given;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.patch;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@WebMvcTest(LanguageController.class)
@AutoConfigureMockMvc(secure = false)
public class LanguageControllerTests {

    @Autowired
    private MockMvc mvc;

    @MockBean
    private LanguageService languageService;
    @MockBean
    private JwtService jwtService;
    @MockBean
    private JwtUserDetailsService jwtUserDetailsService;


    @Test
    public void testFetchAllLanguage() throws Exception {
        given(languageService.fetchAllLanguages()).willReturn(new ArrayList<>());
        mvc.perform(get("/api/language/")).andExpect(status().isOk()).andExpect(mvcResult -> new ArrayList<>());

    }

    @Test
    public void testFetchAllLanguageWithUnexpectedError() throws Exception {
        given(languageService.fetchAllLanguages()).willThrow(new FatalException());
        mvc.perform(get("/api/language/")).andExpect(status().is5xxServerError());

    }

    @Test
    public void testChangeUserLanguageGoodData() throws Exception {
        given(languageService.changeUserLanguage(any(String.class))).willReturn("");
        MediaType MEDIA_TYPE_JSON_UTF8 = new MediaType("application", "json", StandardCharsets.UTF_8);
        mvc.perform(patch("/api/language/")
                .accept(MEDIA_TYPE_JSON_UTF8).
                        contentType(MEDIA_TYPE_JSON_UTF8).
                        content(asJsonString("fr")))
                .andExpect(status().isOk()).andExpect(mvcResult -> new String("fr"));

    }

    @Test
    public void testChangeUserLanguageWithUnexpectedError() throws Exception {
        given(languageService.changeUserLanguage(any(String.class))).willThrow(new FatalException());
        MediaType MEDIA_TYPE_JSON_UTF8 = new MediaType("application", "json", StandardCharsets.UTF_8);
        mvc.perform(patch("/api/language/").accept(MEDIA_TYPE_JSON_UTF8).
                contentType(MEDIA_TYPE_JSON_UTF8).
                content(asJsonString("fr"))).andExpect(status().is5xxServerError());

    }

    public String asJsonString(final Object obj) {
        try {
            return new ObjectMapper().writeValueAsString(obj);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }


}
