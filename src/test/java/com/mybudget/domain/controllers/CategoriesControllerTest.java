package com.mybudget.domain.controllers;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.mybudget.domain.dto.CategoryDto;
import com.mybudget.domain.dto.CategorySerializedDto;
import com.mybudget.domain.dto.response.CategoryTransactionResponse;
import com.mybudget.domain.dto.response.ObjectResponse;
import com.mybudget.domain.exceptions.CategoriesException;
import com.mybudget.domain.exceptions.FatalException;
import com.mybudget.domain.services.CategoriesService;
import com.mybudget.domain.services.JwtUserDetailsService;
import com.mybudget.domain.utils.JwtService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;

import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.BDDMockito.given;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

/**
 * @author Eva Tumia
 */

@RunWith(SpringRunner.class)
@WebMvcTest(CategoriesController.class)
@AutoConfigureMockMvc(secure = false)
public class CategoriesControllerTest {

    @Autowired
    private MockMvc mvc;
    @MockBean
    private CategoriesService categoriesService;
    @MockBean
    private JwtService jwtService;
    @MockBean
    private JwtUserDetailsService jwtUserDetailsService;


    @Test
    public void testAddCategoryWithUnexpectedError() throws Exception {
        MediaType MEDIA_TYPE_JSON_UTF8 = new MediaType("application", "json", StandardCharsets.UTF_8);

        given(categoriesService.addCategory(any(CategoryDto.class))).willThrow(new FatalException());

        MvcResult test = mvc.perform(post("/api/categories")
                .accept(MEDIA_TYPE_JSON_UTF8)
                .contentType(MEDIA_TYPE_JSON_UTF8)
                .content(asJsonString(new CategoryDto())))
                .andExpect(status().is5xxServerError())
                .andReturn();
    }

    @Test
    public void testAddCategoryCorrectData() throws Exception {
        MediaType MEDIA_TYPE_JSON_UTF8 = new MediaType("application", "json", StandardCharsets.UTF_8);

        given(categoriesService.addCategory(any(CategoryDto.class))).willReturn(new CategorySerializedDto());

        MvcResult test = mvc.perform(post("/api/categories")
                .accept(MEDIA_TYPE_JSON_UTF8)
                .contentType(MEDIA_TYPE_JSON_UTF8)
                .content(asJsonString(new CategoryDto())))
                .andExpect(status().isOk())
                .andExpect(mvcResult -> asJsonString(new CategorySerializedDto()))
                .andReturn();
    }



    @Test
    public void testGetAllByOwnerWithUnexpectedError() throws Exception{
        given(categoriesService.getAllByUser()).willThrow(new FatalException());
        MvcResult test = mvc.perform(get("/api/categories/all/*")
                .param("token","exampleToken"))
                .andExpect(status().is5xxServerError())
                .andReturn();
    }

    @Test
    public void testGetAllByOwnerWithCorrectData() throws Exception{
        List<CategorySerializedDto> list = new ArrayList<CategorySerializedDto>();
        given(categoriesService.getAllByUser()).willReturn(list);
        MvcResult test = mvc.perform(get("/api/categories/all/*")
                .param("token","exampleToken"))
                .andExpect(status().isOk())
                .andExpect(mvcResult -> new ArrayList<CategorySerializedDto>())
                .andReturn();
    }

    @Test
    public void testUpdateParentCategoryWithInexistentCategory() throws Exception{
        MediaType MEDIA_TYPE_JSON_UTF8 = new MediaType("application", "json", StandardCharsets.UTF_8);

        given(categoriesService.patchParentCategory(any(CategorySerializedDto.class))).willThrow(new CategoriesException("Category does not exist"));

        MvcResult test = mvc.perform(patch("/api/categories/updateParentCategory")
                .accept(MEDIA_TYPE_JSON_UTF8)
                .contentType(MEDIA_TYPE_JSON_UTF8)
                .content(asJsonString(new CategorySerializedDto())))
                .andExpect(status().isNotFound())
                .andReturn();
    }

    @Test
    public void testUpdateParentCategoryWithUnexpectedError() throws Exception{
        MediaType MEDIA_TYPE_JSON_UTF8 = new MediaType("application", "json", StandardCharsets.UTF_8);

        given(categoriesService.patchParentCategory(any(CategorySerializedDto.class))).willThrow(new FatalException());

        MvcResult test = mvc.perform(patch("/api/categories/updateParentCategory")
                .accept(MEDIA_TYPE_JSON_UTF8)
                .contentType(MEDIA_TYPE_JSON_UTF8)
                .content(asJsonString(new CategorySerializedDto())))
                .andExpect(status().is5xxServerError())
                .andReturn();

    }

    @Test
    public void testUpdateParentCategoryWithCorrectData() throws Exception{
        MediaType MEDIA_TYPE_JSON_UTF8 = new MediaType("application", "json", StandardCharsets.UTF_8);

        given(categoriesService.patchParentCategory(any(CategorySerializedDto.class))).willReturn(new CategorySerializedDto());

        MvcResult test = mvc.perform(patch("/api/categories/updateParentCategory")
                .accept(MEDIA_TYPE_JSON_UTF8)
                .contentType(MEDIA_TYPE_JSON_UTF8)
                .content(asJsonString( new CategorySerializedDto())))
                .andExpect(status().isOk())
                .andExpect(mvcResult -> asJsonString( new CategorySerializedDto()))
                .andReturn();

    }

    @Test
    public void testUpdateCategoryWithInexistentCategory() throws Exception{
        MediaType MEDIA_TYPE_JSON_UTF8 = new MediaType("application", "json", StandardCharsets.UTF_8);

        given(categoriesService.updateCategory(any(CategorySerializedDto.class))).willThrow(new CategoriesException("Category does not exist"));

        MvcResult test = mvc.perform(patch("/api/categories/updateCategory")
                .accept(MEDIA_TYPE_JSON_UTF8)
                .contentType(MEDIA_TYPE_JSON_UTF8)
                .content(asJsonString(new CategorySerializedDto())))
                .andExpect(status().isNotFound())
                .andReturn();

    }

    @Test
    public void testUpdateCategoryWithUnexpectedError() throws Exception{
        MediaType MEDIA_TYPE_JSON_UTF8 = new MediaType("application", "json", StandardCharsets.UTF_8);

        given(categoriesService.updateCategory(any(CategorySerializedDto.class))).willThrow(new FatalException());

        MvcResult test = mvc.perform(patch("/api/categories/updateCategory")
                .accept(MEDIA_TYPE_JSON_UTF8)
                .contentType(MEDIA_TYPE_JSON_UTF8)
                .content(asJsonString(new CategorySerializedDto())))
                .andExpect(status().is5xxServerError())
                .andReturn();
    }

    @Test
    public void testUpdateCategoryWithCorrectData() throws Exception{
        MediaType MEDIA_TYPE_JSON_UTF8 = new MediaType("application", "json", StandardCharsets.UTF_8);

        given(categoriesService.updateCategory(any(CategorySerializedDto.class))).willReturn(new CategorySerializedDto());

        MvcResult test = mvc.perform(patch("/api/categories/updateCategory")
                .accept(MEDIA_TYPE_JSON_UTF8)
                .contentType(MEDIA_TYPE_JSON_UTF8)
                .content(asJsonString(new CategorySerializedDto())))
                .andExpect(status().isOk())
                .andExpect(mvcResult -> asJsonString(new CategorySerializedDto()))
                .andReturn();
    }

    @Test
    public void testDeleteCategoryWithKeyViolation() throws Exception{
        given(categoriesService.deleteCategory(any(Integer.class))).willThrow(new CategoriesException("Impossible to delete a category containing sub-categories or transactions"));
        MvcResult test = mvc.perform(delete("/api/categories/delete/1"))
                .andExpect(status().isConflict())
                .andReturn();
    }

    @Test
    public void testDeleteCategoryWithUnexpectedError() throws Exception{
        given(categoriesService.deleteCategory(any(Integer.class))).willThrow(new FatalException());
        MvcResult test = mvc.perform(delete("/api/categories/delete/1"))
                .andExpect(status().is5xxServerError())
                .andReturn();
    }

    @Test
    public void testDeleteCategoryWithCorrectData() throws Exception{
        given(categoriesService.deleteCategory(any(Integer.class))).willReturn(new ObjectResponse(1));
        MvcResult test = mvc.perform(delete("/api/categories/delete/1"))
                .andExpect(status().isOk())
                .andExpect(mvcResult -> asJsonString(new ObjectResponse(1)))
                .andReturn();
    }

    @Test
    public void testGetByOwnerAndTypeWithWrongType() throws Exception{
        given(categoriesService.getByOwnerAndType(any(String.class))).willThrow(new CategoriesException("Category type does not exist"));
        MvcResult test = mvc.perform(get("/api/categories/categoriesPerType")
                .param("type","input"))
                .andExpect(status().isNotFound())
                .andReturn();
    }

    @Test
    public void testGetByOwnerAndTypeWithUnexpectedError() throws Exception{
        given(categoriesService.getByOwnerAndType(any(String.class))).willThrow(new FatalException());
        MvcResult test = mvc.perform(get("/api/categories/categoriesPerType")
                .param("type","input"))
                .andExpect(status().is5xxServerError())
                .andReturn();
    }

    @Test
    public void testGetByOwnerAndTypeWithGoodData() throws Exception{
        given(categoriesService.getByOwnerAndType(any(String.class))).willReturn(new ArrayList<CategorySerializedDto>());
        MvcResult test = mvc.perform(get("/api/categories/categoriesPerType")
                .param("type","input"))
                .andExpect(status().isOk())
                .andExpect(MvcResult -> asJsonString(new ArrayList<CategorySerializedDto>()))
                .andReturn();
    }
    @Test
    public void BudgetPerMonthWithException() throws Exception{
        given(categoriesService.getCategoryAmountSumPerMonth(3,2021,"OUTPUT")).willThrow(new FatalException());
        mvc.perform(get("/api/categories/budget/3/2021/OUTPUT")).andExpect(status().is5xxServerError()).andReturn();
    }

    @Test
    public void BudgetPerMonthWithGoodDAta() throws Exception{
        List<CategoryTransactionResponse> list = new ArrayList<>();
        given(categoriesService.getCategoryAmountSumPerMonth(3,2021,"OUTPUT")).willReturn(list);
        mvc.perform(get("/api/categories/budget/3/2021/OUTPUT")).andExpect(status().isOk()).andReturn();
    }

    public String asJsonString(final Object obj) {
        try {
            return new ObjectMapper().writeValueAsString(obj);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }
}
