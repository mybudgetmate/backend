package com.mybudget.domain.controllers;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.mybudget.domain.dto.FriendShipDto;
import com.mybudget.domain.dto.response.ErrorResponse;
import com.mybudget.domain.dto.response.FriendshipListResponse;
import com.mybudget.domain.exceptions.FatalException;
import com.mybudget.domain.exceptions.FriendshipException;
import com.mybudget.domain.services.FriendshipService;
import com.mybudget.domain.services.JwtUserDetailsService;
import com.mybudget.domain.utils.JwtService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.BDDMockito.given;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

/**
 * @author Soumaya Izmar
 */
@RunWith(SpringRunner.class)
@WebMvcTest(FriendshipController.class)
@AutoConfigureMockMvc(secure = false)
public class FriendshipControllerTest {

    @Autowired
    private MockMvc mvc;

    @MockBean
    private FriendshipService friendshipService;
    @MockBean
    private JwtService jwtService;
    @MockBean
    private JwtUserDetailsService jwtUserDetailsService;

    @Test
    public void testGetAllFriendshipException() throws Exception {
        given(friendshipService.getFriendshipByOwner()).willThrow(new FatalException());
        mvc.perform(get("/api/friendship/")).andExpect(status().is5xxServerError()).andReturn();
    }
    @Test
    public void testGetAllFriendshipGoodData() throws Exception {
        given(friendshipService.getFriendshipByOwner()).willReturn(new FriendshipListResponse());
        mvc.perform(get("/api/friendship/")).andExpect(status().isOk()).andExpect(mvcResult -> new FriendshipListResponse()).andReturn();
    }

    @Test
    public void testAddFriendshipException() throws Exception {
        given(friendshipService.addFriendship(any(String.class))).willThrow(new FatalException());
        mvc.perform(get("/api/friendship/a")).andExpect(status().is5xxServerError()).andReturn();
    }
    @Test
    public void testAddFriendshipClientException() throws Exception {
        String error = "error";
        given(friendshipService.addFriendship(any(String.class))).willThrow(new FriendshipException(""));
        mvc.perform(get("/api/friendship/a")).andExpect(status().is4xxClientError()).andExpect(mvcResult -> asJsonString(new ErrorResponse(error))).andReturn();

    }
    @Test
    public void testAddFriendshipWithGoodData() throws Exception {
        given(friendshipService.addFriendship(any(String.class))).willReturn(new FriendShipDto());
        mvc.perform(get("/api/friendship/a"))
                .andExpect(status().isOk()).andExpect(mvcResult -> asJsonString(new FriendShipDto())).andReturn();
    }


    @Test
    public void testAcceptFriendshipException() throws Exception {
        given(friendshipService.acceptFriendship(any(Integer.class))).willThrow(new FatalException());
        mvc.perform(get("/api/friendship/accept/1")).andExpect(status().is5xxServerError()).andReturn();
    }
    @Test
    public void testAcceptFriendshipClientException() throws Exception {
        String error = "error";
        given(friendshipService.acceptFriendship(any(Integer.class))).willThrow(new FriendshipException(""));
        mvc.perform(get("/api/friendship/accept/1")).andExpect(status().is4xxClientError()).andExpect(mvcResult -> asJsonString(new ErrorResponse(error))).andReturn();

    }
    @Test
    public void testAcceptFriendshipWithGoodData() throws Exception {
        given(friendshipService.acceptFriendship(any(Integer.class))).willReturn(new FriendShipDto());
        mvc.perform(get("/api/friendship/accept/1"))
                .andExpect(status().isOk()).andExpect(mvcResult -> asJsonString(new FriendShipDto())).andReturn();
    }

    @Test
    public void testRefuseFriendshipException() throws Exception {
        given(friendshipService.refuseFriendship(any(Integer.class))).willThrow(new FatalException());
        mvc.perform(get("/api/friendship/refuse/1")).andExpect(status().is5xxServerError()).andReturn();
    }
    @Test
    public void testRefuseFriendshipClientException() throws Exception {
        String error = "error";
        given(friendshipService.refuseFriendship(any(Integer.class))).willThrow(new FriendshipException(""));
        mvc.perform(get("/api/friendship/refuse/1")).andExpect(status().is4xxClientError()).andExpect(mvcResult -> asJsonString(new ErrorResponse(error))).andReturn();

    }
    @Test
    public void testRefuseFriendshipWithGoodData() throws Exception {
        given(friendshipService.refuseFriendship(any(Integer.class))).willReturn(new FriendShipDto());
        mvc.perform(get("/api/friendship/refuse/1"))
                .andExpect(status().isOk()).andExpect(mvcResult -> asJsonString(new FriendShipDto())).andReturn();
    }
    @Test
    public void testDeleteFriendshipException() throws Exception {
        given(friendshipService.deleteFriendship(any(Integer.class),any(Integer.class))).willThrow(new FatalException());
        mvc.perform(delete("/api/friendship/2/1")).andExpect(status().is5xxServerError()).andReturn();
    }
    @Test
    public void testDeleteFriendshipClientException() throws Exception {
        String error = "error";
        given(friendshipService.deleteFriendship(any(Integer.class),any(Integer.class))).willThrow(new FriendshipException(""));
        mvc.perform(delete("/api/friendship/2/1")).andExpect(status().is4xxClientError()).andExpect(mvcResult -> asJsonString(new ErrorResponse(error))).andReturn();

    }
    @Test
    public void testDeleteFriendshipWithGoodData() throws Exception {
        given(friendshipService.deleteFriendship(any(Integer.class),any(Integer.class))).willReturn(new FriendShipDto());
        mvc.perform(delete("/api/friendship/2/1"))
                .andExpect(status().isOk()).andExpect(mvcResult -> asJsonString(new FriendShipDto())).andReturn();
    }





    public String asJsonString(final Object obj) {
        try {
            return new ObjectMapper().writeValueAsString(obj);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }
}
