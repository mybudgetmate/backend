package com.mybudget.domain.controllers;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.mybudget.domain.dto.AccountDto;
import com.mybudget.domain.dto.CategoryDto;
import com.mybudget.domain.exceptions.AccountTypeException;
import com.mybudget.domain.exceptions.FatalException;
import com.mybudget.domain.services.AccountService;
import com.mybudget.domain.services.JwtUserDetailsService;
import com.mybudget.domain.utils.JwtService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;

import java.nio.charset.StandardCharsets;
import java.util.ArrayList;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.BDDMockito.given;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;


/**
 * @Author Eva Tumia
 */
@RunWith(SpringRunner.class)
@WebMvcTest(AccountController.class)
@AutoConfigureMockMvc(secure = false)
public class AccountControllerTest {

    @Autowired
    private MockMvc mvc;

    @MockBean
    private AccountService accountService;
    @MockBean
    private JwtService jwtService;
    @MockBean
    private JwtUserDetailsService jwtUserDetailsService;

    @Test
    public void getAllByOwner() throws Exception {
        given(accountService.getAll()).willReturn(new ArrayList<AccountDto>());
        MvcResult test = mvc.perform(get("/api/account/all"))
                .andExpect(status().isOk())
                .andExpect(mvcResult -> asJsonString(new ArrayList<AccountDto>()))
                .andReturn();
    }

    @Test
    public void getAllByOwnerWithUnexpectedError() throws Exception {
        given(accountService.getAll()).willThrow(new FatalException());
        MvcResult test = mvc.perform(get("/api/account/all"))
                .andExpect(status().is5xxServerError())
                .andReturn();
    }


    @Test
    public void getAllDefaultAccount() throws Exception {
        given(accountService.getAllDefaultAccount()).willReturn(new ArrayList<AccountDto>());
        mvc.perform(get("/api/account/default"))
                .andExpect(status().isOk())
                .andExpect(mvcResult -> asJsonString(new ArrayList<AccountDto>()))
                .andReturn();
    }

    @Test
    public void getAllDefaultAccountWithUnexpectedError() throws Exception {
        given(accountService.getAllDefaultAccount()).willThrow(new FatalException());
        mvc.perform(get("/api/account/default"))
                .andExpect(status().is5xxServerError())
                .andReturn();
    }

    @Test
    public void addAccountWithGoodData() throws Exception {
        MediaType MEDIA_TYPE_JSON_UTF8 = new MediaType("application", "json", StandardCharsets.UTF_8);

        given(accountService.addAccount(any(AccountDto.class))).willReturn(new AccountDto());

        MvcResult test = mvc.perform(post("/api/account/add")
                .accept(MEDIA_TYPE_JSON_UTF8)
                .contentType(MEDIA_TYPE_JSON_UTF8)
                .content(asJsonString(new CategoryDto())))
                .andExpect(status().isOk())
                .andExpect(mvcResult -> asJsonString(new AccountDto()))
                .andReturn();
    }

    @Test
    public void addAccountWithWrongAccountType() throws Exception {
        MediaType MEDIA_TYPE_JSON_UTF8 = new MediaType("application", "json", StandardCharsets.UTF_8);

        given(accountService.addAccount(any(AccountDto.class))).willThrow(new AccountTypeException());

        MvcResult test = mvc.perform(post("/api/account/add")
                .accept(MEDIA_TYPE_JSON_UTF8)
                .contentType(MEDIA_TYPE_JSON_UTF8)
                .content(asJsonString(new CategoryDto())))
                .andExpect(status().isNotFound())
                .andReturn();
    }

    @Test
    public void addAccountWithUnexpectedError() throws Exception {
        MediaType MEDIA_TYPE_JSON_UTF8 = new MediaType("application", "json", StandardCharsets.UTF_8);

        given(accountService.addAccount(any(AccountDto.class))).willThrow(new FatalException());

        MvcResult test = mvc.perform(post("/api/account/add")
                .accept(MEDIA_TYPE_JSON_UTF8)
                .contentType(MEDIA_TYPE_JSON_UTF8)
                .content(asJsonString(new CategoryDto())))
                .andExpect(status().is5xxServerError())
                .andReturn();
    }

    public String asJsonString(final Object obj) {
        try {
            return new ObjectMapper().writeValueAsString(obj);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }
}
