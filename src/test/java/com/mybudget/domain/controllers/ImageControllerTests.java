package com.mybudget.domain.controllers;

import com.mybudget.domain.exceptions.FatalException;
import com.mybudget.domain.services.ImageService;
import com.mybudget.domain.services.JwtUserDetailsService;
import com.mybudget.domain.utils.JwtService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;

import java.util.ArrayList;

import static org.mockito.BDDMockito.given;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

/**
 * @author Soumaya Izmar
 */
@RunWith(SpringRunner.class)
@WebMvcTest(ImageController.class)
@AutoConfigureMockMvc(secure = false)
public class ImageControllerTests {
    @Autowired
    private MockMvc mvc;

    @MockBean
    private ImageService imageService;
    @MockBean
    private JwtService jwtService;
    @MockBean
    private JwtUserDetailsService jwtUserDetailsService;

    @Test
    public void testFetchAllImage() throws Exception {
        given(imageService.fetchAllImages()).willReturn(new ArrayList<>());
        mvc.perform(get("/api/images/")).andExpect(status().isOk()).andExpect(mvcResult -> new ArrayList<>()).andReturn();

    }

    @Test
    public void testFetchAllStatusWithUnexpectedError() throws Exception {
        given(imageService.fetchAllImages()).willThrow(new FatalException());
        MvcResult test = mvc.perform(get("/api/images/")).andExpect(status().is5xxServerError()).andReturn();
    }


}
