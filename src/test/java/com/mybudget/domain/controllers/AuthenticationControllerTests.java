package com.mybudget.domain.controllers;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.mybudget.domain.dto.LoginDto;
import com.mybudget.domain.dto.UserDto;
import com.mybudget.domain.dto.UserRegistrationDto;
import com.mybudget.domain.dto.response.AuthenticationResponse;
import com.mybudget.domain.dto.response.ErrorResponse;
import com.mybudget.domain.exceptions.FatalException;
import com.mybudget.domain.exceptions.LoginException;
import com.mybudget.domain.exceptions.RegistrationException;
import com.mybudget.domain.services.AuthenticationService;
import com.mybudget.domain.services.JwtUserDetailsService;
import com.mybudget.domain.utils.JwtService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;

import java.nio.charset.StandardCharsets;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.BDDMockito.given;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.patch;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

/**
 * @author Soumaya Izmar
 */
@RunWith(SpringRunner.class)
@WebMvcTest(AuthenticationController.class)
@AutoConfigureMockMvc(secure = false)
public class AuthenticationControllerTests {

    @Autowired
    private MockMvc mvc;

    @MockBean
    private AuthenticationService authenticationService;
    @MockBean
    private JwtService jwtService;
    @MockBean
    private JwtUserDetailsService jwtUserDetailsService;


    @Test
    public void testLoginWrongData() throws Exception {
        MediaType MEDIA_TYPE_JSON_UTF8 = new MediaType("application", "json", StandardCharsets.UTF_8);
        LoginDto loginDto = new LoginDto();
        loginDto.setEmail("a@hotmail.com");
        loginDto.setPassword("123ff");
        loginDto.setPreferredLanguage("fr");

        String error = "login error";
        given(authenticationService.loginUser(any(LoginDto.class))).willThrow(new LoginException(error));

        MvcResult test = mvc.perform(post("/api/auth/login").
                accept(MEDIA_TYPE_JSON_UTF8).
                contentType(MEDIA_TYPE_JSON_UTF8).
                content(asJsonString(loginDto))).andExpect(status().is4xxClientError()).andExpect(mvcResult -> asJsonString(new ErrorResponse(error))).andReturn();

    }


    @Test
    public void testLoginGoodData() throws Exception {
        MediaType MEDIA_TYPE_JSON_UTF8 = new MediaType("application", "json", StandardCharsets.UTF_8);
        LoginDto loginDto = new LoginDto();
        loginDto.setEmail("goodData@hotmail.com");
        loginDto.setPassword("a");
        loginDto.setPreferredLanguage("fr");
        AuthenticationResponse authenticationResponse = new AuthenticationResponse("token");

        given(authenticationService.loginUser(any(LoginDto.class))).willReturn(authenticationResponse);

        MvcResult test = mvc.perform(post("/api/auth/login").
                accept(MEDIA_TYPE_JSON_UTF8).
                contentType(MEDIA_TYPE_JSON_UTF8).
                content(asJsonString(loginDto))).andExpect(status().isOk()).andExpect(mvcResult -> asJsonString(authenticationResponse)).andReturn();



    }


    @Test
    public void testLoginInternalServerError() throws Exception {

        MediaType MEDIA_TYPE_JSON_UTF8 = new MediaType("application", "json", StandardCharsets.UTF_8);
        LoginDto loginDto = new LoginDto();
        loginDto.setEmail("a@hotmail.com");
        loginDto.setPassword("a");
        loginDto.setPreferredLanguage("");

        given(authenticationService.loginUser(any(LoginDto.class))).willThrow(new ClassNotFoundException());
        MvcResult test = mvc.perform(post("/api/auth/login").
                accept(MEDIA_TYPE_JSON_UTF8).
                contentType(MEDIA_TYPE_JSON_UTF8).
                content(asJsonString(loginDto))).andExpect(status().is5xxServerError()).andReturn();


    }

    @Test
    public void testRegisterWithGoodData() throws Exception {

        MediaType MEDIA_TYPE_JSON_UTF8 = new MediaType("application", "json", StandardCharsets.UTF_8);
        UserRegistrationDto userRegistrationDto = new UserRegistrationDto();
        AuthenticationResponse authenticationResponse = new AuthenticationResponse("token");


        given(authenticationService.registerUser(any(UserRegistrationDto.class))).willReturn(authenticationResponse);
        MvcResult test = mvc.perform(post("/api/auth/register").
                accept(MEDIA_TYPE_JSON_UTF8).
                contentType(MEDIA_TYPE_JSON_UTF8).
                content(asJsonString(userRegistrationDto))).andExpect(status().isOk()).andExpect(mvcResult -> asJsonString(authenticationResponse)).andReturn();

    }

    @Test
    public void testRegisterWithWrongData() throws Exception {
        MediaType MEDIA_TYPE_JSON_UTF8 = new MediaType("application", "json", StandardCharsets.UTF_8);
        UserRegistrationDto userRegistrationDto = new UserRegistrationDto();
        userRegistrationDto.setEmail("wrong data");

        String error = "register error";
        given(authenticationService.registerUser(any(UserRegistrationDto.class))).willThrow(new RegistrationException(error));

        MvcResult test = mvc.perform(post("/api/auth/register").
                accept(MEDIA_TYPE_JSON_UTF8).
                contentType(MEDIA_TYPE_JSON_UTF8).
                content(asJsonString(userRegistrationDto))).andExpect(status().is4xxClientError()).andExpect(mvcResult -> asJsonString(new ErrorResponse(error))).andReturn();


    }


    @Test
    public void testRegisterWithInternalServerError() throws Exception {

        MediaType MEDIA_TYPE_JSON_UTF8 = new MediaType("application", "json", StandardCharsets.UTF_8);
        UserRegistrationDto userRegistrationDto = new UserRegistrationDto();
        userRegistrationDto.setEmail("InternalError");

        given(authenticationService.registerUser(any(UserRegistrationDto.class))).willThrow(new FatalException());

        MvcResult test = mvc.perform(post("/api/auth/register").
                accept(MEDIA_TYPE_JSON_UTF8).
                contentType(MEDIA_TYPE_JSON_UTF8).
                content(asJsonString(userRegistrationDto))).andExpect(status().is5xxServerError()).andReturn();


    }

    @Test
    public void testPatchPseudoInternalServerError() throws Exception {

        MediaType MEDIA_TYPE_JSON_UTF8 = new MediaType("application", "json", StandardCharsets.UTF_8);
        UserDto userDto = new UserDto();
        userDto.setPseudo("InternalError");

        given(authenticationService.changePseudo(any(String.class))).willThrow(new FatalException());

        MvcResult test = mvc.perform(patch("/api/auth/pseudo/").
                accept(MEDIA_TYPE_JSON_UTF8).
                contentType(MEDIA_TYPE_JSON_UTF8).
                content(asJsonString(userDto))).andExpect(status().is5xxServerError()).andReturn();


    }

    @Test
    public void testPatchPseudoInternalClientError() throws Exception {

        MediaType MEDIA_TYPE_JSON_UTF8 = new MediaType("application", "json", StandardCharsets.UTF_8);
        UserDto userDto = new UserDto();
        userDto.setPseudo("InternalError");

        given(authenticationService.changePseudo(any(String.class))).willThrow(new RegistrationException(""));

        String error = "error";
        MvcResult test = mvc.perform(patch("/api/auth/pseudo/").
                accept(MEDIA_TYPE_JSON_UTF8).
                contentType(MEDIA_TYPE_JSON_UTF8).
                content(asJsonString(userDto))).andExpect(status().is4xxClientError()).andExpect(mvcResult -> asJsonString(new ErrorResponse(error))).andReturn();
    }
    @Test
    public void testPatchPseudoWithGoodData() throws Exception {
        MediaType MEDIA_TYPE_JSON_UTF8 = new MediaType("application", "json", StandardCharsets.UTF_8);
        UserDto userDto = new UserDto();
        userDto.setPseudo("InternalError");

        given(authenticationService.changePseudo(any(String.class))).willReturn(new UserDto());

        MvcResult test = mvc.perform(patch("/api/auth/pseudo/").
                accept(MEDIA_TYPE_JSON_UTF8).
                contentType(MEDIA_TYPE_JSON_UTF8).
                content(asJsonString(userDto))).andExpect(status().isOk()).andExpect(mvcResult -> asJsonString(new UserDto())).andReturn();

    }

    @Test
    public void testGetPseudoWithGoodData() throws Exception {
        MediaType MEDIA_TYPE_JSON_UTF8 = new MediaType("application", "json", StandardCharsets.UTF_8);
        UserDto userDto = new UserDto();
        userDto.setPseudo("InternalError");

        given(authenticationService.changePseudo(any(String.class))).willReturn(new UserDto());

        MvcResult test = mvc.perform(get("/api/auth/pseudo/").
                accept(MEDIA_TYPE_JSON_UTF8).
                contentType(MEDIA_TYPE_JSON_UTF8).
                content(asJsonString(userDto))).andExpect(status().isOk()).andExpect(mvcResult -> asJsonString(new UserDto())).andReturn();

    }


    @Test
    public void testGetPseudoInternalServerError() throws Exception {

        MediaType MEDIA_TYPE_JSON_UTF8 = new MediaType("application", "json", StandardCharsets.UTF_8);
        UserDto userDto = new UserDto();
        userDto.setPseudo("InternalError");

        given(authenticationService.getPseudo()).willThrow(new FatalException());

        MvcResult test = mvc.perform(get("/api/auth/pseudo/").
                accept(MEDIA_TYPE_JSON_UTF8).
                contentType(MEDIA_TYPE_JSON_UTF8).
                content(asJsonString(userDto))).andExpect(status().is5xxServerError()).andReturn();


    }

    @Test
    public void testGetPseudoInternalClientError() throws Exception {

        MediaType MEDIA_TYPE_JSON_UTF8 = new MediaType("application", "json", StandardCharsets.UTF_8);
        UserDto userDto = new UserDto();
        userDto.setPseudo("InternalError");

        given(authenticationService.getPseudo()).willThrow(new RegistrationException(""));

        String error = "error";
        MvcResult test = mvc.perform(get("/api/auth/pseudo/").
                accept(MEDIA_TYPE_JSON_UTF8).
                contentType(MEDIA_TYPE_JSON_UTF8).
                content(asJsonString(userDto))).andExpect(status().is4xxClientError()).andExpect(mvcResult -> asJsonString(new ErrorResponse(error))).andReturn();
    }

    @Test
    public void testGetSocialCodeWithGoodData() throws Exception {
        MediaType MEDIA_TYPE_JSON_UTF8 = new MediaType("application", "json", StandardCharsets.UTF_8);
        UserDto userDto = new UserDto();
        userDto.setPseudo("InternalError");

        given(authenticationService.getSocialCode()).willReturn(new UserDto());

        MvcResult test = mvc.perform(get("/api/auth/socialCode/").
                accept(MEDIA_TYPE_JSON_UTF8).
                contentType(MEDIA_TYPE_JSON_UTF8).
                content(asJsonString(userDto))).andExpect(status().isOk()).andExpect(mvcResult -> asJsonString(new UserDto())).andReturn();

    }


    @Test
    public void testGetSocialCodeInternalServerError() throws Exception {

        MediaType MEDIA_TYPE_JSON_UTF8 = new MediaType("application", "json", StandardCharsets.UTF_8);
        UserDto userDto = new UserDto();
        userDto.setPseudo("InternalError");

        given(authenticationService.getSocialCode()).willThrow(new FatalException());

        MvcResult test = mvc.perform(get("/api/auth/socialCode/").
                accept(MEDIA_TYPE_JSON_UTF8).
                contentType(MEDIA_TYPE_JSON_UTF8).
                content(asJsonString(userDto))).andExpect(status().is5xxServerError()).andReturn();


    }

    @Test
    public void testGetSocialCodeInternalClientError() throws Exception {

        MediaType MEDIA_TYPE_JSON_UTF8 = new MediaType("application", "json", StandardCharsets.UTF_8);
        UserDto userDto = new UserDto();
        userDto.setPseudo("InternalError");

        given(authenticationService.getSocialCode()).willThrow(new RegistrationException(""));

        String error = "error";
        MvcResult test = mvc.perform(get("/api/auth/socialCode/").
                accept(MEDIA_TYPE_JSON_UTF8).
                contentType(MEDIA_TYPE_JSON_UTF8).
                content(asJsonString(userDto))).andExpect(status().is4xxClientError()).andExpect(mvcResult -> asJsonString(new ErrorResponse(error))).andReturn();
    }
    public String asJsonString(final Object obj) {
        try {
            return new ObjectMapper().writeValueAsString(obj);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }
}
