package com.mybudget.domain.controllers;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.mybudget.domain.dto.InvoiceDto;
import com.mybudget.domain.dto.TransactionDto;
import com.mybudget.domain.exceptions.FatalException;
import com.mybudget.domain.services.AuthenticationService;
import com.mybudget.domain.services.InvoiceService;
import com.mybudget.domain.services.JwtUserDetailsService;
import com.mybudget.domain.utils.JwtService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.http.MediaType;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;

import java.nio.charset.StandardCharsets;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.TreeMap;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.BDDMockito.given;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

/**
 * @author Eva Tumia
 */

@RunWith(SpringRunner.class)
@WebMvcTest(InvoiceController.class)
@AutoConfigureMockMvc(secure = false)
public class InvoiceControllerTests {

    @Autowired
    private MockMvc mvc;

    @MockBean
    private InvoiceService invoiceService;
    @MockBean
    private JwtService jwtService;
    @MockBean
    private JwtUserDetailsService jwtUserDetailsService;
    @MockBean
    private AuthenticationService authenticationService;

    @Test
    public void testAddInvoiceWithUnexpectedError() throws Exception{
        MediaType MEDIA_TYPE_JSON_UTF8 = new MediaType("application", "json", StandardCharsets.UTF_8);

        given(invoiceService.addNewInvoice(any(InvoiceDto.class))).willThrow(new FatalException());

        MvcResult test = mvc.perform(post("/api/invoice/add")
                .accept(MEDIA_TYPE_JSON_UTF8)
                .contentType(MEDIA_TYPE_JSON_UTF8)
                .content(asJsonString(new InvoiceDto())))
                .andExpect(status().is5xxServerError())
                .andReturn();
    }

    @Test
    public void testAddInvoiceWithGoodData() throws Exception{
        MediaType MEDIA_TYPE_JSON_UTF8 = new MediaType("application", "json", StandardCharsets.UTF_8);

        given(invoiceService.addNewInvoice(any(InvoiceDto.class))).willReturn(new InvoiceDto());

        MvcResult test = mvc.perform(post("/api/invoice/add")
                .accept(MEDIA_TYPE_JSON_UTF8)
                .contentType(MEDIA_TYPE_JSON_UTF8)
                .content(asJsonString(new InvoiceDto())))
                .andExpect(status().isOk())
                .andExpect(mvcResult -> asJsonString(new InvoiceDto()))
                .andReturn();
    }

    @Test
    public void testGetAllByOwnerWithUnexpectedError() throws Exception{
        given(invoiceService.getAllByOwner()).willThrow(new FatalException());
        MvcResult test = mvc.perform(get("/api/invoice/all"))
                .andExpect(status().is5xxServerError())
                .andReturn();
    }

    @Test
    public void testGetAllByOwnerWithGoodData() throws Exception{
        given(invoiceService.getAllByOwner()).willReturn(new ArrayList<InvoiceDto>());
        MvcResult test = mvc.perform(get("/api/invoice/all"))
                .andExpect(status().isOk())
                .andExpect(mvcResult -> new ArrayList<InvoiceDto>())
                .andReturn();
    }

    public String asJsonString(final Object obj) {
        try {
            return new ObjectMapper().writeValueAsString(obj);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

}
