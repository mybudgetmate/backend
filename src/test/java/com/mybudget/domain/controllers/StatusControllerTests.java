package com.mybudget.domain.controllers;

import com.mybudget.domain.exceptions.FatalException;
import com.mybudget.domain.services.JwtUserDetailsService;
import com.mybudget.domain.services.StatusService;
import com.mybudget.domain.utils.JwtService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import java.util.ArrayList;
import java.util.Map;
import java.util.TreeMap;

import static org.mockito.BDDMockito.given;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@WebMvcTest(StatusController.class)
@AutoConfigureMockMvc(secure = false)
public class StatusControllerTests {


    @Autowired
    private MockMvc mvc;

    @MockBean
    private StatusService statusService;
    @MockBean
    private JwtService jwtService;
    @MockBean
    private JwtUserDetailsService jwtUserDetailsService;

    @Test
    public void testFetchAllStatus() throws Exception {
        given(statusService.fetchAllStatus("fr")).willReturn(new TreeMap<>());
        mvc.perform(get("/api/status/?languageCode=fr")).andExpect(status().isOk()).andExpect(mvcResult -> new ArrayList<>());
    }

    @Test
    public void testFetchAllStatusWithUnexpectedError() throws Exception {
        given(statusService.fetchAllStatus("fr")).willThrow(new FatalException());
        mvc.perform(get("/api/status/?languageCode=fr")).andExpect(status().is5xxServerError());

    }
}
