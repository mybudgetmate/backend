package com.mybudget.domain.controllers;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.mybudget.domain.dto.TransactionCSV;
import com.mybudget.domain.dto.TransactionDto;
import com.mybudget.domain.dto.response.BudgetResponse;
import com.mybudget.domain.dto.response.ObjectResponse;
import com.mybudget.domain.dto.response.ShoppingBudgetResponse;
import com.mybudget.domain.dto.response.ShoppingWeekResponse;
import com.mybudget.domain.exceptions.AccountException;
import com.mybudget.domain.exceptions.CategoriesException;
import com.mybudget.domain.exceptions.FatalException;
import com.mybudget.domain.exceptions.TransactionException;
import com.mybudget.domain.models.Language;
import com.mybudget.domain.models.User;
import com.mybudget.domain.services.AuthenticationService;
import com.mybudget.domain.services.JwtUserDetailsService;
import com.mybudget.domain.services.TransactionService;
import com.mybudget.domain.services.TranslationService;
import com.mybudget.domain.utils.JwtService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;

import java.nio.charset.StandardCharsets;
import java.time.LocalDate;
import java.util.*;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.doThrow;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@WebMvcTest(TransactionController.class)
@AutoConfigureMockMvc(secure = false)
public class TransactionControllerTests {

    @Autowired
    private MockMvc mvc;

    @MockBean
    private TransactionService transactionService;
    @MockBean
    private JwtService jwtService;
    @MockBean
    private JwtUserDetailsService jwtUserDetailsService;
    @MockBean
    private AuthenticationService authenticationService;
    @MockBean
    private TranslationService translationService;

    @Test
    public void addTransactionWithInexistentAccount() throws Exception {
        MediaType MEDIA_TYPE_JSON_UTF8 = new MediaType("application", "json", StandardCharsets.UTF_8);

        given(transactionService.addTransaction(any(TransactionDto.class))).willThrow(new AccountException());

        MvcResult test = mvc.perform(post("/api/transactions/add")
                .accept(MEDIA_TYPE_JSON_UTF8)
                .contentType(MEDIA_TYPE_JSON_UTF8)
                .content(asJsonString(new TransactionDto())))
                .andExpect(status().isNotFound())
                .andReturn();
    }

    @Test
    public void addTransactionWithNonexistentCategory() throws Exception {
        MediaType MEDIA_TYPE_JSON_UTF8 = new MediaType("application", "json", StandardCharsets.UTF_8);

        given(transactionService.addTransaction(any(TransactionDto.class))).willThrow(new CategoriesException());

        MvcResult test = mvc.perform(post("/api/transactions/add")
                .accept(MEDIA_TYPE_JSON_UTF8)
                .contentType(MEDIA_TYPE_JSON_UTF8)
                .content(asJsonString(new TransactionDto())))
                .andExpect(status().isNotFound())
                .andReturn();
    }

    @Test
    public void testAddCategoryWithUnexpectedError() throws Exception {
        MediaType MEDIA_TYPE_JSON_UTF8 = new MediaType("application", "json", StandardCharsets.UTF_8);

        given(transactionService.addTransaction(any(TransactionDto.class))).willThrow(new FatalException());

        MvcResult test = mvc.perform(post("/api/transactions/add")
                .accept(MEDIA_TYPE_JSON_UTF8)
                .contentType(MEDIA_TYPE_JSON_UTF8)
                .content(asJsonString(new TransactionDto())))
                .andExpect(status().is5xxServerError())
                .andReturn();
    }

    @Test
    public void testAddCategoryWithGoodData() throws Exception {
        MediaType MEDIA_TYPE_JSON_UTF8 = new MediaType("application", "json", StandardCharsets.UTF_8);

        given(transactionService.addTransaction(any(TransactionDto.class))).willReturn(new TransactionDto());

        MvcResult test = mvc.perform(post("/api/transactions/add")
                .accept(MEDIA_TYPE_JSON_UTF8)
                .contentType(MEDIA_TYPE_JSON_UTF8)
                .content(asJsonString(new TransactionDto())))
                .andExpect(status().isOk())
                .andReturn();
    }

    @Test
    public void testGetAllCourseTransactionsByMonthException() throws Exception {

        given(transactionService.getAllCourseTransactions(true,3, 2020, "course", "output")).willThrow(new FatalException());
        given(transactionService.getAmountBudgetTransactionsPerWeek(3, 2020)).willThrow(new FatalException());
        mvc.perform(get("/api/transactions/3/2020")).andExpect(status().is5xxServerError()).andReturn();
    }

    @Test
    public void testGetAllCourseTransactionsByMonthWithGoodData() throws Exception {
        List<ShoppingWeekResponse> transactionsPerWeek = new ArrayList<>();
        ShoppingBudgetResponse shoppingBudgetResponse = new ShoppingBudgetResponse();
        shoppingBudgetResponse.setAllShoppingTransactions(new ArrayList<>());
        User u = new User();
        Language language = new Language();
        language.setCountryCode("gb");
        u.setPreferedLanguage(language);
        given(authenticationService.getCurrentUser()).willReturn(u);
        Map<String, String> map = new HashMap<>();
        map.put("groceries", "groceries");

        given(translationService.fetchCategoryTemplateTranslation(any(String.class))).willReturn(map);
        given(transactionService.getAmountBudgetTransactionsPerWeek(3, 2020)).willReturn(transactionsPerWeek);
        given(transactionService.getAllCourseTransactions(any(Boolean.class),any(Integer.class), any(Integer.class), any(String.class), any(String.class))).willReturn(shoppingBudgetResponse);

        mvc.perform(get("/api/transactions/3/2020")).andExpect(status().isOk()).andReturn();
    }

    @Test
    public void testGetUserBalanceWithUnexpectedError() throws Exception {
        given(transactionService.getUserBalance()).willThrow(new FatalException());
        MvcResult test = mvc.perform(get("/api/transactions/userBalance"))
                .andExpect(status().is5xxServerError())
                .andReturn();
    }

    @Test
    public void testGetUserBalanceWithGoodData() throws Exception {
        given(transactionService.getUserBalance()).willReturn(new TransactionDto());
        MvcResult test = mvc.perform(get("/api/transactions/userBalance"))
                .andExpect(status().isOk())
                .andExpect(mvcResult -> new TransactionDto())
                .andReturn();
    }

    @Test
    public void testGetAllByUserWithUnexpectedError() throws Exception {
        given(transactionService.getAllByUser()).willThrow(new FatalException());
        MvcResult test = mvc.perform(get("/api/transactions/allByUser"))
                .andExpect(status().is5xxServerError())
                .andReturn();
    }

    @Test
    public void testGetAllByUserWithGoodData() throws Exception {
        Mockito.doReturn(new TreeMap<LocalDate, List<TransactionDto>>()).when(transactionService).getAllByUser();
        MvcResult test = mvc.perform(get("/api/transactions/allByUser"))
                .andExpect(status().isOk())
                .andExpect(mvcResult -> new TreeMap<LocalDate, List<TransactionDto>>())
                .andReturn();
    }

    @Test
    public void testPathTransactionWithNonexistentAccount() throws Exception {
        MediaType MEDIA_TYPE_JSON_UTF8 = new MediaType("application", "json", StandardCharsets.UTF_8);

        given(transactionService.patchTransaction(any(TransactionDto.class))).willThrow(new AccountException());

        MvcResult test = mvc.perform(patch("/api/transactions/patch")
                .accept(MEDIA_TYPE_JSON_UTF8)
                .contentType(MEDIA_TYPE_JSON_UTF8)
                .content(asJsonString(new TransactionDto())))
                .andExpect(status().isNotFound())
                .andReturn();
    }

    @Test
    public void testPathTransactionWithNonexistentTransaction() throws Exception {
        MediaType MEDIA_TYPE_JSON_UTF8 = new MediaType("application", "json", StandardCharsets.UTF_8);

        given(transactionService.patchTransaction(any(TransactionDto.class))).willThrow(new TransactionException());

        MvcResult test = mvc.perform(patch("/api/transactions/patch")
                .accept(MEDIA_TYPE_JSON_UTF8)
                .contentType(MEDIA_TYPE_JSON_UTF8)
                .content(asJsonString(new TransactionDto())))
                .andExpect(status().isNotFound())
                .andReturn();
    }

    @Test
    public void testPathTransactionWithUnexepctedError() throws Exception {
        MediaType MEDIA_TYPE_JSON_UTF8 = new MediaType("application", "json", StandardCharsets.UTF_8);

        given(transactionService.patchTransaction(any(TransactionDto.class))).willThrow(new FatalException());

        MvcResult test = mvc.perform(patch("/api/transactions/patch")
                .accept(MEDIA_TYPE_JSON_UTF8)
                .contentType(MEDIA_TYPE_JSON_UTF8)
                .content(asJsonString(new TransactionDto())))
                .andExpect(status().is5xxServerError())
                .andReturn();
    }

    @Test
    public void testPathTransactionWithGoodData() throws Exception {
        MediaType MEDIA_TYPE_JSON_UTF8 = new MediaType("application", "json", StandardCharsets.UTF_8);

        given(transactionService.patchTransaction(any(TransactionDto.class))).willReturn(new TransactionDto());

        MvcResult test = mvc.perform(patch("/api/transactions/patch")
                .accept(MEDIA_TYPE_JSON_UTF8)
                .contentType(MEDIA_TYPE_JSON_UTF8)
                .content(asJsonString(new TransactionDto())))
                .andExpect(mvcResult -> new TransactionDto())
                .andExpect(status().isOk())
                .andReturn();
    }

    @Test
    public void testDeleteWithNonexistentTransaction() throws Exception {
        given(transactionService.deleteTransaction(any(Integer.class))).willThrow(new TransactionException());
        MvcResult test = mvc.perform(delete("/api/transactions/delete/1"))
                .andExpect(status().isNotFound())
                .andReturn();
    }

    @Test
    public void testDeleteTransactionWithUnexpectedError() throws Exception {
        given(transactionService.deleteTransaction(any(Integer.class))).willThrow(new FatalException());
        MvcResult test = mvc.perform(delete("/api/transactions/delete/1"))
                .andExpect(status().is5xxServerError())
                .andReturn();
    }

    @Test
    public void testDeleteTransactionWithCorrectData() throws Exception {
        given(transactionService.deleteTransaction(any(Integer.class))).willReturn(new ObjectResponse(1));
        MvcResult test = mvc.perform(delete("/api/transactions/delete/1"))
                .andExpect(status().isOk())
                .andExpect(mvcResult -> asJsonString(new ObjectResponse(1)))
                .andReturn();
    }

    @Test
    public void testGetDailyBudgetWithUnexpectedError() throws Exception {
        given(transactionService.getDailyBudget()).willThrow(new FatalException());
        MvcResult test = mvc.perform(get("/api/transactions/dailyBudget"))
                .andExpect(status().is5xxServerError())
                .andReturn();
    }

    @Test

    public void testGetDailyBudgetWithGoodData() throws Exception{

        MvcResult test = mvc.perform(get("/api/transactions/dailyBudget"))
                .andExpect(status().isOk())
                .andExpect(mvcResult -> new BudgetResponse())
                .andReturn();
    }

    @Test
    public void testAddTransactionCsvWithUnexpectedError() throws Exception {

        MediaType MEDIA_TYPE_JSON_UTF8 = new MediaType("application", "json", StandardCharsets.UTF_8);

        doThrow(new TransactionException()).when(transactionService).addTransactionWithCSV(any(TransactionCSV.class));
        mvc.perform(post("/api/transactions/addCSV")
                .accept(MEDIA_TYPE_JSON_UTF8)
                .contentType(MEDIA_TYPE_JSON_UTF8)
                .content(asJsonString(new TransactionCSV())))
                .andExpect(status().is5xxServerError())
                .andReturn();
    }

    @Test
    public void testAddTransactionCscWithGoodData() throws Exception {

        MediaType MEDIA_TYPE_JSON_UTF8 = new MediaType("application", "json", StandardCharsets.UTF_8);


        mvc.perform(patch("/api/transactions/patch")
                .accept(MEDIA_TYPE_JSON_UTF8)
                .contentType(MEDIA_TYPE_JSON_UTF8)
                .content(asJsonString(new TransactionCSV())))
                .andExpect(status().isOk())
                .andReturn();
    }


    public String asJsonString(final Object obj) {
        try {
            return new ObjectMapper().writeValueAsString(obj);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }
}
