package com.mybudget.domain.controllers;

import com.mybudget.domain.dto.RecipeDto;
import com.mybudget.domain.services.JwtUserDetailsService;
import com.mybudget.domain.services.RecipeService;
import com.mybudget.domain.utils.JwtService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;

import java.util.Map;
import java.util.TreeMap;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.BDDMockito.given;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@WebMvcTest(RecipeController.class)
@AutoConfigureMockMvc(secure = false)
public class RecipeControllerTest {
    @Autowired
    private MockMvc mvc;

    @MockBean
    private RecipeService recipeService;
    @MockBean
    private JwtService jwtService;
    @MockBean
    private JwtUserDetailsService jwtUserDetailsService;

    @Test
    public void getAll() throws Exception {
        MvcResult test = mvc.perform(get("/api/recipes/?languageCode=fr"))
                .andExpect(status().isOk())
                .andExpect(mvcResult -> new TreeMap<Integer, Map<Long, RecipeDto>>())
                .andReturn();
    }
}