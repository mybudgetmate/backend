package com.mybudget.domain.controllers;


import com.fasterxml.jackson.databind.ObjectMapper;
import com.mybudget.domain.exceptions.FatalException;
import com.mybudget.domain.models.Icon;
import com.mybudget.domain.services.IconsService;
import com.mybudget.domain.services.JwtUserDetailsService;
import com.mybudget.domain.utils.JwtService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;

import java.util.ArrayList;

import static org.mockito.BDDMockito.given;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

/**
 * @author Eva Tumia
 */

@RunWith(SpringRunner.class)
@WebMvcTest(IconsController.class)
@AutoConfigureMockMvc(secure = false)
public class IconsControllerTest {

    @Autowired
    private MockMvc mvc;

    @MockBean
    private IconsService iconsService;
    @MockBean
    private JwtService jwtService;
    @MockBean
    private JwtUserDetailsService jwtUserDetailsService;

    @Test
    public void getAllWithUnexpectedError() throws Exception{
        given(iconsService.getAll()).willThrow(new FatalException());
        MvcResult test = mvc.perform(get("/api/icons/all"))
                            .andExpect(status().is5xxServerError())
                            .andReturn();
    }

    @Test
    public void getAll() throws Exception{
        given(iconsService.getAll()).willReturn(new ArrayList<Icon>());
        MvcResult test = mvc.perform(get("/api/icons/all"))
                .andExpect(status().isOk())
                .andExpect(mvcResult -> asJsonString(new ArrayList<Icon>()))
                .andReturn();
    }

    public String asJsonString(final Object obj) {
        try {
            return new ObjectMapper().writeValueAsString(obj);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }
}
