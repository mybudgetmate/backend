package com.mybudget.domain.controllers;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.mybudget.domain.dto.ProjectDto;
import com.mybudget.domain.dto.response.ErrorResponse;
import com.mybudget.domain.exceptions.FatalException;
import com.mybudget.domain.exceptions.ProjectException;
import com.mybudget.domain.services.JwtUserDetailsService;
import com.mybudget.domain.services.ProjectService;
import com.mybudget.domain.utils.JwtService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import java.nio.charset.StandardCharsets;
import java.util.ArrayList;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.doThrow;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

/**
 * @author Soumaya Izmar
 */
@RunWith(SpringRunner.class)
@WebMvcTest(ProjectController.class)
@AutoConfigureMockMvc(secure = false)
public class ProjectControllerTests {

    @Autowired
    private MockMvc mvc;
    @MockBean
    private ProjectService projectService;
    @MockBean
    private JwtService jwtService;
    @MockBean
    private JwtUserDetailsService jwtUserDetailsService;

    @Test
    public void testGetAllProjectWithGoodData() throws Exception {
        given(projectService.fetchAllProject()).willReturn(new ArrayList<>());

        mvc.perform(get("/api/project/"))
                .andExpect(status().isOk())
                .andExpect(mvcResult -> new ArrayList<ProjectDto>());

    }

    @Test
    public void testGetAllProjectWithException() throws Exception {
        String error = "Project error";
        given(projectService.fetchAllProject()).willThrow(new ProjectException(error));
        mvc.perform(get("/api/project/")).andExpect(status().is5xxServerError()).andExpect(mvcResult -> asJsonString(new ErrorResponse(error))).andReturn();
    }

    @Test
    public void testGetSubProjectWithProjectException() throws Exception {
        String error = "Project error";
        given(projectService.fetchSubProject(2)).willThrow(new ProjectException(error));
        mvc.perform(get("/api/project/2")).andExpect(status().is4xxClientError()).andExpect(mvcResult -> asJsonString(new ErrorResponse(error))).andReturn();
    }

    @Test
    public void testGetSubProjectWithException() throws Exception {
        String error = "Project error";
        given(projectService.fetchSubProject(2)).willThrow(new FatalException());
        mvc.perform(get("/api/project/2")).andExpect(status().is5xxServerError()).andReturn();
    }

    @Test
    public void testGetSubProjectWithGoodData() throws Exception {
        given(projectService.fetchSubProject(2)).willReturn(new ArrayList<>());
        mvc.perform(get("/api/project/2"))
                .andExpect(status().isOk())
                .andExpect(mvcResult -> new ArrayList<ProjectDto>())
                .andReturn();
    }


    @Test
    public void testAddProjectWithProjectException() throws Exception {
        MediaType MEDIA_TYPE_JSON_UTF8 = new MediaType("application", "json", StandardCharsets.UTF_8);

        String error = "project error";
        ProjectDto p = new ProjectDto();
        given(projectService.addNewProject(any(ProjectDto.class))).willThrow(new ProjectException(error));
        mvc.perform(post("/api/project/").
                accept(MEDIA_TYPE_JSON_UTF8).
                contentType(MEDIA_TYPE_JSON_UTF8).
                content(asJsonString(p))).andExpect(status().is4xxClientError()).andExpect(mvcResult -> asJsonString(new ErrorResponse(error))).andReturn();
    }


    @Test
    public void testAddProjectWithException() throws Exception {
        MediaType MEDIA_TYPE_JSON_UTF8 = new MediaType("application", "json", StandardCharsets.UTF_8);

        ProjectDto p = new ProjectDto();
        given(projectService.addNewProject(any(ProjectDto.class))).willThrow(new FatalException());
        mvc.perform(post("/api/project/").
                accept(MEDIA_TYPE_JSON_UTF8).
                contentType(MEDIA_TYPE_JSON_UTF8).
                content(asJsonString(p))).andExpect(status().is5xxServerError());
    }

    @Test
    public void testAddProjectWithGoodData() throws Exception {
        MediaType MEDIA_TYPE_JSON_UTF8 = new MediaType("application", "json", StandardCharsets.UTF_8);
        ProjectDto p = new ProjectDto();

        given(projectService.addNewProject(any(ProjectDto.class))).willReturn(p);
        mvc.perform(post("/api/project/").
                accept(MEDIA_TYPE_JSON_UTF8).
                contentType(MEDIA_TYPE_JSON_UTF8).
                content(asJsonString(p))).andExpect(status().isOk()).andExpect(mvcResult -> asJsonString(p)).andReturn();
    }



    @Test
    public void testPatchProjectWithProjectException() throws Exception {
        MediaType MEDIA_TYPE_JSON_UTF8 = new MediaType("application", "json", StandardCharsets.UTF_8);

        String error = "project error";
        ProjectDto p = new ProjectDto();
        given(projectService.updateProject(any(ProjectDto.class))).willThrow(new ProjectException(error));
        mvc.perform(patch("/api/project/").
                accept(MEDIA_TYPE_JSON_UTF8).
                contentType(MEDIA_TYPE_JSON_UTF8).
                content(asJsonString(p))).andExpect(status().is4xxClientError()).andExpect(mvcResult -> asJsonString(new ErrorResponse(error))).andReturn();
    }
    @Test
    public void testPatchProjectWithGoodData() throws Exception {
        MediaType MEDIA_TYPE_JSON_UTF8 = new MediaType("application", "json", StandardCharsets.UTF_8);

        String error = "project error";
        ProjectDto p = new ProjectDto();
        given(projectService.updateProject(any(ProjectDto.class))).willReturn(p);
        mvc.perform(patch("/api/project/").
                accept(MEDIA_TYPE_JSON_UTF8).
                contentType(MEDIA_TYPE_JSON_UTF8).
                content(asJsonString(p))).andExpect(status().isOk()).andExpect(mvcResult -> asJsonString(p)).andReturn();
    }

    @Test
    public void testPatchProjectWithException() throws Exception {
        MediaType MEDIA_TYPE_JSON_UTF8 = new MediaType("application", "json", StandardCharsets.UTF_8);

        String error = "project error";
        ProjectDto p = new ProjectDto();
        given(projectService.updateProject(any(ProjectDto.class))).willThrow(new FatalException());
        mvc.perform(patch("/api/project/").
                accept(MEDIA_TYPE_JSON_UTF8).
                contentType(MEDIA_TYPE_JSON_UTF8).
                content(asJsonString(p))).andExpect(status().is5xxServerError());
    }


    public String asJsonString(final Object obj) {
        try {
            return new ObjectMapper().writeValueAsString(obj);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }


    @Test
    public void testDeleteProjectWithException() throws Exception {

        doThrow(new FatalException()).when(projectService).deleteProject(2);
        mvc.perform(delete("/api/project/2"))
                .andExpect(status().is5xxServerError());
    }
    @Test
    public void testDeleteProjectWithProjectException() throws Exception {
        String error = "project Error";
        doThrow(new ProjectException(error)).when(projectService).deleteProject(2);
        mvc.perform(delete("/api/project/2"))
                .andExpect(status().is4xxClientError()).andExpect(mvcResult -> asJsonString(new ErrorResponse(error))).andReturn();
    }

    @Test
    public void testDeleteProjectWithGoodData() throws Exception {
        mvc.perform(delete("/api/project/2"))
                .andExpect(status().isNoContent());
    }


}
