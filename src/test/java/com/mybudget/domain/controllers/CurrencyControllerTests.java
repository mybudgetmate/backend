package com.mybudget.domain.controllers;

import com.mybudget.domain.dto.response.CurrencyResponse;
import com.mybudget.domain.exceptions.FatalException;
import com.mybudget.domain.services.CurrencyService;
import com.mybudget.domain.services.JwtUserDetailsService;
import com.mybudget.domain.utils.JwtService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;

import java.util.ArrayList;

import static org.mockito.BDDMockito.given;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@WebMvcTest(CurrencyController.class)
@AutoConfigureMockMvc(secure = false)
public class CurrencyControllerTests {
    @Autowired
    private MockMvc mvc;

    @MockBean
    private CurrencyService currencyService;
    @MockBean
    private JwtService jwtService;
    @MockBean
    private JwtUserDetailsService jwtUserDetailsService;



    @Test
    public void testFetchAllCurrency() throws Exception {
        given(currencyService.fetchAllCurrencies()).willReturn(new ArrayList<>());
        MvcResult test = mvc.perform(get("/api/currency/"))
                            .andExpect(status().isOk())
                            .andExpect(mvcResult -> new ArrayList<>())
                            .andReturn();
    }
    @Test
    public void testFetchAllCurrencyWithUnexpectedError() throws Exception {
        given(currencyService.fetchAllCurrencies()).willThrow(new FatalException());
        MvcResult test = mvc.perform(get("/api/currency/"))
                            .andExpect(status().is5xxServerError())
                            .andReturn();
    }

    @Test
    public void fetchPreferedCurrencyUserWithUnexpectedError() throws Exception {
        given(currencyService.fetchPreferedCurrencyUser()).willThrow(new FatalException());
        MvcResult test = mvc.perform(get("/api/currency/byUser"))
                .andExpect(status().is5xxServerError())
                .andReturn();
    }

    @Test
    public void fetchPreferedCurrencyUser() throws Exception {
        given(currencyService.fetchPreferedCurrencyUser()).willReturn(new CurrencyResponse());
        MvcResult test = mvc.perform(get("/api/currency/byUser"))
                .andExpect(status().isOk())
                .andExpect(mvcResult -> new CurrencyResponse())
                .andReturn();
    }


}
